#include<stdio.h>
#include<curl/curl.h>

int main()
{
	CURL *curl;
	CURLcode res;
	struct curl_slist *headers = NULL;

	curl = curl_easy_init();

	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
	curl_easy_setopt(curl, CURLOPT_URL, "http://flip3.engr.oregonstate.edu:3021/iota/send");

	headers = curl_slist_append(headers, "cache-control: no-cache");
	headers = curl_slist_append(headers, "Content-Type: application/json");

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "{\n\t\"value\":\"hello there\"\n}");

	res = curl_easy_perform(curl);
}
