#include <stdio.h>
#include <stdlib.h>

// You are only allowed to make changes to this code as specified by the comments in it.

// The code you submit must have these two values.
#define N_TIMES		600000
#define ARRAY_SIZE	 10000

int main(void)
{
	double	*array = calloc(ARRAY_SIZE, sizeof(double));
	double	sum = 0;
   double value=0;

    // putting some values in the array
    for(int i=0; i<ARRAY_SIZE; i++, value++)
         array[i] = value/1000000.0;
	// You can add variables between this comment ...

	double *mySum = calloc(10, sizeof(double));
	double *beginning = &array[0];
	double *middle = &array[ARRAY_SIZE/2];
	double *end = &array[ARRAY_SIZE];
	double *j = middle;
	// ... and this one.

	// Please change 'your name' to your actual name.
	printf("CS201 - Asgmt 4 - Kerry Vance\n");

	for (int i = 0; i < N_TIMES; i++)
	{
		j = middle;
		mySum[0] += *j;
		mySum[1] += *(j-1);
		mySum[2] += *(j-2);
		mySum[3] += *(j-3);
		mySum[4] += *(j-4);
		mySum[5] += *(j-5);
		mySum[6] += *(j-6);
		mySum[7] += *(j-7);
		mySum[8] += *(j-8);
		mySum[9] += *(j-9);
		j = middle;
		mySum[1] += *(j+1);
		mySum[2] += *(j+2);
		mySum[3] += *(j+3);
		mySum[4] += *(j+4);
		mySum[5] += *(j+5);
		mySum[6] += *(j+6);
		mySum[7] += *(j+7);
		mySum[8] += *(j+8);
		mySum[9] += *(j+9);
		// You can change anything between this comment ...
		for(j = middle - 10; j > beginning; j -= 10)
		{
			mySum[0] += *j + *(j-1) + *(j-2) + *(j-3) + *(j-4);
			mySum[1] += *(j-5) + *(j-6) + *(j-7) + *(j-8) + *(j-9);
		}
		for(j = middle + 10; j < end; j += 10)
		{
			mySum[0] += *j;
			mySum[1] += *(j+1);
			mySum[2] += *(j+2);
			mySum[3] += *(j+3);
			mySum[4] += *(j+4);
			mySum[5] += *(j+5);
			mySum[6] += *(j+6);
			mySum[7] += *(j+7);
			mySum[8] += *(j+8);
			mySum[9] += *(j+9);
		}

		// ... and this one. But your inner loop must do the same
		// number of additions as this one does.

	}
	// You can add some final code between this comment ...
	for(int i = 0; i < 10; i++)
	{
		sum += mySum[i];
	}

	// ... and this one.
    printf("sum is %10.2f\n",sum);
	 //should print 29997000.00
	return 0;
}
