// Numbers from the user are sent to child process
// from parent process one at a time through pipe.
//
// Child process divides numbers sent through pipe.
//
// Child process returns quotient of numbers to parent process.
//
// Parent process prints quotient of numbers.
// NOTE: Child process should not print anything
// EXCEPT the error message when the SIGFPE signal is given.
//
// PREMIUM:  Child catches divide by zero (SIGFPE) and
// prints error message (see instructions). Note that
// parent will need to handle a situation for the
// child might end abnormally.

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>
#include<signal.h>

// need signal handler - put it here
void signalHandler(int signal)
{
	printf("\n\nSignal SIGFPE : %d received\tpid: %d\n\n", signal, getpid());
	exit(-1);
}

int main()
{
	printf("CS201 - Assignment 3 Premium - Kerry Vance\n");

	pid_t pid;
	char buffer[8];
	int myPipe[2], result;

	if(signal(SIGFPE, signalHandler) == SIG_ERR)
	{
		printf("signal error\n");
	}

	if(pipe(myPipe))
	{
		printf("Pipe Failed\n");
	}

	pid = fork();

	if(pid == 0) //Child
	{
		close(myPipe[1]);
		read(myPipe[0], buffer, 8);
		result = atoi(buffer);

		while(buffer[0] != 'q')
		{
			read(myPipe[0], buffer, 8);
			if(buffer[0] != 'q')
			{
				result /= atoi(buffer);
			}
		}
		close(myPipe[0]);
		return result;
	}
	else if(pid > 0) //Parent
	{
		close(myPipe[0]);

		while(buffer[0] != 'q')
		{
			printf("Enter a number(or 'q' to quit): ");
			scanf("%7s", buffer);
			if(!waitpid(pid,&result,WNOHANG)) //guards against SIGPIPE
			{
				write(myPipe[1],buffer,8);
			}
		}
	}
	else if(pid < 0)
	{
		printf("Failed to create child\n");
	}

	close(myPipe[1]);

	waitpid(pid,&result,0);
	result >>= 8;

	if(result == 255)
	{
		printf("\nChild aborted\n");
	}
	else
	{
		printf("quotient = %d\n",result);
	}

//	printf("Enter a number (or 'q' to quit): ");
//	scanf("7%s", input);
	// start code here. remember to set up pipe before
    // doing a fork. Always check for errors with all
    // system calls. set up signal handling inside the child
    // and when the quotient returned is 255 in the parent, this will
    // be considered an abort.
    return 0;
}
