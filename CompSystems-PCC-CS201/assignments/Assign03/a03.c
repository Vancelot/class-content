// Numbers from the user are sent to child process
// from parent process one at a time through pipe.
//
// Child process divides numbers sent through pipe.
//
// Child process returns quotient of numbers to parent process.
//
// Parent process prints quotient of numbers.
// NOTE: Make sure child does NOT print anything to the terminal
//       or you will get much fewer points.

#include <stdio.h>

int main(int argc, char **argv)
{
	

	printf("CS201 - Assignment 3 Regular - I. Forgot\n");

	// start code here. remember to set up pipe before
    // doing a fork. Always check for errors with all
    // system calls.
    
}
