#include <stdio.h>
#include <stdlib.h>

// You are only allowed to make changes to this code as specified by the comments in it.

// The code you submit must have these two values.
#define N_TIMES		600000
#define ARRAY_SIZE	 10000

int main(void)
{
	double	*array = calloc(ARRAY_SIZE, sizeof(double));
	double	sum = 0;
   double value=0;

    // putting some values in the array
    for(int i=0; i<ARRAY_SIZE; i++, value++)
         array[i] = value/1000000.0;
	// You can add variables between this comment ...

	// ... and this one.

	// Please change 'your name' to your actual name.
	printf("CS201 - Asgmt 4 - Kerry Vance\n");

	int size = ARRAY_SIZE;
	int segSize = 25;
	double *myArray = array, *arraySeg = array + segSize , *segStart;
	double *end = myArray + size;
	double mySum[10] = {0};

	for(segStart = array; segStart < array + ARRAY_SIZE; segStart += segSize, myArray += segSize)
	{
		for (unsigned int i = 0; i < N_TIMES; i++)
		{
			mySum[8] = myArray[1];
			mySum[9] = *myArray + 1;
			mySum[0] += myArray[0] + myArray[1] + myArray[2];
			mySum[1] += myArray[3] + myArray[4] + myArray[5];
			mySum[2] += myArray[6] + myArray[7] + myArray[8];
			mySum[3] += myArray[9] + myArray[10] + myArray[11];
			mySum[4] += myArray[12] + myArray[13] + myArray[14];
			mySum[5] += myArray[15] + myArray[16] + myArray[17];
			mySum[6] += myArray[18] + myArray[19] + myArray[20];
			mySum[7] += myArray[21] + myArray[22] + myArray[23] + myArray[24];
		}
	}
	// You can add some final code between this comment ...

	sum += mySum[0] + mySum[1] + mySum[2] + mySum[3] + mySum[4] + mySum[5] +mySum[6] + mySum[7] + mySum[8] + mySum[9];
	// ... and this one.
    printf("sum is %10.2f\n",sum);

	 //should print 29997000.00
	free(array);
	return 0;
}
