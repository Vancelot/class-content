
./a.out:     file format elf32-i386


Disassembly of section .init:

00001000 <_init>:
    1000:	53                   	push   %ebx
    1001:	83 ec 08             	sub    $0x8,%esp
    1004:	e8 c7 00 00 00       	call   10d0 <__x86.get_pc_thunk.bx>
    1009:	81 c3 f7 2f 00 00    	add    $0x2ff7,%ebx
    100f:	8b 83 f4 ff ff ff    	mov    -0xc(%ebx),%eax
    1015:	85 c0                	test   %eax,%eax
    1017:	74 02                	je     101b <_init+0x1b>
    1019:	ff d0                	call   *%eax
    101b:	83 c4 08             	add    $0x8,%esp
    101e:	5b                   	pop    %ebx
    101f:	c3                   	ret    

Disassembly of section .plt:

00001020 <.plt>:
    1020:	ff b3 04 00 00 00    	pushl  0x4(%ebx)
    1026:	ff a3 08 00 00 00    	jmp    *0x8(%ebx)
    102c:	00 00                	add    %al,(%eax)
	...

00001030 <printf@plt>:
    1030:	ff a3 0c 00 00 00    	jmp    *0xc(%ebx)
    1036:	68 00 00 00 00       	push   $0x0
    103b:	e9 e0 ff ff ff       	jmp    1020 <.plt>

00001040 <free@plt>:
    1040:	ff a3 10 00 00 00    	jmp    *0x10(%ebx)
    1046:	68 08 00 00 00       	push   $0x8
    104b:	e9 d0 ff ff ff       	jmp    1020 <.plt>

00001050 <__stack_chk_fail@plt>:
    1050:	ff a3 14 00 00 00    	jmp    *0x14(%ebx)
    1056:	68 10 00 00 00       	push   $0x10
    105b:	e9 c0 ff ff ff       	jmp    1020 <.plt>

00001060 <puts@plt>:
    1060:	ff a3 18 00 00 00    	jmp    *0x18(%ebx)
    1066:	68 18 00 00 00       	push   $0x18
    106b:	e9 b0 ff ff ff       	jmp    1020 <.plt>

00001070 <__libc_start_main@plt>:
    1070:	ff a3 1c 00 00 00    	jmp    *0x1c(%ebx)
    1076:	68 20 00 00 00       	push   $0x20
    107b:	e9 a0 ff ff ff       	jmp    1020 <.plt>

00001080 <calloc@plt>:
    1080:	ff a3 20 00 00 00    	jmp    *0x20(%ebx)
    1086:	68 28 00 00 00       	push   $0x28
    108b:	e9 90 ff ff ff       	jmp    1020 <.plt>

Disassembly of section .text:

00001090 <_start>:
    1090:	31 ed                	xor    %ebp,%ebp
    1092:	5e                   	pop    %esi
    1093:	89 e1                	mov    %esp,%ecx
    1095:	83 e4 f0             	and    $0xfffffff0,%esp
    1098:	50                   	push   %eax
    1099:	54                   	push   %esp
    109a:	52                   	push   %edx
    109b:	e8 22 00 00 00       	call   10c2 <_start+0x32>
    10a0:	81 c3 60 2f 00 00    	add    $0x2f60,%ebx
    10a6:	8d 83 d0 d5 ff ff    	lea    -0x2a30(%ebx),%eax
    10ac:	50                   	push   %eax
    10ad:	8d 83 70 d5 ff ff    	lea    -0x2a90(%ebx),%eax
    10b3:	50                   	push   %eax
    10b4:	51                   	push   %ecx
    10b5:	56                   	push   %esi
    10b6:	ff b3 f8 ff ff ff    	pushl  -0x8(%ebx)
    10bc:	e8 af ff ff ff       	call   1070 <__libc_start_main@plt>
    10c1:	f4                   	hlt    
    10c2:	8b 1c 24             	mov    (%esp),%ebx
    10c5:	c3                   	ret    
    10c6:	66 90                	xchg   %ax,%ax
    10c8:	66 90                	xchg   %ax,%ax
    10ca:	66 90                	xchg   %ax,%ax
    10cc:	66 90                	xchg   %ax,%ax
    10ce:	66 90                	xchg   %ax,%ax

000010d0 <__x86.get_pc_thunk.bx>:
    10d0:	8b 1c 24             	mov    (%esp),%ebx
    10d3:	c3                   	ret    
    10d4:	66 90                	xchg   %ax,%ax
    10d6:	66 90                	xchg   %ax,%ax
    10d8:	66 90                	xchg   %ax,%ax
    10da:	66 90                	xchg   %ax,%ax
    10dc:	66 90                	xchg   %ax,%ax
    10de:	66 90                	xchg   %ax,%ax

000010e0 <deregister_tm_clones>:
    10e0:	e8 e4 00 00 00       	call   11c9 <__x86.get_pc_thunk.dx>
    10e5:	81 c2 1b 2f 00 00    	add    $0x2f1b,%edx
    10eb:	8d 8a 2c 00 00 00    	lea    0x2c(%edx),%ecx
    10f1:	8d 82 2c 00 00 00    	lea    0x2c(%edx),%eax
    10f7:	39 c8                	cmp    %ecx,%eax
    10f9:	74 1d                	je     1118 <deregister_tm_clones+0x38>
    10fb:	8b 82 ec ff ff ff    	mov    -0x14(%edx),%eax
    1101:	85 c0                	test   %eax,%eax
    1103:	74 13                	je     1118 <deregister_tm_clones+0x38>
    1105:	55                   	push   %ebp
    1106:	89 e5                	mov    %esp,%ebp
    1108:	83 ec 14             	sub    $0x14,%esp
    110b:	51                   	push   %ecx
    110c:	ff d0                	call   *%eax
    110e:	83 c4 10             	add    $0x10,%esp
    1111:	c9                   	leave  
    1112:	c3                   	ret    
    1113:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    1117:	90                   	nop
    1118:	c3                   	ret    
    1119:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00001120 <register_tm_clones>:
    1120:	e8 a4 00 00 00       	call   11c9 <__x86.get_pc_thunk.dx>
    1125:	81 c2 db 2e 00 00    	add    $0x2edb,%edx
    112b:	55                   	push   %ebp
    112c:	89 e5                	mov    %esp,%ebp
    112e:	53                   	push   %ebx
    112f:	8d 8a 2c 00 00 00    	lea    0x2c(%edx),%ecx
    1135:	8d 82 2c 00 00 00    	lea    0x2c(%edx),%eax
    113b:	83 ec 04             	sub    $0x4,%esp
    113e:	29 c8                	sub    %ecx,%eax
    1140:	c1 f8 02             	sar    $0x2,%eax
    1143:	89 c3                	mov    %eax,%ebx
    1145:	c1 eb 1f             	shr    $0x1f,%ebx
    1148:	01 d8                	add    %ebx,%eax
    114a:	d1 f8                	sar    %eax
    114c:	74 14                	je     1162 <register_tm_clones+0x42>
    114e:	8b 92 fc ff ff ff    	mov    -0x4(%edx),%edx
    1154:	85 d2                	test   %edx,%edx
    1156:	74 0a                	je     1162 <register_tm_clones+0x42>
    1158:	83 ec 08             	sub    $0x8,%esp
    115b:	50                   	push   %eax
    115c:	51                   	push   %ecx
    115d:	ff d2                	call   *%edx
    115f:	83 c4 10             	add    $0x10,%esp
    1162:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    1165:	c9                   	leave  
    1166:	c3                   	ret    
    1167:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    116e:	66 90                	xchg   %ax,%ax

00001170 <__do_global_dtors_aux>:
    1170:	f3 0f 1e fb          	endbr32 
    1174:	55                   	push   %ebp
    1175:	89 e5                	mov    %esp,%ebp
    1177:	53                   	push   %ebx
    1178:	e8 53 ff ff ff       	call   10d0 <__x86.get_pc_thunk.bx>
    117d:	81 c3 83 2e 00 00    	add    $0x2e83,%ebx
    1183:	83 ec 04             	sub    $0x4,%esp
    1186:	80 bb 2c 00 00 00 00 	cmpb   $0x0,0x2c(%ebx)
    118d:	75 28                	jne    11b7 <__do_global_dtors_aux+0x47>
    118f:	8b 83 f0 ff ff ff    	mov    -0x10(%ebx),%eax
    1195:	85 c0                	test   %eax,%eax
    1197:	74 12                	je     11ab <__do_global_dtors_aux+0x3b>
    1199:	83 ec 0c             	sub    $0xc,%esp
    119c:	ff b3 28 00 00 00    	pushl  0x28(%ebx)
    11a2:	ff 93 f0 ff ff ff    	call   *-0x10(%ebx)
    11a8:	83 c4 10             	add    $0x10,%esp
    11ab:	e8 30 ff ff ff       	call   10e0 <deregister_tm_clones>
    11b0:	c6 83 2c 00 00 00 01 	movb   $0x1,0x2c(%ebx)
    11b7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    11ba:	c9                   	leave  
    11bb:	c3                   	ret    
    11bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000011c0 <frame_dummy>:
    11c0:	f3 0f 1e fb          	endbr32 
    11c4:	e9 57 ff ff ff       	jmp    1120 <register_tm_clones>

000011c9 <__x86.get_pc_thunk.dx>:
    11c9:	8b 14 24             	mov    (%esp),%edx
    11cc:	c3                   	ret    

000011cd <main>:
    11cd:	8d 4c 24 04          	lea    0x4(%esp),%ecx
    11d1:	83 e4 f0             	and    $0xfffffff0,%esp
    11d4:	ff 71 fc             	pushl  -0x4(%ecx)
    11d7:	55                   	push   %ebp
    11d8:	89 e5                	mov    %esp,%ebp
    11da:	57                   	push   %edi
    11db:	53                   	push   %ebx
    11dc:	51                   	push   %ecx
    11dd:	81 ec 9c 00 00 00    	sub    $0x9c,%esp
    11e3:	e8 e8 fe ff ff       	call   10d0 <__x86.get_pc_thunk.bx>
    11e8:	81 c3 18 2e 00 00    	add    $0x2e18,%ebx
    11ee:	65 a1 14 00 00 00    	mov    %gs:0x14,%eax
    11f4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    11f7:	31 c0                	xor    %eax,%eax
    11f9:	83 ec 08             	sub    $0x8,%esp
    11fc:	6a 08                	push   $0x8
    11fe:	68 10 27 00 00       	push   $0x2710
    1203:	e8 78 fe ff ff       	call   1080 <calloc@plt>
    1208:	83 c4 10             	add    $0x10,%esp
    120b:	89 85 6c ff ff ff    	mov    %eax,-0x94(%ebp)
    1211:	d9 ee                	fldz   
    1213:	dd 5d 88             	fstpl  -0x78(%ebp)
    1216:	d9 ee                	fldz   
    1218:	dd 5d 80             	fstpl  -0x80(%ebp)
    121b:	c7 85 5c ff ff ff 00 	movl   $0x0,-0xa4(%ebp)
    1222:	00 00 00 
    1225:	eb 33                	jmp    125a <main+0x8d>
    1227:	8b 85 5c ff ff ff    	mov    -0xa4(%ebp),%eax
    122d:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
    1234:	8b 85 6c ff ff ff    	mov    -0x94(%ebp),%eax
    123a:	01 d0                	add    %edx,%eax
    123c:	dd 45 80             	fldl   -0x80(%ebp)
    123f:	dd 83 38 e0 ff ff    	fldl   -0x1fc8(%ebx)
    1245:	de f9                	fdivrp %st,%st(1)
    1247:	dd 18                	fstpl  (%eax)
    1249:	83 85 5c ff ff ff 01 	addl   $0x1,-0xa4(%ebp)
    1250:	dd 45 80             	fldl   -0x80(%ebp)
    1253:	d9 e8                	fld1   
    1255:	de c1                	faddp  %st,%st(1)
    1257:	dd 5d 80             	fstpl  -0x80(%ebp)
    125a:	81 bd 5c ff ff ff 0f 	cmpl   $0x270f,-0xa4(%ebp)
    1261:	27 00 00 
    1264:	7e c1                	jle    1227 <main+0x5a>
    1266:	83 ec 0c             	sub    $0xc,%esp
    1269:	8d 83 08 e0 ff ff    	lea    -0x1ff8(%ebx),%eax
    126f:	50                   	push   %eax
    1270:	e8 eb fd ff ff       	call   1060 <puts@plt>
    1275:	83 c4 10             	add    $0x10,%esp
    1278:	c7 85 70 ff ff ff 10 	movl   $0x2710,-0x90(%ebp)
    127f:	27 00 00 
    1282:	c7 85 74 ff ff ff 19 	movl   $0x19,-0x8c(%ebp)
    1289:	00 00 00 
    128c:	8b 85 6c ff ff ff    	mov    -0x94(%ebp),%eax
    1292:	89 85 60 ff ff ff    	mov    %eax,-0xa0(%ebp)
    1298:	8b 85 74 ff ff ff    	mov    -0x8c(%ebp),%eax
    129e:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
    12a5:	8b 85 6c ff ff ff    	mov    -0x94(%ebp),%eax
    12ab:	01 d0                	add    %edx,%eax
    12ad:	89 85 78 ff ff ff    	mov    %eax,-0x88(%ebp)
    12b3:	8b 85 70 ff ff ff    	mov    -0x90(%ebp),%eax
    12b9:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
    12c0:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    12c6:	01 d0                	add    %edx,%eax
    12c8:	89 85 7c ff ff ff    	mov    %eax,-0x84(%ebp)
    12ce:	8d 55 90             	lea    -0x70(%ebp),%edx
    12d1:	b8 00 00 00 00       	mov    $0x0,%eax
    12d6:	b9 14 00 00 00       	mov    $0x14,%ecx
    12db:	89 d7                	mov    %edx,%edi
    12dd:	f3 ab                	rep stos %eax,%es:(%edi)
    12df:	8b 85 6c ff ff ff    	mov    -0x94(%ebp),%eax
    12e5:	89 85 64 ff ff ff    	mov    %eax,-0x9c(%ebp)
    12eb:	e9 e1 01 00 00       	jmp    14d1 <main+0x304>
    12f0:	c7 85 68 ff ff ff 00 	movl   $0x0,-0x98(%ebp)
    12f7:	00 00 00 
    12fa:	e9 a4 01 00 00       	jmp    14a3 <main+0x2d6>
    12ff:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1305:	dd 40 08             	fldl   0x8(%eax)
    1308:	dd 5d d0             	fstpl  -0x30(%ebp)
    130b:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1311:	dd 00                	fldl   (%eax)
    1313:	d9 e8                	fld1   
    1315:	de c1                	faddp  %st,%st(1)
    1317:	dd 5d d8             	fstpl  -0x28(%ebp)
    131a:	dd 45 90             	fldl   -0x70(%ebp)
    131d:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1323:	dd 00                	fldl   (%eax)
    1325:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    132b:	83 c0 08             	add    $0x8,%eax
    132e:	dd 00                	fldl   (%eax)
    1330:	de c1                	faddp  %st,%st(1)
    1332:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1338:	83 c0 10             	add    $0x10,%eax
    133b:	dd 00                	fldl   (%eax)
    133d:	de c1                	faddp  %st,%st(1)
    133f:	de c1                	faddp  %st,%st(1)
    1341:	dd 5d 90             	fstpl  -0x70(%ebp)
    1344:	dd 45 98             	fldl   -0x68(%ebp)
    1347:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    134d:	83 c0 18             	add    $0x18,%eax
    1350:	dd 00                	fldl   (%eax)
    1352:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1358:	83 c0 20             	add    $0x20,%eax
    135b:	dd 00                	fldl   (%eax)
    135d:	de c1                	faddp  %st,%st(1)
    135f:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1365:	83 c0 28             	add    $0x28,%eax
    1368:	dd 00                	fldl   (%eax)
    136a:	de c1                	faddp  %st,%st(1)
    136c:	de c1                	faddp  %st,%st(1)
    136e:	dd 5d 98             	fstpl  -0x68(%ebp)
    1371:	dd 45 a0             	fldl   -0x60(%ebp)
    1374:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    137a:	83 c0 30             	add    $0x30,%eax
    137d:	dd 00                	fldl   (%eax)
    137f:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1385:	83 c0 38             	add    $0x38,%eax
    1388:	dd 00                	fldl   (%eax)
    138a:	de c1                	faddp  %st,%st(1)
    138c:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1392:	83 c0 40             	add    $0x40,%eax
    1395:	dd 00                	fldl   (%eax)
    1397:	de c1                	faddp  %st,%st(1)
    1399:	de c1                	faddp  %st,%st(1)
    139b:	dd 5d a0             	fstpl  -0x60(%ebp)
    139e:	dd 45 a8             	fldl   -0x58(%ebp)
    13a1:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    13a7:	83 c0 48             	add    $0x48,%eax
    13aa:	dd 00                	fldl   (%eax)
    13ac:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    13b2:	83 c0 50             	add    $0x50,%eax
    13b5:	dd 00                	fldl   (%eax)
    13b7:	de c1                	faddp  %st,%st(1)
    13b9:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    13bf:	83 c0 58             	add    $0x58,%eax
    13c2:	dd 00                	fldl   (%eax)
    13c4:	de c1                	faddp  %st,%st(1)
    13c6:	de c1                	faddp  %st,%st(1)
    13c8:	dd 5d a8             	fstpl  -0x58(%ebp)
    13cb:	dd 45 b0             	fldl   -0x50(%ebp)
    13ce:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    13d4:	83 c0 60             	add    $0x60,%eax
    13d7:	dd 00                	fldl   (%eax)
    13d9:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    13df:	83 c0 68             	add    $0x68,%eax
    13e2:	dd 00                	fldl   (%eax)
    13e4:	de c1                	faddp  %st,%st(1)
    13e6:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    13ec:	83 c0 70             	add    $0x70,%eax
    13ef:	dd 00                	fldl   (%eax)
    13f1:	de c1                	faddp  %st,%st(1)
    13f3:	de c1                	faddp  %st,%st(1)
    13f5:	dd 5d b0             	fstpl  -0x50(%ebp)
    13f8:	dd 45 b8             	fldl   -0x48(%ebp)
    13fb:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1401:	83 c0 78             	add    $0x78,%eax
    1404:	dd 00                	fldl   (%eax)
    1406:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    140c:	83 e8 80             	sub    $0xffffff80,%eax
    140f:	dd 00                	fldl   (%eax)
    1411:	de c1                	faddp  %st,%st(1)
    1413:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1419:	05 88 00 00 00       	add    $0x88,%eax
    141e:	dd 00                	fldl   (%eax)
    1420:	de c1                	faddp  %st,%st(1)
    1422:	de c1                	faddp  %st,%st(1)
    1424:	dd 5d b8             	fstpl  -0x48(%ebp)
    1427:	dd 45 c0             	fldl   -0x40(%ebp)
    142a:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1430:	05 90 00 00 00       	add    $0x90,%eax
    1435:	dd 00                	fldl   (%eax)
    1437:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    143d:	05 98 00 00 00       	add    $0x98,%eax
    1442:	dd 00                	fldl   (%eax)
    1444:	de c1                	faddp  %st,%st(1)
    1446:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    144c:	05 a0 00 00 00       	add    $0xa0,%eax
    1451:	dd 00                	fldl   (%eax)
    1453:	de c1                	faddp  %st,%st(1)
    1455:	de c1                	faddp  %st,%st(1)
    1457:	dd 5d c0             	fstpl  -0x40(%ebp)
    145a:	dd 45 c8             	fldl   -0x38(%ebp)
    145d:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1463:	05 a8 00 00 00       	add    $0xa8,%eax
    1468:	dd 00                	fldl   (%eax)
    146a:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    1470:	05 b0 00 00 00       	add    $0xb0,%eax
    1475:	dd 00                	fldl   (%eax)
    1477:	de c1                	faddp  %st,%st(1)
    1479:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    147f:	05 b8 00 00 00       	add    $0xb8,%eax
    1484:	dd 00                	fldl   (%eax)
    1486:	de c1                	faddp  %st,%st(1)
    1488:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
    148e:	05 c0 00 00 00       	add    $0xc0,%eax
    1493:	dd 00                	fldl   (%eax)
    1495:	de c1                	faddp  %st,%st(1)
    1497:	de c1                	faddp  %st,%st(1)
    1499:	dd 5d c8             	fstpl  -0x38(%ebp)
    149c:	83 85 68 ff ff ff 01 	addl   $0x1,-0x98(%ebp)
    14a3:	81 bd 68 ff ff ff bf 	cmpl   $0x927bf,-0x98(%ebp)
    14aa:	27 09 00 
    14ad:	0f 86 4c fe ff ff    	jbe    12ff <main+0x132>
    14b3:	8b 85 74 ff ff ff    	mov    -0x8c(%ebp),%eax
    14b9:	c1 e0 03             	shl    $0x3,%eax
    14bc:	01 85 64 ff ff ff    	add    %eax,-0x9c(%ebp)
    14c2:	8b 85 74 ff ff ff    	mov    -0x8c(%ebp),%eax
    14c8:	c1 e0 03             	shl    $0x3,%eax
    14cb:	01 85 60 ff ff ff    	add    %eax,-0xa0(%ebp)
    14d1:	8b 85 6c ff ff ff    	mov    -0x94(%ebp),%eax
    14d7:	05 80 38 01 00       	add    $0x13880,%eax
    14dc:	39 85 64 ff ff ff    	cmp    %eax,-0x9c(%ebp)
    14e2:	0f 82 08 fe ff ff    	jb     12f0 <main+0x123>
    14e8:	dd 45 90             	fldl   -0x70(%ebp)
    14eb:	dd 45 98             	fldl   -0x68(%ebp)
    14ee:	de c1                	faddp  %st,%st(1)
    14f0:	dd 45 a0             	fldl   -0x60(%ebp)
    14f3:	de c1                	faddp  %st,%st(1)
    14f5:	dd 45 a8             	fldl   -0x58(%ebp)
    14f8:	de c1                	faddp  %st,%st(1)
    14fa:	dd 45 b0             	fldl   -0x50(%ebp)
    14fd:	de c1                	faddp  %st,%st(1)
    14ff:	dd 45 b8             	fldl   -0x48(%ebp)
    1502:	de c1                	faddp  %st,%st(1)
    1504:	dd 45 c0             	fldl   -0x40(%ebp)
    1507:	de c1                	faddp  %st,%st(1)
    1509:	dd 45 c8             	fldl   -0x38(%ebp)
    150c:	de c1                	faddp  %st,%st(1)
    150e:	dd 45 d0             	fldl   -0x30(%ebp)
    1511:	de c1                	faddp  %st,%st(1)
    1513:	dd 45 d8             	fldl   -0x28(%ebp)
    1516:	de c1                	faddp  %st,%st(1)
    1518:	dd 45 88             	fldl   -0x78(%ebp)
    151b:	de c1                	faddp  %st,%st(1)
    151d:	dd 5d 88             	fstpl  -0x78(%ebp)
    1520:	83 ec 04             	sub    $0x4,%esp
    1523:	ff 75 8c             	pushl  -0x74(%ebp)
    1526:	ff 75 88             	pushl  -0x78(%ebp)
    1529:	8d 83 26 e0 ff ff    	lea    -0x1fda(%ebx),%eax
    152f:	50                   	push   %eax
    1530:	e8 fb fa ff ff       	call   1030 <printf@plt>
    1535:	83 c4 10             	add    $0x10,%esp
    1538:	83 ec 0c             	sub    $0xc,%esp
    153b:	ff b5 6c ff ff ff    	pushl  -0x94(%ebp)
    1541:	e8 fa fa ff ff       	call   1040 <free@plt>
    1546:	83 c4 10             	add    $0x10,%esp
    1549:	b8 00 00 00 00       	mov    $0x0,%eax
    154e:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
    1551:	65 33 0d 14 00 00 00 	xor    %gs:0x14,%ecx
    1558:	74 05                	je     155f <main+0x392>
    155a:	e8 81 00 00 00       	call   15e0 <__stack_chk_fail_local>
    155f:	8d 65 f4             	lea    -0xc(%ebp),%esp
    1562:	59                   	pop    %ecx
    1563:	5b                   	pop    %ebx
    1564:	5f                   	pop    %edi
    1565:	5d                   	pop    %ebp
    1566:	8d 61 fc             	lea    -0x4(%ecx),%esp
    1569:	c3                   	ret    
    156a:	66 90                	xchg   %ax,%ax
    156c:	66 90                	xchg   %ax,%ax
    156e:	66 90                	xchg   %ax,%ax

00001570 <__libc_csu_init>:
    1570:	55                   	push   %ebp
    1571:	57                   	push   %edi
    1572:	56                   	push   %esi
    1573:	53                   	push   %ebx
    1574:	e8 57 fb ff ff       	call   10d0 <__x86.get_pc_thunk.bx>
    1579:	81 c3 87 2a 00 00    	add    $0x2a87,%ebx
    157f:	83 ec 0c             	sub    $0xc,%esp
    1582:	8b 6c 24 28          	mov    0x28(%esp),%ebp
    1586:	e8 75 fa ff ff       	call   1000 <_init>
    158b:	8d b3 f8 fe ff ff    	lea    -0x108(%ebx),%esi
    1591:	8d 83 f4 fe ff ff    	lea    -0x10c(%ebx),%eax
    1597:	29 c6                	sub    %eax,%esi
    1599:	c1 fe 02             	sar    $0x2,%esi
    159c:	74 1f                	je     15bd <__libc_csu_init+0x4d>
    159e:	31 ff                	xor    %edi,%edi
    15a0:	83 ec 04             	sub    $0x4,%esp
    15a3:	55                   	push   %ebp
    15a4:	ff 74 24 2c          	pushl  0x2c(%esp)
    15a8:	ff 74 24 2c          	pushl  0x2c(%esp)
    15ac:	ff 94 bb f4 fe ff ff 	call   *-0x10c(%ebx,%edi,4)
    15b3:	83 c7 01             	add    $0x1,%edi
    15b6:	83 c4 10             	add    $0x10,%esp
    15b9:	39 fe                	cmp    %edi,%esi
    15bb:	75 e3                	jne    15a0 <__libc_csu_init+0x30>
    15bd:	83 c4 0c             	add    $0xc,%esp
    15c0:	5b                   	pop    %ebx
    15c1:	5e                   	pop    %esi
    15c2:	5f                   	pop    %edi
    15c3:	5d                   	pop    %ebp
    15c4:	c3                   	ret    
    15c5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    15cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000015d0 <__libc_csu_fini>:
    15d0:	c3                   	ret    
    15d1:	66 90                	xchg   %ax,%ax
    15d3:	66 90                	xchg   %ax,%ax
    15d5:	66 90                	xchg   %ax,%ax
    15d7:	66 90                	xchg   %ax,%ax
    15d9:	66 90                	xchg   %ax,%ax
    15db:	66 90                	xchg   %ax,%ax
    15dd:	66 90                	xchg   %ax,%ax
    15df:	90                   	nop

000015e0 <__stack_chk_fail_local>:
    15e0:	53                   	push   %ebx
    15e1:	e8 ea fa ff ff       	call   10d0 <__x86.get_pc_thunk.bx>
    15e6:	81 c3 1a 2a 00 00    	add    $0x2a1a,%ebx
    15ec:	83 ec 08             	sub    $0x8,%esp
    15ef:	e8 5c fa ff ff       	call   1050 <__stack_chk_fail@plt>

Disassembly of section .fini:

000015f4 <_fini>:
    15f4:	53                   	push   %ebx
    15f5:	83 ec 08             	sub    $0x8,%esp
    15f8:	e8 d3 fa ff ff       	call   10d0 <__x86.get_pc_thunk.bx>
    15fd:	81 c3 03 2a 00 00    	add    $0x2a03,%ebx
    1603:	83 c4 08             	add    $0x8,%esp
    1606:	5b                   	pop    %ebx
    1607:	c3                   	ret    
