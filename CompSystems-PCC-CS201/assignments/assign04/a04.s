	.file	"a04.c"
	.text
	.section	.rodata
.LC3:
	.string	"CS201 - Asgmt 4 - Kerry Vance"
.LC4:
	.string	"Hello"
.LC5:
	.string	"sum is %10.2f\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB5:
	.cfi_startproc
	leal	4(%esp), %ecx
	.cfi_def_cfa 1, 0
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	.cfi_escape 0x10,0x5,0x2,0x75,0
	movl	%esp, %ebp
	pushl	%edi
	pushl	%ebx
	pushl	%ecx
	.cfi_escape 0xf,0x3,0x75,0x74,0x6
	.cfi_escape 0x10,0x7,0x2,0x75,0x7c
	.cfi_escape 0x10,0x3,0x2,0x75,0x78
	subl	$156, %esp
	call	__x86.get_pc_thunk.bx
	addl	$_GLOBAL_OFFSET_TABLE_, %ebx
	movl	%gs:20, %eax
	movl	%eax, -28(%ebp)
	xorl	%eax, %eax
	subl	$8, %esp
	pushl	$8
	pushl	$10000
	call	calloc@PLT
	addl	$16, %esp
	movl	%eax, -148(%ebp)
	fldz
	fstpl	-120(%ebp)
	fldz
	fstpl	-128(%ebp)
	movl	$0, -164(%ebp)
	jmp	.L2
.L3:
	movl	-164(%ebp), %eax
	leal	0(,%eax,8), %edx
	movl	-148(%ebp), %eax
	addl	%edx, %eax
	fldl	-128(%ebp)
	fldl	.LC1@GOTOFF(%ebx)
	fdivrp	%st, %st(1)
	fstpl	(%eax)
	addl	$1, -164(%ebp)
	fldl	-128(%ebp)
	fld1
	faddp	%st, %st(1)
	fstpl	-128(%ebp)
.L2:
	cmpl	$9999, -164(%ebp)
	jle	.L3
	subl	$12, %esp
	leal	.LC3@GOTOFF(%ebx), %eax
	pushl	%eax
	call	puts@PLT
	addl	$16, %esp
	movl	$10000, -144(%ebp)
	movl	$25, -140(%ebp)
	movl	-148(%ebp), %eax
	movl	%eax, -160(%ebp)
	movl	-140(%ebp), %eax
	leal	0(,%eax,8), %edx
	movl	-148(%ebp), %eax
	addl	%edx, %eax
	movl	%eax, -136(%ebp)
	movl	-144(%ebp), %eax
	leal	0(,%eax,8), %edx
	movl	-160(%ebp), %eax
	addl	%edx, %eax
	movl	%eax, -132(%ebp)
	leal	-112(%ebp), %edx
	movl	$0, %eax
	movl	$20, %ecx
	movl	%edx, %edi
	rep stosl
	movl	-148(%ebp), %eax
	movl	%eax, -156(%ebp)
	jmp	.L4
.L7:
	movl	$0, -152(%ebp)
	jmp	.L5
.L6:
	subl	$12, %esp
	leal	.LC4@GOTOFF(%ebx), %eax
	pushl	%eax
	call	printf@PLT
	addl	$16, %esp
	movl	-160(%ebp), %eax
	fldl	8(%eax)
	fstpl	-48(%ebp)
	subl	$12, %esp
	leal	.LC4@GOTOFF(%ebx), %eax
	pushl	%eax
	call	printf@PLT
	addl	$16, %esp
	movl	-160(%ebp), %eax
	fldl	(%eax)
	fld1
	faddp	%st, %st(1)
	fstpl	-40(%ebp)
	subl	$12, %esp
	leal	.LC4@GOTOFF(%ebx), %eax
	pushl	%eax
	call	printf@PLT
	addl	$16, %esp
	fldl	-112(%ebp)
	movl	-160(%ebp), %eax
	fldl	(%eax)
	movl	-160(%ebp), %eax
	addl	$8, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	movl	-160(%ebp), %eax
	addl	$16, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpl	-112(%ebp)
	fldl	-104(%ebp)
	movl	-160(%ebp), %eax
	addl	$24, %eax
	fldl	(%eax)
	movl	-160(%ebp), %eax
	addl	$32, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	movl	-160(%ebp), %eax
	addl	$40, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpl	-104(%ebp)
	fldl	-96(%ebp)
	movl	-160(%ebp), %eax
	addl	$48, %eax
	fldl	(%eax)
	movl	-160(%ebp), %eax
	addl	$56, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	movl	-160(%ebp), %eax
	addl	$64, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpl	-96(%ebp)
	fldl	-88(%ebp)
	movl	-160(%ebp), %eax
	addl	$72, %eax
	fldl	(%eax)
	movl	-160(%ebp), %eax
	addl	$80, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	movl	-160(%ebp), %eax
	addl	$88, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpl	-88(%ebp)
	fldl	-80(%ebp)
	movl	-160(%ebp), %eax
	addl	$96, %eax
	fldl	(%eax)
	movl	-160(%ebp), %eax
	addl	$104, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	movl	-160(%ebp), %eax
	addl	$112, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpl	-80(%ebp)
	fldl	-72(%ebp)
	movl	-160(%ebp), %eax
	addl	$120, %eax
	fldl	(%eax)
	movl	-160(%ebp), %eax
	subl	$-128, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	movl	-160(%ebp), %eax
	addl	$136, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpl	-72(%ebp)
	fldl	-64(%ebp)
	movl	-160(%ebp), %eax
	addl	$144, %eax
	fldl	(%eax)
	movl	-160(%ebp), %eax
	addl	$152, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	movl	-160(%ebp), %eax
	addl	$160, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpl	-64(%ebp)
	fldl	-56(%ebp)
	movl	-160(%ebp), %eax
	addl	$168, %eax
	fldl	(%eax)
	movl	-160(%ebp), %eax
	addl	$176, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	movl	-160(%ebp), %eax
	addl	$184, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	movl	-160(%ebp), %eax
	addl	$192, %eax
	fldl	(%eax)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpl	-56(%ebp)
	addl	$1, -152(%ebp)
.L5:
	cmpl	$599999, -152(%ebp)
	jbe	.L6
	movl	-140(%ebp), %eax
	sall	$3, %eax
	addl	%eax, -156(%ebp)
	movl	-140(%ebp), %eax
	sall	$3, %eax
	addl	%eax, -160(%ebp)
.L4:
	movl	-148(%ebp), %eax
	addl	$80000, %eax
	cmpl	%eax, -156(%ebp)
	jb	.L7
	fldl	-112(%ebp)
	fldl	-104(%ebp)
	faddp	%st, %st(1)
	fldl	-96(%ebp)
	faddp	%st, %st(1)
	fldl	-88(%ebp)
	faddp	%st, %st(1)
	fldl	-80(%ebp)
	faddp	%st, %st(1)
	fldl	-72(%ebp)
	faddp	%st, %st(1)
	fldl	-64(%ebp)
	faddp	%st, %st(1)
	fldl	-56(%ebp)
	faddp	%st, %st(1)
	fldl	-48(%ebp)
	faddp	%st, %st(1)
	fldl	-40(%ebp)
	faddp	%st, %st(1)
	fldl	-120(%ebp)
	faddp	%st, %st(1)
	fstpl	-120(%ebp)
	subl	$4, %esp
	pushl	-116(%ebp)
	pushl	-120(%ebp)
	leal	.LC5@GOTOFF(%ebx), %eax
	pushl	%eax
	call	printf@PLT
	addl	$16, %esp
	subl	$12, %esp
	pushl	-148(%ebp)
	call	free@PLT
	addl	$16, %esp
	movl	$0, %eax
	movl	-28(%ebp), %ecx
	xorl	%gs:20, %ecx
	je	.L9
	call	__stack_chk_fail_local
.L9:
	leal	-12(%ebp), %esp
	popl	%ecx
	.cfi_restore 1
	.cfi_def_cfa 1, 0
	popl	%ebx
	.cfi_restore 3
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	leal	-4(%ecx), %esp
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE5:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC1:
	.long	0
	.long	1093567616
	.section	.text.__x86.get_pc_thunk.bx,"axG",@progbits,__x86.get_pc_thunk.bx,comdat
	.globl	__x86.get_pc_thunk.bx
	.hidden	__x86.get_pc_thunk.bx
	.type	__x86.get_pc_thunk.bx, @function
__x86.get_pc_thunk.bx:
.LFB6:
	.cfi_startproc
	movl	(%esp), %ebx
	ret
	.cfi_endproc
.LFE6:
	.hidden	__stack_chk_fail_local
	.ident	"GCC: (GNU) 8.1.1 20180531"
	.section	.note.GNU-stack,"",@progbits
