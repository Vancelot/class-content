#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// do not change anything above this comment

unsigned int authenticate(unsigned int x)
{
    unsigned int okay = 0;
	/* Here is where you will put the assembly code to authenticate
     * the user:
     * 1. calculate the value 4*x + 7 where x is the input value.
     * 2. XOR this with the number 0xDEAD (57005 in decimal).
     * 3. if this value is larger than or equal to 0xDEAD,
     *    then the user is authenticated. Set the return value to 1.
     * 4. if this value is less than to 0xDEAD, then the user is not
     *    authenticated. Set the return value to 0.
     */
   
	return okay;
}

// this is for debugging purposes so you can compare values
unsigned int authenticateC(unsigned int x)
{
	/* this C implementation of the value computation is provided
	 * so you can compare its correct results with the results of
     * your assembly code.
     * you can break up the calculation process and put printf
     * statements various stages of calculation against your asm code.
    */
    
    unsigned int c = 0xDEAD; /*57005*/
    unsigned int y, res;
    y = 4*x + 7;
    res = c^y;
    printf("%u\n",res);
    if(res <= c) 
       return 1;
    else
       return 0;
}

// you can change things for debugging
// purposes (like running the C
// versions of functions). but PLEASE MAKE SURE TO
// TURN IN THE ORIGINAL CODE BELOW
//  THIS LINE.
int main (int argc, char **argv)
{
    int x, xC;

	printf("CS201 - Assignment 02 - I. Forgot\n");
	if (argc != 2)
    {
		printf("need 1 argument: an integer number\n");
		return -1;
    }
    
	x = (unsigned int) atoi(argv[1]);
    authenticateC(x);
    
    if(!authenticate(x))
    {
	    printf("WARNING! You are not authorized to run this program!\n");
        printf("This attempt will be logged!\n");
    }
    else
    {
        printf("You are authorized to see the secret number.\n");
    }

	return 0;
}
