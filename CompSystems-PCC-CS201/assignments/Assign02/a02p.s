	.file	"a02p.c"
	.text
	.globl	authenticate
	.type	authenticate, @function
authenticate:
.LFB24:
	.cfi_startproc
	movl	$0, %eax
	ret
	.cfi_endproc
.LFE24:
	.size	authenticate, .-authenticate
	.globl	authenticateC
	.type	authenticateC, @function
authenticateC:
.LFB25:
	.cfi_startproc
	leal	7(,%rdi,4), %eax
	xorl	$57005, %eax
	cmpl	$57005, %eax
	jbe	.L5
	movl	$0, %eax
	ret
.L5:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE25:
	.size	authenticateC, .-authenticateC
	.globl	calcSecretNumber
	.type	calcSecretNumber, @function
calcSecretNumber:
.LFB26:
	.cfi_startproc
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE26:
	.size	calcSecretNumber, .-calcSecretNumber
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"%10.4f\n"
	.text
	.globl	calcSecretNumberC
	.type	calcSecretNumberC, @function
calcSecretNumberC:
.LFB27:
	.cfi_startproc
	subq	$24, %rsp
	.cfi_def_cfa_offset 32
	mulsd	%xmm0, %xmm0
	addsd	.LC1(%rip), %xmm0
	call	sqrt
	mulsd	.LC2(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)
	movl	$.LC3, %edi
	movl	$1, %eax
	call	printf
	movsd	8(%rsp), %xmm0
	addq	$24, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE27:
	.size	calcSecretNumberC, .-calcSecretNumberC
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"CS201 - Assignment 02 - I. Forgot"
	.align 8
.LC5:
	.string	"need 1 argument: an integer number"
	.align 8
.LC6:
	.string	"WARNING! You are not authorized to run this program!"
	.section	.rodata.str1.1
.LC7:
	.string	"This attempt will be logged!"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"You are authorized to see the secret number."
	.align 8
.LC10:
	.string	"Sorry! the secret number couldn't be calculated. Please try again."
	.align 8
.LC11:
	.string	"Here is the secret number: %10.4f\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB28:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	movl	%edi, %ebx
	movq	%rsi, %rbp
	movl	$.LC4, %edi
	call	puts
	cmpl	$2, %ebx
	jne	.L17
	movq	8(%rbp), %rdi
	movl	$10, %edx
	movl	$0, %esi
	call	strtol
	movq	%rax, %rbx
	movl	%eax, %edi
	call	authenticate
	testl	%eax, %eax
	je	.L18
	movl	$.LC8, %edi
	call	puts
	pxor	%xmm0, %xmm0
	cvtsi2sd	%ebx, %xmm0
	call	calcSecretNumber
	ucomisd	.LC9(%rip), %xmm0
	jp	.L13
	je	.L19
.L13:
	movl	$.LC11, %edi
	movl	$1, %eax
	call	printf
	movl	$0, %eax
	jmp	.L9
.L17:
	movl	$.LC5, %edi
	call	puts
	movl	$-1, %eax
	jmp	.L9
.L18:
	movl	$.LC6, %edi
	call	puts
	movl	$.LC7, %edi
	call	puts
	movl	$0, %eax
.L9:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L19:
	.cfi_restore_state
	movl	$.LC10, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	jmp	.L9
	.cfi_endproc
.LFE28:
	.size	main, .-main
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	2576980378
	.long	1074502041
	.align 8
.LC2:
	.long	0
	.long	1073872896
	.align 8
.LC9:
	.long	0
	.long	-1074790400
	.ident	"GCC: (GNU) 7.2.0"
	.section	.note.GNU-stack,"",@progbits
