#include <stdio.h>
#include <stdlib.h>
#include <math.h>


// do not change anything above this comment

unsigned int authenticate(unsigned int x)
{
	unsigned int okay = 0;
	//Directions for this code say to authenticate if the value is larger than 0xDEAD, but in the authenticateC code, the user is authenticated if the result is less than 0xDEAD. I modified my assembly code to match the output of authenticateC.
	asm(
		"lea 7(,%1,4),	%%eax;"
		"xor $0xDEAD, %%eax;"
		"cmp $0xDEAD, %%eax;"
		"jle TRUE\n"
		"mov $0, %0\n"
		"jmp .done;"
		"TRUE:"
		"mov $1, %1;"
		".done:"
		: "=r" (okay)
		: "r" (x)		
		: "%eax"
		);
	/* Here is where you will put the assembly code to authenticate
     * the user:
     * 1. calculate the value 4*x + 7 where x is the input value.
     * 2. XOR this with the number 0xDEAD (57005 in decimal).
     * 3. if this value is larger than or equal to 0xDEAD,
     *    then the user is authenticated. Set the return value to 1.
     * 4. if this value is less than to 0xDEAD, then the user is not
     *    authenticated. Set the return value to 0.
     */
	return okay;
}

// this is for debugging purposes so you can compare values
unsigned int authenticateC(unsigned int x)
{
	    /* this C implementation of the value computation is provided
	 * so you can compare its correct results with the results of
     * your assembly code.
     * you can break up the calculation process and put printf
     * statements various stages of calculation against your asm code.
    */
    
    unsigned int c = 0xDEAD; /*57005*/
    unsigned int y, res;
    y = 4*x + 7;
    res = c^y;
    //printf("%u\n",res);
    if(res <= c) 
       return 1;
    else
       return 0;
}

double calcSecretNumber(double x)
{
    double result = 0.0;
	 double num0 = 3.45, num1 = 2.25;
    
   /*  need to calculate and return
    * the secret number using this
    * formula:
    * fnum =  2.25*sqrt(3.45 + fx^2)
    */
	 asm(
		"fldl %1;"
		"fldl %1;"
		"fmulp;"
		"fldl %2;"
		"faddp;"
		"fsqrt;"
		"fldl %3;"
		"fmulp;"
		"fstl %0;"
		:"=m"(result)
		:"m"(x), "m"(num0), "m"(num1)
		);

    return result;
}


// this is for debugging purposes so you can compare values
double calcSecretNumberC(double fx)
{
    /* this C implementation of the value computation is provided
	 * so you can compare its correct results with the results of
     * your assembly code.
     * you can break up the calculation process and put printf
     * statements various stages of calculation against your asm code.
    */
    double fnum =  2.25*sqrt(3.45 + fx*fx);
    printf("%10.4f\n", fnum);
    return fnum;
    
}

// you can change things for debugging
// purposes (like running the C
// versions of functions). but PLEASE MAKE SURE TO
// TURN IN THE ORIGINAL CODE BELOW
//  THIS LINE.
int main (int argc, char **argv)
{
   int x, xC;
	printf("CS201 - Assignment 02 - Kerry Vance\n");
	if (argc != 2)
    {
		printf("need 1 argument: an integer number\n");
		return -1;
    }
    
	x = (unsigned int) atoi(argv[1]);
    
    if(!authenticate(x)) 
    {
	    printf("WARNING! You are not authorized to run this program!\n");
        printf("This attempt will be logged!\n");
    }
    else
    {
        printf("You are authorized to see the secret number.\n");
        double result = calcSecretNumber((double) x); 
        if(result == -1)
        {
           printf("Sorry! the secret number couldn't be calculated. Please try again.");
        }
        else
        {
           printf("Here is the secret number: %10.4f\n", result);
        }
    }

	return 0;
}
