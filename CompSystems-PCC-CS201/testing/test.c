#include<stdio.h>
#include<stdlib.h>

#define ARRAY_SIZE 50000
#define ITERATIONS 10000

void initArray(double[], double);

int main(int argc, char **argv)
{
	double *array = calloc(ARRAY_SIZE,sizeof(double));
	double sum = 0;
	double x;
	x = atof(argv[1]);
	initArray(array, x);

	for(unsigned int i = 0; i < ITERATIONS; i++) //can't change
	{
		for(int j = 0; j < ARRAY_SIZE;)
		{
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
			sum += array[j]; j++;
		}
	}

	printf("sum: %f\n", sum);

	free(array);
	return sum;
} 

void initArray(double array[], double num)
{
	num /= 1000000000.0;
	for(int i = 0; i < ARRAY_SIZE; num++, i++)
	{
		array[i] = num;
	}
}
