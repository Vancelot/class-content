	.file	"test.c"
	.text
	.globl	foo1
	.type	foo1, @function
foo1:
.LFB25:
	.cfi_startproc
	leal	3(%rdi), %eax
	ret
	.cfi_endproc
.LFE25:
	.size	foo1, .-foo1
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"x: %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB24:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$11, %edi
	call	foo1
	movl	%eax, %esi
	leaq	.LC0(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$0, %eax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE24:
	.size	main, .-main
	.globl	foo2
	.type	foo2, @function
foo2:
.LFB26:
	.cfi_startproc
	leal	-3(%rdi), %eax
	ret
	.cfi_endproc
.LFE26:
	.size	foo2, .-foo2
	.ident	"GCC: (GNU) 8.1.1 20180531"
	.section	.note.GNU-stack,"",@progbits
