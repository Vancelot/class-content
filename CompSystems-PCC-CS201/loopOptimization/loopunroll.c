// a loop that has been unrolled

#pragma GCC diagnostic ignored "-Wmisleading-indentation"

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<unistd.h>
#include"iterations.h"

#define ARRAYSIZE 100


int main(int argc, char** argv)
{
	int *a = calloc(ARRAYSIZE, sizeof(int)); 
	int		i, j;
	a[99] = 1;
	pid_t pid;
	int	*sum0 = malloc(sizeof(int));
	int sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
	clock_t	start = clock();

	pid = fork();

	if(pid == 0)
	{
		for (i = 0; i < ITERATIONS/2; i++)
			for (j = 0; j < 100; j += 10)
			{
				sum0 += a[j];
				sum0 += a[j + 1];
				sum1 += a[j + 2];
				sum1 += a[j + 3];
				sum2 += a[j + 4];
				sum2 += a[j + 5];
				sum3 += a[j + 6];
				sum3 += a[j + 7];
				sum4 += a[j + 8];
				sum4 += a[j + 9];
			}
		return 1;
	}

	if(pid > 0)
	{
		for (i = 0; i < ITERATIONS/2; i++)
			for (j = 0; j < 100; j += 10)
			{
				*sum0 += a[j];
				*sum0 += a[j + 1];
				*sum1 += a[j + 2];
				*sum1 += a[j + 3];
				sum2 += a[j + 4];
				sum2 += a[j + 5];
				sum3 += a[j + 6];
				sum3 += a[j + 7];
				sum4 += a[j + 8];
				sum4 += a[j + 9];
			}
	}
	*sum0 = *sum0 + sum1 + sum2 + sum3 + sum4;
	printf("sum: %d\t%.1f seconds\n", *sum0 ,
		   (double) (clock() - start) / CLOCKS_PER_SEC);

	return *sum0 + sum1 + sum2 + sum3 + sum4;	// so optimizer will compile the code
}
