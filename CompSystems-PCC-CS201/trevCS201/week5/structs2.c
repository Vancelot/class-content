// structure example

#include <stdio.h>

struct S1 {
    char c;       // 1 bytes
    int i[2];     // 4 bytes each
    double v;     // 8 bytes
};

struct S2 {
    char c; // 1 byte
    double v; // 8 bytes
    char d; // 1 byte
};

int main (int argc, char **argv)
{
    struct S1 a;
    struct S2 b;
    a.c = 'A';
    a.i[0] = 23;
    a.i[1] = 32;
    a.v = 32.00;
    b.c = 'B';
    b.d = 'C';
    b.v = 53.32;
    
    printf("%c %d %d %.2f\n", a.c, a.i[0], a.i[1], a.v);
    printf("%c %c %.2f\n", b.c, b.d, b.v);
    printf("%d\n", (int) sizeof(struct S1));
    printf("%d\n", (int) sizeof(struct S2));
    return 0;
}