// procedures

#include <stdio.h>

int functC(int a)
{
    return a*12;
}

int functB(int a, int b, int c, int d)
{
   int e = a+b+c+d;

   e *= functC(a);
   return e;
}

int functA(int a, int b)
{
   int c = 14;
   int d = functB(a, b, 15,14);
   
   return a*c + d*b;

}

int main (int argc, char **argv)
{
    
    int j = functA(32,43);
    printf("%d\n", j);
    return 0;
}