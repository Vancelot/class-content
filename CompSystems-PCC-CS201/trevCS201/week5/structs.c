// structure example

#include <stdio.h>

struct record {
    long hours;    // 8 bytes
    char id;       // 1 byte
    float payrate; // 4 bytes
};

int main (int argc, char **argv)
{
    struct record emp;
    emp.id = 'A';
    emp.hours = 34;
    emp.payrate = 20.00;
    
    printf("%c %ld %.2f\n", emp.id, emp.hours, emp.payrate);
    printf("%d\n", (int) sizeof(struct record));
    return 0;
}