// a loop that has been unrolled, which both:
//
//     * lowers the loop control overhead per "iteration", and
//
//     * provides an opportunity for the Pentium's built-in
//           parallel processing capabilities

#include <stdlib.h>


int main (int argc, char** argv)
{
    int     *a;
    int     arraySize = 100;
    int     repitions = 100000;
    int     unrollBy = 1;
    int     rep, index, unroll;

    if (argc >= 2)
        unrollBy = atoi(argv[1]);
    if (argc >= 3)
        arraySize = atoi(argv[2]);
    
    a = calloc(arraySize * sizeof(int));

    for (rep = 0; rep < repitions; i++)
        for (index = 0; index < arraySize; index += unrollBy) {
            a[index] += 3;
            }
    
    free(a);

    return 0;
}
