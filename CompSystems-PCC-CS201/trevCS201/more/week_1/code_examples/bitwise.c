
#include <stdio.h>

// get the bits at beg position with length of n
int getMiddle(int num, int beg, int n)
{
   // (((1 << k) - 1) & (number >> (p - 1)));
    // need to get a mask of n bits of 1
    // This will shift 1 by n places
    // for example:  if n = 4, 0...01 => 0...010000
    int mask = (1 << n);
    // then minus 2's complement 1
    //  0...010000 - 0..0000001  = 0...0001111 
    mask = mask - 1;
    printf("%X", mask);
    
    // now we shift num to left to truncate
    // bits on the lower end
    // beg = 24, num = FAFFFFFF => num << 24 => 00000FA
    num = (num >> beg);
    // and with mask. this will
    // turn off all bits on the upper end
    // num & mask = 0...0 1111 1010 & 0...0 0000 1111 =>
    num = num & mask;
    
    return num;
}


void doBitWise(int num, int mask)
{
   int and, or, xor, neg;
   and = num & mask;
   or = num | mask;
   xor = num ^ mask;
   neg = ~num;
   printf("and:  %.2d or: %.2d xor: %.2d neg: %.2d\n", and, or, xor, neg);
   
}

// turn off (unset) the nth bit
int turnItOff(int num, int bit)
{
    // start by creating a mask
    // with 1 where the bit is
    // and all 0's elswhere
    // for example: bit = 5, 0...01 << 5 0...0100000 
    int mask = (1 << (bit-1));
    // now negate the mask to get 0 where you want
    // to turn off the bit and 1's everywhere else:
    //  0..0100000  => 1..11011111
    //mask = ~mask;
    //   now 'and' it with the number
    //  0..01111001 &  1..11011111 => 0..01011001
    //  note that & 1 is the whatever the bit was
    //  and & 0 is always zero no matter what the bit was
   return num | mask;
}

// Now...how would you write a function to turn ON (or set) the nth bit?
//  ... and how do you tell if a bit is set or unset?

int main()
{
    int num, n; //, beg, mid;

     // this does a few bitwise operations
     // on two numbers. 
    /*printf("type two numbers: ");
    scanf("%d %d", &num, &n);
    doBitWise(num, n);*/

    // this will turn off a bit (make it 0 if it isn't and keep it 0
    // if it is)
    
    printf("what number in hex: ");
    scanf("%x", &num);
    printf("bit to turn off: ");
    scanf("%d", &n);
    num = turnItOff(num,n);
    printf("%x\n", num);
    

    /*printf("enter number in 32 bit hex: ");
    scanf("%x", &num);
    printf("enter bit to begin extraction: (0...31) ");
    scanf("%d", &beg);
    printf("how many bits to extract: ( < 31) ");
    scanf("%d", &n);
    mid = getMiddle(num, beg, n);
    printf("Middle bits are %X\n", mid);
    */

    return 0;
}
