<html lang="en">
<head>
<title>The GNU C Programming Tutorial</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name=description content="The GNU C Programming Tutorial">
<meta name=generator content="makeinfo 4.2">
<link href="http://www.gnu.org/software/texinfo/" rel=generator-home>
</head>
<body>
<p>
Node:<a name="Formatted%20output%20conversion%20specifiers">Formatted output conversion specifiers</a>,
Previous:<a rel=previous accesskey=p href="printf.html#printf">printf</a>,
Up:<a rel=up accesskey=u href="Formatted-string-output.html#Formatted%20string%20output">Formatted string output</a>
<hr><br>

<h5>Formatted output conversion specifiers</h5>

<p>There are many different conversion specifiers that can be used for
various data types. Conversion specifiers can become quite complex; for
example, <code>%-17.7ld</code> specifies that <code>printf</code> should print the
number left-justified (<code>-</code>), in a field at least seventeen
characters wide (<code>17</code>), with a minimum of seven digits (<code>.7</code>),
and that the number is a long integer (<code>l</code>) and should be printed
in decimal notation (<code>%d</code>).

<p>In this section, we will examine the basics of <code>printf</code> and its
conversion specifiers.  (For even more detail, <a href="../libc/Formatted-Output.html#Formatted%20Output">Formatted Output</a>.)

<p>A conversion specifier begins with a percent sign, and ends with one of
the following <dfn>output conversion characters</dfn>.  The most basic
conversion specifiers simply use a percent sign and one of these
characters, such as <code>%d</code> to print an integer.  (Note that characters
in the template string that are not part of a conversion specifier are
printed as-is.)

<dl>

<br><dt><code>c</code>
<dd>Print a single character.

<br><dt><code>d</code>
<dd>Print an integer as a signed decimal number.

<br><dt><code>e</code>
<dd>Print a floating-point number in exponential notation, using lower-case
letters.  The exponent always contains at least two digits.  Example:
<code>6.02e23</code>.

<br><dt><code>E</code>
<dd>Same as <code>e</code>, but uses upper-case letters.  Example: <code>6.02E23</code>.

<br><dt><code>f</code>
<dd>Print a floating-point number in normal, fixed-point notation.

<br><dt><code>i</code>
<dd>Same as <code>d</code>.

<br><dt><code>m</code>
<dd>Print the string corresponding to the specified value of the system
<code>errno</code> variable.  (See <a href="Usual-file-name-errors.html#Usual%20file%20name%20errors">Usual file name errors</a>.)  GNU systems
only.

<br><dt><code>s</code>
<dd>Print a string.

<br><dt><code>u</code>
<dd>Print an unsigned integer.

<br><dt><code>x</code>
<dd>Print an integer as an unsigned hexadecimal number, using lower-case letters.

<br><dt><code>X</code>
<dd>Same as <code>x</code>, but uses upper-case letters.

<br><dt><code>%</code>
<dd>Print a percent sign (<code>%</code>).

</dl>

<p>In between the percent sign (<code>%</code>) and the output conversion
character, you can place some combination of the following
<dfn>modifiers</dfn>.  (Note that the percent sign conversion (<code>%%</code>)
doesn't use arguments or modifiers.)

<ul>

<li>Zero or more flag characters, from the following table:

<dl>

<br><dt><code>-</code>
<dd>
Left-justify the number in the field (right justification is the
default).  Can also be used for string and character conversions
(<code>%s</code> and <code>%c</code>).

<br><dt><code>+</code>
<dd>
Always print a plus or minus sign to indicate whether the number is
positive or negative.  Valid for <code>%d</code>, <code>%e</code>, <code>%E</code>, and
<code>%i</code>.

<br><dt><code>Space character</code>
<dd>
If the number does not start with a plus or minus sign, prefix it with a
space character instead.  This flag is ignored if the <code>+</code> flag is
specified.

<br><dt><code>#</code>
<dd>
For <code>%e</code>, <code>%E</code>, and <code>%f</code>, forces the number to include a
decimal point, even if no digits follow.  For <code>%x</code> and <code>%X</code>,
prefixes <code>0x</code> or <code>0X</code>, respectively.

<br><dt><code>'</code>
<dd>
Separate the digits of the integer part of the number into groups, using
a locale-specific character.  In the United States, for example, this
will usually be a comma, so that one million will be rendered
<code>1,000,000</code>.  GNU systems only.

<br><dt><code>0</code>
<dd>
Pad the field with zeroes instead of spaces; any sign or indication of
base (such as <code>0x</code>) will be printed before the zeroes.  This flag
is ignored if the <code>-</code> flag or a precision is specified.

</dl>

<p>In the example given above, <code>%-17.7ld</code>, the flag given is <code>-</code>.

</p><li>An optional non-negative decimal integer specifying the minimum
field width within which the conversion will be printed.  If the
conversion contains fewer characters, it will be padded with spaces (or
zeroes, if the <code>0</code> flag was specified).  If the conversion contains
more characters, it will not be truncated, and will overflow the field. 
The output will be right-justified within the field, unless the <code>-</code>
flag was specified.  In the example given above, <code>%-17.7ld</code>, the
field width is <code>17</code>.

<li>For numeric conversions, an optional precision that specifies the
number of digits to be written.  If it is specified, it consists of a
dot character (<code>.</code>), followed by a non-negative decimal integer
(which may be omitted, and defaults to zero if it is). In the example
given above, <code>%-17.7ld</code>, the precision is <code>.7</code>.  Leading
zeroes are produced if necessary.  If you don't specify a precision, the
number is printed with as many digits as necessary (with a default of
six digits after the decimal point).  If you supply an argument of zero
with and explicit precision of zero, <code>printf</code> will not print any
characters.  Specifying a precision for a string conversion (<code>%s</code>)
indicates the maximum number of characters to write.

<li>An optional <dfn>type modifier character</dfn> from the table below. 
This character specifies the data type of the argument if it is
different from the default.  In the example given above,
<code>%-17.7ld</code>, the type modifier character is <code>l</code>; normally, the
<code>d</code> output conversion character expects a data type of <code>int</code>,
but the <code>l</code> specifies that a <code>long int</code> is being used instead.

<p>The numeric conversions usually expect an argument of either type
<code>int</code>, <code>unsigned int</code>, or <code>double</code>.  (The <code>%c</code>
conversion converts its argument to <code>unsigned char</code>.)  For the
integer conversions (<code>%d</code> and <code>%i</code>), <code>char</code> and
<code>short</code> arguments are automatically converted to type <code>int</code>,
and for the unsigned integer conversions (<code>%u</code>, <code>%x</code>, and
<code>%X</code>), they are converted to type <code>unsigned int</code>.  For the
floating-point conversions (<code>%e</code>, <code>%E</code>, and <code>%f</code>), all
<code>float</code> arguments are converted to type <code>double</code>.  You can use
one of the type modifiers from the table below to specify another type
of argument.

<dl>

<br><dt><code>l</code>
<dd>Specifies that the argument is a <code>long int</code> (for <code>%d</code> and <code>%i</code>),
or an <code>unsigned long int</code> (for <code>%u</code>, <code>%x</code>, and <code>%X</code>).

<br><dt><code>L</code>
<dd>Specifies that the argument is a <code>long double</code> for the
floating-point conversions (<code>%e</code>, <code>%E</code>, and <code>%f</code>).  Same
as <code>ll</code>, for integer conversions (<code>%d</code> and <code>%i</code>).

<br><dt><code>ll</code>
<dd>Specifies that the argument is a <code>long long int</code> (for <code>%d</code> and <code>%i</code>). 
On systems that do not have extra-long integers, this has the same effect as <code>l</code>.

<br><dt><code>q</code>
<dd>Same as <code>ll</code>; comes from calling extra-long integers "quad ints".

<br><dt><code>z</code>
<dd>Same as <code>Z</code>, but GNU only, and deprecated.

<br><dt><code>Z</code>
<dd>Specifies that the argument is of type <code>size_t</code>.  (The
<code>size_t</code> type is used to specify the sizes of blocks of memory, and
many functions in this chapter use it.)

</dl>

</ul>

<p>Make sure that your conversion specifiers use valid syntax; if they do
not, if you do not supply enough arguments for all conversion
specifiers, or if any arguments are of the wrong type, unpredictable
results may follow.  Supplying too many arguments is not a problem,
however; the extra arguments are simply ignored.

<p>Here is a code example that shows various uses of <code>printf</code>.

<br><pre>#include &lt;stdio.h&gt;
#include &lt;errno.h&gt;

int main()
{
  int my_integer = -42;
  unsigned int my_ui = 23;
  float my_float = 3.56;
  double my_double = 424242.171717;
  char my_char = 'w';
  char my_string[] = "Pardon me, may I borrow your nose?";

  printf ("Integer: %d\n", my_integer);
  printf ("Unsigned integer: %u\n", my_ui);

  printf ("The same, as hexadecimal: %#x %#x\n", my_integer, my_ui);

  printf ("Floating-point: %f\n", my_float);
  printf ("Double, exponential notation: %17.11e\n", my_double);

  printf ("Single character: %c\n", my_char);
  printf ("String: %s\n", my_string);

  errno = EACCES;
  printf ("errno string (EACCES): %m\n");

  return 0;
}
</pre>

<p>The code example above produces the following output on a GNU system:

<br><pre>Integer: -42
Unsigned integer: 23
The same, as hexadecimal: 0xffffffd6 0x17
Floating-point: 3.560000
Double, exponential notation: 4.24242171717e+05
Single character: w
String: Pardon me, may I borrow your nose?
errno string (EACCES): Permission denied
</pre>

</body></html>

