<html lang="en">
<head>
<title>The GNU C Programming Tutorial</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name=description content="The GNU C Programming Tutorial">
<meta name=generator content="makeinfo 4.2">
<link href="http://www.gnu.org/software/texinfo/" rel=generator-home>
</head>
<body>
<p>
Node:<a name="argp%20description">argp description</a>,
Next:<a rel=next accesskey=n href="argp-example.html#argp%20example">argp example</a>,
Previous:<a rel=previous accesskey=p href="Processing-command-line-options.html#Processing%20command-line%20options">Processing command-line options</a>,
Up:<a rel=up accesskey=u href="Processing-command-line-options.html#Processing%20command-line%20options">Processing command-line options</a>
<hr><br>

<h4><code>argp</code> description</h4>

<p>This section will describe how to write a simple program that implements
most of the standards mentioned above.  It assumes some knowledge of
advanced C data structures that we have not yet covered in this book; if
you are confused, you might want to consult the chapter that discusses
this material. (See <a href="More-data-types.html#More%20data%20types">More data types</a>.)  Note that we are only
discussing the basics of <code>argp</code> in this chapter; to read more about
this complicated and flexible facility of the GNU C Library, consult
<a href="../libc/Argp.html#Argp">Parsing Program Options with Argp</a>.  Nevertheless, what you learn in this chapter may be
all you need to develop a program that is compliant with GNU coding
standards, with respect to command-line options.

<p>The main interface to <code>argp</code> is the <code>argp_parse</code> function. 
Usually, the only argument-parsing code you will need in <code>main</code> is
a call to this function.  The first parameter it takes is of type
<code>const struct argp *argp</code>, and specifies an <code>ARGP</code> structure
(see below).  (A value of zero is the same as a structure containing all
zeros.)  The second parameter is simply <code>argc</code>, the third simply
<code>argv</code>.  The fourth parameter is a set of flags that modify the
parsing behaviour; setting this to zero usually doesn't hurt unless
you're doing something fancy, and the same goes for the fifth parameter. 
The sixth parameter can be useful; in the example below, we use it to
pass information from <code>main</code> to our function <code>parse_opt</code>,
which does most of the work of initalizing internal variables (fields in
the <code>arguments</code> structure) based on command-line options and
arguments.

<p>The <code>argp_parse</code> returns a value of type <code>error_t</code>: usually
either 0 for success, <code>ENOMEM</code> if a memory allocation error
occurred, or <code>EINVAL</code> if an unknown option or argument was met
with.

<p>For this example, we are using only the first four fields in
<code>ARGP</code>, which are usually all that is needed.  The rest of the
fields will default to zero.  The four fields are, in order:

<ol type=1 start=1>

</p><li><code>OPTIONS</code>: A pointer to a vector the elements of which are of type
<code>struct argp_option</code>, which contains four fields.  The vector
elements specify which options this parser understands.  If you assign
your option structure by initializing the array as we do in this
section's main example, unspecified fields will default to zero, and
need not be specified.  The whole vector may contain zero if there are
no options at all.  It should in any case be terminated by an entry
with a zero in all fields (as we do by specifying the last item in the
<code>options</code> vector to be <code>{0}</code> in the main example below.

<p>The four main <code>argp_option</code> structure fields are as
follows.  (We will ignore the fifth one, which is relatively unimportant
and will simply default to zero if you do not specify it.)

<ol type=1 start=1>

</p><li><code>NAME</code>: The name of this option's long option (may be zero). 
To specify multiple names for an option, follow it with additional entries, with
the <code>OPTION_ALIAS</code> flag set.

<li><code>KEY</code>: The integer key to pass to the <code>PARSER</code> function when
parsing the current option; this is the same as the name of the current
option's short option, if it is a printable ASCII character.

<li><code>ARG</code>: The name of this option's argument, if any.

<li><code>FLAGS</code>: Flags describing this option.  You can specify multiple
flags with logical OR (for example, <code>OPTION_ARG_OPTIONAL |
OPTION_ALIAS</code>).

<p>Some of the available options are:

<ul>

<li><code>OPTION_ARG_OPTIONAL</code>:
The argument to the current option is optional.

<li><code>OPTION_ALIAS</code>:
The current option is an alias for the previous option.

<li><code>OPTION_HIDDEN</code>:
Don't show the current option in <code>--help</code> output.

</ul>

<li><code>DOC</code>: A documentation string for the current option; will be shown
in <code>--help</code> output.

</ol>

<li><code>PARSER</code>: A pointer to a function to be called by <code>argp</code> for
each option parsed.  It should return one of the following values:

<ul>

<li><code>0</code>: Success.

<li><code>ARGP_ERR_UNKNOWN</code>: The given key was not recognized.

<li>An <code>errno</code> value indicating some other error.  (See <a href="Usual-file-name-errors.html#Usual%20file%20name%20errors">Usual file name errors</a>.)

</ul>

<p>The parser function takes the following arguments:

<ol type=1 start=1>

</p><li><code>KEY</code>: An integer specifying which argument this is, taken from the <code>KEY</code>
field in each <code>argp_option</code> structure, or else a key with a special meaning,
such as one of the following:

<ul>

<li><code>ARGP_KEY_ARG</code>: The current command-line argument is not an option.

<li><code>ARGP_KEY_END</code>: All command-line arguments have been parsed.

</ul>

<li><code>ARG</code>: The string value of the current command-line argument,
or NULL if it has none.

<li><code>STATE</code>: A pointer to an <code>argp_state</code> structure, containing
information about the parsing state, such as the following fields:

<ol type=1 start=1>

<li><code>input</code>: The same as the last parameter to <code>argp_parse</code>.  We
use this in the main code example below to pass information between the
<code>main</code> and <code>parse_opt</code> functions.

<li><code>arg_num</code>: The number of the current non-option argument being parsed.

</ol>

</ol>

<li><code>ARGS_DOC</code>: If non-zero, a string describing how the non-option arguments should look. 
It is only used to print the <code>Usage:</code> message.  If it contains newlines,
the strings separated by them are considered alternative usage patterns, and printed on
separate lines (subsequent lines are preceded by <code>or:</code> rather than <code>Usage:</code>.

<li><code>DOC</code>: If non-zero, a descriptive string about this program.  It
will normally be printed before the options in a help message, but if
you include a vertical tab character (<code>\v</code>), the part after the
vertical tab will be printed following the options in the output to the
<code>--help</code> option.  Conventionally, the part before the options is just a short
string that says what the program does, while the part afterwards is longer
and describes the program in more detail.

</ol>

<p>There are also some utility functions associated with <code>argp</code>, such
as <code>argp_usage</code>, which prints out the standard usage message.  We
use this function in the <code>parse_opt</code> function in the following
example.  See <a href="../libc/Argp-Helper-Functions.html#Argp%20Helper%20Functions">Functions For Use in Argp Parsers</a>, for more of these
utility functions.

</body></html>

