#include <stdio.h>
#include <stdlib.h>
#include <sys/times.h>      // for times()
#include <time.h>           // for clock()

char * name = "Kerry Vance";

/* I will be using only the two functions
 * you will be changing when grading. So
 * don't write code you want to keep outside
 *  of the two functions (they won't be copied over)
 */

// These can be changed for debugging purposes
// but DO CHANGE THEM TO THE ORIGINAL VALUES
//    RUNS = 10000
//    SIZE = 10000
#define RUNS 10000
#define SIZE 10000


/*  write a function using gcc inline assembly
 * that calculates the area of a right triangle.
 * the formula for doing so is:
 *    area = 1/2 x side1 x side2
 */
int calcArea(int side1, int side2)
{
   int area = 1.0;

	asm
	(
	"imul %1, %2;"
	"shr $1, %2;"
	"mov %2, %0;"
	: "=r" (area)
	: "r"(side1), "r"(side2)
	);

   // PUT YOUR ASSEMBLY CODE HERE
   // DON'T WRITE ANY OTHER
   // C CODE
  
   return area;
}

/* this is the original function
 * DON"T TOUCH THIS FUNCTION
 * it's for reference only
 */
int sumAreaOrig(int a, int b, int *arr)
{
   int sum, i, j;

   for(i=0; i<RUNS; i++) {
       
       sum = 0;
       for(j=0; j<SIZE; j++)  {
          sum += arr[j]*calcArea(a,b) + j;
//		 		printf("sum: %d\n", sum);
      }
      
   }           
   return sum;
}

/*  PLEASE MODIFY THIS FUNCTION
 *  make sure it returns the same
 *  sum as the original function
 */
int sumAreaImprove(int a, int b, int *arr)
{
    /* you can add any variables here */
   int sum, sum0, sum1, sum2, sum3, i, j, area = calcArea(a,b);
   
   /* leave this outer loop alone*/
   for(i=0; i < RUNS; i++)
   { /* you can change this inner loop */
      /* but remember it must sum over all values */
      sum = 0;
		sum0 = 0;
		sum1 = 0;
		sum2 = 0;
		sum3 = 0;
      for(j=0; j<SIZE; j += 25)
		{
         sum += (arr[j] * area) + (arr[j + 1]*area) + (arr[j + 2]*area) + (arr[j + 3]*area) + (arr[j + 4]*area);
         sum0 += (arr[j+5] * area) + (arr[j + 6]*area) + (arr[j + 7]*area) + (arr[j + 8]*area) + (arr[j + 9]*area);
         sum1 += (arr[j+10] * area) + (arr[j + 11]*area) + (arr[j + 12]*area) + (arr[j + 13]*area) + (arr[j + 14]*area);
         sum2 += (arr[j+15] * area) + (arr[j + 16]*area) + (arr[j + 17]*area) + (arr[j + 18]*area) + (arr[j + 19]*area);
         sum3 += (arr[j+20] * area) + (arr[j + 21]*area) + (arr[j + 22]*area) + (arr[j + 23]*area) + (arr[j + 24]*area);

			sum += j + (j+1) + (j+2) + (j+3) + (j+4);
			sum0 += (j+5) + (j+6) + (j+7) + (j+8) + (j+9);
			sum1 += (j+10) + (j+11) + (j+12) + (j+13) + (j+14);
			sum2 += (j+15) + (j+16) + (j+17) + (j+18) + (j+19);
			sum3 += (j+20) + (j+21) + (j+22) + (j+23) + (j+24);
     	}      
   }           
	sum += sum0 + sum1 + sum2 + sum3;
   return sum;
}

/* You can modify main for debugging
 * but PLEASE RETURN IT TO THE ORIGINAL
 * CODE. I will be using only your two functions
 * above when grading.  So don't write code
 * you want to keep outside of the above two
 * functions (they won't be copied over)
 */
int main(int argc, char **argv)
{
    struct tms  origStart, origStop, impStart, impStop;
    double origTime, impTime;
    double difference;
    int area, sumOrig, sumImprove;
    int side1, side2;
    int i;
    int array[SIZE];

    for(i=0; i<SIZE; i++)
       array[i] = i;

    if(argc < 3) {
        printf("Need to provide two side measurements\n");
        exit(-1);
    } else {
        side1 = atoi(argv[1]);
        side2 = atoi(argv[2]);
    }

    printf("side1: %d  side2: %d\n", side1, side2);
        
	printf("Name: %s\n",name);
    
    area = calcArea(side1, side2);
    printf("the area is: %d\n", area);

    //start time here
    times(&origStart);
    sumOrig = sumAreaOrig(side1, side2, array);
    times(&origStop);
    //end time here
    printf("results from orignal: %d\n", sumOrig);

    // start time here
    times(&impStart);
    sumImprove = sumAreaImprove(side1, side2, array);
    times(&impStop);
    // end time here
    printf("results from improved: %d\n", sumImprove);

    //do time calculations here
    origTime = (origStop.tms_utime - origStart.tms_utime);
    impTime =  (impStop.tms_utime - impStart.tms_utime);
    printf("original user time: %f\n",origTime);
    printf("improved user time: %f\n", impTime);
    difference = ((origTime - impTime)/origTime)*100.0;
    printf("percent difference:  %f\n", difference);
	return 0;
}
