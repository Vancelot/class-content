5. What do Sapolski and the film Unnatural Causes contend about the connection between health and economic status? 

The impact on health relative to social class is multifaceted. Access to health care requires resources, resources that many in America don't have. Abundant resources also reduce financial stress and afford more professional and social opportunities (Adelman, 2008). The lack of those resources create an environment that shapes us as much or more than our biology. The uncertainty of a family that doesn't know how it will eat next week, whether they'll have a job, or how to pay for school, causes exorbitant levels of stressed not felt by those of higher economic status. This abundance of stress equates to a larger negative impact on health than can be accounted for by any predisposition to health harming behaviors correlated with low socio-economic status or those healthy behaviors facilitated surplus resources like a healthy diet or healthcare (Sapolski, 2005). Stress associated with poverty is exacerbated by the fact that the political landscape is littered with rhetoric that belittles those experiencing poverty. This compounds the stress of poverty by making the poor feel poorer. Holistic and comprehensive approaches to improving public health need to focus on social equity projects to become more effective. Access to healthcare is obviously important, but to truly close the gap of life expectancy and rates of disease between the rich and poor, inequality needs to be reduced. 

Work Cited

Sapolski, Robert. 2005. "Sick of Poverty." Scientific American 293(6): 92-99.

Adelman, Larry, Christine Herbes-Sommers, and Llewellyn Smith. Unnatural Causes: Is Inequality Making Us Sick?San Francisco, Calif.: California Newsreel, 2008. Internet resource. 
