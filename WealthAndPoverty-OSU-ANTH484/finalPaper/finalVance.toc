\contentsline {section}{\numberline {1}{Introduction}}{2}{section.1}% 
\contentsline {section}{\numberline {2}{Revolution: Case Studies}}{2}{section.2}% 
\contentsline {subsection}{\numberline {2.1}{French Revolution: 1789-1799}}{2}{subsection.2.1}% 
\contentsline {subsubsection}{\numberline {2.1.1}{Preconditions}}{2}{subsubsection.2.1.1}% 
\contentsline {paragraph}{{Class structure}}{2}{paragraph*.1}% 
\contentsline {paragraph}{{Representation}}{2}{paragraph*.2}% 
\contentsline {subsubsection}{\numberline {2.1.2}{Critical Juncture}}{3}{subsubsection.2.1.2}% 
\contentsline {paragraph}{{Storming of the Bastille}}{3}{paragraph*.3}% 
\contentsline {subsubsection}{\numberline {2.1.3}{Analysis and Outcomes}}{3}{subsubsection.2.1.3}% 
\contentsline {paragraph}{{Antagonistic Factors}}{3}{paragraph*.4}% 
\contentsline {paragraph}{{Reform}}{3}{paragraph*.5}% 
\contentsline {subsection}{\numberline {2.2}{Cuban Revolution: 1953-1959}}{3}{subsection.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.1}{Preconditions}}{3}{subsubsection.2.2.1}% 
\contentsline {paragraph}{{Corruption}}{3}{paragraph*.6}% 
\contentsline {paragraph}{{Representation}}{4}{paragraph*.7}% 
\contentsline {subsubsection}{\numberline {2.2.2}{Critical Juncture}}{4}{subsubsection.2.2.2}% 
\contentsline {paragraph}{{Moncada Barracks}}{4}{paragraph*.8}% 
\contentsline {paragraph}{{Guerillas}}{4}{paragraph*.9}% 
\contentsline {paragraph}{{Solidarity}}{5}{paragraph*.10}% 
\contentsline {subsubsection}{\numberline {2.2.3}{Analysis and Outcomes}}{5}{subsubsection.2.2.3}% 
\contentsline {paragraph}{{Antagonistic Factors}}{5}{paragraph*.11}% 
\contentsline {paragraph}{{Reform}}{5}{paragraph*.12}% 
\contentsline {section}{\numberline {3}{Modern Day America}}{5}{section.3}% 
\contentsline {subsection}{\numberline {3.1}{Statistics}}{5}{subsection.3.1}% 
\contentsline {paragraph}{{Inequality}}{5}{paragraph*.13}% 
\contentsline {section}{\numberline {4}{Results and Discussion}}{6}{section.4}% 
\contentsline {subsection}{\numberline {4.1}{Reactions}}{6}{subsection.4.1}% 
\contentsline {paragraph}{{Statistics vs. Perceptions}}{6}{paragraph*.14}% 
\contentsline {paragraph}{{External vs. Internal}}{6}{paragraph*.15}% 
