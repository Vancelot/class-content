-- write your queries to insert data here
INSERT INTO client (first_name, last_name, dob)
VALUES
	('Sara', 'Smith', '1970-1-2'),
	('Miguel', 'Cabrera', '1988-2-2'),
	('Bo', 'Chang', '1985-3-2');

INSERT INTO employee (first_name, last_name, dob, date_joined)
VALUES
	('Ananya', 'Jaiswal', '1975-1-2', '2009-1-1'),
	('Michael', 'Fern', '1980-10-18', '2013-6-5'),
	('Abdul', 'Rehman', '1984-3-21', '2013-11-10'); 

INSERT INTO project (cid, name, notes)
VALUES
	((SELECT id FROM client
		WHERE first_name = 'Sara' AND last_name = 'Smith'),
	'Diamond', 'Should be done by Jan 2019'),
	((SELECT id FROM client
		WHERE first_name = 'Bo' AND last_name = 'Chang'),
	"Chan'g", 'Ongoing maintenance'),
	((SELECT id FROM client
		WHERE first_name = 'Miguel' AND last_name = 'Cabrera'),
	'The Robinson Project', NULL);

INSERT INTO works_on (eid, pid, start_date)
VALUES
	((SELECT id FROM employee 
		WHERE first_name = 'Ananya' AND last_name = 'Jaiswal'),
	 (SELECT id FROM project
		WHERE name = "Chan'g"),
	'2012-1-1'),
 	((SELECT id FROM employee 
 		WHERE first_name = 'Michael' AND last_name = 'Fern'),
 	 (SELECT id FROM project
 		WHERE name = 'The Robinson Project'),
 	'2013-8-8'),
 	((SELECT id FROM employee 
 		WHERE first_name = 'Abdul' AND last_name = 'Rehman'),
 	 (SELECT id FROM project
 		WHERE name = 'Diamond'),
 	'2014-9-11');

-- Leave the queries below untouched. These are to test your submission correctly.
select * from project;
select * from client;
select * from employee;
select * from works_on;
