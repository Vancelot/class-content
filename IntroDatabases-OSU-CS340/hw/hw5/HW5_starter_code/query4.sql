-- Find the actor_id, first_name and last_name of all actors who have never been in a Sci-Fi film.
-- Order by the actor_id in ascending order.
-- Put your query for Q4 here

SELECT a.actor_id, a.first_name, a.last_name
FROM actor a
WHERE a.actor_id NOT IN (
	SELECT a.actor_id
	FROM actor a
	INNER JOIN film_actor fa ON fa.actor_id = a.actor_id
	INNER JOIN film_category fc ON fc.film_id = fa.film_id
	INNER JOIN category c ON c.category_id = fc.category_id
	WHERE c.name = 'Sci-Fi'
)
GROUP BY a.actor_id
ORDER BY a.actor_id ASC;
