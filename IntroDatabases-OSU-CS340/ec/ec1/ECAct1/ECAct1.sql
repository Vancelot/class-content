-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: classmysql.engr.oregonstate.edu:3306
-- Generation Time: Apr 02, 2020 at 11:15 PM
-- Server version: 10.4.11-MariaDB-log
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `persist_cs340_schutfoj`
--

-- --------------------------------------------------------

--
-- Table structure for table `Parts`
--

CREATE TABLE `Parts` (
  `pid` int(11) NOT NULL,
  `pname` varchar(20) NOT NULL,
  `color` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Parts`
--

INSERT INTO `Parts` (`pid`, `pname`, `color`) VALUES
(1, 'widget', 'white'),
(2, 'gasket', 'blue'),
(3, 'bolt', 'black'),
(5, 'gasket', 'green'),
(6, 'handle', 'black'),
(7, 'gasket', 'black'),
(9, 'nail', 'green'),
(55, 'New Part', 'Blue'),
(111, 'hinge', 'silver'),
(123, 'cylinder', 'black');



-- --------------------------------------------------------

--
-- Table structure for table `Suppliers`
--

CREATE TABLE `Suppliers` (
  `sid` int(11) NOT NULL,
  `sname` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Suppliers`
--

INSERT INTO `Suppliers` (`sid`, `sname`, `city`) VALUES
(14, 'Acme', 'Portland'),
(20, 'Ace', 'Salem'),
(67, 'Factory-4-less', 'Albany'),
(71, 'Part Town', 'Portland'),
(77, 'XYZ Company', 'Albany'),
(213, 'Suppier One', 'Eugene');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Parts`
--
ALTER TABLE `Parts`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `Suppliers`
--
ALTER TABLE `Suppliers`
  ADD PRIMARY KEY (`sid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
