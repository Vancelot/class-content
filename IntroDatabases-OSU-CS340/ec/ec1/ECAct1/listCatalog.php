﻿<!DOCTYPE html>
<?php
		$currentpage="List Catalog";
?>
<html>
	<head>
		<title>List Catalog</title>
		<link rel="stylesheet" href="index.css">
	</head>
<body>


<?php
// change the value of $dbuser and $dbpass to your username and password
	include 'connectvars.php'; 
	include 'header.php';	

	$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if (!$conn) {
		die('Could not connect: ' . mysql_error());
	}	
	
// query to select all information from supplier table
	$query = "SELECT S.sname, P.pname, P.color, C.price 
		FROM Catalog C, Suppliers S, Parts P
		WHERE C.pid=P.pid and C.sid = S.sid";
	
// Get results from query
	$result = mysqli_query($conn, $query);
	if (!$result) {
		die("Query to show fields from table failed");
	}

	if(mysqli_num_rows($result) > 0){
        echo "<h1>Catalog</h1>";  
		echo "<table id='t01' border='1'>";
        echo "<thead>";
			echo "<tr>";
			echo "<th>Supplier</th>";
			echo "<th>Part</th>";
			echo "<th>Color</th>";
			echo "<th>Price</th>";
			echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
		
        while($row = mysqli_fetch_array($result)){
            echo "<tr>";
            echo "<td>" . $row['sname'] . "</td>";
			echo "<td>" . $row['pname'] . "</td>";
            echo "<td>" . $row['color'] . "</td>";
			echo "<td>" . $row['price'] . "</td>";
            echo "</tr>";
        }
        echo "</tbody>";                            
        echo "</table>";
		// Free result set
        mysqli_free_result($result);
    } else{
		echo "<p class='lead'><em>No records were found.</em></p>";
    } 
	mysqli_close($conn);
?>
</body>

</html>

	
