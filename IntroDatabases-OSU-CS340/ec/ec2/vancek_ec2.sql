ALTER TABLE Boats
ADD price int(11);

UPDATE Boats
SET price = 1000
WHERE bid = 101;

UPDATE Boats
SET price = 500
WHERE bid = 102;

UPDATE Boats
SET price = 700
WHERE bid = 103;

UPDATE Boats
SET price = 200
WHERE bid = 104;

ALTER TABLE Reserves
ADD FOREIGN KEY (sid) REFERENCES Sailors(sid),
ADD FOREIGN KEY (bid) REFERENCES Boats(bid);

CREATE TABLE IF NOT EXISTS SailorStats (
	sid int(11) NOT NULL, FOREIGN KEY (sid) REFERENCES Sailors(sid),
	totalReserves int(2) NOT NULL DEFAULT 0,
	amountDue int(11) NOT NULL DEFAULT 0,
	PRIMARY KEY (sid)
);

DELIMITER //

CREATE OR REPLACE PROCEDURE updateAllSailorStats ()
BEGIN
	INSERT INTO SailorStats (sid, totalReserves, amountDue)
	SELECT s.sid, COUNT(r.sid), SUM(b.price)
	FROM Sailors s
	LEFT JOIN Reserves r ON r.sid = s.sid
	LEFT JOIN Boats b ON b.bid = r.bid
	GROUP BY s.sid
	ORDER BY s.sid ASC;
END;

//

DELIMITER ;

CREATE OR REPLACE TRIGGER decr_SailorStats
	AFTER DELETE ON Reserves FOR EACH ROW
		UPDATE SailorStats
		SET totalReserves = totalReserves - 1, amountDue = amountDue - (SELECT price FROM Boats WHERE bid = OLD.bid)
		WHERE sid = OLD.sid;

CREATE OR REPLACE TRIGGER incr_SailorStats
	AFTER INSERT ON Reserves FOR EACH ROW
		UPDATE SailorStats
		SET totalReserves = totalReserves + 1, amountDue = amountDue + (SELECT price FROM Boats WHERE bid = NEW.bid)
		WHERE sid = NEW.sid;

CREATE OR REPLACE TRIGGER update_SailorStats
	AFTER UPDATE ON Reserves FOR EACH ROW
		UPDATE SailorStats
		SET amountDue = amountDue + ((SELECT price FROM Boats WHERE bid = NEW.bid) - (SELECT price FROM Boats WHERE bid = OLD.bid))
		WHERE sid = NEW.sid;

DELIMITER //

CREATE OR REPLACE TRIGGER MaxReserves
	BEFORE INSERT ON Reserves FOR EACH ROW
		IF (SELECT totalReserves FROM SailorStats WHERE sid = NEW.sid) >= 4 THEN
			SIGNAL SQLSTATE '02000' SET
			MYSQL_ERRNO = 1645,
			MESSAGE_TEXT = 'At most 4 boats can be reserved';
		END IF;

//

CREATE OR REPLACE FUNCTION sailorRank (s int(11)) RETURNS varchar(20) 
BEGIN
	DECLARE rat int(11) DEFAULT 0;
	SELECT rating INTO rat FROM Sailors WHERE sid = s;
	CASE
		WHEN rat >= 8 THEN RETURN 'Expert';
		WHEN rat <= 4 THEN RETURN 'Novice';
		ELSE RETURN 'Intermediate';
	END CASE;
END

//

DELIMITER ;
