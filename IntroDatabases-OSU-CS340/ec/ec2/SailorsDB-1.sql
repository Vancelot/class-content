-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: classmysql.engr.oregonstate.edu:3306
-- Generation Time: Apr 25, 2018 at 01:30 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.0.29

DROP TABLE IF EXISTS Boats;
DROP TABLE IF EXISTS Sailors;
DROP TABLE IF EXISTS Reserves;
DROP TABLE IF EXISTS SailorStats;


-- --------------------------------------------------------

--
-- Table structure for table `Boats`
--

CREATE TABLE `Boats` (
  `bid` int(11) NOT NULL,
  `bname` varchar(20) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL
);

--
-- Dumping data for table `Boats`
--

INSERT INTO `Boats` (`bid`, `bname`, `color`) VALUES
(101, 'Interlake', 'pink'),
(102, 'Interlake', 'red'),
(103, 'Clipper', 'green'),
(104, 'Marine', 'red');

-- --------------------------------------------------------

--
-- Table structure for table `Reserves`
--

CREATE TABLE `Reserves` (
  `sid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `day` date NOT NULL
);

--
-- Dumping data for table `Reserves` b
--

INSERT INTO `Reserves` (`sid`, `bid`, `day`) VALUES
(21, 101, '2020-08-12'),
(21, 102, '2020-06-10'),
(21, 103, '2020-06-08'),
(21, 104, '2020-06-07'),
(31, 102, '2020-07-10'),
(31, 103, '2020-07-06'),
(31, 104, '2020-07-12'),
(64, 101, '2020-08-05'),
(64, 102, '2020-06-08'),
(74, 103, '2020-08-08');

-- --------------------------------------------------------

--
-- Table structure for table `Sailors`
--

CREATE TABLE `Sailors` (
  `sid` int(11) NOT NULL,
  `sname` varchar(20) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `age` decimal(3,1) DEFAULT NULL
);

--
-- Dumping data for table `Sailors`
--

INSERT INTO `Sailors` (`sid`, `sname`, `rating`, `age`) VALUES
(21, 'Dustin', 7, '45.0'),
(29, 'Brutus', 1, '33.0'),
(31, 'Lubber', 8, '55.5'),
(32, 'Andy', 8, '25.5'),
(58, 'Rusty', 10, '35.0'),
(64, 'Horatio', 7, '35.0'),
(74, 'Horatio', 9, '38.0'),
(99, 'Art', 3, '25.5');



--
-- Indexes for table `Boats`
--
ALTER TABLE `Boats`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `Reserves`
--
ALTER TABLE `Reserves`
  ADD PRIMARY KEY (`sid`,`bid`,`day`);

--
-- Indexes for table `Sailors`
--
ALTER TABLE `Sailors`
  ADD PRIMARY KEY (`sid`);

