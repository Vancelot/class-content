// Kerry Vance
// Project 1 // Sources:

#include<iostream>
#include<fstream>
#include<iomanip>
#include<cctype>

using namespace std;

const int SIZE = 111; 

int main()
{
		  char collName[SIZE], cardName[SIZE], cardEd[SIZE], cardType[SIZE], choice = ' ';
		  double cardPrice, totalVal;
		  int cardQuant;
		  ofstream outfile;

		  cout << "Welcome to the Magic card catalog program.\nWhat would you like to name your collection? ";
		  cin.getline(collName, SIZE);
		  outfile.open(collName);
		  do
		  {
					 cout << "What is the name of the card? ";
					 cin.getline(cardName, SIZE);
					 cout << "What is the price of this card? ";
					 cin >> cardPrice;
					 cin.ignore(SIZE, '\n');
					 while(cin.fail() || cardPrice < 0)
					 {
								cin.clear();
								cin.ignore(SIZE, '\n');
								cout << "Please enter a valid price: ";
								cin >> cardPrice;
								cin.ignore(SIZE, '\n');
					 }
					 cout << "What edition is the card in? ";
					 cin.getline(cardEd, SIZE);
					 cout << "What type is the card? ";
					 cin.getline(cardType, SIZE);
					 cout << "How many copies of the card do you have? ";
					 cin >> cardQuant;
					 cin.ignore(SIZE, '\n');
					 while(cin.fail() || cardQuant <= 0)
					 {
								cin.clear();
								cin.ignore(SIZE, '\n');
								cout << "Please enter a valid quantity: ";
								cin >> cardQuant;
								cin.ignore(SIZE, '\n');
					 }
					 totalVal += cardPrice * cardQuant;
					 outfile << showpoint << setprecision(2) << fixed << cardName << ',' << cardPrice << ',' << cardEd << ',' << cardType << ',' << cardQuant << endl;
					 cout << "The card information has been recorded to " << collName << endl << "Would you like to enter another card? (y/n) ";
					 cin >> choice;
					 choice = toupper(choice);
					 cin.ignore(SIZE, '\n');
					 while(cin.fail() || (choice != 'Y' && choice != 'N'))
					 {
								cout << "choice: " << choice << endl;
								cout << "Please enter a valid choice (y/n) ";
								cin >> choice;
								choice = toupper(choice);
								cin.ignore(SIZE, '\n');
					 }
		  } while (choice == 'Y');
		  cout << setfill('-') << setw(9) << "-\n";
		  cout << showpoint << setprecision(2) << fixed << "The total value of your cards is: $" << totalVal << endl << "End of Program\n";
		  outfile.close();

		  return 0;
}
