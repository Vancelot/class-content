// Kerry Vance
// CS 162
// Sources: 

#ifndef __HEADER__
#define __HEADER__

#include<iostream>
#include<fstream>
#include<iomanip>
#include<cstring>

using namespace std;

const int SIZE = 100;

const int ERROR_INT = -999;
const char ERROR_CSTRING[SIZE] = "ERRORERROR";
const double ERROR_DOUBLE = -999.99;

class cardType
{
	char *name;
	double price;
	char *set;
	char *type;
	int cardCount;

	public:
	cardType(char[], double, char[], char[], int); // constructor w/args
	const char *getName();
	const double getPrice();
	const char *getSet();
	const char *getType();
	const int getCount();
	~cardType();
};

class cardColl
{
	cardType *cards[SIZE];
	int count;
	char fileName[SIZE];

	public:
	cardColl();
	int loadCards();
	void searchCard() const;
	void printCardType() const;
	void addCard();
	void printOneCard(int) const;
	void writeCards();
	~cardColl();
};

#endif
