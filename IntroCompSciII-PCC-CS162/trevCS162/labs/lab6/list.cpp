//put your implementation of LinkedList class here
#include "list.h"

LinkedList::LinkedList()
{
    head = NULL;
    tail = NULL;
}

bool LinkedList::addAtBeginning(int val)
{
    if(val)
    {
        Node * newNode = NULL, *curr = NULL;
    
        newNode = new Node;
        newNode->data = val;
        newNode->next = NULL;
        newNode->prev = NULL;

        if(!head)
            {
                head = newNode;
                tail = newNode;
            }
        else 
            {
                head->prev = newNode;
                newNode->next = head;
                head = newNode;
            }
        return true;
        delete curr;
    }
    return false;
}
bool LinkedList::remove(int val)
{
    Node *curr=head;
    if(!head)
    {
        return false;
    }
    else
    {
     int count = 1;
     int removal = 0;
     removal=val;
     
     while(curr && curr->data != removal)
     {
         curr = curr->next;
         count++;
     }
     if(curr == head)
     {
         head = curr->next;
         head->prev = NULL;
     }
     else if(curr == tail)
     {
         curr->prev->next= curr->next;
         tail = curr ->prev;
     }
     else
     {
         curr->prev->next = curr->next;
         curr->next->prev = curr->prev;
     }
     delete curr;
     curr = NULL;
    }
}
           
void LinkedList::printForward() const
{
    
    Node *curr;
    for(curr = head; curr; curr=curr->next)
    {
        cout << "Printed Forward: "<< curr->data << endl;
    }
}

void LinkedList::printBackward() const
{
    Node *curr; 
    for(curr = tail; curr; curr = curr->prev)
    {
        cout << "Printed Backward: "<< curr->data << endl;
    }
}

