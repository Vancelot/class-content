#ifndef MAJOR_H
#define MAJOR_H
//Class object for major has majorName and majorPay
#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

//constants
const int MAXCHAR = 101;
//data type for Major
class Major
{
    private:
        char majorName[MAXCHAR];
		int majorPay;
    public:
        //constructors
        //default constructor
        Major();
        //constructor with parameters
        Major(char [], int);
        //destructor
        ~Major();
        //mutator functions
        void setName(char []);
        void setPay(int);
        //accessor functions
        void getName(char []) const;
        int getPay() const;
        //print major 
        void printMajor();
};

#endif
