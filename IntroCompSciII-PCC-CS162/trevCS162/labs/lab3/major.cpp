//This is the implementation file for major.h (the Major class)
#include "major.h"

//default constructor
Major::Major()
{
    strcpy(majorName, "No majorName");
    majorPay = 0;
}

//constructor with parameters
Major::Major(char tempName[], int tempPay)
{
    strcpy(majorName, tempName);
    majorPay = tempPay;
}

//destructor
Major::~Major()
{
//nothing to do now
}

//mutator functions
void Major::setName(char newName[])
{
    strcpy(majorName, newName);
}

void Major::setPay(int newPay)
{
    majorPay = newPay;
}

//accessor functions
void Major::getName(char returnName[]) const
{
    strcpy(returnName, majorName);
}

int Major::getPay() const
{
    return majorPay;
}


//prints to console
void Major::printMajor()
{
    cout << majorName << ';' << majorPay << endl;
}
