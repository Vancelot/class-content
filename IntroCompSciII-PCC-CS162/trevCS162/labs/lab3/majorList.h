#ifndef MAJORLIST_H
#define MAJORLIST_H
//class object for a list of majors
#include "major.h"

//constants
const int CAP = 100;

//define class MajorList for array of majors and size

class MajorList
{
    private:
        Major list[CAP];
        int size;
    public:
        //constructors
        MajorList();
        MajorList(char []);
        //destructor
        ~MajorList();
        //database functions
		bool isLessThan(char name1[], char name2[]);
        void addMajor(Major);
        void showList();
};

#endif

