#include "major.h"
#include "majorList.h"

int main()
{
    //create a list of videos.
    char fileName[MAXCHAR] = "major.txt";
    MajorList myList(fileName);
    myList.showList();

    return 0;
}
