//This is the implementation file for MajorList class
#include "majorList.h"

//constructors
MajorList::MajorList()
{
    size = 0;
}

//implement your constructor from file here.
MajorList::MajorList(char fileName[])
{
    size = 0;
    ifstream inFile;
    Major aMajor;
    char tempMajor[MAXCHAR];
    int tempPay;
    
    inFile.open(fileName);
    if(!inFile)
    {
        cout << "Cannot Open File. Exiting Program." << endl;
        exit (0);
    }

    inFile.getline(tempMajor, MAXCHAR, ';');
    while (!inFile.eof())
    {
        inFile >> tempPay;
        inFile.ignore(5, '\n');
        aMajor.setName(tempMajor);
        aMajor.setPay(tempPay);
        addMajor(aMajor);
        inFile.getline(tempMajor, MAXCHAR, ';');
    }
    inFile.close();

}  

//destructor
MajorList::~MajorList()
{
//nothing to do
}

//isLessThan function to be implemented here
bool MajorList::isLessThan(char name1[], char name2[])
{
    
    if(strcmp(name1, name2) < 0)
    {
        return true;
    }
    else
    {
        return false;
    }

}

//Add a major to the list
void MajorList::addMajor(Major aMajor)
{
	//implement your insert sorted function here.
    char name1[MAXCHAR];
    char name2[MAXCHAR];
    int i=0;

    if (size == CAP)
    {
        cout << "No Space Available";
    }
    else if(!size)
    {
        list[size++]=aMajor;    
    }   
    else
    {
        for (i=0; i < size; i++)
        {
            aMajor.getName(name1);
            list[i].getName(name2);
           
            if(!(isLessThan(name2, name1)))
            {
                break;
            }
        }
        
        for (int j = size; j > i; j--)
            {
                list[j] =  list[j-1];
            } 
        list[i]=aMajor;
            
        size++;
    }
}

//displays the list
void MajorList::showList()
{
    for(int i = 0; i < size; i++)
    {
        cout << i+1 << ") ";
        list[i].printMajor();
    }
    cout << endl;
}



