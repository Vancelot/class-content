//Trevor Liberty
//When executed, junk value is created for first removal-it is trying to remove the value at position (5), and the 5th position is (4) because arrays start at 0. That is the only known bug I can see
#include "list.h"
#include <iostream>
using namespace std;

bool insert(int position, int val, int intList[], int& size)
{
        
	if (position == size)
    {
        intList[position]=val;
    }
    else if (position >= 0 && position < size) 
    {
        for (int i = size; position < i; i--)
        {   
            intList[i]=intList[i-1];
        }
        intList[position] = val;
    }
    size++;
    return true;
}

bool remove(int position, int& val, int intList[], int& size)
{
    val = intList[position];//    intList[position]=val;
	if (position >= size)
    {
        return false;
    }
    else if (position > 0 && position < size)
    {
    
        for (int i = position; i < size; i++)
        {
            intList[i]=intList[i+1];
        }
    }
    //val = intList[position];
    size--;
    
     return true;
       
}

void print(const int intList[], int size)
{
	cout << endl << "[ ";
	for(int i=0; i<size; i++)
	{
		cout << intList[i] << " ";
	}
	cout << "]" << endl;
}



