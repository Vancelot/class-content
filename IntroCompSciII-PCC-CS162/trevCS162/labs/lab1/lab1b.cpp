/*This is the starter file for lab1b.
CRN 21797, Spring 2018
Please implement functions per the given prototypes. 
This program reads from a file called women-stem.txt.  There is no need to store the data.
As you read the data, you can implement some of the things in each function.  */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>

using namespace std;
//global constants
const int MAXCHAR = 101;

//Function Prototypes
void openFile(ifstream &inFile);
void analyzeData(ifstream &inFile, int &total, int &men, int &women);
void popularMajor(ifstream &inFile); //this does not compile. I will email ahead of time for the next time this happens!


int main()
{
	ifstream inFile;
	int total = 0, men = 0, women = 0;
	openFile(inFile);
	analyzeData(inFile, total, men, women);
	cout << "The total enrolled: " << total << endl;
	cout << "The total men enrolled: " << men << endl;
	cout << "The total women enrolled: " << women << endl;
	popularMajor(inFile);
	
	return 0;
}

//put your function implementations here.
void openFile(ifstream &inFile)
{
	inFile.open("women-stem.txt");
	if(!inFile)
	{
		cout << "File not found! Program exiting." << endl;
		exit (0);
	}
	
}
void analyzeData(ifstream &inFile, int &total, int &men, int &women)
{
	int tempTotal, tempMen, tempWomen= 0;
	
	char c = inFile.peek();

	while(!inFile.eof() && c!='\n')
	{	
		
		inFile.ignore(256, ';'); //ignores 1st column
		inFile.ignore(256, ';'); //ignores 2nd column
		inFile.ignore(256, ';'); //ignores 3rd column; 
		inFile >> tempTotal;
		total+=tempTotal;
		inFile.ignore(1, ';');
		inFile >> tempMen;
		men+=tempMen;
		inFile.ignore(1, ';');
		inFile >> tempWomen;
		women+=tempWomen;
		
	}
		c = inFile.peek();

	inFile.close();	
}
void popularMajor(ifstream &inFile)
{
	char category[MAXCHAR], tempMajor[MAXCHAR];
	int BIO, COMPSCI, ENG, HEALTH, PE =0;
	while (!inFile.eof())
	{
		inFile.ignore(256, ';'); //ignores 1st column
		inFile.ignore(256, ';'); //ignores 2nd column
		inFile.get(category, MAXCHAR, ';');
		if (strcmp(category, "Biology & Life Science")==0)
		{
			BIO++
		}
		
		else if (strcmp(category, "Computers & Mathematics")==0)
		{
			COMPSCI++;
		}
		else if(strcmp(category,"ENG")==0)
		{
			ENG++;
		}
		else if (strcmp(category,"Health")==0)
		{
			HEALTH++;
		}
		else if (strcmp(category, "Physical Sciences")==0)
		{
			PE++;
		}
		
		inFile.ignore(5, '\n');
		cout << category << ';' << tempMajor << endl;
	}
}


	
