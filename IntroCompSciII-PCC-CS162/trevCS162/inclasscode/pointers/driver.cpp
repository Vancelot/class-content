//This is the driver file for my video class
//We will create pointers to objects
#include "video.h"
using namespace std;

int main()
{
	Video aVideo;
	//create a pointer to a video object
	Video *aVideoptr = NULL;
	//allocate memory for a new Video object
	//aVideo = new Video;
	char tempTitle[MAXCHAR], letter;
	int tempQty;
	double tempRating;
	//Populate a Video from the user
	cout << "Please enter a title:";
	cin.get(tempTitle, MAXCHAR);
	cout << "Please enter a genre (A-ACTION, F-FAMILY, C-COMEDY, H-HORROR, I-ILLEGAL)";
	cin >> letter;
	cout << "Please enter a quantity:";
	cin >> tempQty;
	cout << "Please enter a rating:";
	cin >> tempRating;
	//call mutator functions and set the values
	aVideo.setTitle(tempTitle);
	aVideo.setGenre(readGenre(letter));
	aVideo.setQty(tempQty);
	aVideo.setRating(tempRating);
	//output the values
	cout << "You entered:" << endl;
	aVideo.printVideo();

	//delete aVideo;

	//point the videoptr to aVideo
	aVideoptr = &aVideo;
	//print the aVideo contents through the pointer
	aVideoptr->printVideo();
	//change aVideo's contents through the ptr
	aVideoptr->setTitle("Happy Feet");
	(*aVideoptr).printVideo();
	return 0;
}
