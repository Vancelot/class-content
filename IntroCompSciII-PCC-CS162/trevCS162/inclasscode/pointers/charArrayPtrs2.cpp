//This program demos char arrays
#include <iostream>
#include <string.h>
using namespace std;

//constants
const int MAXCHAR = 101;
int main()
{
	//char title[MAXCHAR];
	char *titlePtr = NULL, *tempPtr = NULL;
	titlePtr = new char[MAXCHAR];
	cout << "Please enter a movie title:";
	cin.get(titlePtr, MAXCHAR);
	cin.ignore(100, '\n');
	cout << "Your title is " << titlePtr << endl;
	//to print the title in reverse using the pointer
	for(tempPtr = titlePtr + strlen(titlePtr); tempPtr >= titlePtr; tempPtr--)
	{
		cout << *tempPtr;
	}
	cout << endl;
	delete [] titlePtr;
	titlePtr = NULL;
	tempPtr = NULL;
	
	return 0;
}
