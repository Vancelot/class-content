//This program demos char arrays
#include <iostream>
#include <string.h>
using namespace std;

//constants
const int MAXCHAR = 101;
int main()
{
	char title[MAXCHAR];
	char *titlePtr = NULL;
	cout << "Please enter a movie title:";
	cin.get(title, MAXCHAR);
	cin.ignore(100, '\n');
	cout << "Your title is " << title << endl;
	//to print the title in reverse using the pointer
	for(titlePtr = title + strlen(title); titlePtr >= title; titlePtr--)
	{
		cout << *titlePtr;
	}
	cout << endl;
	
	return 0;
}
