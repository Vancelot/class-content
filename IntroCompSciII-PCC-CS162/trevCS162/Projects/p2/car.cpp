#include "car.h"
#include "tools.h"

void openFile(char fileName[], ifstream &inFile)
{
    inFile.open(fileName);
    if(!inFile)
    {
        cout << "Input file did not open. Program exiting." << endl;
        exit(0);
    }
}
//opens the output file
void openFile(char fileName[], ofstream &outFile)
{
    outFile.open(fileName);
    if(!outFile)
    {
        cout << "Output file did not open. Program exiting." << endl;
        exit (0);
    }
}

void searchByOrigin(CarType carList[], int size)
{
    char letter;
    Origin srchOrigin;
    char originDesc[MAXCHAR];
    cout << "Enter the origin to search for (U-US, E-EUROPE, J-JAPAN): ";
    cin >> letter;
    srchOrigin = readOrigin(letter);
    for(int i = 0; i < size; i++)
    {
        if(srchOrigin == carList[i].origin)
        {
            printOrigin(carList[i].origin, originDesc);
            cout << carList[i].name << ";" << carList[i].mpg << ";" << carList[i].cylinders << ";" << carList[i].displacement << ";" <<  carList[i].hp << ";" << carList[i].weight << ";" << carList[i].accel << ";" << carList[i].model << ";" << originDesc << endl;
        }
    }
}

void searchByName(CarType carList[], int size)
{
    char searchName[MAXCHAR];
    char originDesc[MAXCHAR];
    cout << "Please enter car name" << endl;
    cin.get(searchName, MAXCHAR, '\n');

    for (int i = 0; i < size; i++)
    {
        convertCase(searchName);
        convertCase(carList[i].name);
        if(strstr(carList[i].name, searchName)!=NULL)
        {
            printOrigin(carList[i].origin, originDesc);
            cout << carList[i].name << ";" << carList[i].mpg << ";" << carList[i].cylinders << ";" << carList[i].displacement << ";" << carList[i].hp << ";" << carList[i].weight << ";" << carList[i].accel << ";" << carList[i].model << ";" << originDesc << endl; 
        }
    }
}
void searchByModel(CarType carList[], int size)
{
    int modelNo;
    char originDesc[MAXCHAR];
    cout << "Please enter a valid Model Number" << endl;
    cin >> modelNo;
    for(int i = 0; i < size; i++)
    {
        if(modelNo == carList[i].model)
        {
            printOrigin(carList[i].origin, originDesc);
            cout << carList[i].name << ";" << carList[i].mpg << ";" << carList[i].cylinders << ";" << carList[i].displacement << ";" <<     carList[i].hp << ";" << carList[i].weight << ";" << carList[i].accel << ";" << carList[i].model << ";" << originDesc << endl;
        }
    }
}
void loadData(ifstream &inFile, CarType carList[], int &size)
{
    char tempOrigin[MAXCHAR];
    inFile.get(carList[size].name, MAXCHAR, ';');
    while(inFile)
    {
        inFile.ignore(5, ';');
        inFile >> carList[size].mpg;
        inFile.ignore(5, ';');
        inFile >> carList[size].cylinders;
        inFile.ignore(5, ';');
        inFile >> carList[size].displacement;
        inFile.ignore(5, ';');
        inFile >> carList[size].hp;
        inFile.ignore(5, ';');
        inFile >> carList[size].weight;
        inFile.ignore(5, ';');
        inFile >> carList[size].accel;
        inFile.ignore(5, ';');
        inFile >> carList[size].model;
        inFile.ignore(5, ';');
        inFile.get(tempOrigin, MAXCHAR);
        if (strncmp(tempOrigin, "US", 2) == 0)
        {
            carList[size].origin = US;
        }
        else if(strncmp(tempOrigin, "Europe", 6) == 0)
        {
            carList[size].origin = EUROPE;
        }
        else if(strncmp(tempOrigin, "Japan", 5) == 0)
        {
            carList[size].origin = JAPAN;
        }
        else
        {
            carList[size].origin = ILLEGAL;
        }
        inFile.ignore(100, '\n');
        size++;
        inFile.get(carList[size].name, MAXCHAR, ';');
    }
    inFile.close();
}
//overload function to add a car to the list

void addCar(CarType carList[], int &size, const CarType &aCar)
{
    carList[size++] = aCar;
}

//print list of cars

void showList(const CarType carList[], const int size)
{
    char originDesc[MAXCHAR];
    for (int i = 0; i < size; i++)
    {
        printOrigin(carList[i].origin, originDesc);
        cout << i+1 << "." <<  carList[i].name << ";" << carList[i].mpg << ";" << carList[i].cylinders << ";" << carList[i].displacement << ";" << carList[i].hp << ";" << carList[i].weight << ";" << carList[i].accel << ";" << carList[i].model << ";" << originDesc << endl;
    }
    cout << endl;
}
//remove car
void removeCar(CarType carList[], int &size)
{
    int delIndex = 0;
    cout << "Please enter index of video to delete:"; 
    cin >> delIndex;
    cin.ignore(100, '\n');
    if(delIndex < size)
    {
        for(int i = delIndex; i < size; i++)
        {
            carList[i-1] = carList[i];
        }
    }
    size--;
}

//assign an Origin based on letter and returns the Origin
Origin readOrigin(char letter)
{
    Origin tempOrigin;
    switch(tolower(letter))
    {
        case 'u':
            tempOrigin = static_cast<Origin>(0);
            break;
        case 'e':
            tempOrigin = static_cast<Origin>(1);
            break;
        case 'j':
            tempOrigin = static_cast<Origin>(2);
            break;
        default:
            tempOrigin = static_cast<Origin>(3);
    }
    return tempOrigin;
}

void printOrigin(Origin tempOrigin, char originDesc[])
{
    switch(tempOrigin)
    {
        case 0:
            strcpy(originDesc, "US");
            break;
        case 1:
            strcpy(originDesc, "Europe");
            break;
        case 2:
            strcpy(originDesc, "Japan");
            break;
        case 3:
            strcpy(originDesc, "ILLEGAL");
            break;
    }
    return;
}
void writeData(const CarType carList[], int size, ofstream &outFile)
{
    char originDesc[MAXCHAR];
    for (int i=0; i < size; i++)
    {
        printOrigin(carList[i].origin, originDesc);
        outFile << carList[i].name << ";" << carList[i].mpg << ";" << carList[i].cylinders << ";" << carList[i].displacement << ";" << carList[i].hp << ";" << carList[i].weight << ";" << carList[i].accel << ";" << carList[i].model << ";" << originDesc << endl;
    }
    outFile.close();
}

