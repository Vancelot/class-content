#ifndef TOOLS_H
#define TOOLS_H

#include "car.h"

//function prototypes
int readInt(char prompt[]);
double readDouble(char prompt[]);
void displayMenu();
char readOption();
void exeCmd(char option, CarType [], int &);
void addCar(CarType &);
void convertCase(char tempStr[]);

#endif
