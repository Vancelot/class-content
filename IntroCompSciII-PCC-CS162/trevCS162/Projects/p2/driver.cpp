//main
#include "car.h"
#include "tools.h"
int main()
{
    //create a variable for a single cacr
    CarType carList[CAP];
    char fileName[MAXCHAR]= "cars.txt";
    char option;
    ifstream inFile;
    ofstream outFile;
    int size = 0;
    openFile(fileName, inFile);
    loadData(inFile, carList, size);
    do
    {
        displayMenu();
        option = readOption();
        exeCmd(option, carList, size);
    }while(tolower(option != 'q'));
    openFile(fileName, outFile);
    writeData(carList, size, outFile);
    return 0;
}

