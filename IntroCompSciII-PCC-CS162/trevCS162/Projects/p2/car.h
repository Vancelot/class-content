#ifndef CAR_H
#define CAR_H
//Struct object for car has Car; MPG; Cylinders; Displacement; HP; Weight; Acceleration; Model; Origin
//Char array; double; int; double; double; double; double; int; cat (enum);
//
//
#include <iostream>
#include <cstring>
#include <fstream>

using namespace std; 

//constants
const int CAP = 1000;
const int MAXCHAR = 101;
//enum datatype for origin
enum Origin {US, EUROPE, JAPAN, ILLEGAL};
//data type for carType

struct CarType
{
    char name[MAXCHAR];
    double mpg;
    int cylinders;
    double displacement;
    double hp;
    double weight;
    double accel;
    int model;
    Origin origin;
};

//function prototypes
Origin readOrigin(char);
int findModel(int);
void printOrigin(Origin, char []);
void openFile(char [], ifstream &);
void openFile(char [], ofstream &);
void searchByOrigin( CarType [], int );
void searchByName( CarType [], int );
void searchByModel( CarType [], int );
void loadData(ifstream &, CarType [], int &);
void addCar(CarType [], int &, const CarType &);
void showList(const CarType [], const int);
void removeCar(CarType [], int &);
void writeData(const CarType [], int size, ofstream &);

#endif
