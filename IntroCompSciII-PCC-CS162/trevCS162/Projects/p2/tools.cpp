//implementation file for tools.h
#include "tools.h"
#include "car.h"

//Funct implementations

//displays the menu
//
void displayMenu()
{
    cout << "Welcome to the vehicle database:" << endl << endl;
    cout << "Pick an option" << endl;
    cout << "[A-a]: Add a car" << endl;
    cout << "[D-d]: Display the List" << endl;
    cout << "[O-o]: Find a car by origin" << endl;
    cout << "[N-n]: Find a car by its name" << endl;
    cout << "[M-m]: Find a car by its model number" << endl;
    cout << "[R-r]: Remove a car" << endl;
    cout << "[Q-q]: Quit" << endl;
}

//read option from the user
char readOption()
{
    char input;
    cin >> input;
    cin.ignore(100, '\n');
    return input;
}
//execute the option
void exeCmd(char option, CarType carList[], int &size)
{
    CarType aCar;
    switch (tolower(option))
    {
    case 'a':
        if(size==CAP)
        {
            cout << "Your list is full. Car not added.";
            return;
        }
        addCar(aCar);
        addCar(carList, size, aCar);
        break;
    case 'd':
        showList(carList, size);
        break;
    case 'r':
        showList(carList, size);
        removeCar(carList, size);
        break;
    case 'o':
        searchByOrigin(carList, size);
        break;
    case 'n':
        searchByName(carList, size);
        break;
    case 'm':
        searchByModel(carList, size);
        break;
    case 'q':
        break;
    default:
        cout << "Invalid. Please choose a valid choice." << endl;
    }
}

//Name: readDouble(reads &validates a double)
//input: none
//output: error message
//return: double
double readDouble(char prompt[])
{
    double temp = 0;
    cout << prompt;
    cin >> temp;
    //data validate
    while (!cin)
    {
        cin.clear();
        cin.ignore(100, '\n');
        cout << "Invalid reponse. Please enter a valid double. ";
        cin >> temp;
    }
    cin.ignore(100, '\n');

    return temp;
}
//Name: readInt(reads & validates an integer
//input: none
//output: error message
//return: int
int readInt(char prompt[])
{
    int temp2 = 0;
    cout << prompt;
    cin >> temp2;
    //data validate
    while (!cin)
    {
        cin.clear();
        cin.ignore(100, '\n');
        cout << "Invalid response. Please enter a valid integer. ";
        cin >> temp2;
    }
    cin.ignore(100, '\n');

    return temp2;
}

//standalone overloaded from addCar function
//populate aCar from the user
void addCar(CarType &aCar)
{
    char letter;
    cout << "Enter a car name: ";
    cin.get(aCar.name, MAXCHAR, '\n'); 
    cin.ignore(100, '\n');
    aCar.mpg = readDouble("Enter MPG: ");
    aCar.cylinders = readInt("Enter number of cylinders: ");
    aCar.displacement = readDouble ("Enter displacement: ");
    aCar.hp = readDouble ("Enter Horespower: ");
    aCar.weight = readDouble("Enter Weight: ");
    aCar.accel = readDouble ("Enter acceleration: ");
    aCar.model = readInt ("Enter model number: ");
    cout << "Enter car orign: (U-US, E-Europe, J-Japan): ";
    cin >> letter;
    cin.ignore(100, '\n');
    aCar.origin = readOrigin(letter);
}
         
//converts all text to upper case
void convertCase(char tempStr[])
{
    for (int i = 0; i < strlen(tempStr); i++)
    {
        tempStr[i] = toupper(tempStr[i]);
    }
}



