//Trevor Liberty Project 1 resubmission
//CS162
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>

using namespace std;
const int MAXCHAR = 101;

void openFile (ifstream &inFile);
void defineOrigin(ifstream &inFile, int &totalUS, int &totalEU, int &totalJAP);
void defineWeight(ifstream &inFile, int &w1, int &w2, int &w3, int &w4);
void totalCars(ifstream &inFile, int &carTotal);

int main()
{
	ifstream inFile;
	openFile(inFile);
	int totalUS, totalEU, totalJAP = 0;
    int w1, w2, w3, w4 = 0;
    int carTotal;
	defineOrigin(inFile, totalUS, totalEU, totalJAP);
 	totalCars(inFile, carTotal);
 	defineWeight(inFile, w1, w2, w3, w4);
	
		
	return 0;	
}

void openFile(ifstream &inFile)
{
	inFile.open("cars.txt");
	if (!inFile)
	{
		cout << "File not found. Program Exiting. " << endl;
		exit (0);
	}
	
}
void totalCars(ifstream &inFile, int &carTotal)
{
	string line;
	int totalTemp = 0;
	while (getline(inFile, line))
 	{
		++totalTemp;
	}
	cout << "Total amount of cars: " << totalTemp << endl;;
 	inFile.clear();
	inFile.seekg(0);	
}


void defineOrigin(ifstream &inFile, int &totalUS, int &totalEU, int &totalJAP)
{
	char tempOrigin[MAXCHAR], car[MAXCHAR]={0};
	int tempUS, tempEU, tempJAP = 0;
	double mpg, displ, hp, weight, accel = 0;
	int cylinders, model = 0;
	
	inFile.get(car, MAXCHAR, ';');
	while(!inFile.eof())
	{
		inFile.ignore(5, ';'); 
		inFile >> mpg;
		inFile.ignore(5, ';'); 
		inFile >> cylinders;
		inFile.ignore(5, ';'); 
		inFile >> displ;
		inFile.ignore(5, ';');
		inFile >> hp;
        inFile.ignore(5, ';');
        inFile >> weight;
        inFile.ignore(5, ';');
        inFile >> accel;
		inFile.ignore(5, ';');
		inFile >> model;
		inFile.ignore(5, ';'); 
		inFile.get(tempOrigin, MAXCHAR);
       if (strncmp(tempOrigin, "US", 2)==0)
		{
            tempUS+= 1;
        }
		else if (strncmp(tempOrigin, "Europe", 6)==0)
		{
			tempEU+=1;
		}
		else if (strncmp(tempOrigin, "Japan", 5) == 0) 
		{
			tempJAP+=1;
		}
		
		
		inFile.get(car, MAXCHAR, ';');
	}
		cout << "Total amount of US Cars: " << tempUS << endl;
		cout << "Total amount of European Cars: " << tempEU << endl;
		cout << "Total amount of Japanese Cars: " << tempJAP << endl;
		inFile.clear();
		inFile.seekg(0);
}
void defineWeight(ifstream &inFile, int &w1, int &w2, int &w3, int &w4)
{
	char tempOrigin[MAXCHAR], car[MAXCHAR]={0};
	double tempW1, tempW2, tempW3, tempW4 = 0;
	double mpg, displ, hp, weight, accel = 0;
	int cylinders, model = 0;
	inFile.seekg(0);
	inFile.get(car, MAXCHAR, ';');
	while(!inFile.eof())
	{
		inFile.ignore(5, ';'); 
		inFile >> mpg;
		inFile.ignore(5, ';'); 
		inFile >> cylinders;
		inFile.ignore(5, ';'); 
		inFile >> displ;
		inFile.ignore(5, ';');
		inFile >> hp;
        inFile.ignore(5, ';');
        inFile >> weight;
        if ((weight >= 1000.0) && (weight < 2000.0))
        {
        	tempW1++;
		}
		else if ((weight >= 2000.0) && (weight < 3000.0))
		{
			tempW2++;
		}
		else if ((weight >= 3000.0) && (weight < 4000.0))
		{
			tempW3++;
		}
		else if (weight >= 4000)
		{
			tempW4++;
		}
        inFile.ignore(5, ';');
        inFile >> accel;
		inFile.ignore(5, ';');
		inFile >> model;
		inFile.ignore(5, ';'); 
		inFile.get(tempOrigin, MAXCHAR);
		inFile.ignore(5, ';');
		inFile.get(car, MAXCHAR, ';');
	}
	cout << "Cars between 1000 and 2000 pounds: " << tempW1 << endl;
	cout << "Cars between 2000 and 3000 pounds: "<< tempW2 << endl;
	cout << "Cars between 3000 and 4000 pounds: "<< tempW3 << endl;
	cout << "Cars over 4000 pounds: " << tempW4 << endl;
	}


