#ifndef CAR_H
#define CAR_H

#include <iostream>
#include <cstring>
#include <fstream>

using namespace std;
//constants
const int MAXCHAR = 101;
enum Origin {US, EUROPE, JAPAN, ILLEGAL};
//data type for CarType
class CarType
{
    private: 
        char name[MAXCHAR];
        double mpg;
        int cylinders;
        double displacement;
        double hp;
        double weight;
        double accel;
        int model;
        Origin origin;
    public:
        //constructors
        //default constructor
        CarType();
        //constructor with parameters
        CarType(char [], double, int, double, double, double, double, int, Origin);
        //destructor
        ~CarType();
        //mutator functions
        void setName(char []);
        void setMpg(double);
        void setCylinders(int);
        void setDisplacement(double);
        void setHp(double);
        void setWeight(double);
        void setAccel(double);
        void setModel(int);
        void setOrigin(Origin);
        //accessor functions
        void getName(char []) const;
        double getMpg() const;
        int getCylinders() const;
        double getDisplacement() const;
        double getHp() const;
        double getWeight() const;
        double getAccel() const;
        int getModel() const;
        Origin getOrigin() const;
        //print CarType
        void printCarType();
        void printCarType(ofstream &);
};

Origin readOrigin(char);
void printOrigin(Origin, char []);

#endif
