//implementation file for tools.h
#include "tools.h"
#include "car.h"

//Funct implementations

//displays the menu
//
void displayMenu()
{
    cout << "Welcome to the vehicle database:" << endl << endl;
    cout << "Pick an option" << endl;
    cout << "[A-a]: Add a car" << endl;
    cout << "[D-d]: Display the List" << endl;
    cout << "[O-o]: Find a car by origin" << endl;
    cout << "[N-n]: Find a car by its name" << endl;
    cout << "[M-m]: Find a car by its model number" << endl;
    cout << "[R-r]: Remove a car" << endl;
    cout << "[Q-q]: Quit" << endl;
}

//read option from the user
char readOption()
{
    char input;
    cin >> input;
    cin.ignore(100, '\n');
    return input;
}
//execute the option
void exeCmd(char option, CarList &list)
{
    CarType aCar;
    switch (tolower(option))
    {
    case 'a':
        addCar(aCar);
        list.addCar(aCar);
        break;
    case 'd':
        list.showList();
        break;
    case 'r':
        list.showList();
        list.removeCarType();
        break;
    case 'o':
        list.searchByOrigin();
        break;
    case 'n':
        list.searchByName();
        break;
    case 'm':
        list.searchByModel();
        break;
    case 'q':
        break;
    default:
        cout << "Invalid. Please choose a valid choice." << endl;
    }
}

//Name: readDouble(reads &validates a double)
//input: none
//output: error message
//return: double
double readDouble(char prompt[])
{
    double temp = 0;
    cout << prompt;
    cin >> temp;
    //data validate
    while (!cin)
    {
        cin.clear();
        cin.ignore(100, '\n');
        cout << "Invalid reponse. Please enter a valid double. ";
        cin >> temp;
    }
    cin.ignore(100, '\n');

    return temp;
}
//Name: readInt(reads & validates an integer
//input: none
//output: error message
//return: int
int readInt(char prompt[])
{
    int temp2 = 0;
    cout << prompt;
    cin >> temp2;
    //data validate
    while (!cin)
    {
        cin.clear();
        cin.ignore(100, '\n');
        cout << "Invalid response. Please enter a valid integer. ";
        cin >> temp2;
    }
    cin.ignore(100, '\n');

    return temp2;
}

//standalone overloaded from addCar function
//populate aCar from the user
void addCar(CarType &aCar)
{
    char letter;
    char tempName [MAXCHAR];
    int tempCylinders, tempModel = 0;
    double tempMpg, tempDisplacement, tempHp, tempWeight, tempAccel;
    Origin tempOrigin;
    cout << "Enter a car name: ";
    cin.get(tempName, MAXCHAR, '\n'); 
    cin.ignore(100, '\n');
    tempMpg = readDouble("Enter MPG: ");
    tempCylinders = readInt("Enter number of cylinders: ");
    tempDisplacement = readDouble ("Enter displacement: ");
    tempHp = readDouble ("Enter Horespower: ");
    tempWeight = readDouble("Enter Weight: ");
    tempAccel = readDouble ("Enter acceleration: ");
    tempModel = readInt ("Enter model number: ");
    cout << "Enter car orign: (U-US, E-Europe, J-Japan): ";
    cin >> letter;
    cin.ignore(100, '\n');
    tempOrigin = readOrigin(letter);
    //populate aCar calling mutator functions in car.h
    aCar.setName(tempName);
    aCar.setMpg(tempMpg);
    aCar.setCylinders(tempCylinders);
    aCar.setDisplacement(tempDisplacement);
    aCar.setHp(tempHp);
    aCar.setWeight(tempWeight);
    aCar.setAccel(tempAccel);
    aCar.setModel(tempModel);
    aCar.setOrigin(tempOrigin);
}
         
//converts all text to upper case
void convertCase(char tempStr[])
{
    for (int i = 0; i < strlen(tempStr); i++)
    {
        tempStr[i] = toupper(tempStr[i]);
    }
}



