#ifndef CARLIST_H
#define CARLIST_H
//class object for list of cars
#include "car.h"


//constants
const int CAP = 1000;

//define class CarList for array of cars and size

class CarList
{
    private:
        CarType list[CAP];
        int size;
    public:
        //constructors
        CarList();
        CarList(char []);
        //destructor
        ~CarList();
        //database functions
        void addCar(CarType);
        void searchByOrigin();
        void searchByName();
        void searchByModel();
        void showList();
        void removeCarType();
        void writeData(char []);
};

#endif
