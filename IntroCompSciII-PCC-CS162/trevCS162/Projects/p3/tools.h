#ifndef TOOLS_H
#define TOOLS_H

#include "car.h"
#include "CarList.h"

//function prototypes
void displayMenu();
int readInt(char prompt[]);
double readDouble(char prompt[]);
char readOption();
void exeCmd(char option, CarList &);
void addCar(CarType &);
void convertCase(char tempStr[]);

#endif
