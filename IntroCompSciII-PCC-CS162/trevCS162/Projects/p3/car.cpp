//implementation for car.h
#include "car.h"

//default constructor
CarType::CarType()
{
    strcpy(name, "No Name");
    mpg = 0;
    cylinders = 0;
    displacement = 0;
    hp = 0;
    weight = 0;
    accel = 0;
    model = 0;
    origin = ILLEGAL;
}

//constructor with parameters
CarType::CarType(char tempName[], double tempMpg, int tempCylinders, double tempDisplacement, double tempHp, double tempWeight, double tempAccel, int tempModel, Origin tempOrigin)
{
    strcpy(name, tempName);
    mpg = tempMpg;
    cylinders = tempCylinders;
    displacement = tempDisplacement;
    hp = tempHp;
    weight = tempWeight;
    accel = tempAccel;
    model = tempModel;
    origin = tempOrigin;
}
//destructor
CarType::~CarType()
{
    //-
}
//mutator functions
void CarType::setName(char newName[])
{
    strcpy(name, newName);
}

void CarType::setMpg(double newMpg)
{
    mpg = newMpg;
}

void CarType::setCylinders(int newCylinders)
{
    cylinders = newCylinders;
}

void CarType::setDisplacement(double newDisplacement)
{
    displacement = newDisplacement;
}

void CarType::setHp(double newHp)
{
    hp = newHp;
}

void CarType::setWeight(double newWeight)
{
    weight = newWeight;
}

void CarType::setAccel(double newAccel)
{
    accel = newAccel;
}

void CarType::setModel(int newModel)
{
    model = newModel;
}

void CarType::setOrigin(Origin newOrigin)
{
    origin = newOrigin;
}

//accessor
void CarType::getName(char returnName[]) const
{
    strcpy(returnName, name);
}

double CarType::getMpg() const
{
    return mpg;
}

int CarType::getCylinders() const
{
    return cylinders;
}

double CarType::getDisplacement() const
{
    return displacement;
}

double CarType::getHp() const
{
    return hp;
}

double CarType::getAccel() const
{
    return accel;
}

int CarType::getModel() const
{
    return model;
}


Origin CarType::getOrigin() const
{
    return origin;
}

//print to console
void CarType::printCarType()
{
    char originDesc[MAXCHAR];
    printOrigin(this->origin, originDesc);
    cout << name << ';' << mpg << ';' << cylinders << ';' << displacement << ';' << hp << ';' << weight << ';' << accel << ';' << model << ';' << originDesc << endl;
}

void CarType::printCarType(ofstream &outFile)
{
    char originDesc[MAXCHAR];
    printOrigin(this->origin, originDesc);
    outFile << name << ';' << mpg << ';' << cylinders << ';'  << displacement << ';' << hp << ';' << weight << ';' << accel << ';' << model << ';' << originDesc << endl;
}

//This function assigns an Origin based on letter and returns the origin

Origin readOrigin(char letter)
{
    Origin tempOrigin;
    switch(tolower(letter))
    {
        case 'u':
            tempOrigin = static_cast<Origin>(0);
            break;
        case 'e':
            tempOrigin = static_cast<Origin>(1);
            break;
        case 'j':
            tempOrigin = static_cast<Origin>(2);
            break;
        default: 
            tempOrigin = static_cast<Origin>(3);
    }
    return tempOrigin;
}

void printOrigin(Origin tempOrigin, char originDesc[])
{
    switch(tempOrigin)
    {
        case 0: 
            strcpy(originDesc, "US");
            break;
        case 1:
            strcpy(originDesc, "Europe");
            break;
        case 2:
            strcpy(originDesc, "Japan");
            break;
        case 3:
            strcpy(originDesc, "ILLEGAL");
            break;
    }
    return;
}
