//implementation for CarList class
#include "CarList.h"
#include "tools.h"
//#include <limits>
//constructors
CarList::CarList()
{
    head = NULL;
    tail = NULL;
    size = 0; 
}

//constructor from file
/*CarList::CarList(char fileName[])
{
    head = NULL;
    tail = NULL;
    size = 0;
    ifstream inFile;
    CarType aCar;
    char tempName[MAXCHAR], originDesc[MAXCHAR];
    int tempCylinders, tempModel;
    double tempMpg, tempDisplacement, tempHp, tempWeight, tempAccel;
    Origin tempOrigin;

    inFile.open(fileName);
    if(!inFile)
    {
        cout << "Cannot open File. Exiting Program" << endl;
        exit (0);
    }
    
    inFile.get(tempName, MAXCHAR, ';');
    while(!inFile.eof())
    {
        inFile.ignore(5, ';');
        inFile >> tempMpg;
        inFile.ignore(5, ';');
        inFile >> tempCylinders;
        inFile.ignore(5, ';');
        inFile >> tempDisplacement;
        inFile.ignore(5, ';');
        inFile >> tempHp;
        inFile.ignore(5, ';');
        inFile >> tempWeight;
        inFile.ignore(5, ';');
        inFile >> tempAccel;
        inFile.ignore(5, ';');
        inFile >> tempModel;
        inFile.ignore(5, ';');
        inFile.get(originDesc, MAXCHAR);
        tempOrigin = readOrigin(originDesc[0]);
        inFile.ignore(100, '\n');
        //populate aCar
        aCar.setName(tempName);
        aCar.setMpg(tempMpg);
        aCar.setCylinders(tempCylinders);
        aCar.setDisplacement(tempDisplacement);
        aCar.setHp(tempHp);
        aCar.setWeight(tempWeight);
        aCar.setAccel(tempAccel);
        aCar.setModel(tempModel);
        aCar.setOrigin(tempOrigin);
        addCar(aCar);
        inFile.get(tempName, MAXCHAR, ';');
    }
    inFile.close();
}*/

//destructor
CarList::~CarList()
{
    //delete list
    Node *curr = head;
    while(curr)
    {
        head = curr->next;
        delete curr;
        curr = head;
    }
    tail = NULL;
}
//returns true/false for name sorting
bool CarList::isLessThan(char name1[], char name2[])
{
    if(strcmp(name1, name2) < 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
//Add a car to the list insert sorted
void CarList::addCar(CarType &aCar)
{
    Node *newNode = NULL, *curr = NULL;
    char name1[MAXCHAR], name2[MAXCHAR], tempName[MAXCHAR];
    double tempMpg;
    int tempCylinders;
    double tempDisplacement;
    double tempHp;
    double tempWeight;
    double tempAccel;
    int tempModel;
    Origin tempOrigin;
    //populate newNode
    newNode = new Node;
    newNode->data = aCar;
    newNode->prev = NULL;
    newNode->next = NULL;
    if(!head)
    {
        head = newNode;
        tail = newNode;
    }
    else
    {
        curr = head;
        curr->data.getName(name1);
        newNode->data.getName(name2);
        while(curr && isLessThan(name1, name2))
        {
            curr = curr->next;
            if(curr)
            {
                curr->data.getName(name1);
            }
        }
        //insert at end of list
        if(!curr)
        {
            tail->next = newNode;
            newNode->prev = tail;
            tail = newNode;
        }
        //insert in between
        else if(curr->prev)
        {
            newNode->next = curr;
            curr->prev->next = newNode;
            newNode->prev = curr->prev;
            curr->prev = newNode;
        }
        //insert at beginning
        else
        {
            newNode->next = curr;
            head->prev = newNode;
            head = newNode;
        }
    }
    size++;
}

//return the size of the list
int CarList::getSize()
{
    return size;
}
//search by Origin
void CarList::searchByOrigin()
{
    char letter;
    Origin srchOrigin;
    Origin tempOrigin;
    bool flag = false;
    cout << "Please enter the origin to search for (U-US, E-Europe, J-Japan): ";
    cin >> letter;
    srchOrigin = readOrigin(letter);
    for(Node *curr = head; curr; curr = curr->next)
    {
        tempOrigin = curr->data.getOrigin();
        if (srchOrigin == tempOrigin)
        {
            curr->data.printCarType();
            flag = true;
        }
    }
    if(!flag)
    {
        cout << "No cars of that origin were found." << endl;
    }
}
void CarList::searchByName()
{
    char searchName[MAXCHAR];
    char tempName[MAXCHAR];
    bool flag = false;
    cout << "Please enter name of car: ";
    cin.get(searchName, MAXCHAR);
    convertCase(searchName);

    for(Node *curr = head; curr; curr = curr->next)
    {
        curr->data.getName(tempName);
        convertCase(tempName);
        if(strstr(tempName, searchName)!=NULL)
        {
            curr->data.printCarType();
            flag = true;
        }
    }
    if(!flag)
    {
        cout << "No cars with that name were found." << endl;
    }
}
void CarList::searchByModel()
{
    int searchModel = 0;
    int tempModel;
    bool flag = false;
    cout << "Please enter a valid Model Number: " << endl;
    while (!(cin >> searchModel))
    {
        cin.clear();
        //cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "invalid, enter int: " <<endl;
    }
    cin >> searchModel;
    for (Node *curr = head; curr; curr = curr->next)
    {
        tempModel = curr->data.getModel();
        if (searchModel == tempModel)
        {
            curr ->data.printCarType();
            flag = true;
        }
    }
    if(!flag)
    {
        cout << "No cars with that model number were found." << endl;
    }
}

void CarList::writeData(char fileName[]) 
{
    ofstream outFile;
    outFile.open(fileName);
    Node *curr;
    for (curr = head; curr; curr = curr->next)
    {
        curr->data.printCarType(outFile);
    }
}
void CarList::showList()
{
    int count = 1;
    Node *curr;
    cout << "Forward Printing \n";
    for(curr = head; curr; curr = curr->next)
    {
        cout << count << ".";
        curr->data.printCarType();
        count++;
    }
    cout << "Backward Printing \n";
    count = size;
    for(curr = tail; curr; curr = curr->prev)
    {
        cout << count << ".";
        curr->data.printCarType();
        count--;
    }
    cout << endl;
}

void CarList::removeCarType()
{
    int count = 1;
    int remove = 0;
    if(!size)
    {
        cout << "Nothing to see here.." << endl;
        return;
    }
    remove = readInt("Select number for removal of car: ");
    Node *curr = head;
    while(remove < 1 || remove > size)
    {
        remove = readInt("Invalid index, try again: ");
    }
    while(curr && count < remove)
    {
        curr = curr->next;
        count++;
    }
    //if curr is head
    if(curr == head)
    {
        head = curr->next;
        head->prev = NULL;
    }
    //if curr is tail
    else if(curr == tail)
    {
        curr->prev->next = curr->next;
        tail = curr->prev;
    }
    //remove from middle
    else
    {
        curr->prev->next = curr->next;
        curr->next->prev = curr->prev;
    }
    delete curr;
    curr = NULL;
    size--;
}
