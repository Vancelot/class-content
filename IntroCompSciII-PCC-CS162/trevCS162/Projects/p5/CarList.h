#ifndef CARLIST_H
#define CARLIST_H
//class object for list of cars
#include "car.h"



class CarList
{
    private:
        struct Node
        {
            CarType data;
            Node *next = NULL;
            Node *prev = NULL;
        };
        Node *head;
        Node *tail;
        int size;
    public:
        //constructors
        CarList();
        CarList(char []);
        //destructor
        ~CarList();
        //database functions
        bool isLessThan(char name1[], char name2[]);
        void addCar(CarType &);
        void searchByOrigin();
        void searchByName();
        void searchByModel();
        void showList();
        int getSize();
        void removeCarType();
        void writeData(char []);
};

#endif
