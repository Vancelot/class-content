#ifndef CARLIST_H
#define CARLIST_H
//class object for list of cars
#include "car.h"


//constants
const int CAP = 1000;
const int GROWTH = 10;
//define class CarList for array of cars and size

class CarList
{
    private:
        CarType *list;
        int size;
        int capacity;
        //growlist is a private function
        void growList();
    public:
        //constructors
        CarList();
        CarList(char []);
        //destructor
        ~CarList();
        //database functions
        bool isLessThan(char name1[], char name2[]);
        void addCar(CarType);
        void searchByOrigin();
        void searchByName();
        void searchByModel();
        void showList();
        void removeCarType();
        void writeData(char []);
};

#endif
