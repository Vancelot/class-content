//implementation for CarList class
#include "CarList.h"
#include "tools.h"

//constructors
CarList::CarList()
{
    capacity = CAP;
    if (list)
    {
        delete [] list;
        list = NULL;
    }
    //allocate memory for an array of videos
    list = new CarType[capacity];
    size = 0; 
}

//constructor from file
CarList::CarList(char fileName[])
{
    capacity = CAP;
    list = new CarType[capacity];
    size = 0;
    ifstream inFile;
    CarType aCar;
    char tempName[MAXCHAR], originDesc[MAXCHAR];
    int tempCylinders, tempModel;
    double tempMpg, tempDisplacement, tempHp, tempWeight, tempAccel;
    Origin tempOrigin;

    inFile.open(fileName);
    if(!inFile)
    {
        cout << "Cannot open File. Exiting Program" << endl;
        exit (0);
    }
    
    inFile.get(tempName, MAXCHAR, ';');
    while(!inFile.eof())
    {
        inFile.ignore(5, ';');
        inFile >> tempMpg;
        inFile.ignore(5, ';');
        inFile >> tempCylinders;
        inFile.ignore(5, ';');
        inFile >> tempDisplacement;
        inFile.ignore(5, ';');
        inFile >> tempHp;
        inFile.ignore(5, ';');
        inFile >> tempWeight;
        inFile.ignore(5, ';');
        inFile >> tempAccel;
        inFile.ignore(5, ';');
        inFile >> tempModel;
        inFile.ignore(5, ';');
        inFile.get(originDesc, MAXCHAR);
        tempOrigin = readOrigin(originDesc[0]);
        inFile.ignore(100, '\n');
        //populate aCar
        aCar.setName(tempName);
        aCar.setMpg(tempMpg);
        aCar.setCylinders(tempCylinders);
        aCar.setDisplacement(tempDisplacement);
        aCar.setHp(tempHp);
        aCar.setWeight(tempWeight);
        aCar.setAccel(tempAccel);
        aCar.setModel(tempModel);
        aCar.setOrigin(tempOrigin);
        addCar(aCar);
        inFile.get(tempName, MAXCHAR, ';');
    }
    inFile.close();
}

//destructor
CarList::~CarList()
{
    //delete
    if(list)
    {
        delete [] list;
        list = NULL;
    }

}
//returns true/false for name sorting
bool CarList::isLessThan(char name1[], char name2[])
{
    if(strcmp(name1, name2) < 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
//Add a car to the list -- growList and insert sorted
void CarList::addCar(CarType aCar)
{
    if(size == capacity)
    {
        growList();
    }

    char name1[MAXCHAR];
    char name2[MAXCHAR];

    int i = 0;

    if (size == capacity)
    {
        growList();
    }
    else if(!size)
    {
        list[size++] =aCar;
    }
    else
    {
        for (i=0; i < size; i++)
        {
            aCar.getName(name1);
            list[i].getName(name2);

            if(!(isLessThan(name2, name1)))
            {
                break;
            }
        }
        
        for (int j = size; j > i; j--)
        {
           list[j] = list[j-1];
        }
        
        list[i]=aCar;

        size++;
    }
}
//search by Origin
void CarList::searchByOrigin()
{
    char letter;
    Origin srchOrigin;
    Origin tempOrigin;
    cout << "Please enter the origin to search for (U-US, E-Europe, J-Japan): ";
    cin >> letter;
    srchOrigin = readOrigin(letter);
    for(int i = 0; i < size; i++)
    {
        tempOrigin = list[i].getOrigin();
        if (srchOrigin == tempOrigin)
        {
            list[i].printCarType();
        }
    }
}
void CarList::searchByName()
{
    char searchName[MAXCHAR];
    char tempName[MAXCHAR];
    cout << "Please enter name of car: ";
    cin.get(searchName, MAXCHAR);
    convertCase(searchName);
    for(int i=0; i < size; i++)
    {
        list[i].getName(tempName);
        convertCase(tempName);
        if(strstr(tempName, searchName)!=NULL)
        {
            list[i].printCarType();
        }
    }
}
void CarList::searchByModel()
{
    int searchModel;
    int tempModel;
    cout << "Please enter a valid Model Number: " << endl;
    cin >> searchModel;
    for (int i = 0; i < size; i++)
    {
        tempModel = list[i].getModel();
        if (searchModel == tempModel)
        {
            list[i].printCarType();
        }
    }
}

void CarList::writeData(char fileName[]) 
{
    ofstream outFile;
    outFile.open(fileName);
    for (int i=0; i<size; i++)
    {
        list[i].printCarType(outFile);
    }
}
void CarList::showList()
{
    for(int i = 0; i < size; i++)
    {
        cout << i+1 << ") ";
        list[i].printCarType();
    }
    cout << endl;
}

void CarList::removeCarType()
{
    int delIndex = 0;
    delIndex = readInt("Please enter index of Car to delete: ");
    if(delIndex < size)
    {
        for(int i = delIndex; i < size; i++)
        {
            list[i-1] = list[i];
        }
    }
    size--;
}

void CarList::growList()
{
    capacity += GROWTH;
    char tempName[MAXCHAR];

    CarType *tempList = new CarType[capacity];
    for (int i = 0; i < size; i++)
    {
        tempList[i] = list[i];
    }
    
    delete [] list;
    list = tempList;
    tempList = NULL;
}

