#include <iostream>
#include <cctype>
#include <cstring>

using namespace std;
//put the implementation of your assigned functions here

struct node
{
    int data;
    node * next;
};

/* These functions are already written and can be called to test out your code */
void build(node * & head);  //supplied
void display(node * head);  //supplied
void destroy(node * &head); //supplied
void duplicate(node * & new_copy); //provides a duplicate copy of the list
int countEven(node * head);
int removeEven(node *&head);

int main()
{
    node * head = NULL;
    build(head);
    display(head);

    //PLEASE PUT YOUR CODE HERE to call the function assigned
    countEven(head);
    removeEven(head);
    display(head);
    destroy(head);
    
    return 0;
}
int countEven(node * head)
{
    node *curr = head;
    int count = 0;
    for (curr=head; curr; curr=curr->next)
    {
        if((curr->data%2)==0)
        {
            ++count;
        }
    }
    cout << "The number of evens in your list is: " << count << endl;
}
int removeEven(node *&head)
{
    node *prev = head, *curr = head;
    int numRemoved = 0;
    while(curr)
    {
        if((curr->data%2)==0)
        {
            ++numRemoved;
            if(curr==head)
            {
                head = head->next;
                curr = head;
            }
            else
            {
                prev->next = curr->next;
                delete curr;
                curr = prev->next;
            }
        }
        else
        {
            prev = curr;
            curr = curr->next;
        }
    }
    cout << "You removed " << numRemoved << " nodes." << endl;
    return numRemoved;
}
