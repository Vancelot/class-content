#include "list.h"
#include <iostream>
#include <cctype>
#include <cstring>

using namespace std;

int sumOfList(node * head)
{
    int sum = 0;
    for(node *curr = head; curr; curr = curr->next)
    {
        sum+=curr->data;
    }
    cout << "Sum of all ya numbies are: " << sum << endl;
    return sum;
}
void insert(node * &head, int position, int newInt)
{
    node *curr = NULL, *newNode = NULL, *tail = NULL, *prev = NULL;
    int tempPos = 0;
    curr = head;
    if(newInt)
    {
        newNode = new node;
        newNode->data = newInt;
        newNode->next = NULL;
        while (curr->next && tempPos!=position)
        {
            prev = curr;
            curr = curr->next;
            tail = curr;
            tempPos++;
        }
        if(position == 0)
        {
            newNode->next = head;
            head = newNode;
        }
        else if(curr->next==NULL && position > tempPos)
        {
            curr->next = newNode;
            tail = newNode;
        }
        else
        {
            prev->next = newNode;
            newNode->next = curr;
        }
    }
}
void remove(node * &head, int position, int &newInt)
{
    node *curr=NULL, *tail=NULL, *prev=NULL;
    int tP = 0;
    curr = head;
    
    if(head)
    {
        while(curr->next && tP!= position)
        {
            prev = curr;
            curr = curr->next;
            tail = curr;
            tP++;
        }
        if(position == 0)
        {
            head = head->next;
        }
        else if(curr->next==NULL && position==tP)
        {
            prev->next = NULL;
        }
        else if(position > tP)
        {
            cout << "nah, g. " << endl;
        }
        else
        {
            prev->next = curr->next;
        }
    }
}
void removeOdd(node * &head)
{
    int delNum = 2;
    node *curr = head, *prev = head;
    while(curr)
    {
        if((curr->data%2)==0)
        {
            if(curr == head)
            {
                head = head->next;
                curr = head;
            }
            else
            {
                prev->next = curr->next;
                delete curr;
                curr = prev->next;
            }
        }
        else
        {
            prev = curr;
            curr = curr->next;
        }
    }
}
void removeDup(node *&head)
{
    node *curr = NULL, *prev = NULL, *temp=NULL, *temp2= NULL;
   
    int tempVal = 0;
    for (prev=head;prev;prev=prev->next)
    {
        temp2=prev;
        curr=prev->next;
        while(curr)
        {
            if(prev->data==curr->data)
            {
                temp2->next=curr->next;
                delete curr;
                curr=temp2->next;
            }
            else
            {
                temp2=curr;
                curr=curr->next;
            }
        }
    }
}
void sortList(node *&head)
{
    node *curr = NULL, *prev = NULL, *temp = NULL, *tail = NULL;
    curr = head;
    for(curr= head; curr; curr=curr->next)
    {
        temp = curr;          
        tail = curr;
        while(curr && curr->data < prev->data)
        {
            prev = curr;
            curr = curr->next;
            prev = NULL;
        }
        if(!prev)
        {
            temp->next = curr;
            head = temp;
        }
        else if(!curr)
        {
            tail->next = temp;
            tail = temp;
        }
    }
}

