#include "list.h"
#include <iostream>
#include <cstring>
#include <cctype>
using namespace std;
//put the implementation of your assigned functions here
//compute and return the sum of integers in the linear linked list
int sumOfList(node * head)
{
    //int count = 1;
    //node *curr=NULL;
    int sum = 0;
    for(node *curr = head; curr; curr = curr->next)
    {
        sum+=curr->data;
    }
    cout << "Sum of Integers in the linear linked list" << sum << endl;
    
    //delete curr;

    return sum;
}
int sumOfEven(node * head)
{
    int sumEv = 0;
    for (node *curr = head; curr; curr = curr->next)
    {
        if((curr->data%2)==1)
        {
            sumEv*=curr->data;
        }
    }
    cout << "Sum of Odd Integers in the linked list is " << sumEv << endl;
    return sumEv;
}

//insert newInt at index "position" where index starts with 0

void insert(node *& head, int position, int newInt)
{
    node *tail = NULL, *newNode = NULL, *curr = NULL, *prev = NULL;
    if(newInt)
    {
       newNode = new node;
       newNode->data = newInt;
       newNode->next = NULL;

       int tempPos = 0;

       curr = head;

       if(head)
       {
           while(curr->next && tempPos != position)
           {
               prev = curr;
               curr = curr->next;
               tempPos++;
               tail = curr;//maybe not
           }
           if(curr->next==NULL && position >= tempPos)
           {
               tail->next = newNode;
               tail = newNode;
           }
           else if(position == 0)
           {
               newNode->next = curr;
               head = newNode;
           }
           else
           {
               newNode->next = curr;
               prev->next = newNode;
           }
       }
    }
//   head = NULL;
    tail = NULL;
    curr = NULL;
    prev = NULL;
}

void remove(node *&head, int position, int &newInt)
{
    node *tail = NULL,  *curr = NULL, *prev = NULL;
    
    int tP=0;
    curr =head;
        if(head)
        {
            while(curr->next && tP != position)
            {
                prev = curr;
                curr = curr->next;
                tail = curr;
                ++tP;
            }
            if(position == 0)
            {
              head = head->next;
            }
        
            else
            {
                prev->next = curr->next;
            }
        }
}

/*bool dupeData(node * head)
{
    int srch, srch2 = 0;
    node *curr = NULL, *prev = NULL;
    curr = head;

    for(curr = head; curr; curr = curr->next)
    {
        srch = curr->data;
        prev = curr;
        curr = curr->next;
        srch2 = curr->data;
        if(srch == srch2)
        {
            return true;
        }
    }
    return false;
}*/
