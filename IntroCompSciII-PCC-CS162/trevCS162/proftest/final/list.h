//list.h
#ifndef LIST_H
#define LIST_H

#include <iostream>
#include <cstring>
#include <cctype>


struct node
{
    int data;
    node * next;
};

/* These functions are already written and can be called to test out your code */
void build(node * & head);  //supplied
void display(node * head);  //supplied
void destroy(node * &head); //supplied
void duplicate(node * & new_copy); //provides a duplicate copy of the list

/* *****************YOUR TURN! ******************************** */
//Write your function prototype here:
//compute and return a sum of integers in a linear linked list
int sumOfList(node * head);
//compute and return sum of even ints in llist
int sumOfEven(node * head);
//insert newInt at index "position" where index starts with 0
void insert(node *& head, int position, int newInt);
//remove value at position
void remove(node *& head, int position, int &newInt);
bool dupeData(node * head);
#endif
