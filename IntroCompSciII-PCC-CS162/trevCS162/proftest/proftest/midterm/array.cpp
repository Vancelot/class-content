//array.cpp 

#include "array.h"

//put the implementations of your assigned functions here
//compute and return the sum of even integers in list
int sumOfEvens(int list[], int size)
{
    int x=0;
    int even=0;
    for (int i = 0; i < size; i++)
    {
        if ((list[i]%2)==0)
        { 
            x=list[i];
            even+=x;
        }
    }
    cout << even << endl;
    return even;
}

        
            
//remove the first numOfInt integers in the list and update the size of the list
void remove(int list[], int &size, int numOfInt)
{
    if (numOfInt >= size)
    {
        cout << "Cannot remove what is greater than size" << endl;
        exit(0);
    }
    else if (numOfInt > 0 && numOfInt < size)
    {
        for (int i=0; i <=numOfInt; i++)
        {
            list[i]=list[i+numOfInt];
            size--;
        }
    }
    
}

