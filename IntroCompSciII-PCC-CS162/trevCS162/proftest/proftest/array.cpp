//array.cpp 

#include "array.h"

//put the implementations of your assigned functions here
//
//compute and return the number of even integers in list
int numOfEven(int list[], int size)
{
    
    int even=0;
    for (int i=0; i < size; i++)
    {
        if ((list[i]%2)==0)
        {
            ++even;
        }
    }
    cout << even << endl;;
    return even;
}
//insert newInt into the list at index "position" and update the size of the list.
void insert(int list[], int &size, int newInt, int position)
{
    if (position==size)
    {
        list[position]=newInt;
    }
    else if (position >= 0 && position < size)
    {
        for(int i = size; position < i; i--)
        {
            list[i] = list[i-1];
        }
        list[position]=newInt;
    }
    size++;
}

void remove(int list[], int &size, int &newInt, int position)
{
    newInt = list[position];
    if (position >= size)
    {
        exit(0);
    }
    else if (position >=0 && position < size)
    {
        for (int i = position; i < size; i++)
        {
            list[i]=list[i+1];
        }
        size--;
    }
}


