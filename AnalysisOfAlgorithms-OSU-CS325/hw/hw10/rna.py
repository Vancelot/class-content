from collections import defaultdict
from heapq import heappush, heappop

pairs = set(["AU", "UA", "CG", "GC", "GU", "UG"])

def best(s):

    def _best(i, j):
        if (i,j) in opt:
            return opt[i,j]
        cur = -1
        for k in range(i, j):
            if _best(i,k) + _best(k+1,j) > cur:
                cur = _best(i,k) + _best(k+1,j)
                back[i,j] = k

        if s[i] + s[j] in pairs:
            if _best(i+1, j-1) + 1 > cur:
                cur = _best(i+1, j-1) + 1
                back[i,j] = -1
        opt[i,j] = cur
        return cur

    def solution(i,j):
        if i == j:
            return "."
        if i > j:
            return ""
        k = back[i,j]
        if k == -1:
            return "(%s)" % solution(i+1, j-1)
        else:
            return solution(i,k) + solution(k+1,j)

    opt = defaultdict(int)
    back = {}
    n = len(s)
    for i in range(n):
        opt[i,i] = 0
        opt[i,i-1] = 0

    return _best(0,n-1), solution(0, n-1)


def total(s):

    def _total(i, j):
        if (i,j) in opt:
            return opt[i,j]
        if i >= j:
            return 1
        cur = 0
        for k in range(i, j):
            if s[j] + s[k] in pairs:
                cur += _total(i,k-1) * _total(k+1,j-1)
        opt[i,j] = cur + _total(i, j-1)
        return opt[i,j]

    opt = defaultdict(int)
    n = len(s)

    result = _total(0, n-1)
    return result

def kbest(x, k):

    def _kbest(i,j):
        def trypushBinary(s, p, q):
            if p < len(topk[i, s]) and q < len(topk[s+1, j]) and (s,p,q) not in visited:
                    heappush(h,(-(topk[i,s][p][0] + topk[s+1,j][q][0]),(s,p,q)))
                    visited.add((s,p,q))
            return
        def trypushUnary(p):
            if p < len(topk[i+1, j-1]):
                heappush(h,(-(topk[i+1,j-1][p][0]+1),(p,)))
            return

        h = []
        visited = set()

        if (i,j) in topk:
            return topk[i,j]

        for s in range(i, j):
            _kbest(i, s)
            _kbest(s+1, j)
            trypushBinary(s,0,0)

        if x[i] + x[j] in pairs:
            _kbest(i+1, j-1)
            trypushUnary(0)

        found = []
        for _ in range(k):
            if h == []:
                break
            score, indices = heappop(h)
            try:
                s, p, q = indices
                if (-score, "%s%s" % (topk[i,s][p][1], topk[s+1,j][q][1])) not in found:
                    topk[i,j].append((-score, "%s%s" % (topk[i,s][p][1], topk[s+1,j][q][1])))
                    found.append((-score, "%s%s" % (topk[i,s][p][1], topk[s+1,j][q][1])))
                trypushBinary(s, p+1, q)
                trypushBinary(s, p, q+1)

            except:
                p = indices[0]
                if (-score, "(%s)" % topk[i+1, j-1][p][1]) not in found:
                    topk[i,j].append((-score, "(%s)" % topk[i+1, j-1][p][1]))
                    found.append((-score, "(%s)" % topk[i+1, j-1][p][1]))
                trypushUnary(p+1)

        return

    topk = defaultdict(list)
    n = len(x)

    for i in range(n):
        topk[i,i] = [(0, '.')]
        topk[i,i-1] = [(0, '')]

    _kbest(0, n-1)
    return topk[0,n-1]

def printSeq(s):
    print(s)
    print(best(s))
    print(total(s))
    print(kbest(s, 3))
    print("----------")
    return

if __name__ == "__main__":
    cases = ["ACAGU", "AC", "GUAC", "GCACG", "CCGG"]

    for i in cases:
        printSeq(i)
