0. What's your name?
	Kerry Vance
1. Approximately how many hours did you spend on this assignment?
	20
2. Would you rate it as easy, moderate, or difficult?
	Difficult
3. Did you work on it mostly alone, or mostly with other people?
	Alone
4. How deeply do you feel you understand the material it covers (0%-100%)? 
	30%
5. Any other comments?
	Really really appreciate the coding sessions. Seeing the process of
	translating the formulation into python has been a huge sticking point
