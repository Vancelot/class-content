def sort(array):
    if array == []:
        return [];
    else:
        pivot = array[0];
        left = [x for x in array if x < pivot];
        right = [x for x in array[1:] if x >= pivot];
        return [sort(left)] + [pivot] + [sort(right)];

def sorted(tree):
    result = [];
    if tree[0] != []:
        result.extend(sorted(tree[0]));
    result.append(tree[1]);
    if tree[2] != []:
        result.extend(sorted(tree[2]));
    return result;
        
def _search(tree, x):
    result = [];
    if tree[1] == x:
        return tree;
    elif x < tree[1] and tree[0] != []:
        return _search(tree[0], x);
    elif x >= tree[1] and tree[2] != []:
        return _search(tree[2], x);


def search(tree, x):
    result = False;
    searchResult = _search(tree, x);
    if searchResult != []:
        result = True;
    return result;

def insert(tree, x):
    if x < tree[1]:
        if tree[0] != []:
            insert(tree[0], x);
        else:
            tree[0] = [[], x, []];
    elif x > tree[1]:
        if tree[2] != []:
            insert(tree[2], x);
        else:
            tree[2] = [[], x, []];

tree = sort([4,2,6,3,5,7,1,9])
print("tree", tree)
insert(tree, 20);
print("tree", tree)
