from qselect import qselect
from random import shuffle
from random import seed

seed(1)
a = list(range(100000))
b = a[:] # keep it sorted
shuffle(a)

print("qselect:", qselect(42, a));
