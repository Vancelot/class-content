import random;

def qselect(x, array):
    if x < 1 or x > len(array) or array == []:
        return []
    else:
        rindex = random.randint(0, len(array) -1);
        array[0], array[rindex] = array[rindex], array[0];
        pivot = array[0]
        left = [x for x in array if x < pivot];
        leftLen = len(left);
        if x-1 < leftLen:
            qselect(x, left);
        elif x-1 == 0:
            return pivot;
        else:
            right = [x for x in array[1:] if x >= pivot];
            qselect(x, right);

# def qselect(x, array):
#     x -= 1;
#     result = -999;
#     if x < 0 or x >= len(array):
#         print("Out of index")
#     else:
#         quicksort(array, 0, len(array) - 1);
#         result = array[x];
#     return result;
# 
# def quicksort(array,p,r): 
#     if p < r: 
#         q = randoPart(array, p, r) 
#         quicksort(array, p, q - 1) 
#         quicksort(array, q + 1, r) 
# 
# def partition(array, p, r):
#     x = array[r];
#     i = p - 1;
#     for j in range(p, r):
#         if array[j] <= x:
#             i = i + 1;
#             array[i], array[j] = array[j], array[i];
#     array[i + 1], array[r] = array[r], array[i + 1];
#     return i + 1;
# 
# def randoPart(array, p, r):
#     i = random.randrange(p, r);
#     array[r], array[i], array[i], array[r];
#     return partition(array, p, r);
# 
