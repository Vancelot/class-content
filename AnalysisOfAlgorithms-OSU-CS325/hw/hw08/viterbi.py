from collections import defaultdict

def _order(V, E):
    adjlist = defaultdict(list)
    indegree = defaultdict(int)

    for u, v in E:
        adjlist[u].append(v)
        indegree[v] += 1

    for i in range(V):
        if i not in indegree:
            indegree[i] = 0;

    stack = []
    for u in range(V):
        if indegree[u] == 0:
            stack.append(u)
    while stack != []:
        u = stack.pop()
        yield u
        for v in adjlist[u]:
            indegree[v] -= 1
            if indegree[v] == 0:
                stack.append(v)

def order(V, E):
    return list(_order(V, E))


def longest(V, E):
    adjlist = defaultdict(list)
    cost = defaultdict(list)
    maxCost = 0
    bestPath = []
    top = order(V, E)

    for u, v in E:
        adjlist[u].append(v)

    for v in top:
        cost[v] = 0


    for i in top:
        for e in adjlist[i]:
            cost[e] = max(cost[e], cost[i] + 1)

    maxCost = cost[max(cost)]

    while len(bestPath) < maxCost:
        j = 0
        for i in cost:
            if cost[i] == j:
                bestPath.append(i)
                j += 1

    return maxCost, bestPath


if __name__ == "__main__":
    def checker(func, argv, answer):
        print("\n\nCHECKER")
        result = func(*argv)
        print(func.__name__, "(",argv,"): ", answer, "\n", result)
        print("PASS") if result == answer else print("FAIL")
        return

    graph = 8, [(0,2), (1,2), (2,3), (2,4), (3,4), (3,5), (4,5), (5,6), (5,7)]
    ans = (5, [0, 2, 3, 4, 5, 6])

    checker(longest, graph, ans)
    checker(longest, (8, [(0,1), (0,2), (1,2), (2,3), (2,4), (4,3), (3,5), (4,5), (5,6), (5,7), (6,7)]), (7, [0, 1, 2, 4, 3, 5, 6, 7]))

    print("\n\nLONGEST:", longest(8, [(0,1), (0,2), (1,2), (2,3), (2,4), (4,3), (3,5), (4,5), (5,6), (5,7), (6,7)]))
