from collections import defaultdict

def _order(V, E):
    adjlist = defaultdict(list)
    indegree = defaultdict(int)

    for u, v in E:
        adjlist[u].append(v)
        indegree[v] += 1

    for i in range(V):
        if i not in indegree:
            indegree[i] = 0;

    stack = []
    for u in range(V):
        if indegree[u] == 0:
            stack.append(u)
    while stack != []:
        u = stack.pop()
        print("U:", u)
        yield u
        for v in adjlist[u]:
            indegree[v] -= 1
            if indegree[v] == 0:
                stack.append(v)

def order(V, E):
    return list(_order(V, E))

if __name__ == "__main__":

    def checker(func, argv, answer):
    #    print("\n\nCHECKER ", argv)
        result = func(*argv)
        print(func.__name__, "(",argv,"): ", answer, "\n", result)
        print("PASS") if result == answer else print("FAIL")
        return

    checker(order, (8, [(0,2), (1,2), (2,3), (2,4), (3,4), (3,5), (4,5), (5,6), (5,7)]) , [0, 1, 2, 3, 4, 5, 6, 7])


    checker(order, (8, [(0,2), (1,2), (2,3), (2,4), (4,3), (3,5), (4,5), (5,6), (5,7)]), [0, 1, 2, 4, 3, 5, 6, 7])

    checker(order, (4, [(0,1), (1,2), (2,1), (2,3)]), None)
