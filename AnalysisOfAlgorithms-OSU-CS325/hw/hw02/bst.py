def sort(a): ## buggy version copied from slides
    if a == []:
        return []
    else:
        pivot = a[0]
        left = [x for x in a if x < pivot]
        right = [x for x in a[1:] if x >= pivot]
        return [sort(left)] + [pivot] + [sort(right)]

if __name__ == "__main__":
    a = list(range(10))
    print(sort(a))
    a[1], a[2] = a[2], a[1] # 0, 2, 1, ...
    print(sort(a))
