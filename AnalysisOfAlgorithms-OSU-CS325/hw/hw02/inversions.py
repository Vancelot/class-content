def inversions(arr): 
    invCount = 0;
    if len(arr) >1: 
        mid = len(arr)//2;
        left = arr[:mid];
        right = arr[mid:];
  
        invCount += inversions(left);
        invCount += inversions(right);
  
        i = j = k = 0;
          
        while i < len(left) and j < len(right): 
            if left[i] < right[j]: 
                arr[k] = left[i];
                i += 1;
            else: 
                arr[k] = right[j]; 
                j += 1;
                invCount += 1;
            k += 1;
          
        while i < len(left): 
            arr[k] = left[i];
            i += 1;
            k += 1;
          
        while j < len(right): 
            arr[k] = right[j]; 
            j += 1;
            k += 1;
            invCount += 1;

    return invCount;

#array = [4,1,3,2];
array = [2,4,1,3];

print("inversions", inversions(array))
