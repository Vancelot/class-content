def longest(tree):
    leftPath = rightPath = leftHeight = rightHeight = 0;
    if tree == []:
        return 0;

    if tree[0] != []:
        leftHeight = height(tree [0]);
    if tree[2] != []:
        rightHeight = height(tree [2]);

    if tree[0] != []:
        leftPath = longest(tree[0]);
    if tree[2] != []:
        rightPath = longest(tree[2]);

    return max(leftHeight + rightHeight , max(leftPath, rightPath));

def height(node):
    leftHeight = rightHeight = 0;
    if node == []:
        return 0;
    if node[0] != []:
        leftHeight = height(node[0]);
    if node[2] != []:
        rightHeight = height(node[2]);
    return 1 + max(leftHeight, rightHeight);
