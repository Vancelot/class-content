import random
import heapq

def nbesta(a, b):
    result = []
    for i in a:
        for j in b:
            result.append((i,j))

    for j in range(0, len(result)):
        for i in range(0, len(result)-1):
            if result[i][0] + result[i][1] >= result[i+1][0] + result[i+1][1]:
                result[i], result[i+1] = result[i+1], result[i]

    return result[:len(a)]

def nbestb(a, b):
    enumeration = []
    result = []
    for i in a:
        for j in b:
            enumeration.append((i,j))

    for i in range(1, len(a) + 1):
        result.append(qselect(i, enumeration))

    return result;

def nbestc(a, b):
    result = [];
    a.sort();
    b.sort();
    i = j = 0;

    for i in range(0, len(a)):
        for j in range(0, len(b)):
            heapq.heappush(result, (a[i], b[j]))

    return heapq.nsmallest(len(a), result, key=lambda x: x[0] + x[1]);

def qselect(i, a):
    if a == []:
        return []
    idx=random.randint(0,len(a)-1)
    pivot = a[idx]

    a[0],a[idx]=a[idx],a[0]

    if a[len(a)-1] == []:
        a.pop(len(a)-1)
    left = [(x,y) for x,y in a if x + y < pivot[0] + pivot[1]]
    if len(left) == i-1:
        return pivot
    if len(left) < i - 1:
        right = [(x,y) for x,y in a[1:] if x + y >= pivot[0] + pivot[1]]
        return qselect(i-len(left)-1,right)
    else:
        # a = left
        return qselect(i,left)

if __name__ == '__main__':
    a, b = [4,1,5,3], [2,6,3,4]
    correctAnswer = [(1, 2), (1, 3), (3, 2), (1, 4)]
    resA = resB = resC = 0
    resA = nbesta(a,b)
    resB = nbestb(a,b)
    resC = nbestc(a,b)

    print("a:", a, "b:", b, "\nExpected result:", correctAnswer,"\n\n")
    print("nbesta(a,b)", resA)
    if resA != correctAnswer:
        print("A FAILED")
    else:
        print("A PASSES")

    print("nbestb(a,b)", resB)
    if resB != correctAnswer:
        print("B FAILED")
    else:
        print("B PASSES")

    print("nbestc(a,b)", resC)
    if resC != correctAnswer:
        print("C FAILED")
    else:
        print("C PASSES")
