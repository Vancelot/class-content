def ksmallest(k, stream):
    result = []

    for i in range(0, k):
        result.append(stream[i])
    result.sort();

    for i in stream[k:]:
        if i < result[k-1]:
            result[k-1] = i
            result.sort()

    return result

if __name__ != "main":
    arr = [10, 2, 9, 3, 7, 8, 11, 5, 7]
    k = 4
    print("ksmallest(", k,",", arr,"):\n", ksmallest(k, arr))

    arr = range(1000000, 0, -1)
    k = 3
    print("ksmallest(", k,", range(1000000, 0, -1):\n", ksmallest(k, arr))
