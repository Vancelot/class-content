from math import ceil

def merge(arr):
    sortedArr = []
    while arr:
        minVal,index = arr[0][0],0
        for i in arr:
            if i[0]<minVal:
                minVal = i[0]
                index = arr.index(i)
        sortedArr.append(minVal)
        arr[index].pop(0)
        if not arr[index]:
            arr.remove(arr[index])     
    return sortedArr

def kmergesort(array, k):
    if len(array) == 1:
        return array

    j = ceil(len(array)/k)

    array = [kmergesort(array[i:i+j], k) for i in range(0, len(array), j)]

    return merge(array)

if __name__ != "main":
    arr = [4,1,5,2,6,3,7,0]
    k = 3

    print("kmergesort(", arr, ",", k,"):\n", kmergesort(arr, k))
