from heapq import heappush, heappop, heapify
def nbest(ABs):    # no need to pass in k or n
    k = len(ABs)
    n = len(ABs[0][0])
    def trypush(i, p, q):  # push pair (A_i,p, B_i,q) if possible
        A, B = ABs[i] # A_i, B_i
        if p < n and q < n and i < k:
#            heappush(h, (A[p] + B[q], i, p, q, (A[p],B[q])))
            heapify(ABs)
            used.add((i, p, q))
    h, used = [], set()
    for i in range(k):  # NEED TO OPTIMIZE
        trypush(i, 0, 0)
    for _ in range(n): 
        _, i, p, q, pair = heappop(h)
        yield pair     # return the next pair (in a lazy list)
        trypush(i, 1, 0)
        trypush(i, 0, 1)

if __name__ == "__main__":
    print("I'm main")

    print(list(nbest([([1,2,4], [2,3,5]), ([0,2,4], [3,4,5])])) )
