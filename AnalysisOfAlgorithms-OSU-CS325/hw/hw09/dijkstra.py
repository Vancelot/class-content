from collections import defaultdict
import heapdict

def shortest(target, E):
    adjlist = defaultdict(list)
    nodes = defaultdict(list)
    backtrace = defaultdict(list)
    hd = heapdict.heapdict()

    for v, u, w in E:
        nodes[v] = float('inf')
        nodes[u] = float('inf')
        adjlist[v].append((u,w))
        adjlist[u].append((v,w))

    nodes[0] = hd[nodes[0]] = 0
    nodes[target-1] = float('inf')

    while len(hd) > 0:
        u, w = hd.popitem()
        for v in adjlist[u]:
            if nodes[v[0]] > w + v[1]:
                nodes[v[0]] = w + v[1]
                hd[v[0]] = nodes[v[0]]
                backtrace[v[0]] = u

    u = target - 1
    path = [u] if nodes[u] != float("inf") else None
    if path != None:
        while u != 0:
            path.append(backtrace[u])
            u = backtrace[u] 
        path.reverse()

    if path != None:
        return nodes[target-1], path
    else:
        return None

if __name__ == "__main__":
    print('shortest(4, [(0,1,1), (0,2,5), (1,2,1), (2,3,2), (1,3,6)])')
    print(shortest(4, [(0,1,1), (0,2,5), (1,2,1), (2,3,2), (1,3,6)]))
    print('(4, [0,1,2,3])')

    print("shortest(4, [(0,1,1), (2,3,1)])")
    print(shortest(4, [(0,1,1), (2,3,1)]))

    print("shortest(10000, []")
    print(shortest(10000, []))
    print("shortest(8, [(0,4,2), (0,1,7), (0,7,12), (1,2,1), (1,3,1), (1,7,5), (2,3,3), (2,4,1), (2,5,1), (2,7,10), (3,6,2), (3,4,5), (3,7,1)], 6)")
    print(shortest(8, [(0,4,2), (0,1,7), (0,7,12), (1,2,1), (1,3,1), (1,7,5), (2,3,3), (2,4,1), (2,5,1), (2,7,10), (3,6,2), (3,4,5), (3,7,1)]))
