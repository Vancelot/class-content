def find(array):
    result = []
    array.sort();

    for i in range(0, len(array) - 2):
        for j in range(i + 1, len(array) - 1):
            for k in range(j + 1, len(array)):
                if(array[i] + array[j] == array[k]):
                    result.append((array[i], array[j], array[k]));

    return result;

def _search(array, x):
    result = 0;

    for i in array:
        if i == x:
            result = i;

    return result;

if __name__ == '__main__':
    arr = [1,4,2,3,5];
    testResult = find(arr)
    print("Test 1:\nfind(", arr, "\n[(1,3,4), (1,2,3), (1,4,5), (2,3,5)]\n", testResult);

