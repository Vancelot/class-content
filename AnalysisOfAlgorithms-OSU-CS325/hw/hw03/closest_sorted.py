from bisect import bisect;

def find(array, x, k):
    result = [];
    count = 0;
    left = bisect(array, x) - 1;
    right = left + 1;

    while left >= 0 and right < len(array) and count < k:
        if abs(array[left] - x) <= abs(array[right] - x):
            result.append(array[left]);
            left -= 1;
        else:
            result.append(array[right]);
            right += 1;
        count += 1;

    while count < k and left >= 0:
        result.append(array[left]);
        left -= 1;
        count += 1;
    while count < k and right < len(array):
        result.append(array[right]);
        right += 1;
        count += 1;

    result.sort();
    return result;

if __name__ == '__main__':
    arr = [1,2,3,4,4,6,6];
    testResult = find(arr, 5, 3)
    print("Test 1: [4,4,6]\nfind([1,2,3,4,4,6,6], 5, 3)\n", testResult);
    if testResult != [4,4,6]:
        print("Test 1: failed");
    else:
        print("Test 1: passed")

    arr = [1,2,3,4,4,5,6];
    testResult = find(arr, 4, 5)
    print("Test 2: [2,3,4,4,5]\nfind([1,2,3,4,4,5,6], 4, 5)\n", testResult);
    if testResult != [2,3,4,4,5]:
        print("Test 2: failed");
    else:
        print("Test 2: passed")
