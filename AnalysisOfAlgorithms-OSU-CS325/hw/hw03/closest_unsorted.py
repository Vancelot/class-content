def find(array, x, k):
    result = array[:k];
    maxDiff = maxIndex = 0;

    for i in range(k, len(array)):
        maxDiff = 0;
        for j in range(0, len(result)):
            if abs(result[j] - x) > maxDiff:
                maxDiff = abs(result[j] - x);
                maxIndex = j;
        if abs(array[i] - x) < maxDiff:
            result.pop(maxIndex);
            result.append(array[i]);

    return result;


if __name__ == '__main__':
    arr = [4,1,3,2,7,4];
    testResult = find(arr, 5.2, 2)
    print("Test 1:\nfind([4,1,3,2,7,4], 5.2, 2)\n", testResult);
    if testResult != [4,4]:
        print("Test 1: failed");
    else:
        print("Test 1: passed")

    arr = [4,1,3,2,7,4];
    testResult = find(arr, 6.5, 3)
    print("Test 2:\nfind([4,1,3,2,7,4], 6.5, 3)\n", testResult);
    if testResult != [4,7,4]:
        print("Test 2: failed");
    else:
        print("Test 2: passed")

    arr = [5,3,4,1,6,3];
    testResult = find(arr, 3.5, 2)
    print("Test 3:\nfind([5,3,4,1,6,3], 3.5, 2)\n", testResult);
    if testResult != [3,4]:
        print("Test 3: failed");
    else:
        print("Test 3: passed")
