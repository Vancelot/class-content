def best(cap, items): 
    table = [[0 for i in range(cap+1)] for j in range(len(items))]

    def _best(cap, items): 

        val = [0 for i in range(cap + 1)] 

        for i in range(cap + 1): 
            for j in range(len(items)): 
                if (items[j][0] <= i): 
                    val[i] = max(val[i], val[i - items[j][0]] + items[j][1]) 

        return val[cap]

    for i in range(cap + 1):
        for j in range(len(items)):
            table[j][i] = _best(i, items[:j+1])

    result = table[len(items)-1][cap]
    count = [0 for x in range(len(items))]
    w = cap

    for i in range(len(items)-1, 0, -1):
        if table[i][w] != table[i-1][w]:
            w -= items[i][1]
            count[i] += 1

    return result, count

    #for i in range(cap+1, 0, -1):

    return result, count



if __name__ == "__main__":

    arr = [(2,4), (3,5)]
    W = 3
    result = best(W, arr)
    expected = (5, [0, 1])
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")

    arr = [(1, 5), (1, 5)]
    W = 3
    expected = (15, [3, 0])
    result = best(W, arr)
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")

    arr = [(1, 2), (1, 5)]
    W = 3 
    expected = (15, [0, 3])
    result = best(W, arr)
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")

    arr = [(1, 2), (2, 5)]
    W = 3 
    expected = (7, [1, 1])
    result = best(W, arr)
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")

    arr = [(5, 9), (9, 18), (6, 12)]
    W = 58 
    expected = (114, [2, 4, 2])
    result = best(W, arr)
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")


    arr = [(8, 9), (9, 10), (10, 12), (5, 6)]
    W = 92
    expected = (109, [1, 1, 7, 1])
    result = best(W, arr)
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")
