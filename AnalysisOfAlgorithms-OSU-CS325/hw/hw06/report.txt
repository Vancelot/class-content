0. For each of the coding problems below:
   (a) Describe a greedy solution.
   (b) Show a counterexample to the greedy solution.
   (c) Define the DP subproblem 
   (d) Write the recurrence relations
   (e) Do not forget base cases

1. Unbounded Knapsack
   (a) Describe a greedy solution.
		Compute best value to weight ratio for each item. Add the best value
		to the bag until capacity. Repeat with remainder of space.
   (b) Show a counterexample to the greedy solution.
		best(3, [(2,4), (3,5)])
		the first item has a better value ratio, but can only fit one item in
		bag so total value is 4. The second item is not as valuable per
		weight, but the total would be 5. 
   (c) Define the DP subproblem 
		best solution for a given bag, w
   (d) Write the recurrence relations
		let,
			W		= initial bag capacity,
			w		= recurrent bag capacity,
			n		= number of items
			w_i	= weight of item,
			v_i	= value of item,
		opt[w]	= i..n: max(opt[w - w_i] + v_i) iff w >= w_i

   (e) Do not forget base cases
		w < i..n: min(w_i) -> opt[w] = 0

   Q: What are the time and space complexities?
		time:
			O(n*W)
		space:
			O(W)

2. Bounded Knapsack
   (a) Describe a greedy solution.
		Compute best value to weight ratio for each item. Add the best value
		to the bag if capacity > 0 until capacity of bag is reached. Repeat with remainder of space. 
   (b) Show a counterexample to the greedy solution.
		best(3, [(2,4,1), (3,5,1)])
   (c) Define the DP subproblem 
		opt[w][i] best value for a bag of w, with items 1..i 
		st: 0 <= w <= W, 1 <= i <= n, w_count > 0
   (d) Write the recurrence relations
		opt[w][i] = max(opt[w - w_i][i-1] + v_i (iff w>= w_i), opt[w][i-1])
   (e) Do not forget base cases
		opt[0][i] = 0
		opt[w][0] = 0


Debriefing (required!): --------------------------

0. What's your name?
	Kerry Vance
1. Approximately how many hours did you spend on this assignment?
	6
2. Would you rate it as easy, moderate, or difficult?
	Very difficult
3. Did you work on it mostly alone, or mostly with other people?
	Mostly alone
4. How deeply do you feel you understand the material it covers (0%-100%)? 
	30%
5. Which part(s) of the course you like the most so far?
	I like breaking down the steps of the algorithm and trying to wrap my
	head around the underlying concepts.
6. Which part(s) of the course you dislike the most so far?
	Python is still stumping me. So much happens automatically that I don't
	know what it's actually doing.

This section is intended to help us calibrate the homework assignments. 
Your answers to this section will *not* affect your grade; however, skipping it
will certainly do.
