def best(cap, items): 
    table = [[0 for i in range(cap+1)] for j in range(len(items))]

    def _best(cap, items): 

        val = [0 for i in range(cap + 1)] 

        for i in range(cap + 1): 
            for j in range(len(items)): 
                if (items[j][0] <= i):
                    if val[i] < val[i - items[j][0]] + items[j][1]:
                        if items[j][2] > 0: 
                            val[i] = val[i - items[j][0]] + items[j][1]
                            items[j] = items[j][0], items[j][1], items[j][2] - 1
                            

        return val[cap]

    for i in range(cap + 1):
        for j in range(len(items)):
            table[j][i] = _best(i, items[:j+1])

    result = table[len(items)-1][cap]
    count = [0 for x in range(len(items))]
    w = cap

    for i in range(len(items)-1, 0, -1):
        if table[i][w] != table[i-1][w]:
            w -= items[i][1]
            count[i] += 1

    return result, count

    #for i in range(cap+1, 0, -1):

    return result, count


    return

if __name__ == "__main__":

    arr = [(2,4,2), (3,5,3)]
    W = 5
    result = best(W, arr)
    expected = (5, [0, 1])
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")

    arr = [(1,5,2), (1,5,3)]
    W = 3
    expected = (15, [2, 1])
    result = best(W, arr)
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")

    [(1, 5, 1), (1, 5, 3)]
    W = 3 
    expected = (15, [1, 2])
    result = best(W, arr)
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")

    arr = [(1, 10, 6), (3, 15, 4), (2, 10, 3)]
    W = 20
    expected = (130, [6, 4, 1])
    result = best(W, arr)
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")

    arr = [(1, 6, 6), (6, 15, 7), (8, 9, 8), (2, 4, 7), (2, 20, 2)]
    W = 92
    expected = (236, [6, 7, 3, 7, 2])
    result = best(W, arr)
    print("best(",W,",",arr,"):\tANSWER:",expected,"\n", result)
    print("PASS\n") if result == expected else print("FAIL\n")
