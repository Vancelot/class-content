trees = {0:1, 1:1, 2:2, 3:5}

def bsts(n):
    if n not in trees:
        trees[n] = 0
        for i in range(0, n):
            trees[n] += bsts(i) * bsts(n - i - 1)

    return trees[n]

if __name__ == "__main__":
    result = bsts(2)
    print("bsts(2):", result)
    success = "PASS\n" if result == 2 else "FAIL\n"
    print(success)

    result = bsts(3)
    print("bsts(3):", result)
    success = "PASS\n" if result == 5 else "FAIL\n"
    print(success)

    result = bsts(5)
    print("bsts(5):", result)
    success = "PASS\n" if result == 42 else "FAIL\n"
    print(success)

    for i in range(10):
        print(bsts(i))
