bitStringsNo = {0:1, 1:2}

def num_no(n):
    if n not in bitStringsNo:
        bitStringsNo[n] = num_no(n-2) + num_no(n-1)

    return bitStringsNo[n]

def num_yes(n):
    return 2**n - num_no(n)

if __name__ == "__main__":
    result = num_no(3)
    print("num_no(3):", result)
    print("PASS") if result == 5 else print("FAIL")

    result = num_yes(3)
    print("num_yes(3):", result)
    print("PASS") if result == 3 else print("FAIL")
