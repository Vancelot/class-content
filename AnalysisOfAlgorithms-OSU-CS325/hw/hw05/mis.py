def max_wis(array):
    mis = {}
    def _max_wis(array):
        length = len(array)
        if(length == 0):
            return 0,[]
        len1 = len(array) - 1
        len2 = len(array) - 2
        if length not in mis:
            val1, list1 = max_wis(array[:len1])
            val2, list2 = max_wis(array[:len2])
            val2 += array[len(array)-1]
            if val2 > val1:
                list2.append(array[len(array) - 1])
                mis[length] = (val2, list2)
                return mis[length]
            else:
                mis[length] = (val1, list1)
                return mis[length]
        return mis[length]
    return _max_wis(array)

def max_wis2(array, mis=None):
    result =  0, []

    if mis == None:
        mis = dict()
    for i in range(0, len(array)):
        if array[i] > 0:
            mis[i] = _max_wis2(array[i], array[i+2:])
        else:
            mis[i] = 0, []
    for i in mis:
        result = mis[i] if mis[i] > result else result
    return result


def _max_wis2(x, array):
    bestSum = x
    bestSet = [x]
    if len(array) == 0:
        result = x, [x]
    else:
        for i in array:
            if x + i > bestSum and i > 0:
                bestSum = x + i
                bestSet = [x, i]
    return (bestSum, bestSet)

    return

if __name__ == "__main__":
    arr = [7,8,5]
    result = max_wis(arr)

    print("max_wis(", arr, "):\t", result)
    if result != (12, [7,5]):
        print("TEST FAILED\n")
    else:
        print("TEST PASSED\n")

    arr = [-1,8,10]
    result = max_wis(arr)

    print("max_wis(", arr, "):\t", result)
    if result != (10, [10]):
        print("TEST FAILED\n")
    else:
        print("TEST PASSED\n")

    arr = []
    result = max_wis(arr)

    print("max_wis(", arr, "):\t", result)
    if result != (0, []):
        print("TEST FAILED\n")
    else:
        print("TEST PASSED\n")

    arr = [-5,-1,-4]
    result = max_wis(arr)
    print("max_wis(", arr, "):\t", result)
    if result != (0, []):
        print("TEST FAILED\n")
    else:
        print("TEST PASSED\n")

    arr = [7,8,5]
    result = max_wis2(arr)

    print("max_wis2(", arr, "):\t", result)
    if result != (12, [7,5]):
        print("TEST FAILED\n")
    else:
        print("TEST PASSED\n")

    arr = [-1,8,10]
    result = max_wis2(arr)

    print("max_wis2(", arr, "):\t", result)
    if result != (10, [10]):
        print("TEST FAILED\n")
    else:
        print("TEST PASSED\n")

    arr = []
    result = max_wis2(arr)

    print("max_wis2(", arr, "):\t", result)
    if result != (0, []):
        print("TEST FAILED\n")
    else:
        print("TEST PASSED\n")

    arr = [-5,-1,-4]
    result = max_wis2(arr)

    print("max_wis2(", arr, "):\t", result)
    if result != (0, []):
        print("TEST FAILED\n")
    else:
        print("TEST PASSED\n")

    arr = [2,7,4,3,-9,8,6,5]
    result = max_wis(arr)
    print("max_wis(",arr,"):\t", result)
    print("PASS") if result == (23, [7, 3, 8, 5]) else print("FAIL")

