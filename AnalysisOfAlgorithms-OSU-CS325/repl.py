def replace(s, c, d):
    result = ""
    for i in s[:d]:
        if i == c:
            result += '0'
        else:
            result += i
    for i in s[d:]:
        result += i
    return result


if __name__ == "__main__":

    print("replace(\"banana\", b, 5)", replace("banana", 'b', 5))
    print("replace(\"turtle\", t, 1)", replace("turtle", 't', 1))
    print("replace(\"banaba\", b, 1)", replace("banaba", 'b', 1))
    print("replace(\"banaba\", b, 5)", replace("banaba", 'b', 5))
