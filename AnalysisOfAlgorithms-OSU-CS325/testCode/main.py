import random;

def arrayPrint(array):
    for i in range(len(array)):
        print(array[i]);

def sort(array):
    for j in range(len(array)):
        for i in range(len(array) - j - 1):
            if array[i] > array[i+1]:
                temp = array[i];
                array[i] = array[i+1];
                array[i+1] = temp;


arr = [];

for i in range(10):
    arr.append(random.randrange(10));

print("Before:");
arrayPrint(arr);

print("sort:")
sort(arr);

print("After:");
arrayPrint(arr);
