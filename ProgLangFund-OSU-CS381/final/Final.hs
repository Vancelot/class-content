module Final where

import Prelude hiding(foldr, (.), flip)

data Stack

data Cmd = Push Int | Add | If Cmd Cmd

empty :: Stack
empty = undefined
arity    :: Cmd -> Int
arity = undefined
execute  :: Cmd -> Stack -> Stack
execute = undefined
foldr    :: (a -> b -> b) -> b -> [a] -> b
foldr = undefined
flip     :: (a -> b -> c) -> b -> a -> c
flip = undefined
(.)      :: (b -> c) -> (a -> b) -> a -> c
(.) = undefined
