plays(alicia,piano).
plays(duke,piano).
plays(jimi,guitar).
plays(norah,guitar).
plays(norah,piano).
plays(yoyo,cello).

duet(X,Y) :- plays(X,Z), plays(Y,Z), X \= Y.
