module While where

data Expr
   = Get
   | Lit Int
   | Add Expr Expr
  deriving (Eq, Show)

data Test
   = LTE Expr Expr
  deriving (Eq, Show)

data Stmt
   = Set Expr
   | While Test Stmt
   | Begin [Stmt]
  deriving (Eq, Show)

-- Example program
--   begin
--     R := 1
--       while R <= 100
--         R + R

p :: Stmt
p = Begin
      [ Set (Lit 1)
      , While (LTE Get (Lit 100))
          (Set (Add Get Get))
      ]

--
--  Semantics
--

type Reg = Int

-- expr: Reg -> Int
-- test: Reg -> Bool
-- stmt: Reg -> Maybe Reg
--

expr :: Expr -> Reg -> Int
expr Get       s = s
expr (Lit i)   s = i
expr (Add l r) s = expr l s + expr r s

test :: Test -> Reg -> Bool
test (LTE l r) s = expr l s <= expr r s

stmt :: Stmt -> Reg -> Reg
stmt (Set e)     s = expr e s
stmt (While c b) s = if test c s then stmt (While c b) (stmt b s) else s
stmt (Begin ss)  s = stmts ss s -- fold (flip stmt) s ss
  where
    stmts []     r = r
    stmts (s:ss) r = stmts ss (stmt s r)
