-- vancek

module HW3 where

import Prelude hiding (Num)

--  num   ::=   (any natural number)  
--  var   ::=   (any variable name)   
--  macro   ::=   (any macro name)  
--          
--  prog  ::=   ε   |   cmd ; prog  sequence of commands
--          
--  mode  ::=   down   |   up   pen status
--          
--  expr  ::=   var   variable reference
--    |   num   literal number
--    |   expr + expr   addition expression
--          
--  cmd   ::=   pen mode  change pen mode
--    |   move ( expr , expr )  move pen to a new position
--    |   define macro ( var* ) { prog }    define a macro
--    |   call macro ( expr* )  invoke a macro
--
--  1. Encode the above grammar as a set of Haskell data types

type Num = Int
type Var = String
type Macro = String

type Prog = [Cmd]

data Mode = Down | Up
   deriving(Eq,Show)

data Expr = MakeVar Var
          | Num
          | Add Expr Expr
  deriving(Eq)

instance Show Expr where
  show (MakeVar v) = v

data Cmd = Pen Mode
         | Move (Expr, Expr)
         | Define Macro [Var] Prog
         | Call Macro [Var]
  deriving(Eq, Show)


--  2. Define a MiniLogo macro line (x1,y1,x2,y2) that (starting from anywhere
--  on the canvas) draws a line segment from (x1,y1) to (x2,y2).
--  
--  First, write the macro in MiniLogo concrete syntax (i.e. the notation
--  defined by the grammar and used in the example programs above). Include
--  this definition in a comment in your submission.
--
--  Second, encode the macro definition as a Haskell value using the data
--  types defined in Task 1. This corresponds to the abstract syntax of
--  MiniLogo. Your Haskell definition should start with something like line
--  = Define "line" ...

--  define line (x1, y1, x2, y2) {
--    pen up; move(x1, y1);
--    pen down; move(x2, y2);
--  }

line = Define "line" ["x1", "y1", "x2", "x3"] [Pen Up, Move (MakeVar "x1", MakeVar "y1"), Pen Down, Move(MakeVar "x2", MakeVar "y2")]


--  3. Use the line macro you just defined to define a new MiniLogo macro nix
--  (x,y,w,h) that draws a big “X” of width w and height h, starting from
--  position (x,y). Your definition should not contain any move commands.
--  
--      First, write the macro in MiniLogo concrete syntax and include this
--      definition in a comment in your submission.
--  
--      Second, encode the macro definition as a Haskell value, representing
--      the abstract syntax of the definition.
--  
--
--  define nix (x,y,w,h) {
--     line(x,y, x+w, y+h);
--     line(x, y+h, x+w, y);
--  }

nix = Define "nix" ["x", "y", "w", "h"] [Call "line" ["x", "y", "x+w", "y+h"], Call "line" ["x", "y+h", "x+w", "y"]]


--  4. Define a Haskell function steps :: Int -> Prog that constructs a
--  MiniLogo program that draws a staircase of n steps starting from (0,0). You
--  may assume that n ≥ 0.k

steps :: Int -> Prog
steps n
   | n > 0      = steps (n - 1)
   | otherwise  = [Call "line" ["n", "n", "n + 1", "n + 1"]]


--  5. Define a Haskell function macros :: Prog -> [Macro] that returns a list
--  of the names of all of the macros that are defined anywhere in a given
--  MiniLogo program. Don’t worry about duplicates—if a macro is defined more
--  than once, the resulting list may include multiple copies of its name.

macros :: Prog -> [Macro]
macros (Define m _ _:cs)  = [m] ++ macros cs
macros []                 = []


--  6. Define a Haskell function pretty :: Prog -> String that pretty-prints a
--  MiniLogo program. That is, it transforms the abstract syntax (a Haskell
--  value) into nicely formatted concrete syntax (a string of characters). Your
--  pretty-printed program should look similar to the example programs given
--  above; however, for simplicity you will probably want to print just one
--  command per line.

getArgs :: [Var] -> String
getArgs (v:[]) = v ++ getArgs []
getArgs (v:vs) = v ++ "," ++ getArgs vs
getArgs [] = ""

pretty :: Prog -> String
pretty (Define m vs p:cs)   = "define " ++ m ++ "(" ++ getArgs vs ++ ") " ++ "{ \n" ++ prettyT p ++ "}\n\n" ++ pretty cs
pretty (Pen m:cs)
               | m == Down  = "pen " ++ "down" ++ ";\n" ++ pretty cs
               | m == Up    = "pen " ++ "up" ++ ";\n" ++ pretty cs
pretty (Move (e0, e1):cs)   = "move(" ++ show e0 ++ ", " ++ show e1 ++ ");\n" ++ pretty cs
pretty (Call m vs:cs)       = m ++ "(" ++ getArgs vs ++ ");\n" ++ pretty cs
pretty []                   = []

prettyT :: Prog -> String
prettyT (c:cs) = "\t" ++ pretty [c] ++ prettyT cs
prettyT [] = ""
