--  vancek

module HW3 where

import MiniMiniLogo
import Render


--
-- * Semantics of MiniMiniLogo
--

-- NOTE:
--  * MiniMiniLogo.hs defines the abstract syntax of MiniMiniLogo and some
--    functions for generating MiniMiniLogo programs. It contains the type
--    definitions for Mode, Cmd, and Prog.
--  * Render.hs contains code for rendering the output of a MiniMiniLogo
--    program in HTML5. It contains the types definitions for Point and Line.

-- | A type to represent the current state of the pen.
type State = (Mode,Point)

-- | The initial state of the pen.
start :: State
start = (Up,(0,0))

-- | A function that renders the image to HTML. Only works after you have
--   implemented `prog`. Applying `draw` to a MiniMiniLogo program will
--   produce an HTML file named MiniMiniLogo.html, which you can load in
--   your browswer to view the rendered image.
draw :: Prog -> IO ()
draw p = let (_,ls) = prog p start in toHTML ls


-- Semantic domains:
--   * Cmd:  State -> (State, Maybe Line)
--   * Prog: State -> (State, [Line])


-- | Semantic function for Cmd.
--   
--   >>> cmd (Pen Down) (Up,(2,3))
--   ((Down,(2,3)),Nothing)
--
--   >>> cmd (Pen Up) (Down,(2,3))
--   ((Up,(2,3)),Nothing)
--
--   >>> cmd (Move 4 5) (Up,(2,3))
--   ((Up,(4,5)),Nothing)
--
--   >>> cmd (Move 4 5) (Down,(2,3))
--   ((Down,(4,5)),Just ((2,3),(4,5)))
--
cmd :: Cmd -> State -> (State, Maybe Line)
cmd (Pen mo) (m,p) = ((mo, p), Nothing)
cmd (Move x y) (m,p)
  | m == Up = ((m, (x,y)), Nothing)
  | m == Down = ((m, (x,y)), Just (p, (x,y)))


-- | Semantic function for Prog.
--
--   >>> prog (nix 10 10 5 7) start
--   ((Down,(15,10)),[((10,10),(15,17)),((10,17),(15,10))])
--
--   >>> prog (steps 2 0 0) start
--   ((Down,(2,2)),[((0,0),(0,1)),((0,1),(1,1)),((1,1),(1,2)),((1,2),(2,2))])
--
                       
prog :: Prog -> State -> (State, [Line])
prog p s = (prog' p (s, []))

prog' :: Prog -> (State, [Line]) -> (State, [Line])
prog' [] x = x
prog' (c:cs) (s,ls) = let (ns, nl) = cmd c s in
                         case nl of
                            Just nl -> prog' cs (ns, ls ++ [nl])
                            Nothing -> prog' cs (ns, ls)

--
-- * Extra credit
--

-- | This should be a MiniMiniLogo program that draws an amazing picture.
--   Add as many helper functions as you want.
--
--   >>> draw amazing
amazing :: Prog
amazing = [Pen Up, Move 0   40, Pen Down, Move 11 20, Move 0  0] ++
          [Pen Up, Move 1   40, Pen Down, Move 12 20, Move 1  0] ++
          [Pen Up, Move 2   40, Pen Down, Move 13 20, Move 2  0] ++
          [Pen Up, Move 3   40, Pen Down, Move 14 20, Move 3  0] ++
          [Pen Up, Move 4   40, Pen Down, Move 15 20, Move 4  0] ++
          [Pen Up, Move 7   40, Pen Down, Move 18 20, Move 7  0] ++n
          [Pen Up, Move 18  20, Pen Down, Move 29 0] ++
          [Pen Up, Move 8   40, Pen Down, Move 19 20, Move 8  0] ++
          [Pen Up, Move 19  20, Pen Down, Move 30 0] ++
          [Pen Up, Move 9   40, Pen Down, Move 20 20, Move 9  0] ++
          [Pen Up, Move 20  20, Pen Down, Move 31 0] ++
          [Pen Up, Move 10  40, Pen Down, Move 21 20, Move 10 0] ++
          [Pen Up, Move 21  20, Pen Down, Move 32 0] ++
          [Pen Up, Move 11  40, Pen Down, Move 22 20, Move 11 0] ++
          [Pen Up, Move 22  20, Pen Down, Move 33 0] ++
          [Pen Up, Move 22  25, Pen Down, Move 80 25] ++
          [Pen Up, Move 23  24, Pen Down, Move 80 24] ++
          [Pen Up, Move 24  23, Pen Down, Move 80 23] ++
          [Pen Up, Move 25  22, Pen Down, Move 80 22] ++
          [Pen Up, Move 26  21, Pen Down, Move 80 21] ++
          [Pen Up, Move 22  25, Pen Down, Move 26 21] ++
          [Pen Up, Move 26  17, Pen Down, Move 80 17] ++
          [Pen Up, Move 27  16, Pen Down, Move 80 16] ++
          [Pen Up, Move 28  15, Pen Down, Move 80 15] ++
          [Pen Up, Move 29  14, Pen Down, Move 80 14] ++
          [Pen Up, Move 30  13, Pen Down, Move 80 13] ++
          [Pen Up, Move 26  17, Pen Down, Move 30 13]
