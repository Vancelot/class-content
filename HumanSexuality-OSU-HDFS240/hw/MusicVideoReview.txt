Video 1:	Phantogram - You Don't Get Me High Anymore
	url:		https://www.youtube.com/watch?v=jryzEU7WAlg
	genre:	Electronic rock

Video 2:	PRXZM - Haze
	url:		https://www.youtube.com/watch?v=cr7WOA3-Mk8
	genre:	EDM

Video 3:	Arctic Monkeys - Why'd You Only Call Me When You're High?
	url:		https://www.youtube.com/watch?v=6366dxFf-Os
	genre:	Rock


1. Compare and contrast how sex, love, and intimacy in later life were
depicted in each video.

	The Phantogram video seemed to be describing a relationship that had
	lost it's sense of excitement. A relationship that was exciting at
	first, but was no longer satisfying. According to Carrol (2014), "Most
	long-term relationships often start out high on passion, but this
	decreases as time passes". The videos by PRXZM and Arctic Monkeys seemed
	to be describing a past relationship that the artist regrets losing or
	wishes to revisit. In both the Phantogram and PRXZM video; love, sex,
	and intimacy were likened to a drug. All three videos depict the
	difficulties of unrequited feelings between two people.


2. In what ways do these videos reinforce or deconstruct sex, love, and
intimacy in later life?

	The Phantogram video reinforces the idea that passion or intimacy can
	fade over time. With their analogies about love and drugs; the
	Phantogram and PRXZM videos deconstruct the idea that love is all
	sunshine and rainbows. Relationships can be exhilarating and exciting,
	but they can also be painful, complex, and difficult. None of these
	videos painted love in later life as positive. They all revolved around
	the struggles of being, or having been in love.


3. What is your opinion on how these videos hinder or enhance efforts to
sex, love, and intimacy in later life?

	Although the tones of these three videos were less than positive, I
	think these videos have a positive impact on views surrounding sex,
	love, and intimacy in later life. There is a lot of media that paints
	relationships as entirely positive. When that is all one is exposed to,
	it can become easy to expect that from your life and relationships, but
	that isn't realistic. Sex, love, and intimacy are rarely clean and
	straightforward. Real life is much more complex and often difficult and
	understanding that can help be prepared for the best and worst intimacy
	has to offer.
