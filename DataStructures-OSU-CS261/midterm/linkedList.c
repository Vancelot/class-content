#include"header.h"

void initDList(DblList *dl)
{
	assert(dl);

	dl->frontSentinel = (Dnode*)malloc(sizeof(Dnode));
	assert(dl->frontSentinel != 0);
	dl->frontSentinel->prev = NULL;

	dl->backSentinel = (Dnode*)malloc(sizeof(Dnode));
	assert(dl->backSentinel != 0);
	dl->backSentinel->next = NULL;

	dl->frontSentinel->next = dl->backSentinel;
	dl->backSentinel->prev = dl->frontSentinel;
}
