#ifndef __HEADER__
#define __HEADER__

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<time.h>
#include<ctype.h>

#define TYPE double

# ifndef LT
# define LT(A, B) ((A) < (B))
# endif

# ifndef EQ
# define EQ(A, B) ((A) == (B))
# endif

/***DOUBLY LINKED LIST***/
typedef struct Dnode{
	TYPE value;
	struct Dnode *next, *prev;
} Dnode;

typedef struct DblList{
	Dnode *frontSentinel;
	Dnode *backSentinel;
} DblList;

void initDList(DblList*);

/***QUEUE***/

/*Queue: Doubly Linked List*/
typedef struct DblListQueue{
	DblList *dl;
	int size;
} DblListQueue;

void initDblListQueue(DblListQueue*);
void addDblListQueue(DblListQueue*, TYPE);
void remDblListQueue(DblListQueue*);
int isEmptyDblListQueue(DblListQueue*);
void printDblListQueue(DblListQueue*);

#endif
