#include"header.h"

void initDblListQueue(DblListQueue *q)
{
	assert(q);
	q->dl = (DblList*)malloc(sizeof(DblList));
	initDList(q->dl);
}

void addDblListQueue(DblListQueue *q, TYPE val)
{
	Dnode *newNode = (Dnode*)malloc(sizeof(Dnode));

	newNode->value = val;
	newNode->next = q->dl->frontSentinel->next;
	newNode->next->prev = newNode;
	newNode->prev = q->dl->frontSentinel;
	q->dl->frontSentinel->next = newNode;
	if(newNode->next == q->dl->backSentinel){
		q->dl->backSentinel->prev = newNode;
	}
	q->size++;
}

void remDblListQueue(DblListQueue *q)
{
	Dnode *temp = q->dl->backSentinel->prev;

	temp->next->prev = temp->prev; 
	temp->prev->next = temp->next; 
	free(temp);
}

int isEmptyDblListQueue(DblListQueue *q)
{
}

void printDblListQueue(DblListQueue *q)
{
	int i = 0;
	Dnode *temp = q->dl->frontSentinel->next;

	while(temp != q->dl->backSentinel)
	{
		printf("Queue @ %d: %.2f\n", i, temp->value);
		i++;
		temp = temp->next;
	}
	putchar('\n');
}
