#ifndef __HEADER__
#define __HEADER__

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

#define TYPE double

typedef struct Node{
	TYPE val;
	Node *left;
	Node *right;
} BSTNode;

typedef struct BST{
	BSTNode *root;
	int height;
	int size;
} BST;

void initBST(BST *tree);
void addBST(BST*, TYPE);
BSTNode* addBSTNode(BSTNode*, TYPE);
void sumTree(BST*, TYPE*);
void sumNodes(BSTNode*, TYPE*);

#endif
