#include"header.h"

int stringHash1(char * str)
{
	int i;
	int r = 0;
	for (i = 0; str[i] != '\0'; i++)
		r += str[i];
	return r;
}

int stringHash2(char * str)
{
	int i;
	int r = 0;
	for (i = 0; str[i] != '\0'; i++)
		r += (i+1) * str[i]; /*the difference between 1 and 2 is on this line*/
	return r;
}

void initHashTable(HashTable *ht, int tableSize)
{
	int index;
	assert(ht);
	ht->table = (HashNode**)malloc(sizeof(HashNode*) * tableSize);
	assert(ht->table);
	ht->tableSize = tableSize;
	ht->count = 0;
	for(index = 0; index < tableSize; index++){
		ht->table[index] = NULL;
	}
}

void freeHashTable(HashTable *ht)
{
	int i = 0;
	HashNode *cur = NULL, *next = NULL;

	for(i = 0; i < ht->tableSize; i++)
	{
		cur = ht->table[i];
		while(cur)
		{
			next = cur->next;
			if(cur->key){free(cur->key);}
			if(cur){free(cur);}
			cur = next;
		}
	}
	if(ht->table){free(ht->table);}
	if(ht){free(ht);}
}

void addHashTable(HashTable * ht, KEYTYPE k, VALTYPE v)
{
	int hash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
	HashNode *cur = NULL, *newLnk = (HashNode*)malloc(sizeof(HashNode));

	assert(newLnk);
	newLnk->next = NULL;
	newLnk->key = k;
	newLnk->value = v;
	ht->count++;

	cur = ht->table[hash % ht->tableSize];
	if(!cur){ht->table[hash % ht->tableSize] = newLnk;}
	else
	{
		while(cur->next){cur = cur->next;}
		cur->next = newLnk;
	}
}

VALTYPE* valAtHashTable(HashTable * ht, KEYTYPE k)
{
	int hash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
	VALTYPE *result = NULL;
	HashNode *pnt = ht->table[hash % ht->tableSize];

	while(pnt != NULL)
	{
		if(strcmp(pnt->key, k) == 0){
			result = &pnt->value;
			break;
		}
		pnt = pnt->next;
	}
	return result;
}

int containsKeyHashTable(HashTable * ht, KEYTYPE k)
{
	int curHash = 0, hash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
	int result = 0;
	HashNode *cur = ht->table[hash % ht->tableSize];

	while(cur)
	{
		curHash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
		if(hash == curHash){
			result = 1;
			break;
		}
		cur = cur->next;
	}
	return result;
}

void remKeyHashTable(HashTable * ht, KEYTYPE k)
{
	int curHash = 0, hash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
	HashNode *cur, *prev;
	prev = cur = ht->table[hash % ht->tableSize];

	if(cur){
		curHash = HASHING_FUNCTION == 1 ? stringHash1(cur->key) : stringHash2(cur->key);
	}
	if(hash == curHash)
	{
		ht->table[hash % ht->tableSize] = cur->next;
		if(cur->key){free(cur->key);}
		if(cur){free(cur);}
	}
	else 
	{
		while(cur)
		{
			curHash = HASHING_FUNCTION == 1 ?
				stringHash1(cur->key) : stringHash2(cur->key);
			if(hash == curHash)
			{
				prev->next = cur->next;
				if(cur->key){free(cur->key);}
				if(cur){free(cur);}
				break;
			}
			prev = cur;
			cur = cur->next;
		}
	}
}

int sizeHashTable(HashTable *ht){return ht->count;}

int capHashTable(HashTable *ht){return ht->tableSize;}

int emptyBuckHashTable(HashTable *ht)
{
	int i = 0, result = 0;
	for(i = 0; i < ht->tableSize; i++){
		if(!ht->table[i]){result++;}
	}
	return result;
}

float loadFactHashTable(HashTable *ht)
{
	float result = (float)ht->count / ht->tableSize;
	return result;
}

void printHashTable(HashTable *ht)
{
	int i = 0;
	HashNode *node = NULL;

	assert(ht);

	for(i = 0; i < ht->tableSize; i++)
	{
		node = ht->table[i];
		while(node){
			printf("%s: %d\n\n", node->key, node->value);
			node = node->next;
		}
	}
	printf("\n\n");
}

void printDetHashTable(HashTable *ht)
{
	int i = 0;
	HashNode *node = NULL;

	assert(ht);

	for(i = 0; i < ht->tableSize; i++)
	{
		node = ht->table[i];
		if(node){printf("\n\n");}
		while(node){
			printf("[ %s:%d ]->", node->key, node->value);
			node = node->next;
		}
	}
	printf("\n\n");
}
