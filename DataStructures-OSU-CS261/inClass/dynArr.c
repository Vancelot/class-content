#include"header.h"


void initDynArr(DynArr *da, int cap)
{
	assert(cap >= 0);
	da->capacity = cap;
	da->size = 0;
	da->data = (TYPE*) malloc(da->capacity * sizeof(TYPE));
	assert(da->data != 0);
}

void freeDynArr(DynArr *da)
{
	assert(da != NULL);
	free(da->data);
}

void addDynArr(DynArr *da, TYPE val)
{
	if(da->size >= da->capacity){
		_dynArrDoubleCap(da);
	}
	da->data[da->size] = val;
	da->size++;
}

void remDynArr(DynArr *da, TYPE val);

void _dynArrDoubleCap(DynArr *da)
{
	TYPE *oldbuffer = da->data;
	int oldsize = da->size;

	initDynArr(da, 2 * da->capacity);
	int i = 0;
	for(int i = 0; i < oldsize; i++){
		da->data[i] = oldbuffer[i];
	}
	da->size = oldsize;
	free(oldbuffer);
}

