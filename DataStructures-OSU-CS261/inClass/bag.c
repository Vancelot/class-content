#include"header.h"

void dblListBagSort(DblListBag *db)
{
	Dnode *dNode1 = db->list->frontSentinel, *dNode2 = NULL;
	TYPE temp = 0;

	while(dNode1->next != db->list->backSentinel->prev)
	{
		dNode1 = dNode1->next;
		dNode2 = dNode1;
		while(dNode2 != db->list->backSentinel)
		{
			dNode2 = dNode2->next;
			if(LT(dNode2->value, dNode1->value))
			{
				temp = dNode1->value;
				dNode1->value = dNode2->value;
				dNode2->value = temp;
			}
		}
	}
}

void addBag(struct Bag *b, TYPE val)
{
	/*Check if initialized*/
	assert(b != NULL);

	/*Check if enough memory*/
	if(b->size >= MAX_SIZE){
		return;
	}

	b->data[b->size] = val;

	/*Increment size*/
	b->size++; 
}

void removeBag(struct Bag *b, TYPE val)
{
	/*Check if initialized*/
	assert(b != NULL);

	/*Start at end of bag and work down*/
	int i = b->size-1;
	for(i = 0; i >= 0; i--)
	{
		if(b->data[i] == val)
		{
			/*Found, remove*/
			/*Bags are unordered. If structure is unordered just move last element to gap*/
			b->data[i] = b->data[b->size-1];
			b->size--;
			return;
		}
	}
}

void removeListBag(ListBag *b, TYPE val)
{
	Node *current = b->next;
	Node *previous = b->sentinel;
	while(b->next != NULL)
	{
		if(current->value == val){
			previous->next = current->next;
			free(current);
			break;
		}
		else{
			previous = current;
			current = current->next;
		}
	}
}
