#include"header.h"

void initList(List *ls)
{
	Node *sent = (Node*)malloc(sizeof(Node));

	assert(ls != NULL);
	assert(sent != NULL);

	sent->next = NULL;
	ls->sentinel = sent;
	ls->sentinel->value = -1;
}

void addList(List *ls, TYPE val)
{
	Node *newNode = (Node*)malloc(sizeof(Node));

	assert(ls != NULL);
	assert(ls->sentinel != NULL);
	newNode->value = val;
	newNode->next = ls->sentinel->next;
	ls->sentinel->next = newNode;
}

void remList(List *ls)
{
	Node *node = ls->sentinel->next;

	if(node){
		ls->sentinel->next = node->next;
		free(node);
	}
}

void printList(List *ls)
{
	int i = 0;
	Node *node = ls->sentinel->next;

	assert(node);
	while(node != NULL){
		i++;
		printf("List at sentinel+%d %7.2f\n", i, node->value);
		node = node->next;
	}
}

void pushListStack(ListStack *lsStk, TYPE val)
{
	Node *newLink = (Node*)malloc(sizeof(Node));

	assert(newLink != 0);
	newLink->value = val;
	newLink->next = lsStk->sentinel->next;
	lsStk->sentinel = newLink;
}

void popListStack(ListStack *lsStk)
{
	Node *lnk = lsStk->sentinel->next;

	if(lnk != NULL){
		lsStk->sentinel->next = lnk->next;
	}
	free(lnk);
}

void initDList(DblList *dl)
{
	assert(dl);

	dl->frontSentinel = (Dnode*)malloc(sizeof(Dnode));
	assert(dl->frontSentinel != 0);
	dl->frontSentinel->prev = NULL;

	dl->backSentinel = (Dnode*)malloc(sizeof(Dnode));
	assert(dl->backSentinel != 0);
	dl->backSentinel->next = NULL;

	dl->frontSentinel->next = dl->backSentinel;
	dl->backSentinel->prev = dl->frontSentinel;
}
