#ifndef __HEADER__
#define __HEADER__

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<time.h>
#include<ctype.h>
#include<string.h>

#define TYPE double
#define MAX_SIZE 100
#define MAX_LOAD 20
#define KEYTYPE char*
#define VALTYPE int
# define HASHING_FUNCTION 1

# ifndef LT
# define LT(A, B) ((A) < (B))
# endif

# ifndef GT
# define GT(A, B) ((A) > (B))
# endif

# ifndef EQ
# define EQ(A, B) ((A) == (B))
# endif

int binarySearch(TYPE *data, int size, TYPE val);
void printDat(TYPE*, int);

/***LINKED LIST***/
typedef struct Node{
	TYPE value;
	struct Node *next;
} Node;

typedef struct List{
	Node *sentinel;
} List;

void initList(List *);
void addList(List *, TYPE);
void remList(List *);
void printList(List *);

/***DOUBLY LINKED LIST***/
typedef struct Dnode{
	TYPE value;
	struct Dnode *next, *prev;
} Dnode;

typedef struct DblList{
	Dnode *frontSentinel;
	Dnode *backSentinel;
} DblList;

void initDList(DblList*);

/***HASH TABLE***/
typedef struct Elem{
	KEYTYPE key;
	VALTYPE value;
} Elem;

typedef struct HashNode{
   KEYTYPE key;
   VALTYPE value;
   struct HashNode *next;
} HashNode;

typedef struct HashTable{
	HashNode **table;
	int count;
	int tableSize;
} HashTable;

int stringHash1(char*);
int stringHash2(char*);

void initHashTable(HashTable*, int);
void addHashTable(HashTable*, KEYTYPE, VALTYPE);
void remKeyHashTable(HashTable*, KEYTYPE);
int containsKeyHashTable(HashTable*, KEYTYPE);
int sizeHashTable(HashTable *ht);
int capHashTable(HashTable*);
int emptyBuckHashTable(HashTable*);
float loadFactHashTable(HashTable*);
void printHashTable(HashTable*);
void printDetHashTable(HashTable*);
void freeHashTable(HashTable*);
VALTYPE* valAtHashTable(HashTable*, KEYTYPE);
int _resizeHashTable(HashTable*);

/***DEQUE***/
typedef struct Deque{			/*backIndex = (start + size) % capacity*/
	TYPE *data;
	int capacity;
	int size;
	int start;
} Deque;

void initDeque(Deque*, int);
void _doubleDeqCap(Deque*);
void addFrontDeque(Deque*, TYPE);
void addBackDeque(Deque*, TYPE);
TYPE frontDeque(Deque*);
TYPE backDeque(Deque*);
void remFrontDeque(Deque*);
void remBackDeque(Deque*);
int isEmptyDeque(Deque*);
void printDeque(Deque*);

/*Deque: Linked List*/
typedef struct ListDeque{
	int size;
	Node *head;
	Node *tail;
} ListDeque;

/***QUEUE***/
typedef struct Queue{			/*backIndex = (start + size) % capacity*/
	TYPE *data;
	int capacity;
	int size;
	int start;
} Queue;

void initQueue(Queue*, int);
void addQueue(Queue*, TYPE);
void remQueue(Queue*);
TYPE frontQueue(Queue*);
int isEmptyQueue(Queue*);
void _doubleQueCap(Queue*);

/*Queue: Doubly Linked List*/
typedef struct DblListQueue{
	DblList *dl;
	int size;
} DblListQueue;

void initDblListQueue(DblListQueue*);
void addDblListQueue(DblListQueue *, TYPE);
void remDblListQueue(DblListQueue *);
int isEmptyDblListQueue(DblListQueue *);

/***BAG***/
/*Bag: Doubly Linked List*/
typedef struct DblListBag{
	TYPE value;
	DblList *list;
} DblListBag;

void dblListBagSort(DblListBag*);

/*Bag*/
typedef struct Bag{
	TYPE data[MAX_SIZE];
	int size;
} Bag;

typedef struct ListBag{
	Node *sentinel, *next;
} ListBag;

/***DYNAMIC ARRAY***/
typedef struct DynArr{
	TYPE *data;
	int size;
	int capacity;
} DynArr;

void initDynArr(DynArr*, int);
void freeDynArr(DynArr*);
void addDynArr(DynArr*, TYPE);
void remDynArr(DynArr*, TYPE);
void _dynArrDoubleCap(DynArr*);

typedef struct Stack{
	TYPE *data;
	int capacity;
} Stack;

typedef struct ListStack{
	Node *sentinel;
} ListStack;

/***TREE***/

/***BST - Binary Search Tree***/
typedef struct BSTNode{
	TYPE val;
	struct BSTNode *left;
	struct BSTNode *right;
} BSTNode;

typedef struct BST{
	BSTNode *root;
	int size;
} BST;

void initBST(BST*);
int containsBST(BST*, TYPE);
void addBST(BST*, TYPE);
BSTNode* _addBSTNode(BSTNode*, TYPE);
void remBST(BST*, TYPE);
int sizeBST(BST*);
void preorderBST(BSTNode*);
void postorderBST(BSTNode*);
void inorderBST(BSTNode*);
int equivalenceBST(BST*, BST*);
int equivalenceBSTNode(BSTNode*, BSTNode*);

/***AVL - Adelson-Velskii and Landis***/
typedef struct AVLNode{
	TYPE val;
	struct AVLNode *left;
	struct AVLNode *right;
	int height;
} AVLNode;

typedef struct AVLTree{
	AVLNode *root;
	int size;
} AVLTree;

void initAVL(AVLTree*);
int _heightAVL(AVLNode*);
void _setHeightAVL(AVLNode*);
AVLNode* balanceAVL(AVLNode*);
int containsAVL(AVLTree*, TYPE);
AVLNode* addAVL(AVLNode*, TYPE);
BSTNode* _addAVLNode(AVLNode*, TYPE);
AVLNode* remNodeAVL(AVLNode*, TYPE);
AVLNode* remAllAVL(AVLTree*, AVLNode*, TYPE);
void remTreeAVL(AVLTree*, TYPE);
int sizeAVL(AVLTree*);
void preorderAVL(AVLNode*);
void postorderAVL(AVLNode*);
void inorderAVL(AVLNode*);

AVLNode* _rotLeftAVL(AVLNode*);
AVLNode* _rotRightAVL(AVLNode*);

/***HEAP***/

/***PRIORITY QUEUE***/
typedef struct DynArrHeap{
	TYPE *data;
	int size;
	int capacity;
} DynArrHeap;

void addDynArrHeap(DynArrHeap*, TYPE);
TYPE getFirstDynArrHeap(DynArrHeap*);
void _adjustDynArrHeap(DynArrHeap*, int, int);
void remMinDynArrHeap(DynArrHeap*);
void initDynArrHeap(DynArrHeap*, int);
void _dynArrHeapDoubleCap(DynArrHeap*);
int _minIdxDynArrHeap(DynArrHeap*, int, int);
void _swapDynArrHeap(DynArrHeap*, int, int);

/***LIST STACK***/
void initListStack(ListStack*);
void pushListStack(ListStack*, TYPE);
void popListStack(ListStack*);
/***BAG***/
void initBag(struct Bag *b);
void addBag(struct Bag *b, TYPE val);
int containsBag(struct Bag *b, TYPE val);
void removeBag(struct Bag *b, TYPE val);
/***LIST BAG***/
/***STACK***/
void initStack(DynArr*, int);
void pushStack(DynArr*, TYPE);
TYPE* topStack(DynArr*);
void popStack(DynArr*);

/***GRAPH***/


#endif
