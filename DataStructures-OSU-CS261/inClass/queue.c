#include"header.h"

void initQueue(Queue *q, int cap)
{
	int i = 0;
	assert(cap > 0);
	q->size = q->start = 0;
	q->capacity = cap;
	q->data = (TYPE*) malloc(cap * sizeof(TYPE));
	assert(q->data != 0);
	for(i = 0; i < q->capacity; i++){
		q->data[i] = 0;
	}
}

void addQueue(Queue *q, TYPE val)
{
	if(q->size == q->capacity){
		_doubleQueCap(q);
	}
	q->data[(q->start + q->size) % q->capacity] = val;
	q->size++;
}

void remQueue(Queue *q)
{
	q->data[q->start] = 0;
	q->start++;
	if(q->start > q->capacity){
		q->start -= q->capacity;
	}
	q->size--;
}

void _doubleQueCap(Queue *q)
{
	int i = 0;

	int oldCap = q->capacity;
	int oldSize = q->size;
	int oldStart = q->start;
	TYPE *oldDat = q->data;

	initQueue(q, 2 * q->capacity);

	q->capacity = 2 * oldCap;
	q->size = oldSize;
	q->start = 0;
	for(i = 0; i < q->capacity; i++){
		q->data[i] = oldDat[(oldStart + i) % oldCap];
	}
	free(oldDat);
}

/***Doubly Linked List***/
void initDblListQueue(DblListQueue *q)
{
	assert(q);
	initDList(q->dl);
}

void addDblListQueue(DblListQueue *q, TYPE val)
{
	Dnode *newNode = (Dnode*)malloc(sizeof(Dnode));
	Dnode *temp = q->dl->frontSentinel;

	newNode->value = val;
	newNode->next = q->dl->frontSentinel->next;
	newNode->prev = q->dl->frontSentinel;
	q->dl->frontSentinel->next = newNode;
	if(newNode->next == q->dl->backSentinel){
		q->dl->backSentinel->prev = newNode;
	}
}

void remDblListQueue(DblListQueue *q)
{
	Dnode *temp = q->dl->backSentinel->prev;

	q->dl->backSentinel->prev = temp->prev;
	temp->prev->next = q->dl->backSentinel;
	free(temp);
}

int isEmptyDblListQueue(DblListQueue *q)
{
}
