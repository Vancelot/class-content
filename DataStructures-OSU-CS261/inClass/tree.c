#include "header.h"

/***BST***/
void initBST(BST *tree)
{
	assert(tree);
	tree->root = (BSTNode*)malloc(sizeof(BSTNode));
}

BSTNode* _addBSTNode(BSTNode *node, TYPE val)
{
	if(node == 0)
	{
		node = (BSTNode*)malloc(sizeof(BSTNode));
		assert(node != 0);
		node->val = val;
		node->left = node->right = NULL;
	}
	else
	{
		if(val < node->val){
			node->left = _addBSTNode(node->left, val);
		}
		else{
			node->right = _addBSTNode(node->left, val);
		}
	}
	return node;
}

void addBST(BST *tree, TYPE val)
{
	assert(tree);
	tree->root = _addBSTNode(tree->root, val);
	tree->size++;
}

void preorderBST(BSTNode *node)
{
	if(node != 0)
	{
		printf("Value: %.2f\n", node->val);
		preorderBST(node->left);
		preorderBST(node->right);
	}
}

void postorderBST(BSTNode *node)
{
	if(node != 0)
	{
		postorderBST(node->left);
		postorderBST(node->right);
		printf("Value: %.2f\n", node->val);
	}
}

void inorderBST(BSTNode *node)
{
	if(node != 0)
	{
		inorderBST(node->left);
		printf("Value: %.2f\n", node->val);
		inorderBST(node->right);
	}
}

int containsBST(BST *tree, TYPE val);
void remBST(BST *tree, TYPE val);
int sizeBST(BST *tree);

int equivalenceBST(BST *tree1, BST*tree2)
{
	int result = 0;
	assert(t1 && t2);
	result = tree1->size == tree2->size ? equivalenceBSTNode(tree1->root, tree2->root) : 0;
	return result;
}

int equivalenceBSTNode(BSTNode *node1, BSTNode *node2)
{
	int result = 0;

	if(!node1 && !node2){ result = 1; }
	else if(node1 && node2)
	{
		result = (EQ(node1->val, node2->val)
			* equivalenceBSTNode(node1->left, node2->left) 
			* equivalenceBSTNode(node1->right, node2->right) 
		);
	}

	return result;
}

/***AVL***/
int _heightAVL(AVLNode *node)
{
	int result = !node ? -1 : node->height;
	return result;
}

void _setHeightAVL(AVLNode *node)
{
	int lh = _heightAVL(node->left), rh = _heightAVL(node->right);
	node->height = lh < rh ? 1 + rh : 1 + lh;
}

AVLNode* _rotLeftAVL(AVLNode *node)
{
	AVLNode *top = node->right;
	node->right = top->left;
	top->left = node;
	_setHeightAVL(node);
	_setHeightAVL(top);

	return top;
}

AVLNode* _rotRightAVL(AVLNode *node)
{
	AVLNode *top = node->left;
	node->right = top->right;
	top->right= node;
	_setHeightAVL(node);
	_setHeightAVL(top);

	return top;
}

AVLNode* balanceAVL(AVLNode *node)
{
	int rotation = _heightAVL(node->right) - _heightAVL(node->left), drotation = 0;

	if(rotation < -1)
	{
		drotation = _heightAVL(node->left->right) - _heightAVL(node->left->left);
		if(drotation > 0){ node->left = _rotLeftAVL(node->left); }
		else if(drotation < 0) node->right = _rotLeftAVL(node->right);
	}
	else if(rotation > 1)
	{

	}
	_setHeightAVL(node);
	return node;
}

AVLNode* addAVL(AVLNode *node, TYPE val)
{
	AVLNode *newNode = NULL;
	if(!node)
	{
		newNode = (AVLNode*)malloc(sizeof(AVLNode));
		newNode->left = NULL;
		newNode->right = NULL;
		newNode->val = val;
	}
	else
	{
		if(LT(val, node->val)){ node->left = addAVL(node->left, val); }
		else{ node->right = addAVL(node->right, val); }
	}
	return balanceAVL(node);
}

AVLNode* remNodeAVL(AVLNode *node, TYPE val)
{
	AVLNode *temp;
	assert(node);
	if(EQ(val, node->val))
	{
		/*TODO replace current with leftmost descendent of right child*/
		temp = node->right;
		while(temp->left){ temp = temp->left; }
		node->val = temp->val;
		free(temp);
	}
	else if(LT(val, node->val)){ node->left = remNodeAVL(node->left, val); }
	else { node->right = remNodeAVL(node->right, val); }

	return balanceAVL(node);
}

void remTreeAVL(AVLTree *tree, TYPE val)
{
	if(containsAVL(tree, val)){
		tree->root = remNodeAVL(tree->root, val);
		tree->size++;
	}
}

AVLNode* remAllAVL(AVLTree *tree, AVLNode *node, TYPE val)
{
	AVLNode *temp = NULL;
	if(node == NULL){ return NULL; }
	while EQ(val, node->val)
	{
		if(!node->right)
		{
			/*
			node->val = leftMostAVL(node->right);
			node->right = removeLeftMostAVL(node->right);
			tree->size--;
			*/
		}
		else
		{

		}
	}
	if(LT(val, node->val)){ remAllAVL(tree, node->left, val); }
	else{ remAllAVL(tree, node->right, val); }
	return balanceAVL(node);
}


void initAVL(AVLTree *tree);
int containsAVL(AVLTree *tree, TYPE val);
int sizeAVL(AVLTree *tree);
void preorderAVL(AVLNode *node);
void postorderAVL(AVLNode *node);
void inorderAVL(AVLNode *node);
