#include"header.h"

int main(int argc, char **argv)
{
	int choice = 0, i = 0, size = 3;
	if(argc <= 1 || !isdigit(*argv[1])){
		printf("Choose demo:\n1.) Linked List\t2.) Deque\n");
		scanf("%d", &choice);
	}
	else{
		choice = atoi(argv[1]);
	}
	switch(choice)
	{
		case 1:
		{
			List *linkedList = (List*)malloc(sizeof(List));

			printf("\nLINKED LIST DEMO\n");
			initList(linkedList);
			printf("%2cADD\n", ' ');
			for(i = 0; i < size; i++)
			{
				printf("Add %.2f:\n", (double)i);
				addList(linkedList, i);
				printList(linkedList);
				putchar('\n');
			}
			printf("%2cREMOVE\n", ' ');
			for(i = 0; i < size; i++)
			{
				printf("Remove:\n");
				printList(linkedList);
				remList(linkedList);
				putchar('\n');
			}
			break;
		}
		case 2:
		{
			Deque *deq = (Deque*)malloc(sizeof(Deque));

			initDeque(deq,size);

			printf("\nDEQUE DEMO\n");
			printf("%2cADD\n", ' ');
			for(i = 0; i < size; i++)
			{
				printf("Add %.2f to front, %.2f to back:\n", (double)i, (double)i + 1);
				addFrontDeque(deq, i);
				addBackDeque(deq, i + 1);
				printDeque(deq);
			}

			printf("%2cREMOVE\n", ' ');
			printDeque(deq);
			for(i = 0; i < size; i++)
			{
				printf("Remove from front:\n");
				remFrontDeque(deq);
				i++;
				printDeque(deq);
				printf("Remove from back:\n");
				remBackDeque(deq);
				printDeque(deq);
			}
			break;
		}
	}

	return 0;
}

void printDat(TYPE *arr, int n)
{
	int i = 0;
	for(i = 0; i < n; i++){
		printf("Arr[%d]: %.2f\n", i, arr[i]);
	}
	putchar('\n');
}

int binarySearch(TYPE *data, int size, TYPE val)
{
	int result = 0, mid = 0, high = size;

	while(result < high)
	{
		mid = (result + high) / 2;
		if(LT(data[mid], val)){
			result = mid + 1;
		}
		else if(EQ(data[mid], val)){
			result = mid;
			break;
		}
		else{
			high = mid;
		}
	}
	return result;
}
