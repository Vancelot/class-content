#include"header.h"

void initDeque(Deque *d, int cap)
{
	int i = 0;
	assert(cap > 0);
	d->size = d->start = 0;
	d->capacity = cap;
	d->data = (TYPE*) malloc(cap * sizeof(TYPE));
	assert(d->data != 0);
	for(i = 0; i < d->capacity; i++){
		d->data[i] = 0;
	}
}

void _doubleDeqCap(Deque *d)
{
	int i = 0;

	int oldCap = d->capacity;
	int oldSize = d->size;
	int oldStart = d->start;
	TYPE *oldDat = d->data;

	initDeque(d, 2 * d->capacity);

	d->capacity = 2 * oldCap;
	d->size = oldSize;
	d->start = 0;
	for(i = 0; i < d->capacity; i++){
		d->data[i] = oldDat[(oldStart + i) % oldCap];
	}
	free(oldDat);
}

void addFrontDeque(Deque *d, TYPE val)
{
	if(d->size == d->capacity){
		_doubleDeqCap(d);
	}
	d->start--;
	if(d->start < 0){
		d->start += d->capacity;
	}
	d->data[d->start] = val;
	d->size++;
}

void addBackDeque(Deque *d, TYPE val)
{
	if(d->size == d->capacity){
		_doubleDeqCap(d);
	}
	/*Back Index = (start + size) % capacity */
	d->data[(d->start + d->size) % d->capacity] = val;
	d->size++;
}

void remFrontDeque(Deque *d)
{
	d->data[d->start] = 0;
	d->start++;
	if(d->start > d->capacity){
		d->start -= d->capacity;
	}
	d->size--;
}

void remBackDeque(Deque *d)
{
	d->data[(d->start + d->size) % d->capacity] = 0;
	if(d->start < 0){
		d->start += d->capacity;
	}
	d->size--;
}

void _addDeque(Deque *d, Dnode *lnk, TYPE e)
{
	assert(d);
	struct Dnode *newLink = (Dnode*)malloc(sizeof(Dnode));
	assert(newLink != NULL);
	newLink->value = e;
	newLink->prev = lnk->prev;
	newLink->next = lnk;
	lnk->prev->next = newLink;
	lnk->prev = newLink;
	d->size++;
}

void _removeDeque(ListDeque *dq, Dnode *lnk)
{
/*	assert(!isEmptyDeque(dq));*/
	lnk->prev->next = lnk->next;
	lnk->next->prev = lnk->prev;
	free(lnk);
	dq->size--;
}

void printDeque(Deque *dq)
{
	int i = 0;
	for(i = 0; i < dq->size; i++){
		printf("Deque[%d] %.2f\n", i, dq->data[(dq->start + i) % dq->capacity]);
	}
	putchar('\n');
}
