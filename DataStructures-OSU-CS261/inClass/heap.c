#include "header.h"

void addDynArrHeap(DynArrHeap *heap, TYPE val)
{
	assert(heap);
	int pos = heap->size;
	int parIdx = (pos - 1) / 2;
	TYPE parVal = 0;

	if(pos >= heap->capacity){
		_dynArrHeapDoubleCap(heap);
	}
	if(pos > 0){ parVal = heap->data[parIdx]; }
	while(pos > 0 && LT(val, parVal))
	{
		heap->data[pos] = parVal;
		pos = parIdx;
		parIdx = (pos - 1) / 2;
		if(pos > 0){ parVal = heap->data[parIdx]; }
	}
	heap->data[pos] = val;
	heap->size++;
}

void recursDynArrHeapAdd(DynArrHeap *heap, TYPE val, int index)
{
	int pidx = (index - 1) / 2;

	if(index > 0 && heap->data[pidx] > val)
	{
		_swapDynArrHeap(heap, pos, smallIdx);
	}
}

TYPE getFirstDynArrHeap(DynArrHeap *heap);

void remMinDynArrHeap(DynArrHeap *heap)
{
	assert(heap);
	int last = heap->size - 1;
	if(last != 0){ heap->data[0] = heap->data[last]; }
	heap->size--;
	_adjustDynArrHeap(heap);
}

void initDynArrHeap(DynArrHeap *heap, int cap)
{
	assert(cap >= 0);
	heap->capacity = cap;
	heap->size = 0;
	heap->data = (TYPE*) malloc(heap->capacity * sizeof(TYPE));
	assert(heap->data != 0);
}

void _adjustDynArrHeap(DynArrHeap *heap, int maxIdx, int pos)
{
	assert(heap);
	int lhIdx = (pos * 2) + 1;
	int rhIdx = (pos * 2) + 2;
	int smallIdx = 0;

	if(rhIdx < maxIdx)
	{
		smallIdx = _minIdxDynArrHeap(heap, lhIdx, rhIdx);
		if(LT(heap->data[smallIdx], heap->data[pos])){
			_swapDynArrHeap(heap, pos, smallIdx);
			_adjustDynArrHeap(heap, maxIdx, smallIdx);
		}
	}
	else if(lhIdx < maxIdx)
	{
		if(LT(heap->data[lhIdx], heap->data[pos])){
			_swapDynArrHeap(heap, pos, lhIdx);
			_adjustDynArrHeap(heap, maxIdx, lhIdx);
		}
	}
}

void _swapDynArrHeap(DynArrHeap *heap, int idx1, int idx2)
{
	TYPE tmp = heap->data[idx1];
	heap->data[idx1] = heap->data[idx2];
	heap->data[idx2] = tmp;
}

int _minIdxDynArrHeap(DynArrHeap *heap, int idx1, int idx2)
{
	int result = LT(heap->data[idx1], heap->data[idx2]) ? idx1 : idx2;
	return result;
}

void _dynArrHeapDoubleCap(DynArrHeap *heap)
{
	TYPE *oldbuffer = heap->data;
	int oldsize = heap->size;

	initDynArrHeap(heap, 2 * heap->capacity);
	int i = 0;
	for(i = 0; i < oldsize; i++){
		heap->data[i] = oldbuffer[i];
	}
	heap->size = oldsize;
	free(oldbuffer);
}
