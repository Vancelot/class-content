#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
#include "avl.h"

typedef struct Node{
	struct AVLnode *avlNode;
	struct Node* next;
} Node;

typedef struct linkedList{
	Node *top;
} linkedList;

void preorder(struct AVLnode *node, TYPE *min_cost, TYPE *path, int *m, TYPE *candidate_path, int *n, TYPE sumDiff, TYPE parent_value);
TYPE absoluteDiff(TYPE a, TYPE b);
int FindMinPath(struct AVLTree *tree, TYPE *path);
void printBreadthFirstTree(struct AVLTree *tree);
void printDetailedAVL(struct AVLnode *node);
void printAVL(struct AVLnode *node);
int power(int, int );

/* -----------------------
The main function param: argv = pointer to the name (and path) of a file that
the program reads for adding elements to the AVL tree
*/
int main(int argc, char** argv)
{
	FILE *file;
	int len, i=1;
	TYPE num; /* value to add to the tree from a file */
	struct timeval stop, start; /* variables for measuring execution time */
	int pathArray[50];  /* static array with values along the min-cost path of
								  the AVL tree */
	struct AVLTree *tree;
	
	tree = newAVLTree(); /*initialize and return an empty tree */
	
	/* filename is passed in argv[1] */
	file = argc > 1 ? fopen(argv[1], "r") : fopen("input.txt", "r");
	assert(file != 0);

	/* Read input file and add numbers to the AVL tree */
	while((fscanf(file, "%i", &num)) != EOF){
		addAVLTree(tree, num);
		i++;
	}
	/* Close the file  */
	fclose(file);

	printf("\nPrinting the tree breadth-first : \n");
	printBreadthFirstTree(tree);
	/*printDetailedAVL(tree->root);*/
	/*printAVL(tree->root);*/

	gettimeofday(&start, NULL);

	/* Find the minimum-cost path in the AVL tree*/
	len = FindMinPath(tree, pathArray);
	
	gettimeofday(&stop, NULL);

	/* Print out all numbers on the minimum-cost path */
	printf("\nThe minimum-cost path: \n");
	for(i = 0; i < len; i++)
		printf("%d ", pathArray[i]);
	printf("\nYour execution time to find the mincost path is %f microseconds\n", (double) (stop.tv_usec - start.tv_usec));

	/* Free memory allocated to the tree */
	deleteAVLTree(tree); 
	
	return 0;
}

TYPE getVal(struct AVLnode *node){
	if(node){ return node->val; }
	else{ return -9999; }
}

void printDetailedAVL(struct AVLnode *node)
{
	printf("\tNODE[ %d h(%d)]\nLH[ %d h(%d)]   RH[ %d h(%d)]\n\n",
		getVal(node),
		h(node),
		getVal(node->left),
		h(node->left),
		getVal(node->right),
		h(node->right));

	if(node->left){ printDetailedAVL(node->left); }
	if(node->right){ printDetailedAVL(node->right); }
}

void printAVL(struct AVLnode *node)
{
	printf("%d\n", getVal(node));
	if(node->left){ printAVL(node->left); }
	if(node->right){ printAVL(node->right); }
}

/* --------------------
Finds the minimum-cost path in an AVL tree
	Input arguments: 
		tree = pointer to the tree,
		path = pointer to array that stores values of nodes along the min-cost
		path, 
   Output: return the min-cost path length 

	pre: assume that
		path is already allocated sufficient memory space tree exists and is not
		NULL
*/
int FindMinPath(struct AVLTree *tree, TYPE *path)
{
	int path_len = 0; /* the initial length of the min-cost path */
	struct AVLnode * current = tree->root;
/* Were you trying to initialize this to 10 to the sixth power? '^' is the
 * bitwise XOR operator. You'd need math.h and pow(10,6) or just use hex.
 * TYPE min_cost = (TYPE) 10^6 * tree->cnt; initial high value for minimum */
	TYPE minCost = 0x7FFFFFFF;
	int c_path_len = 0; /* length of a candidate path */
	TYPE candidate_path[100]; /* candidate path is a static array */

	path[path_len] = tree->root->val; /* min-cost path must contain the root */
	path_len++;

	/* Write this part of the function */
	if (tree->cnt > 1){
	/* Traverse the tree and find the min-cost path */
		preorder(current, &minCost, path, &path_len, candidate_path, &c_path_len,
			0, tree->root->val); 
	}
	printf("\nMIN PATH COST: %d LEN: %d", minCost, path_len);
	return path_len;
}

/* ----------
Finds absolute difference between two input numbers 
*/
TYPE absoluteDiff(TYPE a, TYPE b){
   
   if (a<0) 
     return (TYPE) 0;
   else
     return (TYPE) abs(a-b);
}

/*------------------------- 
Recursively traverses the AVL tree and finds its min cost path
Input argument: 
	node = pointer to the current node visited by recursion,
	min_cost = the latest minimum cost, 
	path = array of values of nodes along the latest best path,
	path_len = number of elements in path array,
	candidate_path = currently considered path array
	c_path_len = number of elements in the candidate path array
     
pre: assume that all input arguments are well initialized and have enough
memory space     

post: path points to the array of values in the min-cost path of the AVL tree
path_len is the number of elements (i.e., nodes) in the min-cost path of the
AVL tree
*/
void preorder(struct AVLnode *node, TYPE *min_cost, TYPE *path, int *path_len, 
	TYPE *candidate_path, int *c_path_len, TYPE sumDiff, TYPE parent_value) 
{
	int i = 0;

	if(node)
	{
		/* put the current node in a candidate path */
		candidate_path[*c_path_len] = node->val; 
		(*c_path_len)++;

		/* cumulative cost of the candidate path */
		sumDiff += absoluteDiff(parent_value, getVal(node)); 

		if(sumDiff < *min_cost){
			preorder(node->left, min_cost, path, path_len, candidate_path,
				c_path_len, sumDiff, getVal(node));
			preorder(node->right, min_cost, path, path_len, candidate_path,
				c_path_len, sumDiff, getVal(node));
		}
		(*c_path_len)--;
	}
	else
	{
		if(*min_cost > sumDiff)
		{
			*min_cost = sumDiff; 
			*path_len = *c_path_len;
			for(i = 0; i < *c_path_len; i++){
				path[i] = candidate_path[i];
			}
		}
	}
	/* Write the recursion case(s) and the stopping-recursion case(s) */
}

int power(int base, int exp)
{
	int i = 1, result = base;
	for(i = 1; i < exp; i++){
		result *= base;
	}
	return result;
}

/* -----------------------
Printing the contents of an AVL tree in breadth-first fashion
	param: pointer to a tree
	pre: assume that tree was initialized well before calling this function
*/
void printBreadthFirstTree(struct AVLTree *tree){
	struct AVLnode **queue;  /* print using a queue, where queue is implemented
										 as a static array */
   struct AVLnode *current = tree->root;
	int start = 0; /* start index of queue indicating the first element to be
							processed */
	int end = 0;  /* end index of queue indicating the latest element added to
						  the queue */
	int cap = power(2, (tree->root->height));
	int i = 0, j = 0, k = 0;
   
	printf("Height: %d\n", tree->root->height);
	/* allocate memory to queque */
   queue = (struct AVLnode**)malloc(cap * sizeof(struct AVLnode));
	queue[(start + end) % cap] = current;
	end++;
	for(i = 0; i <= tree->root->height; i++)
	{
		k = 0;
		for(j = end; j > 0; j--)
		{
			k++;
			current = queue[start];
			/*printf("%-5d", current->val);*/
			printf("%*c%-*d", (50 / ((i+1)*2)), ' ', k, current->val);
			if(current->left){
				queue[(start + end) % cap] = current->left;
				end++;
			}
			if(current->right){
				queue[(start + end) % cap] = current->right;
				end++;
			}

			queue[start] = NULL;
			start++;
			if(start > cap){ start -= cap; }
			end--;
		}
		putchar('\n');
	}
	free(queue);
}
