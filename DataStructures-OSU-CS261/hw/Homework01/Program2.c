/* CS261- HW1 - Program2.c*/
/* Name: Kerry Vance
 * Date: 12 Apr 19
 * Solution description:
 */
 
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#include<time.h>	/*to seed rng*/

#define SIZE 10

struct student
{
	char initials[2];
	int score;
};

struct student* allocate()
{
	/*Allocate memory for ten students*/
	struct student *result = NULL;
	result = (struct student*)malloc(SIZE * sizeof(struct student));

	/*return the pointer*/
	return result;
}

void generate(struct student* students)
{
	unsigned int i = 0;
	for(i = 0; i < SIZE; i++)
	{
		students[i].initials[0] = rand() % 26 + 65;
		students[i].initials[1] = rand() % 26 + 65;
		students[i].score = rand() % 101;
	}
}

void output(struct student* students)
{
	unsigned int i = 0;
	putchar('\n');
	for(i = 0; i < SIZE; i++){
		printf("%3.2d. %-3s %-5d\n", i + 1, students[i].initials, students[i].score);
	}
}

void summary(struct student* students)
{
	/*Compute and print the minimum, maximum and average scores of the ten students*/
	int i = 0, min = 100, max = 0, total = 0;

	for(i = 0; i < SIZE; i++)
	{
		if(students[i].score > max){
			max = students[i].score;
		}
		if(students[i].score < min){
			min = students[i].score;
		}
		total += students[i].score;
	}

	printf("\nSummary:\nMin:%-5dMax:%-5dAvg:%-5d\n\n", min, max, total/SIZE);

}

void deallocate(struct student* stud)
{
	/*Deallocate memory from stud*/
	if(stud != NULL){
		free(stud); 
	}
}

int main()
{
	struct student* stud = NULL;

	srand(time(NULL));

	/*call allocate*/
	stud = allocate();
	/*call generate*/
	generate(stud);
	/*call output*/
	output(stud);
	/*call summary*/
	summary(stud);
	/*call deallocate*/
	deallocate(stud);

    return 0;
}
