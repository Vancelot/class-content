/* CS261- HW1 - Program3.c*/
/* Name: Kerry Vance
 * Date: 12 Apr 19
 * Solution description:
 */
 
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<ctype.h>

void printArr(int *arr, int n)
{
	int i = 0;
	for(i = 0; i < n; i++)
	{
		if(i % 3 == 0){
			putchar('\n');
		}
		printf("%3i. %-15d", i + 1, arr[i]);
	}
	putchar('\n');
}

void sort(int* number, int n)
{
	/*Sort the array of integeres of length n*/     
	int i = 0, j = 0, sent = 0;
	int temp = 0;
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n - i; j++)
		{
			if(number[j] > number[j+1] && j + 1 < n)
			{
				temp = number[j];
				number[j] = number[j+1];
				number[j+1] = temp;
				sent = 1;
			}
		}
		if(sent != 1){
			break;
		}
		sent = 0;
	}
}

int main(int argc, char **argv)
{
	/*Declare an integer n and assign it a value of 20.*/
	unsigned int n = 0; 
	char *buf = NULL;

	srand(time(NULL));

	/*Allocate memory for an array of n integers using malloc.*/
	if(argc > 1){
		buf = argv[1];
		if(!isdigit(*buf)){
			buf = (char*)malloc(80 * sizeof(char));
		}
	}
	else{
		buf = (char*)malloc(80 * sizeof(char));
	}
	do
	{
		if(isdigit(*buf)){
			n = atoi(buf);
		}
		if(n == 0)
		{
			printf("Enter the array size you'd like to generate and sort: ");
			scanf("%s", buf);
		}
	}while(n == 0);
	if(buf != NULL && buf != argv[1]){
		free(buf);
	}
	int *arr = (int*)malloc(n * sizeof(int));

	/*Fill this array with random numbers, using rand().*/
	unsigned int i = 0;
	for(i = 0; i < n; i++){
		arr[i] = rand();
	}

	/*Print the contents of the array.*/
	printf("\nUnsorted List:\n");
	printArr(arr, n);

	/*Pass this array along with n to the sort() function of part a.*/
	sort(arr, n);

	/*Print the contents of the array.*/    
	printf("\nSorted List:\n");
	printArr(arr, n);
	if(arr != NULL){
		free(arr);
	}

    return 0;
}
