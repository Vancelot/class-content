#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <string.h>
#include "hashMap.h"

#define TABLESIZE 10
#define BUFSIZE 100

/*
 the getWord function takes a FILE pointer and returns you a string which was
 the next word in the in the file. words are defined (by this function) to be
 characters or numbers seperated by periods, spaces, or newlines.
 
 when there are no more words in the input file this function will return NULL.
 
 this function will malloc some memory for the char* it returns. it is your job
 to free this memory when you no longer need it.
 */
char* getWord(FILE *file); /* prototype */
void concordance(struct hashMap*, KeyType);
void printMap(hashMap *ht);
void printDebugMap(hashMap *ht);

/****************************************/

int main (int argc, const char **argv)
{
	char *buf = NULL, *test = NULL;
	FILE *infile;
	hashMap *ht = (hashMap*)malloc(sizeof(hashMap));

	assert(ht);

	test = (char*)malloc(sizeof(char) * 20);
	strcpy(test, "does");

	infile = argc < 2 ? fopen("input.txt", "r") : fopen(argv[1], "r");
	while(infile == NULL)
	{
		buf = (char*)malloc(sizeof(char) * BUFSIZE);
		assert(buf);
		printf("Please enter a valid filename: ");
		scanf("%s", buf);
		infile = fopen(buf, "r");
		free(buf);
	}

	assert(infile);
	initMap(ht, TABLESIZE);

	while(!feof(infile))
	{
		buf = getWord(infile);
		concordance(ht, buf);
	}
	fclose(infile);

	printMap(ht);

	freeMap(ht);

	return 0;
}

char* getWord(FILE *file) /* prototype */
{
	char *result;
	int i = 0;
	char *buf = (char*)malloc(sizeof(char) * BUFSIZE);
	assert(buf);

	fscanf(file, "%99[a-zA-Z0-9']%*[^a-zA-Z0-9']", buf);

	result = (char*)malloc(sizeof(char) * i + 1);
	assert(buf);
	strcpy(result, buf);
	free(buf);

	return result;
}

void concordance(struct hashMap * ht, KeyType k)
{
	ValueType *val = atMap(ht, k);
	if(val == NULL){insertMap(ht, k, 1);}
	else{
		*val += 1;
		if(k){free(k);}
	}
}

void printMap(hashMap *ht)
{
	int i = 0;
	hashLink *lnk = NULL;

	assert(ht);

	for(i = 0; i < ht->tableSize; i++)
	{
		lnk = ht->table[i];
		while(lnk){
			printf("%s: %d\n\n", lnk->key, lnk->value);
			lnk = lnk->next;
		}
	}
	printf("\n\n");
}

void printDebugMap(hashMap *ht)
{
	int i = 0;
	hashLink *lnk = NULL;

	assert(ht);

	for(i = 0; i < ht->tableSize; i++)
	{
		lnk = ht->table[i];
		if(lnk){printf("\n\n");}
		while(lnk){
			printf("[ %s:%d ]->", lnk->key, lnk->value);
			lnk = lnk->next;
		}
	}
	printf("\n\n");
}

