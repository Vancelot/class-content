#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "hashMap.h"
#include "structs.h"
#include <string.h>

int stringHash1(char * str)
{
	int i;
	int r = 0;
	for (i = 0; str[i] != '\0'; i++)
		r += str[i];
	return r;
}

int stringHash2(char * str)
{
	int i;
	int r = 0;
	for (i = 0; str[i] != '\0'; i++)
		r += (i+1) * str[i]; /*the difference between 1 and 2 is on this line*/
	return r;
}

void initMap(struct hashMap * ht, int tableSize)
{
	int index;
	assert(ht);
	ht->table = (hashLink**)malloc(sizeof(hashLink*) * tableSize);
	assert(ht->table);
	ht->tableSize = tableSize;
	ht->count = 0;
	for(index = 0; index < tableSize; index++){
		ht->table[index] = NULL;
	}
}

void freeMap(struct hashMap * ht)
{
	int i = 0;
	hashLink *cur = NULL, *next = NULL;

	for(i = 0; i < ht->tableSize; i++)
	{
		cur = ht->table[i];
		while(cur)
		{
			next = cur->next;
			if(cur->key){free(cur->key);}
			if(cur){free(cur);}
			cur = next;
		}
	}
	if(ht->table){free(ht->table);}
	if(ht){free(ht);}
}

void insertMap(struct hashMap * ht, KeyType k, ValueType v)
{
	int hash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
	hashLink *cur = NULL, *newLnk = (hashLink*)malloc(sizeof(hashLink));

	assert(newLnk);
	newLnk->next = NULL;
	newLnk->key = k;
	newLnk->value = v;
	ht->count++;

	cur = ht->table[hash % ht->tableSize];
	if(!cur){ht->table[hash % ht->tableSize] = newLnk;}
	else
	{
		while(cur->next){cur = cur->next;}
		cur->next = newLnk;
	}
}

ValueType* atMap(struct hashMap * ht, KeyType k)
{
	int hash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
	ValueType *result = NULL;
	hashLink *pnt = ht->table[hash % ht->tableSize];

	while(pnt != NULL)
	{
		if(strcmp(pnt->key, k) == 0){
			result = &pnt->value;
			break;
		}
		pnt = pnt->next;
	}
	return result;
}

int containsKey(struct hashMap * ht, KeyType k)
{
	int curHash = 0, hash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
	int result = 0;
	hashLink *cur = ht->table[hash % ht->tableSize];

	while(cur)
	{
		curHash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
		if(hash == curHash){
			result = 1;
			break;
		}
		cur = cur->next;
	}
	return result;
}

void removeKey(struct hashMap * ht, KeyType k)
{
	int curHash = 0, hash = HASHING_FUNCTION == 1 ? stringHash1(k) : stringHash2(k);
	hashLink *cur, *prev;
	prev = cur = ht->table[hash % ht->tableSize];

	if(cur){
		curHash = HASHING_FUNCTION == 1 ? stringHash1(cur->key) : stringHash2(cur->key);
	}
	if(hash == curHash)
	{
		ht->table[hash % ht->tableSize] = cur->next;
		if(cur->key){free(cur->key);}
		if(cur){free(cur);}
	}
	else 
	{
		while(cur)
		{
			curHash = HASHING_FUNCTION == 1 ?
				stringHash1(cur->key) : stringHash2(cur->key);
			if(hash == curHash)
			{
				prev->next = cur->next;
				if(cur->key){free(cur->key);}
				if(cur){free(cur);}
				break;
			}
			prev = cur;
			cur = cur->next;
		}
	}
}

int sizeMap(hashMap *ht){return ht->count;}

int capacityMap(hashMap *ht){return ht->tableSize;}

int emptyBuckets(struct hashMap *ht)
{
	int i = 0, result = 0;
	for(i = 0; i < ht->tableSize; i++){
		if(!ht->table[i]){result++;}
	}
	return result;
}

float tableLoad(struct hashMap *ht)
{
	float result = (float)ht->count / ht->tableSize;
	return result;
}
