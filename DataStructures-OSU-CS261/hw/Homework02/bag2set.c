/* bag2set.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dynArray.h"


void bag2set(struct DynArr *da);

/* An example how to test your bag2set() */
int main(int argc, char* argv[])
{
	int i;
	struct DynArr da;  /* bag */

	initDynArr(&da, 100);
	da.size = 10;
	da.data[0] = 1.3;
	for(i=1;i<da.size;i++){
		da.data[i] = 1.2;
	}

	printf("\nBag:\n\n");
	for(i=0;i<da.size;i++){
		printf("%g  \n", da.data[i]);
	}
	printf("\n\n\n");
	printf("Set:\n\n");
	bag2set(&da);
	for (i=0;i<da.size;i++){
		printf("%g ", da.data[i]);
	}
	putchar('\n');

	deleteDynArr(&da);
	return 0;
}

/* Converts the input bag into a set using dynamic arrays 
	param: 	da -- pointer to a bag 	
	return value: void
	result: after exiting the function da points to a set
*/
void bag2set(struct DynArr *da)
{
	int i=0, j=0;
	DynArr *stack;
	stack = newDynArr(da->size);
	for(i=0;i<da->size;i++)
	{
		if(containsDynArr(stack, da->data[i])<0){
			pushDynArr(stack, da->data[i]);
		}
	}
	for(i=0;i<stack->size;i++){
		da->data[i] = stack->data[i];
	}
	for(j=da->size;j>i;j--){
		removeAtDynArr(da,i);
	}
	deleteDynArr(stack);
}
