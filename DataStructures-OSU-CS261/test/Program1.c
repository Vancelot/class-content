/* CS261- HW1 - Program1.c*/
/* Name: Kerry Vance
 * Date: 12 Apr 19
 * Solution description:
 */
 /*
#include <stdio.h>
#include <stdlib.h>

#include <time.h>	to seed rng (alternative random seed function provided)*/
#include"Prog1.h"

int Main()
{
	/* Declare three integers x,y and z and initialize them randomly to values in [0,10] */
	int x = 0, y = 0, z = 0, result = 0;
	int var1, Var1;

	var1 = 1;
	Var1 = 2;

	srand(time(NULL));	/*Uncomment for random seed using time*/
	/*srand(seedRand());*/	/*Uncomment for time free randomness*/

	x = rand() % 11;
	y = rand() % 11;
	z = rand() % 11;

	/* Print the values of x, y and z */
	printf("\nBefore:\nx = %d, y = %d, z = %d\n\n", x, y, z);

	/* Call foo() appropriately, passing x,y,z as parameters */
	result = foo(&x, &y, z);

	/* Print the values of x, y and z */
	printf("After:\nx = %d, y = %d, z = %d\n\n", x, y, z);

	/* Print the value returned by foo */
	printf("Return of foo: %d\n\n", result);

	/* Is the return value different than the value of z?  Why? */
	if(result == z)
	{
		printf("Well that's weird, we passed 'z' by value not reference so it should be unchanged. It could be a coincidence though that 'a - b' equal 'z'\n\n");
	}
	else
	{
		printf("'z' is different than the return value of 'foo', because 'foo's third argument is passed by value, rather than address. so the original variable is left unchanged\n\n");
	}
	return 0;
}
