/* CS261- HW1 - Program4.c*/
/* Name: Kerry Vance
 * Date: 12 Apr 19
 * Solution description:
 */
 
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

struct student {
	char initials[2];
	int score;
};

void output(struct student* students, int n)
{
	/*Output information about the ten students in the format:
			1. Initials  Score
			2. Initials  Score
			...
			10. Initials Score*/
	int i = 0;
	for(i = 0; i < n; i++)
	{
		printf("%d.\t%s\t%d\n", i + 1, students[i].initials, students[i].score);
	}
}

void sort(struct student* students, int n)
{
	/*Sort n students based on their initials*/     
	int i = 0, j = 0;
	struct student temp;

	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n - i; j++)
		{
			if(students[j].initials[0] > students[j + 1].initials[0] && j+ 1 < n)
			{
				temp = students[j + 1];
				students[j + 1] = students[j];
				students[j] = temp;
			}
			if(students[j].initials[0] == students[j + 1].initials[0] && students[j].initials[1] >= students[j + 1].initials[1] && j + 1 < n)
			{
				temp = students[j + 1];
				students[j + 1] = students[j];
				students[j] = temp;
			}
		}
	}
}
/*
void sort(int* number, int n)
{
	int i = 0, j = 0;
	int temp = 0;
	for(i = 0; i < n - j; i++)
	{
		for(j = 0; j < n; j++)
		{
			if(number[j] < number[j+1])
			{
				temp = number[j];
				number[j] = number[j+1];
				number[j+1] = temp;
			}
		}
		j = 0;
	}
}
*/

struct student* allocate(int n)
{
	struct student *result = NULL;
	result = (struct student*)malloc(n * sizeof(struct student));

	return result;
}

void generate(struct student* students, int n)
{
	/* Generate random initials and scores for ten students.
	The two initial letters must be capital and must be between A and Z. 
	The scores must be between 0 and 100*/
	int i = 0;
	for(i = 0; i < n; i++)
	{
		students[i].initials[0] = rand() % 26 + 65;
		students[i].initials[1] = rand() % 26 + 65;
		students[i].score = rand() % 100;
	}
}

int main()
{
	/*Declare an integer n and assign it a value.*/
	int n = 0;
	struct student* stud = NULL;

	srand(time(NULL));

	printf("How many students? ");
	scanf("%d", &n);

	/*Allocate memory for n students using malloc.*/
	stud = allocate(n);

	/*Generate random IDs and scores for the n students, using rand().*/
	generate(stud, n);

	/*Print the contents of the array of n students.*/
	printf("Unsorted List:\n");
	output(stud, n);

	/*Pass this array along with n to the sort() function*/
	sort(stud, n);

	/*Print the contents of the array of n students.*/
	printf("\nSorted List:\n");
	output(stud, n);
    
    return 0;
}
