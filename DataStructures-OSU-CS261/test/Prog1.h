#ifndef __HEADER__
#define __HEADER__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>	/*to seed rng (alternative random seed function provided)*/

int foo(int* a, int* b, int c);
int seedRand();

#endif
