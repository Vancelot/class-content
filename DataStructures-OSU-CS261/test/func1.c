#include"Prog1.h"

int foo(int* a, int* b, int c)
{
	/* Increment a */
	*a = *a + 1;
	/* Decrement  b */
	*b = *b - 1;
	/* Assign a-b to c */
	c = *a - *b;
	/* Return c */
	return c;
}

int seedRand()
{
	int result = 0;
	char seed[256];

	printf("\nPlease faceroll your keyboard for entropy: ");
	scanf("%255s%n", seed, &result);

	int i = 0;
	while(seed[i]) {
		result += seed[i];
		result *= seed[i];
		i++;
	}

	return result;
}

