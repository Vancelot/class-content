/* CS261- HW1 - Program3.c*/
/* Name: Kerry Vance
 * Date: 12 Apr 19
 * Solution description:
 */
 
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void printArr(int *arr, int n)
{
	int i = 0;
	for(i = 0; i < n; i++) {
		if(i % 5 == 0) {
			putchar('\n');
		}
	}
	putchar('\n');
}

void sort(int* number, int n)
{
	/*Sort the array of integeres of length n*/     
	int i = 0, j = 0, sent = 0;
	int temp = 0;
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n - i; j++)
		{
			if(number[j] < number[j+1])
			{
				temp = number[j];
				number[j] = number[j+1];
				number[j+1] = temp;
				sent = 1;
			}
		}
		if(sent != 1){
			break;
		}
		sent = 0;
	}
	printf("\nLoops: %d\n", i);
}

int main()
{
	/*Declare an integer n and assign it a value of 20.*/
	unsigned int n = 20; 

	srand(time(NULL));

	/*Allocate memory for an array of n integers using malloc.*/
	int *arr = (int*)malloc(n * sizeof(int));

	/*Fill this array with random numbers, using rand().*/
	unsigned int i = 0;
	for(i = 0; i < n; i++){
		arr[i] = rand();
	}

	/*Print the contents of the array.*/
	printf("\nUnsorted List:\n");
	printArr(arr, n);

	/*Pass this array along with n to the sort() function of part a.*/
	sort(arr, n);

	/*Print the contents of the array.*/    
	printf("\nSorted List:\n");
	printArr(arr, n);

    return 0;
}
