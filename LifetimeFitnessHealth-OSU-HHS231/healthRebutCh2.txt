I believe there are several issues with the way that a lot of the information in this chapter is presented and the claims that are made.

	In this chapter it is stressed that metabolism decreases as we age, and that that can explain why people gain weight. But, this study from Cambridge found that the reduction in BMR was due largely to a decrease in total daily energy expenditure. Even then, it is at a rate of approximately 1-2%, or roughly 150 calories a decade. This decline seems hardly dramatic enough of a difference to cause to the significant weight gain and increase in obesity rates discussed in earlier chapters.

https://www.cambridge.org/core/journals/public-health-nutrition/article/energy-requirements-and-aging/E11815CE5C6E12FC1A7D914E42F5F9FD

	This study showed that the ratio of lean-to-fat mass accounted for the decrease in basal metabolic rate.

https://www.physiology.org/doi/abs/10.1152/jappl.1977.43.6.1001?journalCode=jappl

	If we are going to discuss, in class, the decrease in BMR due to age, it would be less misleading to discuss by how much it decreases, and how the effect can be lessened through preventative behaviors. 

	In Chapter 2: Energy Balance and Body Image, the reading states,

	"As we age, our metabolism decreases, which means if we kept all of our habits the same, it becomes more difficult to maintain our current weight, as less calories get burned in comparison to when we were younger."

This statement appears inaccurate. If all habits remain constant, total energy expenditure would not decrease. Likely lean muscle mass would not decrease as dramatically, thus BMR would not decline. At least not at the same rate observed by studies of average populations. Even if it is assumed that BMR decreases, in such a way that it is beyond mitigation, understanding and acknowledging the rate at which the decline happens, can be used to effectively decrease calorie intake to meet the rate of decline.

	In the TED talk that was included in the reading, Sandra Aamodt espouses 'mindful eating' to achieve effective weight management. Mindful eating is not an effective recommendation in current food climates when studies such as this one have shown how so called 'Hyperpalatable foods' overcome the bodies mechanisms through which hunger and satiety are controlled. Mindful eating can not be an effective tool without taking into account the nutritional value of food being consumed.

http://web.b.ebscohost.com.ezproxy.proxy.library.oregonstate.edu/ehost/pdfviewer/pdfviewer?vid=1&sid=21068a34-0559-4cb1-adf5-87a4297484c2%40pdc-v-sessmgr06

	She goes on to discuss 'body set point theory' and how weight regulation mechanisms work asymmetrically to promote weight gain over weight loss. This article supports that theory, but only in the context of 'western style diets'. The article references animal studies where rats, exposed to western style diets, lose intake control, and gain weight. When returned to healthy chow diets, the rats returned to previous healthy weights. Aamodt makes little to no reference about the nutritional value of food, or the impact of environmental variables on a persons set or settling point. 

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2990627/

	This article from Harvard illustrates that genetic markers predisposing a person to obesity play only a small roll, and that these effects can be overcome with healthy lifestyle practices.

https://www.hsph.harvard.edu/obesity-prevention-source/obesity-causes/genes-and-obesity/

	This study showed that resistance training actually preserved RMR despite of a very low calorie diet, 800cal/d. This preservation of metabolic rate stands in direct to contrast to Aamodt's claim that a person who has undergone a 10% weight reduction, will experience a metabolic suppresions of 250-400 cal/d 'forever'. A claim not present in the study, by Dr. Rudy Liebel, that she references. In fact in the study referenced, Liebel claims a decline in energy expenditure of approximately 300 - 400 kcal/day below predicted from changes in body composition over periods of months to years, not forever.

https://www-tandfonline-com.ezproxy.proxy.library.oregonstate.edu/doi/full/10.1080/07315724.1999.10718838?scroll=top&needAccess=true

https://www-sciencedirect-com.ezproxy.proxy.library.oregonstate.edu/science/article/pii/S0006899310012667

	In the section concerning body types, ecto, endo, and mesomorphic body types are discussed. Somatotypes were developed from the research of psychologist William Herbert Sheldon, in an attempt to correlate physique with behavior, intelligence, and social hierarchy, that has been widely debunked and criticized as subjective, racist, and fraudulent. The Heath-Carter formula is a formulaic approach to body type analysis, but it is only used to describe the current shape of a physique. No genetic link to somatotypes has been identified. The claim in the readings that different body types correlate to different metabolic rates contains no citation, and no corroborating research could be found.

	Further, the inclusion of metabolically obese normal weight (MONW) is misleading. This research shows that prevalence of metabolic syndrome increases from 0.9-3.0% at a BMI of 18.5-20.9 to 9.6-22.5% for BMI ranging 25-26.9, suggesting that even being at the high end of the healthy BMI range significantly increases risk.
	
https://www.researchgate.net/profile/Ian_Janssen/publication/8379852_Metabolic_Syndrome_in_Normal-Weight_Americans_New_definition_of_the_metabolically_obese_normal-weight_individual/links/56414d3308aec448fa6077a7/Metabolic-Syndrome-in-Normal-Weight-Americans-New-definition-of-the-metabolically-obese-normal-weight-individual.pdf

	The inclusion of a video from HAES is particularly disturbing in a health class. In the video Linda Bacon claims that dieting decreases the hormone leptin which increases appetite and decreases metabolism. While true this article and others I've linked above suggest there are effective methods for attenuating these unfavorable adaptations. 

https://jissn.biomedcentral.com/articles/10.1186/1550-2783-11-7

	She suggests that biological mechanisms that resist weight loss are too great to overcome and that diet failure can be attributed to the body reducing metabolism beyond reasonable calorie restriction. This study suggests that it's much more likely that diet failure could be attributed to obese subjects incorrectly self-reporting calorie intake. Measured metabolic rates were within 5% of predicted rates for body composition. Food intake was underreported by an average of 47% and physical activity was overreported by an average of 51%. Diet failure can likely be attributed to high amounts of energy in and low amounts of energy out, rather than any metabolic abnormalities.

https://www-nejm-org.ezproxy.proxy.library.oregonstate.edu/doi/full/10.1056/NEJM199212313272701

	Bacon uses the contestants from "The Biggest Loser" as an example of how long term weight reduction is unachievable, but the reality show has been widely criticized for it's methods, as shown in this article. Contestants are not an accurate representation of Americans struggling with weight (Being in the upper 6th percentile of Americans BMI.), weight loss rates are beyond what is considered safe, contestants work out five-six hours a day, and diets are strictly regulated. Contestants tend to regain weight because these circumstances are not compatible with real life, not because it's impossible. 

https://www.livescience.com/9820-biggest-loser-big-problems-health-experts.html

	Bacon states that, "There is extensive research that demonstrates the same process (reduced metabolism, increased appetite) anytime body fat stores are reduced, regardless of method." This is contradicted by the numerous studies referenced above, and others, that she is decidedly ignoring by making this claim. This resource contains a collection of research identifying factors that investigate characteristics of successful long term weight management.

http://www.nwcr.ws/Research/published%20research.htm

	Bacon suggests trusting your body and paying attention to signals like hunger and fullness is all you need to be healthy. This may be true, in a different food environment, but, as discussed earlier, in a calorie dense, nutrient deficient food landscape, hyperpalatable foods overcome the bodies natural signalling paths for hunger and fullness. 

	She suggests that HAES is about body trust and respect rather than body shame, suggesting that dieting is fueled by body shaming. While body shame may play a factor in people participating in unhealthy weight loss strategies, the prime motivator behind weight loss recommendations is the health risks associated with obesity. 

According to the CDC obesity related health risks include,
    All-causes of death (mortality)
    High blood pressure (Hypertension)
    High LDL cholesterol, low HDL cholesterol, or high levels of triglycerides (Dyslipidemia)
    Type 2 diabetes
    Coronary heart disease
    Stroke
    Gallbladder disease
    Osteoarthritis (a breakdown of cartilage and bone within a joint)
    Sleep apnea and breathing problems
    Some cancers (endometrial, breast, colon, kidney, gallbladder, and liver)
    Low quality of life
    Mental illness such as clinical depression, anxiety, and other mental disorders4,5
    Body pain and difficulty with physical functioning6

References

1NHLBI. 2013. Managing Overweight and Obesity in Adults: Systematic Evidence Review from the Obesity Expert Panel.[PDF-5.89MB]

2Clinical Guidelines on the Identification, Evaluation, and Treatment of Overweight and Obesity in Adults.[PDF-1.25MB]

3Bhaskaran K, Douglas I, Forbes H, dos-Santos-Silva I, Leon DA, Smeeth L. Body-mass index and risk of 22 specific cancers: a population-based cohort study of 5•24 million UK adults. Lancet. 2014 Aug 30;384(9945):755-65. doi: 10.1016/S0140-6736(14)60892-8. Epub 2014 Aug 13.

4Kasen, Stephanie, et al. “Obesity and psychopathology in women: a three decade prospective study.” International Journal of Obesity 32.3 (2008): 558-566.

5Luppino, Floriana S., et al. “Overweight, obesity, and depression: a systematic review and meta-analysis of longitudinal studies.”Archives of general psychiatry 67.3 (2010): 220-229.

6Roberts, Robert E., et al. “Prospective association between obesity and depression: evidence from the Alameda County Study.” International journal of obesity 27.4 (2003): 514-521.


	To summarize, I feel that the material presented in this chapter is misleading, incomplete, and sometimes inaccurate. In week one, we discussed methods and strategies for enacting positive behavioral change, by contrast it seems that in this chapter the overarching message was the hopelessness of attempting to achieve a very common goal. Outlining the barriers and struggles of long term weight management, without discussing ways to overcome those barriers, seems to be self defeating in a class focused on behavioral change and overall wellness. Including scientifically dubious opinions, while omitting conflicting research, doesn't serve to assist anyone. While many of the facts and statistics shared in the content are cited and accurate, there is much that isn't, and the way those facts are interpreted could be improved. Instead of looking at the rates at which weight management fails and concluding simply that, "Diets don't work." it would be much more beneficial to discuss the causes of unsuccessful long term weight reduction. Weight loss strategies, like reasonable calorie restriction rather than crash diets paired with more effective macro nutrient profiles. Dispelling misconceptions about diet, such as hitting a goal weight and being done, or acknowledging you have a diet whether you're "on one" or not. Different perspectives on diet, changing relationships with food from seeing it purely as an emotional aspect of our lives to understanding cravings and being something one can feel good about, that fuels your body. Challenge the emotional attachment to our bodies and view it as a machine that we're in the driver seat of. I believe that all of these would have been far more beneficial in guiding students towards healthful behavioral changes, sending an overall message of empowerment rather than a message of intimidation and hopelessness.
