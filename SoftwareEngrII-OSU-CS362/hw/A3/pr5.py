# Pull request 5
def print_sorted(arr: list, inPlace=False):
    """
    Prints sorted array
    If array argument should be sorted pass 'inPlace=True'
    """
    if inPlace:
        arr.sort()
        print(*arr, sep='\n')
    else:
        print(*sorted(arr), sep='\n')


if __name__ == "__main__":
    arr = [1, 2, 5, 2, 10, 45, 9, 100]
    print(arr)
    print_sorted(arr)
