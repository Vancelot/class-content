# PR 4
def get_val_index(arr: list, val):
    """Searches arr for val and returns the index if found, otherwise -1"""
    return arr.index(val) if val in arr else -1
