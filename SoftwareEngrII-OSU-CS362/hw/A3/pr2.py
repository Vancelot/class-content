# PR 2
import random


def create_rand_list(n: int, subset: str):
    """
    Returns list of length n of random even or odd integers
    n:\t\tLength of list to be returned
    subset:\tMust be string in ["even", "odd"]
    """
    if subset not in ["even", "odd"]:
        raise ValueError()
    elif subset == "even":
        return [2 * random.randint(0, 500) for _ in range(n)]
    elif subset == "odd":
        return [2 * random.randint(0, 499) + 1 for _ in range(n)]
