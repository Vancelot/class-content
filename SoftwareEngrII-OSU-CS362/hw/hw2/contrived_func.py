
#     # This function serves no logical purpose
#     # DO NOT try to make sense of it!
#     # Just make sure your tests cover everything requested
#     # val is a numerical value
#     if val < 150 and val > 100:
#         return True
#     elif val * 5 < 361 and val / 2 < 24:
#         if val == 6:
#             return False
#         else:
#             return True
#     elif (val > 75 or val / 8 < 10) and val**val % 5 == 0:
#         return True
#     else:
#         return False


def contrived_func(val):
    # This function serves no logical purpose
    # DO NOT try to make sense of it!
    # Just make sure your tests cover everything requested
    if val < 150 and val > 100:
        print('C1: {}'.format(val))
    if val < 150 and not (val > 100):
        print('C2: {}'.format(val))
    if not (val < 150) and val > 100:
        print('C3: {}'.format(val))
    if not(val < 150) or not (val > 100):
        # Build all other conditions off of this branch
        if val * 5 < 361 and val / 2 < 24:
            print('C4: {}'.format(val))
        if val == 6:
            print('C7: {}'.format(val))
    else:
        print('C8: {}'.format(val))
    if val * 5 < 361 and not (val / 2 < 24):
        print('C5: {}'.format(val))
    if not (val * 5 < 361):
        print('C6: {}'.format(val))
    if not (val * 5 < 361) or not (val / 2 < 24):
        if val > 75 and val**val % 5 == 0:
            print('C9: {}'.format(val))
    if val > 75 and not (val**val % 5 == 0):
        print('C10: {}'.format(val))
    if not (val > 75) and val / 8 < 10 and val**val % 5 == 0:
        print('C11: {}'.format(val))
    if not (val > 75) and val / 8 < 10 and not (val**val % 5 == 0):
        print('C12: {}'.format(val))

    if val < 150 and val > 100:
        return True
    elif val * 5 < 361 and val / 2 < 24:
        print('B2')

    if val == 6:
        print('B5')
        return False
    else:
        print('B6')
        return True
    if (val > 75 or val / 8 < 10) and val**val % 5 == 0:
        print('B3')
        return True
    else:
        print('B4')
        return False
