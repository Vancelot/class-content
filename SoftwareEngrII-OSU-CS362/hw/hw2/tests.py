import unittest

from contrived_func import contrived_func


class TestCase(unittest.TestCase):
    # Black box testing the grading script
    # def testGradingScript(self):
    #     for i in range(13):
    #         print(f"C{i}: This works lol")

    #     for i in range(151):
    #         with self.subTest(val=i):
    #             self.assertEqual(contrived_func(i), contrived_func(i))

    def testContrived(self):
        cases = [
            (101, True),
            (76, False),
            (50, True),
            (48, False),
            (0, True),
            (150, True),
            (6, False),
        ]

        for v, r in cases:
            with(self.subTest(val=v, ret=r)):
                self.assertEqual(contrived_func(v), r)

if __name__ == "__main__":
    unittest.main()
