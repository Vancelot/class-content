def credit_card_validator(num):
    num = str(num)
    visa_prefix = [4]
    visa_len = [16]

    mc_prefix1 = list(range(2221, 2721))
    mc_prefix2 = list(range(51, 56))
    mc_len = [16]

    ae_prefix = [34, 37]
    ae_len = [15]

    if int(num[0]) in visa_prefix:
        card_type = "visa"
    elif int(num[0:2]) in ae_prefix:
        card_type = "ae"
    elif int(num[0:4]) in mc_prefix1 or int(num[0:2]) in mc_prefix2:
        card_type = "mc"
    else:
        card_type = "invalid"

    num_len = len(num)

    check_bit = luhn(num)

    # Error with invalid that are 16 and check_bit pass
    if card_type == "invalid" and num_len == 16 and check_bit:
        print("Bug 1 Found: {}".format(num))

    # Error with Visa's starting with 4052
    if (
        card_type == "visa" and
        num_len in visa_len and
        check_bit and
        int(num[0:4]) == 4052
    ):
        print("Bug 2 Found: {}".format(num))

    # Error with all odd valid AE card numbers
    if (
        card_type == "ae" and
        num_len in ae_len and
        check_bit and
        (int(num) % 2) != 0
    ):
        print("Bug 3 Found: {}".format(num))

    # Error with any card number that has begins and ends with the same 4 digit
    # sequence
    if num[0:4] == num[-4:] and num_len == 16:
        print("Bug 4 Found: {}".format(num))

    # Error when any valid card has '1111' in it
    valid_visa = card_type == "visa" and num_len in visa_len and check_bit
    valid_mc = card_type == "mc" and num_len in mc_len and check_bit
    valid_ae = card_type == "ae" and num_len in ae_len and check_bit
    if (valid_visa or valid_mc or valid_ae) and num.count("1111") == 1:
        print("Bug 5 Found: {}".format(num))

    if check_bit:
        if card_type == "visa" and num_len in visa_len:
            return True
        elif card_type == "mc" and num_len in mc_len:
            return True
        elif card_type == "ae" and num_len in ae_len:
            return True

    return False


# Sourced from Stack Overflow (https://stackoverflow.com/q/39272087/5113389)
def luhn(ccn):
    c = [int(x) for x in ccn[::-2]]
    u2 = [(2 * int(y)) // 10 + (2 * int(y)) % 10 for y in ccn[-2::-2]]
    return sum(c + u2) % 10 == 0


# Sums the digits of a number
def sum_digits(num):
    digit_sum = 0
    while num != 0:
        digit_sum += num % 10
        num //= 10
    return digit_sum
