# Random brute force bug finding. Numbers are randomly generated and validated
# to form appropriate assertions
import unittest
import random
import string
import time

from credit_card_validator import credit_card_validator

start_time = time.time()


def luhnAlg(ccn):
    c = [int(x) for x in ccn[::-2]]
    u2 = [(2 * int(y)) // 10 + (2 * int(y)) % 10 for y in ccn[-2::-2]]
    return sum(c + u2) % 10 == 0


class TestCase(unittest.TestCase):
    def setUp(self):
        self.numTests = 500
        self.tested = []

    def testMisc(self):
        prefix = [1, 2, 6, 7, 8, 9]

        testnum = ""
        with self.subTest(emptyString=testnum):
            self.assertRaises(IndexError, credit_card_validator, testnum)
        for i in range(0, 100):
            testnum = str(random.choice(prefix))
            testnum += "".join(
                random.choice(string.digits)
                for _ in range(0, 15)
            )
            if testnum not in self.tested:
                self.tested.append(testnum)
                with self.subTest(invalidPrefix=testnum):
                    self.assertFalse(credit_card_validator(testnum))
            else:
                i = i - 1

        for i in range(0, 10):
            testnum = ""
            testnum += "".join(
                random.choice(string.ascii_letters + string.digits)
                for _ in range(0, random.randrange(13, 17))
            )
            if testnum not in self.tested:
                self.tested.append(testnum)
                with self.subTest(mixedCharacters=testnum):
                    self.assertRaises(
                        ValueError, credit_card_validator, testnum
                    )
            else:
                i = i - 1

    def testVisa(self):
        prefix = 4

        for i in range(0, self.numTests * 75):
            testnum = str(prefix) + '0'
            testnum += "".join(
                random.choice(string.digits)
                for _ in range(0, 14)
            )
            if testnum not in self.tested:
                self.tested.append(testnum)
                with self.subTest(visa=testnum):
                    if len(testnum) == 16:
                        self.assertTrue(
                            credit_card_validator(testnum)
                        ) if luhnAlg(testnum) else self.assertFalse(
                            credit_card_validator(testnum)
                        )
                    else:
                        self.assertFalse(credit_card_validator(testnum))
            else:
                i = i - 1

    def testAMX(self):
        prefix = [34, 37]

        for i in range(0, self.numTests):
            testnum = str(random.choice(prefix))
            testnum += "".join(
                random.choice(string.digits)
                for _ in range(0, random.randrange(13, 16))
            )
            if testnum not in self.tested:
                self.tested.append(testnum)
                with self.subTest(amx=testnum):
                    if len(testnum) == 15:
                        self.assertTrue(
                            credit_card_validator(testnum)
                        ) if luhnAlg(testnum) else self.assertFalse(
                            credit_card_validator(testnum)
                        )
                    else:
                        self.assertFalse(credit_card_validator(testnum))
            else:
                i = i - 1

    def testMC(self):
        prefix0 = list(range(51, 56))
        prefix1 = list(range(2221, 2721))

        for i in range(0, self.numTests * 10):
            testnum = str(random.choice(prefix0))
            testnum += "".join(
                random.choice(string.digits)
                for _ in range(0, random.randrange(14, 18))
            )
            if testnum not in self.tested:
                self.tested.append(testnum)
                with self.subTest(mastercard=testnum):
                    if len(testnum) == 16:
                        self.assertTrue(
                            credit_card_validator(testnum)
                        ) if luhnAlg(testnum) else self.assertFalse(
                            credit_card_validator(testnum)
                        )
                    else:
                        self.assertFalse(credit_card_validator(testnum))
            else:
                i = i - 1

        for i in range(0, self.numTests * 10):
            testnum = str(random.choice(prefix1))
            testnum += "".join(
                random.choice(string.digits)
                for _ in range(0, random.randrange(11, 16))
            )
            if testnum not in self.tested:
                self.tested.append(testnum)
                with self.subTest(mastercard=testnum):
                    if len(testnum) == 16:
                        self.assertTrue(
                            credit_card_validator(testnum)
                        ) if luhnAlg(testnum) else self.assertFalse(
                            credit_card_validator(testnum)
                        )
                    else:
                        self.assertFalse(credit_card_validator(testnum))
            else:
                i = i - 1


if __name__ == "__main__":
    unittest.main()
