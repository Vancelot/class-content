%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Arsclassica Article
% LaTeX Template
% Version 1.1 (1/8/17)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Lorenzo Pantieri (http://www.lorenzopantieri.net) with extensive modifications by:
% Vel (vel@latextemplates.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[
12pt, % Main document font size
a4paper, % Paper type, use 'letterpaper' for US Letter paper
oneside, % One page layout (no page indentation)
%twoside, % Two page layout (page indentation for binding and different headers)
headinclude,footinclude, % Extra spacing for the header and footer
BCOR=5mm, % Binding correction
]{scrartcl}

\input{structure.tex} % Include the structure.tex file which specified the document structure and layout
\usepackage{wrapfig}

\hyphenation{Fortran hyphenation} % Specify custom hyphenation points in words with dashes where you would like hyphenation to occur, or alternatively, don't put any dashes in a word to stop hyphenation altogether

%----------------------------------------------------------------------------------------
%	TITLE AND AUTHOR(S)
%----------------------------------------------------------------------------------------

\title{\normalfont\spacedallcaps{Graph Theory Applications}} % The article title

\subtitle{Using Directed Graphs To Distinguish Correlation From Causation} % Uncomment to display a subtitle

\author{\spacedlowsmallcaps{Kerry Vance}} % The article author(s) - author affiliations need to be specified in the AUTHOR AFFILIATIONS block

\date{10 June 2019} % An optional date to appear under the author(s)

%----------------------------------------------------------------------------------------

\begin{document}

%----------------------------------------------------------------------------------------
%	HEADERS
%----------------------------------------------------------------------------------------

\renewcommand{\sectionmark}[1]{\markright{\spacedlowsmallcaps{#1}}} % The header for all pages (oneside) or for even pages (twoside)
%\renewcommand{\subsectionmark}[1]{\markright{\thesubsection~#1}} % Uncomment when using the twoside option - this modifies the header on odd pages
\lehead{\mbox{\llap{\small\thepage\kern1em\color{halfgray} \vline}\color{halfgray}\hspace{0.5em}\rightmark\hfil}} % The header style

\pagestyle{scrheadings} % Enable the headers specified in this block

%----------------------------------------------------------------------------------------
%	TABLE OF CONTENTS & LISTS OF FIGURES AND TABLES
%----------------------------------------------------------------------------------------

\maketitle % Print the title/author/date block

\setcounter{tocdepth}{3} % Set the depth of the table of contents to show sections and subsections only

\tableofcontents % Print the table of contents

%\listoffigures % Print the list of figures

%\listoftables % Print the list of tables

%ABSTRACT

% \section*{Abstract} % This section will not appear in the table of contents due to the star (\section*)
\newpage % Start the article content on the second page, remove this if you have a longer abstract that goes onto the second page
%  INTRODUCTION
\section*{Introduction}

Many fields of scientific research can make use of graph theory and logic,
usually restricted to the domain of computer science, mathematics, or physics;
to determine or model the causation or correlation of independent variables. A
causal model can be constructed from observational data to create a web of
variables connected by lines representing how values change in relation to each
other.  Different values can be tested for the different variables accounted for
within the model, to see how the entire system reacts as a consequence. The
variables whose value has a large impact on the overall system are most likely
to be the variables that are causally connected, while those with the least
significant effect are likely merely corollary. The aim of this paper is to
discuss correlation vs causation, and how mathematical and graphical models can
be leveraged in various scientific fields to distinguish one from the other in
complex systems.

%------------------------------------------------
\section{Correlation vs Causation}

Correlation is the association between two phenomenon, regardless of whether or
not one was caused by the other. For example; In the summer time, people eat
more ice cream and people wear shorts more often. It cannot be said that the
ingestion of ice cream causes people to wear shorts. These two phenomenon are
correlated but not causally. The causal factor in this case, would be the
weather. Most have heard the saying, "Correlation does not imply causation."
Unfortunately, in the domain of research, such a conclusion is rarely so
apparent. It's cases like these that illustrate why a distinction needs to be
made between the two. 

\subsection{Causality}

What distinguishes a causal from a corollary factor, is that a causal factor has
an effect of some degree on the occurrence or outcome of another
factor\cite{causation}. The outcome of the second factor is a result of it's
causal factor. The causal factor must always come before the effect. Temporally,
the causal factor must always precede the presumptive consequent
effect\cite{causalityTime}.

\subsection{Types of Causes}

Further it is important to distinguish what kind of cause can be attributed to
some event. There are two important kinds of causes, necessary and
sufficient\cite{humeNecandSuf}.  For some events $p$ and $q$; A necessary cause
means that, if $q$ happened then $p$ must have happened prior to $q$. In short,
"If $q$, then $p$; If no $p$, then no $q$" A sufficient cause means that if $p$
happens, $q$ will happen but $q$ happening doesn't mean that $p$ must have
happened as something else other than $p$ could have caused $q$. In short, "If
$p$, then $q$; If $q$, then $p$ or other variable."

%------------------------------------------------
\section{Graph Theory}

\subsection{Introduction to Graph Theory}

Graphs, by their most basic definition, are made up of two components; One, a
set of vertices, or points; Two, a set of ordered pairs representing edges,
or connections, between those vertices\cite{graphTheory}. Edges can be represented a variety of
ways; As a set of two vertices, that denote connection between the two, where
\{x,y\} represents a connection between vertex x and vertex y; Or as ordered
pairs. This is called a directed graph. An edge (x,y) would denote a connection
from vertex x to y. Edges can also carry a weight, or magnitude. This is called
a weighted graph, where the weight of an edge is representative of the
significance of the connection between two vertices.

Depending on the application, those vertices and edges can be used to represent
whatever is appropriate for the application.  In the case of scientific
research, each vertex could represent one discrete variable or event. The edges can
represent relationships between the vertices. The magnitude of those edges could
represent how large an impact one vertex has on another.  

\subsection{Directed Acyclic Graphs}

\begin{wrapfigure}{r}{0.25\textwidth}
	\includegraphics[width=1.25\linewidth]{DAG.png}
	\caption{Directed Acyclic Graph}
	\label{fig:dag}
\end{wrapfigure}

Directed acyclic graphs are a type of directed graph in which from any given
vertex there are no directed cycles, as illustrated in [Fig. \ref{fig:dag}].
That is, there is no route back to that vertex through any of the edges that
follow. Causal structures are directed acyclic graphs where each vertex
represents some event occurring at a distinct point in time\cite{causalDiag}.
Edges always originate from a vertex at an earlier point in time and terminate
at a vertex positioned at a later time. In this way, by the early definitions of
causation and given some vertex from the set, any vertex to the left is a
potential candidate for causation and any vertex to the right is a potential
candidate for the consequent.


%------------------------------------------------
\section{Applications}

\subsection{Implementation}

The use of causal models allow researchers to do the following\cite{impCaus}.
\begin{itemize}
	\item Determine whether assumptions made are enough to estimate results.
		\begin{itemize}
			\item If Yes, derive an expression for the result.  
			\item If No, decide what data needs to be gathered to reasonably
				estimate result.
		\end{itemize}
\end{itemize}

Statistical analysis only accounts for expected outcomes in a static
environment, whereas causal analysis can be used to dynamically assert outcomes
in changing conditions. A causal understanding infers a belief in the underlying
mechanics at work rather than just establishing associations.

\subsection{Strengths}

DAGs as causal models are useful in identifying what is possible to gather from
the data; e.g. ruling out variables as causal by time of
occurrence\cite{DAGCrit}. Additionally, DAGs can assist in distinguishing direct
and indirect effects, selection bias, confounding variables, and assist in
experiment design. Causal models are more flexible than statistical analysis and
thus are able to make more reliable predictions about future or proposed
experimentation based on past data, even when certain conditions vary. The use
of graphical models assists in communicating and illustrating the axioms and
relationships being asserted by the research.

\subsection{Weaknesses}

It is easy to get careless with how DAGs are applied to causal analysis and
reach conclusions that are not reflective of the reality. To effectively
leverage DAGs, the continuous nature of time must be considered. Structural
models built from DAGs where measurements are taken at too wide of time
intervals can obscure the nature of the relationships considerably.

%------------------------------------------------
\section{Conclusion}

DAGs and causal models offer significantly useful tools for being able to
represent theories mathematically and visually in fields where such
representations are rarely possible with more traditional language. They are
useful for experimental design and analysis. Through the course of research,
those involved become subject matter experts and it's difficult to express ideas
to audiences less intimately familiar with the subject matter. A shared
understanding of how explanations are expressed facilitates more effective
communication of what was learned, and how it fits into the larger picture.
Building structural models however, requires a tempered approach. DAGs are not a
silver bullet to interpreting or representing data. A firm understanding of the
limitations and uses of DAGs is necessary prior to their application, to avoid
obscuring accurate conclusions.  By inferring the 'why' behind the 'what', more
general assertions and expectations can be made about the world.

%Reference to Figure~\vref{fig:gallery}. % The \vref command specifies the location of the reference

%\begin{figure}[tb]
%\centering 
%\includegraphics[width=0.5\columnwidth]{GalleriaStampe} 
%\caption[An example of a floating figure]{An example of a floating figure (a reproduction from the \emph{Gallery of prints}, M.~Escher,\index{Escher, M.~C.} from \url{http://www.mcescher.com/}).} % The text in the square bracket is the caption for the list of figures while the text in the curly brackets is the figure caption
%\label{fig:gallery} 
%\end{figure}

%------------------------------------------------


%\subsubsection{Table}
%\lipsum[13] % Dummy text
%\begin{table}[hbt]
%\caption{Table of Grades}
%\centering
%\begin{tabular}{llr}
%\toprule
%\multicolumn{2}{c}{Name} \\
%\cmidrule(r){1-2}
%First name & Last Name & Grade \\
%\midrule
%John & Doe & $7.5$ \\
%Richard & Miles & $2$ \\
%\bottomrule
%\end{tabular}
%\label{tab:label}
%\end{table}
%
%Reference to Table~\vref{tab:label}. % The \vref command specifies the location of the reference

%------------------------------------------------

%Reference the figure composed of multiple subfigures as Figure~\vref{fig:esempio}. Reference one of the subfigures as Figure~\vref{fig:ipsum}. % The \vref command specifies the location of the reference
%
%\lipsum[15-18] % Dummy text
%
%\begin{figure}[tb]
%\centering
%\subfloat[A city market.]{\includegraphics[width=.45\columnwidth]{Lorem}} \quad
%\subfloat[Forest landscape.]{\includegraphics[width=.45\columnwidth]{Ipsum}\label{fig:ipsum}} \\
%\subfloat[Mountain landscape.]{\includegraphics[width=.45\columnwidth]{Dolor}} \quad
%\subfloat[A tile decoration.]{\includegraphics[width=.45\columnwidth]{Sit}}
%\caption[A number of pictures.]{A number of pictures with no common theme.} % The text in the square bracket is the caption for the list of figures while the text in the curly brackets is the figure caption
%\label{fig:esempio}
%\end{figure}

%	BIBLIOGRAPHY

%\renewcommand{\refname}{\spacedlowsmallcaps{References}} % For modifying the bibliography heading

\newpage

\bibliographystyle{unsrt}

\bibliography{bibliography.bib} % The file containing the bibliography

\end{document}
