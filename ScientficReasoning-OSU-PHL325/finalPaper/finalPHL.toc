\contentsline {section}{\numberline {1}{Correlation vs Causation}}{2}{section.1}% 
\contentsline {subsection}{\numberline {1.1}{Causality}}{2}{subsection.1.1}% 
\contentsline {subsection}{\numberline {1.2}{Types of Causes}}{2}{subsection.1.2}% 
\contentsline {section}{\numberline {2}{Graph Theory}}{3}{section.2}% 
\contentsline {subsection}{\numberline {2.1}{Introduction to Graph Theory}}{3}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}{Directed Acyclic Graphs}}{3}{subsection.2.2}% 
\contentsline {section}{\numberline {3}{Applications}}{4}{section.3}% 
\contentsline {subsection}{\numberline {3.1}{Implementation}}{4}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}{Strengths}}{4}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}{Weaknesses}}{4}{subsection.3.3}% 
\contentsline {section}{\numberline {4}{Conclusion}}{4}{section.4}% 
