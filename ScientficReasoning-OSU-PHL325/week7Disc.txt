Hemple and Oppenheim's model of scientific Explanation (sometimes called the
"Deductive-Nomological" model) is usually thought to have a number of problems,
including at least:

1) It does not seem to be able to distinguish the causal direction. So, while
the height of a flagpole explains the length of the shadow it casts, but the
length of the shadow (rarely!) explains the height of the flagpole. Yet the
model suggests both directions should be equally explanatory. 

2) It demands that we be able to distinguish law-like sentences from things that
look like laws, but aren't really laws of nature. But we can't!

3) It is hard to see how to modify it to deal with explanations for events that
involve invoking causes that are not fully deterministic.

Which problem do you think is the most serious? Briefly explain one of these
three problems, and explain why you think that it is the most serious of the
three. 


I'm struggling to answer this question because I'm failing to see significant
weakness in the model. I think that each of the three supposed problems are
easily overcome.

1) Does not seem able to distinguish the causal direction. 

	In propositional logic, the rule of inference says, "P implies Q" or, "IF P
is true, THEN Q must be true" (notated P->Q). This rule implies causation
because Q->P is not logically equivalent to P->Q. For the flagpole example,
Let P be "The flagpole is 20 ft tall AND the sun is at X angle"(the
proposition P is actually made up of two propositions A AND B), and Q be,
"The shadow cast is 15ft long". P->Q would be a logical statement of
causation and could be sufficiently proven, but the statement Q->P would not
be provable because a bidirectional causation was not supposed by P->Q
therefore the negation is not valid.  To negate P->Q, De Morgan's law is
applied. !(P->Q) becomes !Q->!P (!Q->(!A OR !B)). Or, "If the shadow is not
fifteen feet long IF the flagpole is not 20ft tall OR the sun is not at angle
X", which is a true statement.

2) It requires the ability to distinguish law-like sentences from lookalikes.

	I think within the proof structure, any lookalike statement could be could be
seen to have insufficient supporting explanation or the domain was not correctly
declared. I think even the potential for passing off a weak or happenstance law
in a proof like this, is outweighed by how weaknesses could be identified,
challenged, and the statement modified or reinforced.

3) It is hard to see how to modify it to deal with explanations for events that
involve invoking causes that are not fully deterministic.

	I didn't think I felt anything was particularly weak in this model but after
some thought this is probably the weakest. Laws that are absolute fit nicely
into proof models but establishing a proof around likelihood or probability
doesn't exactly lend itself to an absolute answer. The only way for the model to
deal with it seems to be by expanding the range of the proposition (giving a
+/-), or restricting the domain the such a level that nothing is really being
stated anymore.
