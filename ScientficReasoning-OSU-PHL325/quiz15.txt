van Fraassen writes:

"Which factors are explanatory is decided not by features of the scientific
theory but by concerns brought from outside."

Explain. What does van Fraassen mean by this? How does it fit into van
Fraassen's example of explaining "why the porch light is on"?

NOTE: This question emerges from one of the optional readings -- van Fraassen's
1977 "The Pragmatics of Explanation." Please read page 149 in order to answer
this question.


