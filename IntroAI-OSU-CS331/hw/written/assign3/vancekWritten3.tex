\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usepackage[shortlabels]{enumitem}

\usetikzlibrary{automata,positioning}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass\ \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}


%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{
    \ifnum#1>0
        \setcounter{homeworkProblemCounter}{#1}
    \fi
    \section{Problem \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Written Homework\ \#3}
\newcommand{\hmwkDueDate}{15 May 2020}
\newcommand{\hmwkClass}{Intro to Artificial Intelligence}
\newcommand{\hmwkClassTime}{}
\newcommand{\hmwkClassInstructor}{Dr. Rebecca A. Hutchinson}
\newcommand{\hmwkAuthorName}{\textbf{Kerry Vance}}

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate\ at 10:00am}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor\ \hmwkClassTime}}
    \vspace{3in}
}

\author{\hmwkAuthorName}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}

\setlength{\parindent}{7ex}

\begin{document}

\maketitle

\pagebreak

% Problem 1
\begin{homeworkProblem}
	Write out the following probability distributions.

	\begin{enumerate}[a)]
		\item \textbf{P}(Toothache)

			\begin{tabular}{|c|c|}
				\hline
				P(Toothache = true)	& 0.2	\\
				P(Toothache = false)	& 0.8	\\
				\hline
			\end{tabular}

		\item \textbf{P}(Catch, Cavity)

			\begin{tabular}{|c|c|}
				\hline
				P(Catch = true, Cavity = true)	& 0.18	\\
				P(Catch = true, Cavity = false)	& 0.16	\\
				P(Catch = false, Cavity = true)	& 0.02	\\
				P(Catch = false, Cavity = false)	& 0.64	\\
				\hline
			\end{tabular}

		\item \textbf{P}(Catch $\mid$ Cavity)

			\begin{tabular}{|c|c|}
				\hline
				$\frac{P(Catch = true, Cavity = true)}{P(Cavity = true)}$
				& 0.9	\\
				$\frac{P(Catch = true, Cavity = false)}{P(Cavity = false)}$	&
				0.2	\\
				$\frac{P(Catch = false, Cavity = true)}{P(Cavity = true)}$	& 1
				\\
				$\frac{P(Catch = false, Cavity = false)}{P(Cavity = false)}$ &
				0.8	\\
				\hline
			\end{tabular}
	\end{enumerate}
\end{homeworkProblem}

\pagebreak

%Problem 2
\begin{homeworkProblem}
	Consider a different variant of the Monty Hall problem we did in class.
	In this variant, there are 4 doors; one door reveals a car, and the
	other four doors reveal goats. After you select a door, the host will
	show you 2 goats. The host will not open the door you chose.  Let $C \in
	\{1,2,3,4\}$ be the location of the car, $D \in \{1,2,3,4\}$ be your
	initial choice of door, $H_1 \in \{1,2,3,4\}$ be the first door opened
	by the host, and $H 2 \in \{1,2,3,4\}$ be the second door opened by the
	host. Suppose $\textbf{P}(C) = \textbf{P}(D) = \{ 4 , 4 , 4 , 4 \}$.

	\begin{enumerate}[a)]
		\item What is $\textbf{P}(H_1\mid C, D)$

			\begin{tabular}{|c|c|c|}
				\hline
				C & D & $\textbf{P}(H_1=v_i|C,D)$ \\
				1 & 1 &  $0,\frac{1}{3},\frac{1}{3},\frac{1}{3}$ \\
				1 & 2 &  $0, 0,\frac{1}{2},\frac{1}{2}$ \\
				1 & 3 &  $0, \frac{1}{2},0,\frac{1}{2}$ \\
				1 & 4 &  $0, \frac{1}{2},\frac{1}{2},0$ \\
				2 & 1 &  $0, 0,\frac{1}{2},\frac{1}{2}$ \\
				2 & 2 &  $\frac{1}{3},0,\frac{1}{3},\frac{1}{3} $ \\
				2 & 3 &  $\frac{1}{2},0,0,\frac{1}{2}$ \\
				2 & 4 &  $\frac{1}{2},0,\frac{1}{2},0$ \\
				3 & 1 &  $0,\frac{1}{2},0,\frac{1}{2}$ \\
				3 & 2 &  $\frac{1}{2},0,0,\frac{1}{2}$ \\
				3 & 3 &  $\frac{1}{3},\frac{1}{3},0,\frac{1}{3} $ \\
				3 & 4 &  $\frac{1}{2},\frac{1}{2},0,0$ \\
				4 & 1 &  $0,\frac{1}{2},\frac{1}{2},0$ \\
				4 & 2 &  $\frac{1}{2},0,\frac{1}{2},0$ \\
				4 & 3 &  $\frac{1}{2},\frac{1}{2},0,0$ \\
				4 & 4 &  $\frac{1}{3},\frac{1}{3},\frac{1}{3},0 $ \\
				\hline
			\end{tabular}

		\item What is $\textbf{P}(H_2\mid H_1 = 1, C, D = 3)$


			\begin{tabular}{|c|c|c|c|}
				\hline
				$H_1$ & C & D = 3 & $\textbf{P}(H_2=v_i|H_1=1,C,D=3)$ \\
				1 & 2 & 3 & $0,0,0,1$ \\
				1 & 3 & 3 & $0,\frac{1}{2},0,\frac{1}{2}$ \\
				1 & 4 & 3 & $0,1,0,0$ \\
				\hline
			\end{tabular}
		\item Suppose you select door 3. The host opens doors 1 and 4. What
			are $P(C = 2|D = 3, H_1 = 1, H_2 = 4)$ and $P(C = 3|D = 3, H_1 =
			1, H_2 = 4)$? (At this point, the car cannot be behind doors 1 or
			4 by the rules of the game.) Should you stick with door 3 or
			switch to door 2?

			\setlength{\parindent}{3ex}
			Given D, $H_1,$ and $H_2$ There is a 1/2 probability D=C for 1 or
			3. When D was initially chosen however, the probability of D=C was
			1/4. You should switch to door 2.
	\end{enumerate}

\end{homeworkProblem}

\pagebreak

% Problem 3
\begin{homeworkProblem}
	One test for COVID-19 reports a sensitivity of 95.7\% and a specificity
	of 99.0\%.  Sensitivity means the probability of a positive result,
	given that you have COVID-19. Specificity means the probability of a
	negative result, given that you do NOT have COVID-19. There have been at
	least 2,839 COVID-19 cases in Oregon, and Oregon’s population is
	estimated to be 4,217,737, which gives an infection rate of 0.0673\%.

	\begin{enumerate}[a)]
		\item Based on these numbers, compute the probability that you have
			COVID-19, given that you get a positive test.

			\setlength{\parindent}{3ex}
			\begin{align*}
				\text{Let C} &= \text{COVID} \\
				\text{R}		 &= \text{Test result} \\
			\end{align*}
			\begin{tabular}{|c|c|l|l|l|l|l|}
				\hline
				R & C & $P(C)$ & $P(R | C)$ & $P(R,C)$ & $P(R)$ & $P(C | R)    $\\
				T & T & 0.000673	& 0.957	& 0.000644061	& 0.010637331	&
				0.06054 \\
				T & F & 0.999327	& 0.01	& 0.00999327	& 	& 	\\
				F & T & 0.000673	& 0.043	& 					& 	& 	\\
				F & F & 0.999327	& 0.99	& 					& 	& 	\\
				\hline
			\end{tabular}

			The likelihood You have COVID-19 is ~6.054\% given a positive
			test result.

		\item Since tests have not been widely available, it is possible that
			the actual infection rate is higher than that implied by just the
			confirmed cases. Suppose the infection rate is actually 3\%. How
			does your answer to part (a) change?

			\begin{tabular}{|c|c|l|l|l|l|l|}
				\hline
				R & C & $P(C)$ & $P(R | C)$ & $P(R,C)$ & $P(R)$ & $P(C | R)    $\\
				T & T & 0.03	& 0.957	& 0.02871	& 0.03841	& 0.7474	\\
				T & F & 0.97	& 0.01	& 0.0097		& 	& 	\\
				F & T & 0.03	& 0.043	& 	& 	& 	\\
				F & F & 0.97	& 0.99	& 	& 	& 	\\
				\hline
			\end{tabular}
	\end{enumerate}
\end{homeworkProblem}

\end{document}
