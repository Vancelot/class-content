/*
 * MinimaxPlayer.cpp
 *
 *  Created on: Apr 17, 2015
 *      Author: wong
 */
#include <iostream>
#include <assert.h>
#include "MinimaxPlayer.h"

using std::vector;
using std::pair;
using std::make_pair;

MinimaxPlayer::MinimaxPlayer(char symb) :
		Player(symb) {

}

MinimaxPlayer::~MinimaxPlayer() {

}

void MinimaxPlayer::get_move(OthelloBoard* b, int& col, int& row)
{
	vector<pair<int,int>> as;
	int best = 0, val = 0;
	OthelloBoard tmp = *b;

	succ(b, this->symbol, &as);
	for(auto x : as) {
		tmp.play_move(x.first, x.second, this->symbol);
		if((val = this->min(&tmp)) >= best)
		{
			best = val;
			col = x.first;
			row = x.second;
		}
		tmp = *b;
	}
}


int MinimaxPlayer::max(OthelloBoard *b)
{
	vector<pair<int,int>> as;
	int best = 0, val = 0;
	OthelloBoard tmp = *b;

	if(b->has_legal_moves_remaining(this->symbol))
	{
		succ(b, this->symbol, &as);
		for(auto x : as) {
			tmp.play_move(x.first, x.second, this->symbol);
			if((val = this->min(&tmp)) >= best)
			{
				best = val;
			}
			tmp = *b;
		}
	}
	else {
		best = this->util(b);
	}
	return best;
}

int MinimaxPlayer::min(OthelloBoard *b)
{
	vector<pair<int,int>> as;
	int best = 99999, val = 0;
	OthelloBoard tmp = *b;
	char s = this->symbol == b->get_p1_symbol() ? b->get_p2_symbol() : b->get_p1_symbol();

	if(b->has_legal_moves_remaining(s))
	{
		succ(b, s, &as);
		for(auto x : as) {
			tmp.play_move(x.first, x.second, s);
			if((val = this->max(&tmp)) <= best) {
				best = val;
			}
			tmp = *b;
		}
	}
	else {
		best = this->util(b);
	}
	return best;
}

void MinimaxPlayer::succ(OthelloBoard* b, char s, std::vector<std::pair<int,int>> *as)
{
	for(int i = 0; i < b->get_num_cols(); i++)
	{
		for(int j = 0; j < b->get_num_rows(); j++)
		{
			if(b->is_legal_move(i, j, s)) {
				as->push_back(make_pair(i, j));
			}
		}
	}
}

int MinimaxPlayer::util(OthelloBoard* b) {
	return b->count_score(this->symbol);
}

MinimaxPlayer* MinimaxPlayer::clone() {
	MinimaxPlayer* result = new MinimaxPlayer(symbol);
	return result;
}
