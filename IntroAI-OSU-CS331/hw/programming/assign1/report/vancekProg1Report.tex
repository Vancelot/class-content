\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}

\usetikzlibrary{automata,positioning}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\rhead{\hmwkTitle}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{
    \ifnum#1>0
        \setcounter{homeworkProblemCounter}{#1}
    \fi
    \section{Problem \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Programming Assignment \#1 Report}
\newcommand{\hmwkDueDate}{20, April, 2020}
\newcommand{\hmwkClass}{Intro to Artificial Intelligence}
\newcommand{\hmwkClassInstructor}{Dr. Rebecca A. Hutchinson}
\newcommand{\hmwkAuthorName}{\textbf{Kerry Vance}}

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate\ at 3:10pm}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor}}
    \vspace{3in}
}

\author{\hmwkAuthorName}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}

\setlength{\parindent}{7ex}

\begin{document}

\maketitle

\pagebreak

\section{Methodology}

\indent

	For the first test case, I ran the algorithm by hand and compared the
	output with the expected optimal solution. I then compared the number of
	nodes each algorithm expanded to confirm the performance differences
	between them was reasonable.

	My implementation dfs and iddfs use the same helper function, 'Agent::\_dfs()'. The
	difference being, dfs sets the depth limit to '0 - 1' while iddfs sets
	it to 0 and increments every failed run. The depth limit variable is an
	unsigned integer, so subtracting 1 from 0 effectively sets it to the
	maximum value an integer can hold.

	For the heuristic, I overloaded the '-' operator in my State class that
	returns the overall difference between two States. That information plus
	the path length determines the ordering of the priority queue. The
	heuristic is arguably admissible because each action can only change the
	values by at most 3. So long as the difference is greater than 3, the
	heuristic is admissible.
	
	I implemented a variation of the iterative deepening algorithm that
	significantly improves it's performance greatly while maintaining the
	benefits over dfs we discussed. I called it quickdfs because it's
	resemblance to quicksort in how it divides and conquers. It can be run
	by passing the mode 'qdfs'. Initially it functions similar to iddfs but
	by multiplying the depth by 2 each iteration. Each depth limit at which
	a goal state is not found, a lower limit is set to that depth. Once a
	goal state is reached, that cost is set as an upper limit, the lower
	limit is set to 0. The depth limit is then set to the halfway point
	between the upper and lower. If a goal state is found, it's cost is the
	new upper limit.  If a goal state is not found, the lower is set to that
	depth limit. This process is repeated until the upper and lower limit
	converge. The depth at which they converge is the optimal solution.

\section{Results}
\begin{center}
	\begin{tabular}{*{6}{|l}|}
		\hline
		 & bfs & dfs & iddfs & qdfs & astar \\
		\hline
		test 1	&	11, 14		&	11, 11		&	11, 97		& 11, 71		&	11, 12	\\
		test 2	&	35, 60		&	35, 48		&	35, 1185		& 35, 389	&	35, 50	\\
		test 3	&	387, 970		&	391, 865		&	389, 206630	& 389, 9154 &	387, 389		\\
		\hline
	\end{tabular}

	Key: path cost, nodes expanded
\end{center}

\section{Discussion}

\indent

	We discussed that iddfs had worse complexity than dfs, but not
	significantly due to most nodes being at the bottom anyway. I was
	surprised by how many more nodes were expanded compared to the other
	algorithms.

	I believe there is something wrong with my underlying '\_bfs()'
	function. On test case 3 iddfs is not finding the optimal solution of
	387. I believe this has to do with the way I'm checking the frontier and
	explored lists. My find functions do not compare the paths, only the
	states. There must be a path to a similar state that's not being
	checked. The cost is only off by two, so I feel like the results are
	still rather good. The fact that my qdfs algorithm is finding the same
	solution as iddfs with significantly fewer expansions shows promise.

\section{Conclusion}

\indent

	I don't really see any case where iddfs would be a desirable algorithm
	to use. I understand it's advantages over dfs but there is a lot of room
	for optimization. I'm sure in my qdfs there could be implementations
	similar such as using a randomized pivot point between the upper and
	lower bounds rather than the center point. Astar performs best, which
	was expected. Astar has more information about the problem, whereas the
	other algorithms are more like brute force approaches.

\end{document}

