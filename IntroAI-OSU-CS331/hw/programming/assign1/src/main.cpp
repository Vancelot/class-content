#include"Header.h"

void printUsage() {
	cout << "Usage: ./main <InitialState> <GoalState> <mode> <OutputFile>" << endl;
	error("");
}

// ./main initialState, goalState, mode, outputFile
int main(int argc, char *argv[])
{
	Agent agent;
	ofstream of;

	if(argc != 5) {
		cout << "Incorrect number of arguments." << endl;
		printUsage();
	}
	else {
		agent.init(argv[1], argv[2], argv[4]);
	}
	agent.call(argv[3]);
	return 0;
}
