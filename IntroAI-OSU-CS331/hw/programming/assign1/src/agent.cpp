#include"Header.h"

using namespace std;

Agent::Agent()
{
	fmap.insert(make_pair("bfs", &Agent::bfs));
	fmap.insert(make_pair("dfs", &Agent::dfs));
	fmap.insert(make_pair("iddfs", &Agent::iddfs));
	fmap.insert(make_pair("astar", &Agent::astar));
	fmap.insert(make_pair("qdfs", &Agent::qdfs));
}

void Agent::init(string iName, string gName, string ofName)
{
	ifstream ifs;
	smatch m;
	regex e("(\\d+),(\\d+),(\\d+)");
	string buf;
	int tempState[2][3];

	ifs.open(iName);
	for(int i = 0; i < 2; i++)
	{
		getline(ifs, buf);
		regex_search(buf, m, e);
		for(int j = 0; j < 3; j++) {
			tempState[i][j] = stoi(m[j + 1]);
		}
	}
	ifs.close();
	initialState = {tempState[1][0], tempState[1][1], tempState[1][2], tempState[0][0], tempState[0][1], tempState[0][2], {}};
	frontier.push_back(initialState);

	ifs.open(gName);
	for(int i = 0; i < 2; i++)
	{
		getline(ifs, buf);
		regex_search(buf, m, e);
		for(int j = 0; j < 3; j++) {
			tempState[i][j] = stoi(m[j + 1]);
		}
	}
	goalState = {tempState[1][0], tempState[1][1], tempState[1][2], tempState[0][0], tempState[0][1], tempState[0][2], {}};
	ifs.close();
	ofs.open(ofName);
	cout << endl;
}

void Agent::call(string s)
{
	func fp = fmap[s];
	if(fp != NULL) (this->*fp)();
	else error("Invalid mode");
}

void Agent::bfs()
{
	State tmp, cur;

	while(!(frontier.front() == goalState) && frontier.size() > 0)
	{
		explored.insert(frontier.front());
		cur = frontier.front();
		frontier.pop_front();
		expanded++;
		for(int i = 0; i < NUM_ACTION; i++)
		{
			tmp = action(i, cur);
			if(tmp.valid() && find(frontier.begin(), frontier.end(), tmp) == frontier.end() && explored.find(tmp) == explored.end()) {
				tmp.path.push_back(i);
				frontier.push_back(tmp);
			}
		}
	}
	if(frontier.front() == goalState)
	{
		cout << "MODE: bfs" << endl;
		ofs << "MODE: bfs" << endl;
		goalState = frontier.front();
		goalState.printState();
		printPath();
	}
	else
		cout << "No solution?" << endl;
}

void Agent::dfs()
{
	depthLimit = 0 - 1;
	_dfs();
	if(frontier.front() == goalState)
	{
		goalState = frontier.front();
		cout << "MODE: dfs" << endl;
		ofs << "MODE: dfs" << endl;
		printPath();
	}
}

void Agent::_dfs()
{
	State tmp, cur;

	while(!(frontier.front() == goalState) && !frontier.empty())
	{
		explored.insert(frontier.front());
		cur = frontier.front();
		frontier.pop_front();
		expanded++;
		if((unsigned int)cur.path.size() < depthLimit)
		{
			for(int i = 0; i < NUM_ACTION; i++)
			{
				tmp = action(i, cur);
				if(tmp.valid() && find(frontier.begin(), frontier.end(), tmp) == frontier.end() && explored.find(tmp) == explored.end()) {
					tmp.path.push_back(i);
					frontier.push_front(tmp);
				}
			}
		}
	}
}

void Agent::qdfs()
{
	depthLimit = 1;
	while(!(frontier.front() == goalState))
	{
		lowdl = depthLimit;
		explored.clear();
		frontier.clear();
		frontier.push_front(initialState);
		depthLimit *= 2;
		_dfs();
	}
	goalState = frontier.front();
	updl = goalState.path.size();

	while(lowdl != updl)
	{
		lowdl += updl - lowdl <= 1 ? 1 : 0;
		explored.clear();
		frontier.clear();
		frontier.push_front(initialState);
		depthLimit = (updl + lowdl) / 2;
		_dfs();
		if(!frontier.empty())
		{
			goalState = frontier.front();
			updl = depthLimit;
		}
		else {
			lowdl = depthLimit;
		}
	}

	if(frontier.front() == goalState)
	{
		goalState = frontier.front();
		cout << "MODE: qdfs" << endl;
		ofs << "MODE: qdfs" << endl;
		printPath();
	}
}


void Agent::iddfs()
{
	while(!(frontier.front() == goalState))
	{
		explored.clear();
		frontier.clear();
		frontier.push_front(initialState);
		depthLimit++;
		_dfs();
	}
	if(frontier.front() == goalState)
	{
		goalState = frontier.front();
		cout << "MODE: iddfs" << endl;
		ofs << "MODE: iddfs" << endl;
		printPath();
	}
}

void Agent::astar()
{
	starFront.push(initialState);
	starFrontCont.insert(initialState);
	_astar();
}

void Agent::_astar()
{
	State tmp, cur;

	if(starFront.top() == goalState)
	{
		cout << "MODE: astar" << endl;
		ofs << "MODE: astar" << endl;
		goalState = starFront.top();
		printPath();
	}
	else if(starFront.size() == 0) {
		cout << "No solution? " << endl;
	}
	else
	{
		explored.insert(starFront.top());
		cur = starFront.top();
		starFront.pop();
		starFrontCont.erase(cur);
		expanded++;
		for(int i = 0; i < NUM_ACTION; i++)
		{
			tmp = action(i, cur);
			if(tmp.valid() && starFrontCont.find(tmp) == starFrontCont.end() && explored.find(tmp) == explored.end()) {
				tmp.path.push_back(i);
				starFront.push(tmp);
				starFrontCont.insert(tmp);
			}
		}
		_astar();
	}
}

State Agent::action(int a, State st)
{
	State tmp = st;
	switch(a)
	{
		// Put one chicken in the boat
		case 0:
			if(tmp.lb == 1) {
				tmp.lc--;
				tmp.lb = 0;
				tmp.rb = 1;
				tmp.rc++;
			}
			else if(tmp.rb == 1) {
				tmp.rc--;
				tmp.rb = 0;
				tmp.lb = 1;
				tmp.lc++;
			}
			break;
		// Put two chickens in the boat
		case 1:
			if(tmp.lb == 1) {
				tmp.lc -= 2;
				tmp.lb = 0;
				tmp.rb = 1;
				tmp.rc += 2;
			}
			else if(tmp.rb == 1) {
				tmp.rc -= 2;
				tmp.rb = 0;
				tmp.lb = 1;
				tmp.lc += 2;
			}
			break;
		// Put one wolf in the boat
		case 2:
			if(tmp.lb == 1) {
				tmp.lw--;
				tmp.lb = 0;
				tmp.rb = 1;
				tmp.rw++;
			}
			else if(tmp.rb == 1) {
				tmp.rw--;
				tmp.rb = 0;
				tmp.lb = 1;
				tmp.lw++;
			}
			break;
		// Put one wolf and one chicken in the boat
		case 3:
			if(tmp.lb == 1) {
				tmp.lw--;
				tmp.lc--;
				tmp.lb = 0;
				tmp.rb = 1;
				tmp.rw++;
				tmp.rc++;
			}
			else if(tmp.rb == 1) {
				tmp.rw--;
				tmp.rc--;
				tmp.rb = 0;
				tmp.lb = 1;
				tmp.lw++;
				tmp.lc++;
			}
			break;
		// Put two wolves in the boat
		case 4:
			if(tmp.lb == 1) {
				tmp.lw -= 2;
				tmp.lb = 0;
				tmp.rb = 1;
				tmp.rw += 2;
			}
			else if(tmp.rb == 1) {
				tmp.rw -= 2;
				tmp.rb = 0;
				tmp.lb = 1;
				tmp.lw += 2;
			}
			break;
	}
	return tmp;
}

void Agent::printPath()
{
	State tmp = initialState;

	cout << "Goal state reached\nNodes expanded: " << expanded << "\nCost: " << goalState.path.size() << endl;
	ofs << "Goal state reached\nNodes expanded: " << expanded << "\nCost: " << goalState.path.size() << endl;

	cout << "ACTION PATH: ";
	ofs << "ACTION PATH: ";
	for(auto x:goalState.path) {
		cout << x + 1 << ' ';
		ofs << x + 1 << ' ';
	}
	cout << endl;
	ofs << endl;

	ofs << "INITIAL STATE:";
	ofs << "\n\t\tLEFT\tRIGHT\nwolves:\t\t" << tmp.lw << '\t' << tmp.rw << "\nchickens:\t"  << tmp.lc << '\t' << tmp.rc << "\nboat:\t\t" << tmp.lb << '\t' << tmp.rb << endl << endl;
	for(auto x:goalState.path) {
		ofs << "ACTION " << x + 1 << ": " << actions[x];
		tmp = action(x, tmp);
		ofs << "\n\t\tLEFT\tRIGHT\nwolves:\t\t" << tmp.lw << '\t' << tmp.rw << "\nchickens:\t"  << tmp.lc << '\t' << tmp.rc << "\nboat:\t\t" << tmp.lb << '\t' << tmp.rb << endl << endl;
	}
}
