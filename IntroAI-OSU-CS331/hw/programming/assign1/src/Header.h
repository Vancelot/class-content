#ifndef __HEADER__
#define __HEADER__

#include<iostream>
#include<fstream>
#include<map>
#include<deque>
#include<queue>
#include<vector>
#include<unordered_set>
#include<regex>

#define NUM_ACTION 5

using namespace std;

inline void error(string s) { cerr << s << endl; exit(EXIT_FAILURE);};

const string actions[5] {	"Put one chicken in the boat",
									"Put two chickens in the boat",
									"Put one wolf in the boat",
									"Put one wolf and one chicken in the boat",
									"Put two wolves in the boat"};

struct State
{
	void printState() const
	{
		cout << "PATH: ";
		for(auto x:path) {
			cout << x  << ' ';
		}
		cout << "\n\t\tLEFT\tRIGHT\nwolves:\t\t" << lw << '\t' << rw << "\nchickens:\t"  << lc << '\t' << rc << "\nboat:\t\t" << lb << '\t' << rb << endl << endl;
	}
	bool operator== (const State& st) const {
		return (this->lw  == st.lw && this->lc == st.lc && this->lb == st.lb && this->rw  == st.rw && this->rc == st.rc && this->rb == st.rb);
	}
	bool valid() {
		return (lw <= lc || lc == 0) && (rw <= rc || rc == 0) && lw >= 0 && rw >= 0 && lc >= 0 && rc >= 0;
	}
	int operator - (const State st) {
		return lc - st.lc + lw - st.lw + lb - st.rb + rc - st.rc + rw - st.rw + rb - st.rb;
	}
	int lc, lw, lb, rc, rw, rb;
	deque<int> path;
};

class StateHash
{
	public:
	int operator()(const State& st) const {
		return st.lw << 0 & st.lc << 1 & st.lb << 3 & st.rw << 4 & st.rc << 5 & st.rb << 6;
	}
};

class Heuristic
{
	private:
		State *g;
	public:
		Heuristic(State *goal) { g = goal; }
		bool operator() (State lh, State rh)
		{
			return ((lh - *g) + (int)lh.path.size()) < ((rh - *g) + (int)rh.path.size()) ?  true : false;
		}
};

class Agent
{
	private:
		void bfs();
		void dfs();
		void _dfs();
		void iddfs();
		void astar();
		void _astar();
		void qdfs();
		State action(int, State);
		void printPath();

		typedef void (Agent::*func)();
		map <string, func> fmap;

		State initialState, goalState;
		ofstream ofs;
		int expanded = 0;
		unsigned int depthLimit = 0, lowdl = 0, updl = 0;

		typedef priority_queue<State, vector<State>, Heuristic> StatePq;
		StatePq starFront {Heuristic(&goalState)};
		deque<State> frontier;
		unordered_set<State, StateHash> explored, starFrontCont;
	public:

		Agent();
		void init(string, string, string);
		void call(string);
};

#endif
