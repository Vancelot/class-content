#! /bin/python3

import re
import math

def bayes():
    vocab = []
    trainSet = []
    testSet = []
    dist = {}
    absDist = {}

    def getVocab(fileName):
        with open(fileName) as f:
            for i in f:
                for j in re.findall("[a-zA-Z]+", i.lower()):
                    if j not in vocab:
                        vocab.append(j)
        vocab.sort()
        vocab.append("classlabel")

    def preproc():
        trues = 0
        count = 0
        with open("trainingSet.txt") as f:
            for i in f:
                temp = []
                for j in re.findall("[a-zA-Z]+", i.lower()):
                    temp.append(vocab.index(j))
                if i[len(i)-3] == "1":
                    temp.append(vocab.index("classlabel"))
                    trues += 1
                count += 1
                trainSet.append(temp)

        with open("preprocessed_train.txt", "w") as f:
            for i in range(0,len(vocab)-2):
                f.write(f"{vocab[i]},")
            f.write(f"{vocab[len(vocab)-1]}\n")

            for i in trainSet:
                temp=""
                for j in range(0, len(vocab)-2):
                    temp += ("0,", "1,")[j in i]
                temp += ("0","1")[vocab.index("classlabel") in i]
                f.write(temp + '\n')

        absDist["TOTAL"] = count
        absDist["classlabel"] = {True: trues, False: count-trues}

        with open("testSet.txt") as f:
            for i in f:
                temp = []
                for j in re.findall("[a-zA-Z]+", i.lower()):
                    if j in vocab:
                        temp.append(vocab.index(j))
                if i[len(i)-3] == "1":
                    temp.append(vocab.index("classlabel"))
                testSet.append(temp)

        with open("preprocessed_test.txt", "w") as f:
            for i in range(0,len(vocab)-2):
                f.write(f"{vocab[i]},")
            f.write(f"{vocab[len(vocab)-1]}\n")

            for i in testSet:
                temp=""
                for j in range(0, len(vocab)-2):
                    temp += ("0,", "1,")[j in i]
                temp += ("0","1")[vocab.index("classlabel") in i]
                f.write(temp + '\n')

    def countRec(k):
        if k[0] not in absDist:
            count = 0
            idx = vocab.index(k[0])
            for i in trainSet:
                if idx in i:
                    count += 1
            absDist[k[0]] = {True: count, False: absDist["TOTAL"] - count}
        return absDist[k[0]][k[1]]

    def P(q, e = []):
        if len(e) == 0:
            if q[0] not in dist:
                if q[1] == True:
                    dist[q] = countRec(q)/absDist["TOTAL"]
                if q[1] == False:
                    dist[q] = countRec(q)/absDist["TOTAL"]
            return dist[q]
        elif len(e) == 1:
            e = e[0]
            key = f"{q[0]}={q[1]}|{e[0]}={e[1]}"
            if key not in dist:
                num = 0
                l = (vocab.index(q[0]), q[1])
                r = (vocab.index(e[0]), e[1])
                for i in trainSet:
                    if l[1] == (l[0] in i) and r[1] == (r[0] in i):
                        num += 1
                dist[key] = (num + 1) / (countRec(e) + 2)
            return dist[key]
        else:
            e.sort()
            key = f"{q[0]}={q[1]}|"
            for i in e:
                key += f"{i[0]}={i[1]},"
            if key not in dist:
                dist[key] = math.log(P(q), 10)
                for i in e:
                    dist[key] += math.log(P(i,[q]), 10)
            return dist[key]

    def predict(s):
        hits = 0
        total = 0
        for i in s:
            temp = []
            for j in range(0,len(vocab) - 2):
                if j in i:
                    temp.append((vocab[j], True))
                else:
                    temp.append((vocab[j], False))
            if P(("classlabel", True), temp) > P(("classlabel", False), temp):
                hits += 1 if vocab.index("classlabel") in i else 0
            else:
                hits += 1 if vocab.index("classlabel") not in i else 0
            total += 1
        return hits, total

    getVocab("trainingSet.txt")

    preproc()

    with open("results.txt", "w") as f:
        f.write("Training File: trainingSet.txt\t Test File: trainingSet.txt:\n")
        results = predict(trainSet)
        f.write("Accuracy: {:.2%}\n".format(results[0]/results[1]))

        f.write("Training File: trainingSet.txt\t Test File: testSet.txt:\n")
        results = predict(testSet)
        f.write("Accuracy: {:.2%}\n".format(results[0]/results[1]))

    predict(testSet)

if __name__ == "__main__":
    print("Naive bayes net\n")


    bayes()
