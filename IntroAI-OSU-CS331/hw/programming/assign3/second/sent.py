#! /bin/python3

import re
import math

def bayes():
    vocab = []
    trainSet = []
    testSet = []
    distribution = {}
    absDist = {}
    prediction = {}

    def getVocab(fileName):
        with open(fileName) as f:
            for i in f:
                for j in re.findall("[a-zA-Z]+", i.lower()):
                    if j not in vocab:
                        vocab.append(j)
        vocab.sort()

    def preprocTrain():
        trues = 0
        count = 0
        with open("trainingSet.txt") as f:
            for i in f:
                temp = []
                for j in re.findall("[a-zA-Z]+", i.lower()):
                        temp.append(vocab.index(j))
                temp.append(re.search("([01])\s+$", i).groups()[0])
                count += 1
                trues += int(temp[len(temp)-1])
                trainSet.append(temp)

        with open("preprocessed_train.txt", "w") as f:
            for i in vocab:
                f.write(f"{i},")
            f.write("classlabel\n")

            for i in range(0, len(trainSet)):
                temp=""
                for j in range(0, len(vocab)):
                    temp += ("0,", "1,")[j in trainSet[i]]
                temp += trainSet[i][len(trainSet[i])-1]
                f.write(temp + '\n')

        absDist[("classlabel")] = {True: trues, False: count - trues}
        absDist["TOTAL"] = count
        distribution[("classlabel",True)] = trues/count
        distribution[("classlabel",False)] = (count - trues)/count

    def preproc():
        with open("testSet.txt") as f:
            for i in f:
                temp = []
                for j in re.findall("[a-zA-Z]+", i.lower()):
                    if j in vocab:
                        temp.append(vocab.index(j))
                temp.append(re.search("([01])\s+$", i).groups()[0])
                testSet.append(temp)

        with open("preprocessed_test.txt", "w") as f:
            for i in vocab:
                f.write(f"{i},")
            f.write("classlabel\n")

            for i in range(0, len(testSet)):
                temp=""
                for j in range(0, len(vocab)):
                    temp += ("0,", "1,")[j in trainSet[i]]
                temp += trainSet[i][len(trainSet[i])-1]
                f.write(temp + '\n')

    def countRec(k):
        if k[0] not in absDist:
            count = 0
            idx = vocab.index(k[0])
            for i in trainSet:
                if idx in i:
                    count += 1
            absDist[k[0]] = {True: count, False: absDist["TOTAL"] - count}
        return absDist[k[0]][k[1]]


    def P(q, e = []):
        if len(e) == 0:
            if q in distribution:
                return distribution[q]
            else:
                distribution[(q[0], False)] = countRec((q[0], False))/distribution["TOTAL"]
                distribution[(q[0], True)] = countRec((q[0], True)) / distribution["TOTAL"]
                return distribution[q]
        if len(e) == 1:
            e = e[0]
            if f"{q[0]}={q[1]}|{e[0]}={e[1]}" not in distribution:
                l = (vocab.index(q[0]) if q[0] != "classlabel" else -1, "1" if q[1] else "0")
                r = (vocab.index(e[0][0]) if e[0] != "classlabel" else -1, "1" if e[1] else "0")
                count = 0

                for i in trainSet:
                    if l[0] >= 0 and r[0] >= 0:
                        if l[0] in i and r[0] in i:
                            count += 1
                    elif l[0] >= 0 and r[0] < 0:
                        if l[0] in i and i[len(i) - 1] == r[1]:
                            count += 1
                    elif l[0] < 0 and r[0] >= 0:
                        if i[len(i) - 1] == l[1] and r[0] in i:
                            count += 1
                distribution[f"{q[0]}={q[1]}|{e[0]}={e[1]}"] = (count + 1) / (countRec(e) + 2)
            return distribution[f"{q[0]}={q[1]}|{e[0]}={e[1]}"]
        else:
            e.sort()
            key = f"{q[0]}={q[1]}|"
            for i in e:
                key += f"{i[0]}={i[1]},"
            if key not in distribution:
                distribution[key] = math.log(P(q))
                for i in e:
                    distribution[key] += math.log(P(i, [q]))
            return distribution[key]

    def classify():
        for i in testSet:
            cond = []
            for j in range(0, len(i) - 1):
                cond.append((vocab[i[j]], True))
            if P(("classlabel", True), cond) > P(("classlabel", False), cond):
                print("1")
            else:
                print("0")


    getVocab("trainingSet.txt")

    preprocTrain()
    preproc()

#    classify()

    print(distribution)

if __name__ == "__main__":
    print("Naive bayes net\n")
    bayes()
