# Defending Dissent in the United States:

## Cryptography, Digital Suppression, and Collective Defense 
### An open textbook written in support of CS175: Communications Security & Social Movements (Oregon State University)

Version 0.9 

#### Glencora Borradaile & Michele Gretes

Glencora Borradaile, Associate Professor of Electrical Engineering and Computer Science, Oregon State University 

Michele Gretes, Digital Security Coordinator at the Civil Liberties Defense Center; Research Associate & Instructor at Oregon State University

## Table of Contents

<!--[in accordance with the original book proposal, future revisions of this text will add feature Part 0/Preface: History of Surveillance Abuses as well as an expanded Part III: expanded Countermeasures]-->

#### Reading Note

This open textbook can be read linearly -- begin with cryptography fundamentals, next learn about their application to the suppression of social movement groups and how they enable effective countermeasures.  Alternative reading sequences are possible, and will be provided in future revisions to the text (for example, playlists for social movement groups, digital security trainers, or other academic courses).

<!--comments made like so are suppressed in HTML rendering and PDF printing-->

<!--to-do: hooks for inserting theme-specific social-movement/suppression history, i.e. highlight where CS 175 uses black liberation struggles as a focus, and where other social struggles, based on available expertise or course-specific interest could be incorporated -->

### Introduction

1. [Scope of this text: social movements, suppression, and the goals of digital security](0-1_intro.md)

### Part I: Cryptography - Principles & Examples
1. [What is encryption?](1-1_cryptography.md)   
1. [Modern cryptography](1-2_modern-cryptography.md)
1. [Key Exchange: How to agree on a cryptographic key over the Internet](1-3_key-exchange.md)
1. [Cryptographic hash](1-4_cryptographic-hash.md)
1. [How the man in the middle can foil your crypto, and what you can do about it](1-5_man-in-the-middle.md)
1. [Passwords](1-6_passwords.md)
1. [Public key cryptography](1-7_public-key-cryptography.md)
1. [Crytographic signing](1-8_authenticity.md)
1. [Metadata](1-9_metadata.md)
1. [Anonymous Routing](1-10_anonymous-routing.md)


<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.*

*once the open textbook reaches maturity (v.1.0) a more permissive license (to allow remixing) will likely apply.
