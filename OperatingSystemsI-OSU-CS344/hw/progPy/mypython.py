#! /usr/bin/python3

import random
import string

def createFiles():
    fo = []
    for i in range(0,3,1):
        fo.append(open(f"file{i}", "w"))
        for j in range(0,10,1):
            fo[i].write(random.choice(string.ascii_lowercase))
        fo[i].write('\n')
        fo[i].close

def readFiles():
    fo = []
    for i in range(0,3,1):
        fo.append(open(f"file{i}", "r"))
        for l in fo[i]:
            print(l.rstrip()) 
    
def randNum():
    x = random.randrange(0, 42)
    y = random.randrange(0, 42)
    print(f"{x}\n{y}\n{x*y}")

if __name__ == "__main__":
    createFiles()
    readFiles()
    randNum()
