#include"Header.h"

jmp_buf jmpINT, jmpUSR1;

void SIGINThndl(int sig) {
	fprintf(stderr, "SIGINT %d received\n", sig);
	longjmp(jmpINT, 1);
}
void SIGUSR1hndl(int sig) {
	fprintf(stderr, "SIGUSR1 %d received\n", sig);
	longjmp(jmpUSR1, 1);
}

int main(int argc, char *argv[])
{
	int port = 0, sockFD = 0, connFD = 0, t = 1, i = 0, procCount = 0, status = 0, errsv = 0, bufSize = 0;
	pid_t pid[5], wpid[5];
	char *key, *buf, *encBuf, *cliName;
	struct sockaddr_in servAddr, cliAddr;
	socklen_t cInfoSize = sizeof(cliAddr);

	if(signal(SIGINT, SIGINThndl) == SIG_ERR) error("Could not handle SIGINT");
	if(signal(SIGUSR1, SIGUSR1hndl) == SIG_ERR) error("Could not handle SIGUSR1");

	for(i = 0; i < 5; i++) {
		pid[i] = wpid[i] = 0;
	}

	// check for valid argument count
	if(argc < 2 || argc > 2) error("incorrect number of arguments\nUsage: otp_enc_d [PORT_NUM]\n");

	// check for valid port
	if((port = atoi(argv[1])) <= 0) error("Argument not a valid port\n");

	memset((char *)&servAddr, '\0', sizeof(servAddr)); // Initialize sockaddr_in struct
	servAddr.sin_family = AF_INET; // AF_INET ==  IP protocol family
	servAddr.sin_port = htons(port); // converts host byte order to network byte order
	servAddr.sin_addr.s_addr = INADDR_ANY; // INADDR_ANY == accept any incoming messages

	// Setup socket domain = IP protocol, type = byte streams, protocol = default
	if((sockFD = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0)) < 0) error("Error opening socket\n");
	if((setsockopt(sockFD, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(t))) < 0) error("Error setting reuse address socket option\n");
	// Assign local socket address
	if((bind(sockFD, (struct sockaddr *)&servAddr, sizeof(servAddr))) < 0)
		error("Error binding socket\n");
	// Open socket to listen to up to 5 connections
	if(listen(sockFD, 5) < 0) error("Error listening on socket\n");

	while(!setjmp(jmpINT))
	{
		// Accept connection blocking until available
		errsv = 0;
		do
		{
			if(procCount > 0)
			{
				for(i = 0; i < procCount; i++)
				{
					if(pid[i] > 0)
					{
						wpid[i] = waitpid(pid[i], &status, WNOHANG | WUNTRACED);
						if(wpid[i] == pid[i])
						{
							if(WIFEXITED(status)) printf("Process %d exited with %d\n", pid[i], WEXITSTATUS(status));
							else if(WIFSIGNALED(status))  printf("Process %d, terminated by signal %d\n", pid[i], WTERMSIG(status));
							else if(WIFSTOPPED(status))  printf("Process %d, stopped by signal %d\n", pid[i], WSTOPSIG(status));
							wpid[i] = pid[i] = 0;
							procCount--;
						}
					}
				}
			}
			// Don't use so much cpu while waiting in background
			//sleep(1);
			connFD = accept(sockFD, (struct sockaddr *)&cliAddr, &cInfoSize);
			errsv = errno;
		} while((errsv == EAGAIN || errsv == EWOULDBLOCK || errsv == 0) && connFD < 0);
		switch(pid[procCount] = fork())
		{
			// Error forking
			case -1:
				error("Error forking process \n");		

			// Child process
			case 0:
				if(setjmp(jmpUSR1)) {
					if((close(connFD)) != 0) error("Error closing connection\n");
					if((close(sockFD)) != 0) error("Error closing listen socket\n");
					exit(1);
				}
				// Receive Client name
				recvMsg(&cliName, connFD);
				if(strstr(cliName, "otp_enc") == NULL)
				{
					sendMsg(cliName, connFD);
					free(cliName);
					close(connFD);
					error("Error program not allowed to communicate with server\n");
				}
				free(cliName);

				sendMsg(argv[0], connFD);

				// Receive Key
				recvMsg(&key, connFD);

				// Receive plaintext
				recvMsg(&buf, connFD);

				bufSize = strlen(buf);
				encBuf = (char*)malloc(sizeof(char) * (bufSize + 1));
				memset(encBuf, '\0', bufSize + 1);
				for(i = 0; i < bufSize; i++) {
					encBuf[i] = buf[i] == ' ' ? buf[i] : (((buf[i] - 65) + (key[i] - 65)) % 26) + 65;
				}

				// Send encoded text
				sendMsg(encBuf, connFD);

				if((close(connFD)) != 0) error("Error closing connection\n");
				free(buf);
				free(key);
				free(encBuf);
				return 0;

			// Parent process
			default:
				procCount = procCount == 4 ? 0 : procCount + 1;
		}
	}

	for(i = 0; i < 5; i++) {
		if(pid[i] > 0)
		{
			wpid[i] = waitpid(pid[i], &status, WNOHANG | WUNTRACED);
			if(wpid[i] == pid[i])
			{
				if(WIFEXITED(status)) printf("Process %d exited with %d\n", pid[i], WEXITSTATUS(status));
				else if(WIFSIGNALED(status))  printf("Process %d, terminated by signal %d\n", pid[i], WTERMSIG(status));
				else if(WIFSTOPPED(status))  printf("Process %d, stopped by signal %d\n", pid[i], WSTOPSIG(status));
				wpid[i] = pid[i] = 0;
			}
			kill(pid[i], SIGUSR1);
		}
	}
	if(fcntl(connFD, F_GETFD) != -1 || errno != EBADF )
		if((close(connFD)) != 0) error("Error closing connection\n");
	if(fcntl(sockFD, F_GETFD) != -1 || errno != EBADF )
		if((close(sockFD)) != 0) error("Error closing listen socket\n");

	return 0;
}
