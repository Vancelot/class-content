#ifndef __HEADER__
#define __HEADER__

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<netinet/in.h>
#include<netdb.h>
#include<assert.h>
#include<sys/wait.h>
#include<signal.h>
#include<setjmp.h>
#include<errno.h>
#include<fcntl.h>

#define h_addr h_addr_list[0]

#define BUFSIZE 256

void error(const char *msg) { perror(">>>SERVER: "); perror(msg); exit(1); }

void sendMsg(char *msg, int sock)
{
	int totalBytes = 0, bytes = 0, size = strlen(msg);
	char *ptr = msg;

	if((bytes = send(sock, &size, sizeof(size), 0)) < 0) error("Error sending size of message\n");

	bytes = 0;
	do
	{
		ptr += bytes;
		if(send(sock, ptr, size - totalBytes, 0) < 0) error("Error sending data\n");
		if(recv(sock, &bytes, sizeof(bytes), 0) < 0) error("Error receiving data\n");
		totalBytes += bytes;
	} while(totalBytes < size);
}

void recvMsg(char **dest, int sock)
{
	int size = 0, totalBytes = 0, bytes = 0;
	char *ptr;

	if((bytes = recv(sock, &size, sizeof(size), 0)) < 0) error("Error reading from socket\n");

	*dest = (char*)malloc(sizeof(char) * (size + 1));
	memset(*dest, '\0', size + 1);
	ptr = *dest;

	bytes = 0;
	do
	{
		ptr += bytes;
		totalBytes += bytes = recv(sock, ptr, size - totalBytes, 0);
		if(send(sock, &bytes, sizeof(bytes), 0) < 0) error("Error sending data\n");
	} while(totalBytes < size);
}

#endif
