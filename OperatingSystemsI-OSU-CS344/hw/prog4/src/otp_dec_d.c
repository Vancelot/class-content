#include"Header.h"

jmp_buf jmpINT, jmpUSR1;

void SIGINThndl(int sig) {
	fprintf(stderr, "SIGINT %d received\n", sig);
	longjmp(jmpINT, 1);
}
void SIGUSR1hndl(int sig) {
	fprintf(stderr, "SIGUSR1 %d received\n", sig);
	longjmp(jmpUSR1, 1);
}

int main(int argc, char *argv[])
{
	int port = 0, sockfd = 0, connfd = 0, t = 1, i = 0, procCount = 0, status = 0, errsv = 0, bufSize = 0;
	pid_t pid[5], wpid[5];
	char *key, *buf, *decBuf, *cliName;
	struct sockaddr_in servAddr, cliAddr;
	socklen_t cInfoSize = sizeof(cliAddr);

	if(signal(SIGINT, SIGINThndl) == SIG_ERR) error("Could not handle SIGINT");
	if(signal(SIGUSR1, SIGUSR1hndl) == SIG_ERR) error("Could not handle SIGUSR1");

	for(i = 0; i < 5; i++) {
		pid[i] = wpid[i] = 0;
	}

	// check for valid argument count
	if(argc < 2 || argc > 2) error("incorrect number of arguments\nUsage: otp_enc_d [PORT_NUM]\n");

	// check for valid port
	if((port = atoi(argv[1])) <= 0) error("Argument not a valid port\n");

	memset((char *)&servAddr, '\0', sizeof(servAddr)); // Initialize sockaddr_in struct
	servAddr.sin_family = AF_INET; // AF_INET ==  IP protocol family
	servAddr.sin_port = htons(port); // converts host byte order to network byte order
	servAddr.sin_addr.s_addr = INADDR_ANY; // INADDR_ANY == accept any incoming messages

	// Setup socket domain = IP protocol, type = byte streams, protocol = default
	if((sockfd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0)) < 0) error("Error opening socket\n");
	if((setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(t))) < 0) error("Error setting reuse address socket option\n");
	// Assign local socket address
	if((bind(sockfd, (struct sockaddr *)&servAddr, sizeof(servAddr))) < 0)
		error("Error binding socket\n");
	// Open socket to listen to up to 5 connections
	if(listen(sockfd, 5) < 0) error("Error listening on socket\n");

	while(!setjmp(jmpINT))
	{
		// Accept connection blocking until available
		errsv = 0;
		do
		{
			if(procCount > 0)
			{
				for(i = 0; i < procCount; i++)
				{
					if(pid[i] > 0)
					{
						wpid[i] = waitpid(pid[i], &status, WNOHANG | WUNTRACED);
						if(wpid[i] == pid[i])
						{
							if(WIFEXITED(status)) printf("Process %d exited with %d\n", pid[i], WEXITSTATUS(status));
							else if(WIFSIGNALED(status))  printf("Process %d, terminated by signal %d\n", pid[i], WTERMSIG(status));
							else if(WIFSTOPPED(status))  printf("Process %d, stopped by signal %d\n", pid[i], WSTOPSIG(status));
							wpid[i] = pid[i] = 0;
							procCount--;
						}
					}
				}
			}
			// Don't use so much cpu while waiting in background
			//sleep(1);
			connfd = accept(sockfd, (struct sockaddr *)&cliAddr, &cInfoSize);
			errsv = errno;
		} while((errsv == EAGAIN || errsv == EWOULDBLOCK || errsv == 0) && connfd < 0);
		switch(pid[procCount] = fork())
		{
			// Error forking
			case -1:
				error("Error forking process \n");		

			// Child process
			case 0:
				if(setjmp(jmpUSR1)) {
					if((close(connfd)) != 0) error("Error closing connection\n");
					if((close(sockfd)) != 0) error("Error closing listen socket\n");
					exit(1);
				}
				// Receive Client name
				recvMsg(&cliName, connfd);
				if(strstr(cliName, "otp_dec") == NULL)
				{
					sendMsg(cliName, connfd);
					free(cliName);
					close(connfd);
					error("Error program not allowed to communicate with server\n");
				}
				free(cliName);

				sendMsg(argv[0], connfd);

				// Receive Key
				recvMsg(&key, connfd);

				// Receive plaintext
				recvMsg(&buf, connfd);

				bufSize = strlen(buf);
				decBuf = (char*)malloc(sizeof(char) * (bufSize + 1));
				memset(decBuf, '\0', bufSize + 1);
				for(i = 0; i < bufSize; i++) {
					decBuf[i] = buf[i] == ' ' ? buf[i] : (((buf[i] - 65) - (key[i] - 65)) % 26) + 65;
					decBuf[i] = decBuf[i] < 65 && decBuf[i] != ' ' ? decBuf[i] + 26 : decBuf[i];
				}

				// Send encoded text
				sendMsg(decBuf, connfd);

				if((close(connfd)) != 0) error("Error closing connection\n");
				free(buf);
				free(key);
				free(decBuf);
				return 0;

			// Parent process
			default:
				procCount = procCount == 4 ? 0 : procCount + 1;
		}
	}

	for(i = 0; i < 5; i++) {
		if(pid[i] > 0)
		{
			wpid[i] = waitpid(pid[i], &status, WNOHANG | WUNTRACED);
			if(wpid[i] == pid[i])
			{
				if(WIFEXITED(status)) printf("Process %d exited with %d\n", pid[i], WEXITSTATUS(status));
				else if(WIFSIGNALED(status))  printf("Process %d, terminated by signal %d\n", pid[i], WTERMSIG(status));
				else if(WIFSTOPPED(status))  printf("Process %d, stopped by signal %d\n", pid[i], WSTOPSIG(status));
				wpid[i] = pid[i] = 0;
			}
			kill(pid[i], SIGUSR1);
		}
	}
	if(fcntl(connfd, F_GETFD) != -1 || errno != EBADF )
		if((close(connfd)) != 0) error("Error closing connection\n");
	if(fcntl(sockfd, F_GETFD) != -1 || errno != EBADF )
		if((close(sockfd)) != 0) error("Error closing listen socket\n");

	return 0;
}
