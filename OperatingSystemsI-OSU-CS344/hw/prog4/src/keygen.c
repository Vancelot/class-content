#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

int main(int argc, char *argv[])
{
	int retVal = 0, keySize = 0, i = 0;

	if(argc < 2 || argc > 2) { // checks argument count
		fprintf(stderr,"incorrect number of arguments\nUsage: keygen [LENGTH]\n");
		retVal = 1;
	}
	else
	{
		if((keySize = atoi(argv[1])) <= 0) { // check argument is valid number
			fprintf(stderr, "Argument not a valid number\n");
			retVal = 1;
		}
		else
		{
			srand(time(NULL));
			for(i = 0; i < keySize; i++) {
				putchar((rand() % 26) + 65);
			}
			putchar('\n');
		}
	}
	return retVal;
}
