#include"Header.h"

int main(int argc, char *argv[])
{
	int port = 0, sockFD = 0, t = 1, keySize = 0, plainTextSize = 0;
	char *plainText, *buf, *key, *servName;
	FILE *inFile = NULL, *keyFile = NULL;
	struct sockaddr_in servAddr;
	struct hostent *servHostInfo;

	if(argc < 4 || argc > 4) error("Incorrect number of arguments\nUsage: otp_enc [PLAINTEXT] [PORT]\n");
	if((port = atoi(argv[3])) <= 0) error("Argument not a valid port\n");
	if((keyFile = fopen(argv[2], "r")) == NULL) error("Error opening keyFile]n");
	if((inFile = fopen(argv[1], "r")) == NULL) error("Error opening file\n");

	// Get plainTextSize
	if((fseek(inFile, 0, SEEK_END)) == -1 ) error("Error seeking file\n");
	plainTextSize = ftell(inFile);
	rewind(inFile);
	// Get key size
	if((fseek(keyFile, 0, SEEK_END)) == -1 ) error("Error seeking file\n");
	keySize = ftell(keyFile);
	rewind(keyFile);

	// Allocate memory to store plainText
	if((plainText = (char*)malloc(sizeof(char) * (plainTextSize + 1))) == NULL) error("Error allocating space for plainText\n");
	memset(plainText, '\0', plainTextSize);
	// Read plainText from file
	if((int)fread(plainText,sizeof(char), plainTextSize, inFile) != plainTextSize) error("Error reading inFile\n");
	fclose(inFile);
	plainText[strcspn(plainText, "\n")] = '\0'; // Remove \n

	plainTextSize = strlen(plainText);

	// Allocate memory to store key, we only need the portion the first n bytes of the key, where n is the size of the plainText
	if((key = (char*)malloc(sizeof(char) * (plainTextSize + 1))) == NULL) error("Error allocating space for key\n");
	memset(key, '\0', plainTextSize + 1);
	// Read key from file
	if((int)fread(key,sizeof(char), plainTextSize, keyFile) != plainTextSize) error("Error reading keyFile\n");
	fclose(keyFile);

	key[strcspn(key, "\n")] = '\0'; // Remove \n
	keySize = strlen(key);

	if(keySize < plainTextSize) error("Key too short\n");

	memset((char*)&servAddr, '\0', sizeof(servAddr)); // init servAddr
	servAddr.sin_family = AF_INET; // AF_INET == IP protocol family
	servAddr.sin_port = htons(port); // converts host byte order to network byte order

	// Convert localhost
	if((servHostInfo = gethostbyname("localhost")) == NULL) error("Error getting host entry\n");
	memcpy((char*)&servAddr.sin_addr.s_addr, (char*)servHostInfo->h_addr, servHostInfo->h_length);

	// Setup socket domain = IP protocol, type = byte streams, protocol = default
	if((sockFD = socket(AF_INET, SOCK_STREAM, 0)) < 0) error("Error opening socket\n");
	if((setsockopt(sockFD, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(t))) < 0) error("Error setting reuse address socket option\n");

	// Connect to server
	if(connect(sockFD, (struct sockaddr*)&servAddr, sizeof(servAddr)) < 0) error("Error connecting\n");

	// Send process name to server
	sendMsg(argv[0], sockFD);
	
	// Receive server name
	recvMsg(&servName, sockFD);
	if(strstr(servName, "otp_enc_d") == NULL)
	{
		free(key);
		free(plainText);
		close(sockFD);
		error("Connection closed by server\n");
	}

	// Send key to server
	sendMsg(key, sockFD);

	// Send plainText to server
	sendMsg(plainText, sockFD);
	
	// Receive and print encoded text
	recvMsg(&buf, sockFD);
	printf("%s\n", buf);

	close(sockFD);
	free(key);
	free(buf);
	free(plainText);

	return 0;
}
