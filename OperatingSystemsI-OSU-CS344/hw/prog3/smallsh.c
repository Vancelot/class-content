#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<unistd.h>
#include<assert.h>
#include<sys/wait.h>
#include<signal.h>

#define BUFSIZE 2048
#define ARGSIZE 512

void signalHandler(int signal) {
	switch(signal)
	{
		case 2:
			printf("Signal SIGINT %d received\n", signal);
			break;
		case 15:
			printf("Signal SIGTERM %d received\n", signal);
			break;
		default:
			printf("Some kinda signal %d received\n", signal);
	}
}

typedef struct Node {
	pid_t pid, wpid;
	struct Node *next;
} Node;

typedef struct List {
	Node *sent;
} List;

void remList(List *ls, pid_t p) {
	Node *cur = ls->sent->next, *prev = ls->sent;
	prev = ls->sent;

	while(cur != NULL)
	{
		if(cur->pid == p) {
			prev->next = cur->next;
			free(cur);
		}
		else {
			prev = cur;
			cur = cur->next;
		}
	}
}

typedef struct Shell
{
	char *cmd, **argv, *ifName, *ofName;
	int argc, back, status;
	FILE *file;
	List *ch;
} Shell;

void initShell(Shell*);
void resetShell(Shell*);
void parseInput(Shell*, char*);
void printInput(Shell*);
void freeInput(Shell*);
void checkChildren(Shell*);

int prompt(Shell*);
void noBuiltIn(Shell*);
void smallCD(Shell*);
void smallStatus(Shell *sh);

int main()
{
	Shell shell;
	int go = 0;

	if(signal(SIGINT, signalHandler) == SIG_ERR) {
		printf("Signal error");
	}
	if(signal(SIGTERM, signalHandler) == SIG_ERR) {
		printf("Signal error");
	}

	initShell(&shell);

	while(go == 0)
	{
		resetShell(&shell);
		go = prompt(&shell);
		freeInput(&shell);
	}

	return 0;
}

void initShell(Shell *sh)
{
	sh->cmd = sh->ifName = sh->ofName = NULL;
	sh->argv = NULL;
	sh->argc = sh->back = sh->status = 0;
	sh->file = NULL;

	sh->ch = (List*) malloc(sizeof(List));
	sh->ch->sent = (Node*) malloc(sizeof(Node));
	sh->ch->sent->pid = sh->ch->sent->wpid = 0;
	sh->ch->sent->next = NULL;
}

void resetShell(Shell *s)
{
	s->cmd = NULL;
	s->ifName = s->ofName = NULL;
	s->argv = NULL;
	s->argc = s->back = 0;
	s->file = NULL;
}

void freeInput(Shell *sh) {
	int i = 0;

	for(i = 0; i < sh->argc; i++) {
		if(sh->argv[i] != NULL){ free(sh->argv[i]); }
	}
	if(sh->argv != NULL){ free(sh->argv); }
}

int prompt(Shell *sh)
{
	int result = 0;
	char buf[BUFSIZE];

	memset(buf, '\0', BUFSIZE-1);

	if(sh->ch->sent->next) {
		checkChildren(sh);
	}
	printf(": ");
	fflush(NULL);
	fgets(buf, BUFSIZE - 1, stdin);
	buf[strcspn(buf, "\r\n")] = '\0';

	if(buf[0] != '#' && buf[0] != '\0')
	{
		parseInput(sh, buf);
//		printInput(sh);
		if(strcmp(sh->cmd, "cd") == 0) {
			smallCD(sh);
		}
		else if(strncmp(sh->cmd, "status", BUFSIZE - 1) == 0) {
			smallStatus(sh);
		}
		else if(strncmp(sh->cmd, "exit", BUFSIZE - 1) == 0) {
			result = 1;
			sh->status = 0;
		}
		else {
			noBuiltIn(sh);
			if(sh->status == 255) {
				printf("Command not found\n");
				fflush(NULL);
			}
		}
	}
	return result;
}

void parseInput(Shell *sh, char *buf)
{
	const char *delim = " ";
	char *tok = strtok(buf,delim), *argvBuf[ARGSIZE], *procPtr = NULL, *prev = NULL, temp[BUFSIZE], procStr[25];
	int i = 0, len = 0;

	while(tok != NULL)
	{
		if((procPtr = strstr(tok, "$$")) != NULL)
		{
			memset(temp, '\0', BUFSIZE);
			strncpy(temp, tok, procPtr - tok);
			sprintf(procStr, "%d", getpid());
			while(procPtr != NULL)
			{
				strncat(temp, procStr, BUFSIZE-1);
				prev = procPtr + 2;
				procPtr = strstr(prev, "$$");
				strncat(temp, prev, procPtr == NULL ? BUFSIZE - 1 : procPtr - prev);
			}
			len = strlen(temp);
			argvBuf[sh->argc] = (char*) malloc(sizeof(char) * (len + 1));
			memset(argvBuf[sh->argc], '\0', len + 1);
			strncpy(argvBuf[sh->argc], temp, len);
		}
		else
		{
			len = strlen(tok);
			argvBuf[sh->argc] = (char*) malloc(sizeof(char) * (len + 1));
			memset(argvBuf[sh->argc], '\0', len + 1);
			strncpy(argvBuf[sh->argc], tok, len);
		}

		sh->argc++;
		tok = strtok(NULL, delim);
	}

	sh->argv = (char**) malloc(sizeof(char*) * (sh->argc + 1));

	for(i = 0; i < sh->argc; i++)
	{
		sh->argv[i] = argvBuf[i];
		if(sh->argv[i][0] == '<' && strlen(sh->argv[i]) < 2) {
			sh->ifName = i <= sh->argc ? argvBuf[i + 1] : NULL;
		}
		else if(sh->argv[i][0] == '>' && strlen(sh->argv[i]) < 2) {
			sh->ofName = i <= sh->argc ? argvBuf[i + 1] : NULL;
		}
		else if(sh->argv[i][0] == '&' && strlen(sh->argv[i]) < 2 && i == sh->argc - 1) {
			sh->back = 1;
		}
	}
	sh->cmd = sh->argv[0];
	sh->argv[sh->argc] = NULL;
}

void smallCD(Shell *sh)
{
	if(sh->argc < 1) {
		printf("How did you even get here?\n");
	}
	else if(sh->argc > 2) {
		printf("That's too many, maybe chill?\n");
	}
	else {
		chdir(sh->argc == 1 ? getenv("HOME") : sh->argv[1]);
	}
}

void smallStatus(Shell *sh)
{
	printf("exit value %d\n", sh->status);
	sh->status = 0;
}

void noBuiltIn(Shell *sh)
{
	int i = 0, j = 0;
	char *args[sh->argc + 1];

	Node *ch = (Node*) malloc(sizeof(Node));

	assert((ch->pid = fork()) != -1);
	if(ch->pid == 0)
	{
		for(i = 0; i < sh->argc; i++)
		{
			if((	sh->argv[i][0] == '<'
					|| sh->argv[i][0] == '>'
					|| sh->argv[i][0] == '&' )
					&& strlen(sh->argv[i]) < 2
				)
			{
				break;
			}
			else {
				args[j++] = sh->argv[i];
			}
		}
		args[j] = NULL;
		if(sh->ofName != NULL) {
			sh->file = fopen(sh->ofName, "w");
			dup2(fileno(sh->file), STDOUT_FILENO);
			fclose(sh->file);
		}
		if(sh->ifName != NULL) {
			if((sh->file = fopen(sh->ifName, "r")) == NULL) {
				printf("Cannot open %s\n", sh->ifName);
				exit(1);
			}
			else {
				dup2(fileno(sh->file), STDIN_FILENO);
				fclose(sh->file);
			}
		}
		exit(execvp(args[0], args));
	}
	else
	{
		if(!sh->back) {
			ch->wpid = waitpid(ch->pid, &sh->status, WUNTRACED);
			if(WIFEXITED(sh->status)) {
				sh->status = WEXITSTATUS(sh->status);
			}
			else if(WIFSIGNALED(sh->status)) {
				sh->status = WTERMSIG(sh->status);
			}
			else if(WIFSTOPPED(sh->status)) {
				sh->status = WSTOPSIG(sh->status);
			}
			sh->status = WEXITSTATUS(sh->status);
		}
		else {
			ch->next = sh->ch->sent->next;
			sh->ch->sent->next = ch;
			printf("Background pid is %d\n", ch->pid);
		}
	}
}

void checkChildren(Shell *sh)
{
	Node *cur = sh->ch->sent->next;
	pid_t r;

	while(cur != NULL)
	{
		cur->wpid = waitpid(cur->pid, &sh->status, WUNTRACED | WNOHANG);
		if(cur->wpid == cur->pid) {
			if(WIFEXITED(sh->status)) {
				printf("Process %d exited with value %d\n", cur->pid, WEXITSTATUS(sh->status));
			}
			else if(WIFSIGNALED(sh->status)) {
				printf("Process %d terminated by signal %d\n", cur->pid, WTERMSIG(sh->status));
			}
			else if(WIFSTOPPED(sh->status)) {
				printf("Process %d stopped by signal %d\n", cur->pid, WSTOPSIG(sh->status));
			}
			r = cur->pid;
			cur = cur->next;
			remList(sh->ch, r);
		}
		else {
			cur = cur->next;
		}
	}
}

void printInput(Shell *sh)
{
	int i = 0;
	printf("cmd: %s\nargc: %d\nargv: ", sh->cmd, sh->argc); 
	if(sh->argv)
	{
		for(i = 0; i < sh->argc + 1; i++) {
			printf("%d: %s\t", i, sh->argv[i]);
		}
	}
	printf("\nifName: %s\nofName: %s\nback: %d\n", sh->ifName, sh->ofName, sh->back);
	printf("\n");
	fflush(NULL);
}
