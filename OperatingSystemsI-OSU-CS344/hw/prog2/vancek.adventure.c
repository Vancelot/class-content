#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<dirent.h>
#include<time.h>
#include<pthread.h>

#define BUFSIZE 256
#define ROOMCOUNT 7

pthread_t pthreadID;
pthread_mutex_t lock;
int keepalive = 0;

/***LINKED LIST***/
typedef struct Node{
	char *value;
	struct Node *next;
} Node;

typedef struct List{
	Node *sentinel;
} List;

void initList(List *);
void addList(List *, char*);
void printList(List *);
void freeList(List *);

typedef struct DynArr{
	char **data;
	int size;
	int capacity;
} DynArr;

void initDynArr(DynArr*, int);
void freeDynArr(DynArr*);
void addDynArr(DynArr*, char*);
void remDynArr(DynArr*, char*);
void _dynArrDoubleCap(DynArr*);

typedef struct Room
{
	FILE	*fp;
	char	name[BUFSIZE];
	char	path[BUFSIZE];
	char	type[BUFSIZE];
	DynArr connections;
} Room;

void initRoom(Room*, char*, char*);
int getRoomIdx(const Room rooms[], char*);
void freeRooms(Room rooms[]);

void printRoomStruct(const Room *r);

typedef struct Game
{
	Room rooms[ROOMCOUNT];
	int startIdx, curIdx, endIdx, steps, threadResult;
	DIR *dr;
	struct dirent *de;
	char roomDir[BUFSIZE];
	List path;
} Game;

void initGame(Game*);
void playGame(Game*);
int playRoom(Game*, int idx);
void freeGame(Game*);
void startThread();

void* timeThread();

struct dirent* getRoomDir(DIR *dr);

int main()
{ 
	assert(pthread_mutex_init(&lock, NULL) == 0);
	pthread_mutex_lock(&lock);
	pthread_create(&pthreadID, NULL, &timeThread, NULL);

	Game game;
	initGame(&game);
	playGame(&game);
	freeGame(&game);

	return 0;
}

/*
 * Initialize all values in Game struct to NULL values
 * Get latest rooms directory
 * Read room files and initialize room Structs
 * Get index of START_ROOM and END_ROOM
*/
void initGame(Game *g)
{
	int i = 0;

	g->startIdx = g->curIdx = g->endIdx = g->steps = 0;
	g->dr = opendir(".");
	g->de = NULL;
	memset(g->roomDir, '\0', BUFSIZE);
	initList(&g->path);

	g->de = getRoomDir(g->dr);
	strncpy(g->roomDir, g->de->d_name, BUFSIZE);
	g->dr = opendir(g->roomDir);

	while((g->de = readdir(g->dr)) != NULL) {
		if(strstr(g->de->d_name, "_room") != NULL) {
			initRoom(&g->rooms[i], g->roomDir, g->de->d_name);
			if(strncmp(g->rooms[i].type, "START_ROOM", 5) == 0) {
				g->curIdx = g->startIdx = i;
			}
			if(strncmp(g->rooms[i].type, "END_ROOM", 5) == 0) {
				g->endIdx = i;
			}
			i++;
		}
	}
}

/*
 * Loops through player prompts until player reaches END_ROOM
 * Prints victory message, steps, and path taken
*/
void playGame(Game *g)
{
	while(g->curIdx != g->endIdx) {
		g->curIdx = playRoom(g, g->curIdx);
		addList(&g->path, g->rooms[g->curIdx].name);
		g->steps++;
	}
	printf("YOU HAVE FOUND THE END ROOM. CONGRATULATIONS!\nYOU TOOK %d STEPS. YOUR PATH TO VICTORY WAS:\n", g->steps);
	printList(&g->path);
}

/*
 * Takes search string as argument and returns index in rooms array
*/
int getRoomIdx(const Room rooms[], char* srch)
{
	int i = 0, result = 0;

	for(i = 0; i < ROOMCOUNT; i++) {
		if(strcmp(srch, rooms[i].name) == 0) {
			result = i;
			break;
		}
	}
	return result;
}

/*
 * Prompts player based on current location and returns next location
*/
int playRoom(Game *g, int idx)
{
	int i = 0, match = 0;
	char buf[BUFSIZE];
	Room r = g->rooms[idx];

	printf("CURRENT LOCATION: %s\nPOSSIBLE CONNECTIONS:", r.name);
	for(i = 0; i < r.connections.size; i++) {
		if( i == r.connections.size - 1){
			printf("%s.", r.connections.data[i]);
		}
		else {
			printf("%s, ", r.connections.data[i]);
		}
	}

	do
	{
		memset(buf, '\0', BUFSIZE);
		printf("\nWHERE TO? > ");
		scanf("%255s", buf);
		for(i = 0; i < r.connections.size; i++) {
			if(strcmp(buf, "time") == 0){
				startThread();
				match = -1;
				break;
			}
			else if(strcmp(buf, r.connections.data[i]) == 0) {
				match = 1;
				break;
			}
			else
				match = 0;
		}
		if(match == 0)
		{
			printf("\nHUH? I DON’T UNDERSTAND THAT ROOM. TRY AGAIN\nPOSSIBLE CONNECTIONS:");
			for(i = 0; i < r.connections.size; i++) {
				if( i == r.connections.size - 1){
					printf("%s.", r.connections.data[i]);
				}
				else {
					printf("%s, ", r.connections.data[i]);
				}
			}
		}
	} while( match != 1 );
	printf("\n");
	strcat(buf, "_room");
	return getRoomIdx(g->rooms, buf);
}

/*
 * Reads roomfile and initializes Room struct with attributes
*/
void initRoom(Room *r, char *path, char *name)
{
	char buf[BUFSIZE];
	char *tok, *temp;

	memset(r->name, '\0', BUFSIZE);
	memset(r->path, '\0', BUFSIZE);
	memset(r->type, '\0', BUFSIZE);

	sprintf(r->path, "%s/%s", path, name);
	strncpy(r->name, name, BUFSIZE);
	initDynArr(&r->connections, 3);

	assert((r->fp = fopen(r->path, "r")) != NULL);
	while(fgets(buf, BUFSIZE, r->fp) != NULL)
	{
		tok = strtok(buf, " :");
		while(tok != NULL)
		{
			if(strncmp(tok, "CONNECTION", BUFSIZE) == 0)
			{
				tok = strtok(NULL, " :\n");
				tok = strtok(NULL, " :_\n");
				temp = (char*) malloc(sizeof(char) * BUFSIZE);
				memset(temp, '\0', BUFSIZE);
				strncpy(temp, tok, BUFSIZE);
				addDynArr(&r->connections, temp);
			}
			else if(strncmp(tok, "TYPE", BUFSIZE) == 0){
				tok = strtok(NULL, " :");
				strncpy(r->type, tok, BUFSIZE);
			}
			else {
				tok = strtok(NULL, " :");
			}
		}
	}
	fclose(r->fp);
}

/*
 * Searches PWD for latest rooms directory and returns it's entry
*/
struct dirent* getRoomDir(DIR *dr)
{
	struct dirent *de = NULL, *roomDir = NULL;
	struct stat bufa, bufb;

	while((de = readdir(dr)) != NULL)
	{
		if(strncmp("vancek.rooms.", de->d_name, 13) == 0)
		{
			if(roomDir == NULL) {
				roomDir = de;
			}
			else
			{
				stat(de->d_name, &bufa);
				stat(roomDir->d_name, &bufb);
				if(difftime(bufa.st_mtime, bufb.st_mtime) < 0) {
					roomDir = de;
				}
			}
		}
	}
	closedir(dr);
	return roomDir;
}

/*
 * Dynamic array functions
*/
void initDynArr(DynArr *da, int cap)
{
	assert(cap >= 0);
	da->capacity = cap;
	da->size = 0;
	da->data = (char**) malloc(da->capacity * sizeof(char*));
	assert(da->data != 0);
}

void freeDynArr(DynArr *da) {
	int i = 0;

	assert(da != NULL);

	for(i = 0; i < da->size; i++) {
		if(da->data[i] != NULL ) free(da->data[i]);
	}
	free(da->data);
}

void addDynArr(DynArr *da, char* val)
{
	if(da->size >= da->capacity){
		_dynArrDoubleCap(da);
	}
	da->data[da->size] = val;
	da->size++;
}

void _dynArrDoubleCap(DynArr *da)
{
	int i = 0;
	char* *oldbuffer = da->data;
	int oldsize = da->size;

	initDynArr(da, 2 * da->capacity);
	for(i = 0; i < oldsize; i++){
		da->data[i] = oldbuffer[i];
	}
	da->size = oldsize;
	free(oldbuffer);
}

/*
 * Print statement for Room struct, Debugging purposes
*/
void printRoomStruct(const Room *r)
{
	int i = 0;

	printf("Name: %s\nPath: %s\nType: %s\nConnections:\n", r->name, r->path, r->type);
	for(i = 0; i < r->connections.size; i++) {
		printf("%d: %s", i, r->connections.data[i]);
	}
	printf("\n");
}

/*
 * Rooms Struct destructor
*/
void freeRooms(Room rooms[])
{
	int i = 0;

	for(i = 0; i < ROOMCOUNT; i++) {
		freeDynArr(&rooms[i].connections);
	}
}

/*
 * Game destructor
*/
void freeGame(Game *g)
{
	freeRooms(g->rooms);
	free(g->dr);
	freeList(&g->path);
	pthread_mutex_destroy(&lock);
}

/*
 * Linked list functions
*/
void initList(List *ls)
{
	Node *sent = (Node*)malloc(sizeof(Node));

	assert(ls != NULL);
	assert(sent != NULL);

	sent->next = NULL;
	ls->sentinel = sent;
	ls->sentinel->value = NULL;
}

void addList(List *ls, char *val)
{
	Node *newNode = (Node*)malloc(sizeof(Node)), *cur = ls->sentinel;

	while(cur->next != NULL) cur = cur->next;

	assert(ls != NULL);
	assert(ls->sentinel != NULL);
	newNode->value = (char*)malloc(sizeof(char) * BUFSIZE);
	strcpy(newNode->value, val); 
	newNode->next = NULL;
	cur->next = newNode;
}

void printList(List *ls)
{
	Node *node = ls->sentinel->next;

	assert(node);
	while(node != NULL){
		printf("%s\n", node->value);
		node = node->next;
	}
}

void freeList(List *ls)
{
	Node *cur = ls->sentinel, *next = ls->sentinel->next;

	while(cur != NULL)
	{
		if(cur->value != NULL) free(cur->value);
		if(cur != NULL) free(cur);
		cur = next;
		next = next == NULL ?  NULL : next->next;
	}
}

/*
 * pthread function for printing time to stdout and file
*/
void* timeThread()
{
	time_t now;
	char buf[BUFSIZE];
	FILE *fp;

	fp = fopen("currentTime.txt", "w");
	pthread_mutex_lock(&lock);
	now = time(NULL);
	memset(buf, '\0', BUFSIZE);
	strftime(buf, BUFSIZE - 1, "%l:%M%P, %A, %B,%e, %G\n", localtime(&now));
	printf("%s\n", buf);
	fputs(buf, fp);
	pthread_mutex_unlock(&lock);
	fclose(fp);

	return NULL;
}

/*
 * Function to unlock pthread mutex and create new pthread
*/
void startThread()
{
	pthread_mutex_unlock(&lock);
	pthread_join(pthreadID, NULL);
	pthread_create(&pthreadID, NULL, &timeThread, NULL);
}
