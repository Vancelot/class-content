#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<sys/stat.h>
#include<unistd.h>
#include<time.h>

#define BUFSIZE 256
#define ROOMCOUNT 7

const char roomTypes[10][BUFSIZE] = 
{
	"dry",
	"dripping",
	"rainy",
	"smoky",
	"dark",
	"bright",
	"dreary",
	"hot",
	"cold",
	"danger"
};

typedef struct Room
{
	FILE	*fp; char	name[BUFSIZE];
	char	path[BUFSIZE];
	char	type[BUFSIZE];
	int	connections[ROOMCOUNT];
	int	conCount;
} Room;

typedef struct Cave
{
	char	prefix[15];
	char	roomDir[BUFSIZE];
	char	proc[BUFSIZE];
	Room	rooms[ROOMCOUNT];
} Cave;

Cave *initCave();
void initGame(Cave *c);
void freeGame(Cave *c);
void writeName(Room *r);
void writeConnections(Cave *c);
void writeType(Room *r);
void printRoomStruct(const Room *r);

int main()
{
	Cave *cave = initCave();

	srand(time(NULL));

	initGame(cave);

	freeGame(cave);
	return 0;
}

Cave* initCave()
{
	int i = 0, j = 0;
	Cave *c = (Cave*)malloc(sizeof(Cave));

	assert(c != NULL);

	strncpy(c->prefix, "vancek.rooms.", 15);

	for(i = 0; i < ROOMCOUNT; i++) {
		memset(c->rooms[i].name, '\0', BUFSIZE);
		memset(c->rooms[i].path, '\0', BUFSIZE);
		memset(c->rooms[i].type, '\0', BUFSIZE);
		c->rooms[i].conCount = 0;
		for(j = 0; j < ROOMCOUNT; j++) {
			c->rooms[i].connections[j] = 0;
		}
	}

	for(i = 1; i < ROOMCOUNT - 1; i++) {
		strncpy(c->rooms[i].type, "MID_ROOM", BUFSIZE - 1);
	}
	strncpy(c->rooms[0].type, "START_ROOM", BUFSIZE - 1);
	strncpy(c->rooms[ROOMCOUNT - 1].type, "END_ROOM", BUFSIZE - 1);

	return c;
}

void initGame(Cave *c)
{
	int i = 0, used[10], ri;

	sprintf(c->proc, "%d", getpid());
	strncpy(c->roomDir, c->prefix, BUFSIZE - 1);
	strncat(c->roomDir, c->proc, BUFSIZE - 1);
	strncat(c->roomDir, "/", BUFSIZE);

	assert(mkdir(c->roomDir, 0755) != -1);

	for(i = 0; i < 10; i++) {
		used[i] = 0;
	}

	for(i = 0; i < ROOMCOUNT; i++)
	{
		do
			ri = rand() % 10;
		while (used[ri] != 0);
		used[ri] = 1;
		strncat(c->rooms[i].name, roomTypes[ri], BUFSIZE - 1);
		strncat(c->rooms[i].name, "_room", BUFSIZE - 1);
		strncat(c->rooms[i].path, c->roomDir, BUFSIZE - 1);
		strncat(c->rooms[i].path, c->rooms[i].name, BUFSIZE - 1);

		assert((c->rooms[i].fp = fopen(c->rooms[i].path, "w+")) != NULL);
		writeName(&c->rooms[i]);
	}

	for(i = 0; i < ROOMCOUNT; i++)
	{
		while(c->rooms[i].conCount < 3 && !(c->rooms[i].conCount > 6))
		{
			ri = rand() % (ROOMCOUNT);
			if(c->rooms[i].connections[ri] == 0 && c->rooms[ri].conCount < 5 && ri != i)
			{
				c->rooms[i].connections[ri] = 1;
				c->rooms[i].conCount++;
				c->rooms[ri].connections[i] = 1;
				c->rooms[ri].conCount++;
			}
		}
	}
	writeConnections(c);

	for(i = 0; i < ROOMCOUNT; i++) {
		writeType(&c->rooms[i]);
	}
}

void writeConnections(Cave *c)
{
	int i = 0, j = 0, count = 1;
	char buf[BUFSIZE * 2];


	for(i = 0; i < ROOMCOUNT; i++)
	{
		count = 1;
		for(j = 0; j < ROOMCOUNT; j++)
		{
			if(c->rooms[i].connections[j] == 1)
			{
				memset(buf, '\0', BUFSIZE);
				sprintf(buf, "%s %d: %s\n", "CONNECTION", count, c->rooms[j].name);
				assert(fputs(buf, c->rooms[i].fp) != EOF);
				count++;
			}
		}
	}
}

void writeName(Room *r)
{
	char buf[BUFSIZE * 2];

	memset(buf, '\0', BUFSIZE);
	sprintf(buf, "ROOM NAME: %s\n", r->name);
	assert(fputs(buf, r->fp) != EOF);
}

void writeType(Room *r)
{
	char buf[BUFSIZE];

	memset(buf, '\0', BUFSIZE);
	strncat(buf, "ROOM TYPE: ", BUFSIZE - 1);
	strncat(buf, r->type, BUFSIZE - 1);
	assert(fputs(buf, r->fp) != EOF);
	assert(fputc('\n', r->fp) != EOF);
}

void freeGame(Cave *c)
{
	int i = 0;

	for(i = 0; i < ROOMCOUNT; i++) {
		assert(fclose(c->rooms[i].fp) == 0);
	}

	if(c != NULL){ free(c); }
}

void printRoomStruct(const Room *r)
{
	int i = 0;

	printf("Name: %s\nPath: %s\nType: %s\nConnections: ", r->name, r->path, r->type);

	for(i = 0; i < ROOMCOUNT; i++) {
		printf("%d ", r->connections[i]);
	}
	printf("\nConnection Count: %d\n\n", r->conCount);
}
