#include<iostream>

using namespace std;

const int SIZE = 5;

void loop(int);

int main()
{
	loop(SIZE);
	return 0;
}

void loop(int loops)
{
	int result = 0;
	for(int i = 0; i < loops; i++)
	{
		for(int j = 0; j < loops - i; j++)
		{
			result += 1;
		}
	}
	cout << result;
}
