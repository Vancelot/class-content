#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

const int SIZE = 5;

void randomize(int[]);
void printArray(int[]);
void printIndentArray(int array[]);
void sort(int[]);
int loopCount(int);

int main()
{
	int array[SIZE] = {4, 3, 2, 1, 0}; 
//	clock_t currentTime, endTime;

//	cout << SIZE << endl;

//	randomize(array);

	cout << "Unsorted Set:\n";
	printArray(array);
//	currentTime = clock();
	sort(array);
//	endTime = clock();
	cout << "Sorted Set:\n";
	printArray(array);
	cout << endl;

/*
	loopCount(1);
	loopCount(2);
	loopCount(3);
	loopCount(4);
	loopCount(5);
	loopCount(6);
	loopCount(7);
	loopCount(8);
	loopCount(9);
	loopCount(10);
	loopCount(100);
	loopCount(200);
*/
//	loopCount(SIZE);

//	cout << "Time at " << SIZE << " size: " << endTime - currentTime << endl;

	return 0;
}

void randomize(int array[])
{
	srand(time(NULL));
	for(int i = 0; i < SIZE; i++)
	{
		array[i] = rand() % 100;
	}
}

void printArray(int array[])
{
	for(int i = 0; i < SIZE; i++)
	{
		cout << array[i];
		if(i != SIZE - 1)
		{
			cout << ", ";
		}

	}
	cout << endl;
}

void printIndentArray(int array[])
{
	cout << '\t';
	for(int i = 0; i < SIZE; i++)
	{
		cout << array[i];
		if(i != SIZE - 1)
		{
			cout << ", ";
		}

	}
	cout << endl;
}

void sort(int array[])
{
	int temp;
	for(int i = 0; i < SIZE; i++)
	{
		cout << "Array at " << i << " :\n";
		printArray(array);
		for(int j = 1; j < SIZE - i; j++)
		{
			if(array[j] <= array[j-1])
			{
				temp = array[j-1];
				array[j-1] = array[j];
				array[j] = temp;
			}
		}
	}
}

int loopCount(int n)
{
cout << endl;
	int result = 0;
	for(int i =0; i < n; i++)
	{
		for(int j = 1; j < n - i; j++)
		{
			result += 1;
		}
	}

	cout << "loop count at N=" << n << ": " << result << endl;
	return result;
}
