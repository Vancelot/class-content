Algorithm: Expanded Definition

An algorithm is a procedural process for producing an output by performing pre-defined instructions on given inputs [1]. This process is made up of a set of instructions, that are performed manipulate the data structures, and sets of conditionals, that dictate where in the list of instructions the program should jump to next, dependent on the results. Algorithms are the basis on which all computer logic and programs work.

*TODO 

*PARAGRAPH: Example of algorithm
A typical example of an algorithm is one that sorts an input set of numbers into a linked list from least significant to most. We begin with a set of unordered numbers

*VISUAL: Algorithm flowchart

*PARAGRAPH: Describe how algorithm is coded
*VISUAL: Equivalent code snippet for above algorithm

*PARAGRAPH: Describe mathematical notation of algorithm
*VISUAL: 


When writing computer algorithms, the engineer has to take into account the limitations of the system the algorithm will run on. Algorithms should be optimized for the circumstances, either to run as fast as possible, or limiting use of system resources. In either case the algorithm should be as efficient as possible.



References
	[1] T. Cormen, C. Leiserson, R. Rivest, and C. Stein, "Introduction to Algorithms, 3rd Edition". The MIT press, 2009.
