TITLE Program Template     (template.asm)

; Author:
; Course / Project ID                 Date:
; Description:

INCLUDE Irvine32.inc

; (insert constant definitions here)
HISTLIMIT = 100

.data
bal	DWORD	0
acc	WORD	?
hist	WORD	HISTLIMIT DUP(?)
val	BYTE	0

; (insert variable definitions here)

.code
main PROC
	lea	eax,	hist
	call	WriteHex
	call	Crlf
	lea	eax,	hist[198]
	call	WriteHex
	call	Crlf
	

; (insert executable instructions here)

	exit	; exit to operating system
main ENDP

; (insert additional procedures here)

END main
