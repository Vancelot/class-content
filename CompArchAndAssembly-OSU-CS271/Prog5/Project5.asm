TITLE Project05     (Project05.asm)

; Author: Kerry Vance
; CS271 / Project 05                 Date: 24 Feb 19
; Description: Sorting Random Integers

INCLUDE Irvine32.inc

MIN = 10
MAX = 200
LO = 100
HI = 899
ARRAYTYPE EQU <DWORD>					;declare array type as global for modularity
XPARAM	EQU [ebp+8]	
YPARAM	EQU [ebp+12]


.data
intro	BYTE	0Ah,"Welcome to the Random Integer array sorting... thing",0Ah,09h,"-Kerry Vance",0Ah,0Ah,"This program generates random numbers on the interval [100,999], displays the original list, sorts the list, and displays it again in descending order.",0Ah,0Ah,0
prompt0	BYTE	0Ah,"How many numbers should be generated? [10,200]: ",0
prompt1	BYTE	"Sorry please enter a number on the interval [10,200]: ",0
sorted	BYTE	"Sorted List: ",0Ah,0
unsorted	BYTE	"Unsorted List: ",0Ah,0
median	BYTE	"Median Value: ",0
list	ARRAYTYPE	MAX DUP(?)
req	DWORD	?

.code
main PROC
	call	Randomize					;initialize PRG
	call	introProc					;introduce program
	push	OFFSET	req				;pass req variable location
	call	getDat					;get request from user
	add	esp,	4				;clean stack

	push	OFFSET list				;push array address to stack for subsequent calls to fillArray, printArray, sort, and get Median
	push	req					;pass req by value to fillArray
	call	fillArray 
	add	esp,	4				;remove req from stack

	lea	edx,	unsorted				;unsorted prompt
	call	WriteString
	call	printArray				;print unsorted array
	call	Crlf
	call	sort					;sort array
	call	getMedian					;determine median value, returns in eax.
	lea	edx,	median				;median prompt
	call	WriteString
	call	WriteDec					;median still in eax
	call	Crlf
	call	Crlf
	lea	edx,	sorted				;sorted prompt
	call	WriteString
	call	printArray				;print sorted array
	add	esp,	4				;clean stack
	call	Crlf

	exit
main ENDP

introProc PROC USES edx
	lea	edx,	intro				;load intro address to edx
	call	WriteString				;write

	ret
introProc ENDP

getDat PROC USES edx
	push	ebp
	mov	ebp,	esp
	lea	edx,	prompt0				;load instructions
	call	WriteString
L1:
	call	ReadDec					;Take input
	call	validate					;pass input in eax to validate PROC
	cmp	eax,	0				;validate returns 0 in eax if not valid
	jz	L1					;try again if bad input
	mov	ebx,	YPARAM
	mov	[ebx],	eax
	pop	ebp
	call	Crlf					;else move on

	ret
getDat ENDP

fillArray PROC	;takes two parameters, XPARAM is requested numbers in array by value, YPARAM is beginning address of array. Array at index 0 acts as counter numbers in array
	push	ebp					;push base pointer
	mov	ebp,	esp
	mov	ecx,	XPARAM				;XPARAM is req
	mov	esi,	YPARAM				;YPARAM is array
	mov	ebx,	0				;ebx holds count for size of array
	mov	edi,	esi

L1:
	add	edi,	SIZEOF ARRAYTYPE			;start filling array at index 1
	call	getRand					;getRand returns random number in eax
	mov	[edi],	eax				;load random number into array
	inc	ebx					;increment size of array
	loop	L1					;loop on users requested size
	mov	[esi],	ebx				;array at index 0 stores how many numbers have been put in the array

	pop	ebp
	ret
fillArray ENDP

printArray PROC	;takes address of array in XPARAM. Assumes array at index 0 is size of array
	push	ebp
	mov	ebp,	esp
	mov	esi,	XPARAM				;esi is beginning of array
	mov	ecx,	[esi]				;value at index 0 is size, move into counter

L1:
	add	esi,	SIZEOF ARRAYTYPE			;increment esi by SIZEOF array to start printing at index 1
	mov	eax,	[esi]				;dereference address of esi, move into eax for WriteDec
	call	WriteDec
	mov	al,	20h				;print space
	call	WriteChar
	loop	L1					;loop on size of array
	call	Crlf
	
	pop	ebp
	ret
printArray ENDP

getMedian PROC	;takes address of array as XPARAM
	push	ebp
	mov	ebp,	esp
	mov	esi,	XPARAM				;esi stores beginning of array
	mov	edx,	0				;initialize edx to zero for division
	mov	eax,	[esi]				;move number of numbers into eax
	mov	ecx,	2				;divisor 2
	div	ecx					;divide, middle point in eax
	mov	ecx,	eax				;loop steps through half of total size
	add	esi,	SIZEOF ARRAYTYPE			;starts esi at first array value

L1:
	add	esi,	SIZEOF ARRAYTYPE			;increments esi to middle of array
	loop	L1
	mov	eax,	[esi]				;moves found value into eax
	cmp	edx,	0				;if remainder of earlier division is 0, array is even size and must be averaged
	jnz	L2					;if remainder isn't 0, it's 1 and therefore odd, no averaging required jump past
	add	eax,	[esi-SIZEOF ARRAYTYPE]		;takes value behind supposed middle and add to already found middle
	mov	ecx,	2				;divisor of 2
	div	ecx					;if size is even, edx already contains 0, no need to initialize to 0
L2:

	pop	ebp
	ret
getMedian ENDP

sort PROC		;takes address of array in XPARAM, assumes array at 0 is size
	push	ebp
	mov	ebp,	esp
	mov	edi,	XPARAM				;edi stores address of first number in array throughout process
	mov	ecx,	[edi]				;put size into counter
	add	edi,	SIZEOF ARRAYTYPE			;increment edi to first number

L1:
	push	ecx					;store counter of outer function
	mov	esi,	edi				;set esi to first number in array
L3:
	mov	eax,	[esi]				;get first number
	cmp	eax,	[esi+SIZEOF ARRAYTYPE]		;compare to next number
	jg	L2					;if greater, jump over swap exchange process not required, just use eax
	push	eax					;store first number 
	mov	eax,	[esi+SIZEOF ARRAYTYPE]		;get next number
	mov	[esi],	eax				;mov next number into first
	pop	[esi+SIZEOF ARRAYTYPE]			;pop stored first number into next index
L2:
	add	esi,	SIZEOF ARRAYTYPE			;increment esi by size of arraytype
	loop	L3					;inner loop
	pop	ecx					;restores outer loop ecx
	loop	L1					;outer loop

	pop	ebp
	ret
sort ENDP

validate PROC					
	cmp	eax,	MIN
	jb	L1					;jump to error label if zero or below
	cmp	eax,	MAX				;compare eax to MAX allowable value
	ja	L1					;jmp to error label if too large.
	jmp	L2					;else jump to end
L1:
	lea	edx,	prompt1				;print error message
	call	WriteString
	mov	eax,	0				;return error of 0 in eax
	jmp	L2					;jmp to end to avoid initializing counter register
L2:
	mov	ecx,	eax				;good input, intialize counter L3:
	ret
validate ENDP

getRand PROC USES ebx	;returns random number between HI and LO in eax
	call	Random32
	mov	edx,	0
	mov	ebx,	HI
	div	ebx
	add	edx,	LO
	mov	eax,	edx

	ret
getRand ENDP

END main
