TITLE Project01     (Project01.asm)

; Author: Kerry Vance
; CS271 / Project 01                 Date: 01 Jan 19
; Description: Elementary Arithmetic

INCLUDE Irvine32.inc

.data
intro			BYTE	09h,"Elementary Arithmetic",09h,"Kerry Vance",0Ah,0Ah,"Enter two numbers to see the sum, difference, product, quotient, and remainder.",0Ah,"**EC: Program repeats. Validates num1 > num2",0Ah,0Ah,0
num1Prompt	BYTE	"First Number: ",0
num2Prompt	BYTE	"Second Number: ",0
num2Less		BYTE	"The second number must be less than the first: ",0
num2Zero		BYTE	"Second number can't be zero (Can't divide by zero): ",0
num1			DWORD	?	;only need variable for num1, num2 stays in ebx which doesn't get modified by instructions
rem			DWORD ?	;variable for remainder, needed for print statement
remText		BYTE	",",09h," remainder: ",0
repPrompt	BYTE	"Would you like to go around again? (1 = yes): ",0
ciao			BYTE	"you: I love you...",0Ah,0Ah,09h,09h,"I know",0Ah,0

.code
main PROC

;intro
	lea	edx,	intro
	call	WriteString
;prompt and read num1 and num2
L1:
	lea	edx,	num1Prompt
	call	WriteString
	call	ReadInt
	mov	num1,	eax
	jmp	L4							;jump to L4 bypassing conditional statements
L2:									;conditional for num2 less than num1
	lea	edx,	num2Less
	call	WriteString
	jmp	L4
L3:									;conditional for num2 = 0 (protects from divide by zero exception)
	lea	edx,	num2Zero
	call	WriteString
L4:									;get num2 and store in ebx
	call	Crlf
	lea	edx,	num2Prompt	
	call	WriteString
	call	ReadInt
	mov	ebx,	eax
	cmp	ebx,	num1
	jg		L2							;compare num2 and num1, jump if num2 > num1
	cmp	ebx,	0
	je		L3							;compare num2 and 0, jump if num2 = 0
;addition
	call	Crlf
	mov	eax,	num1
	mov	ecx,	num1
	add	ecx,	ebx
	mov	dl,	'+'
	call	printRes
;subtraction
	call	Crlf
	mov	eax,	num1
	mov	ecx,	num1
	sub	ecx,	ebx
	mov	dl,	'-'
	call	printRes
;multiplication
	call	Crlf
	mov	eax,	num1
	mul	ebx
	mov	ecx,	eax
	mov	eax,	num1
	mov	dl,	'x'
	call	printRes
;division
	call Crlf
	mov	edx,	0
	mov	eax,	num1
	div	ebx	
	mov	ecx,	eax
	mov	rem,	edx
	mov	eax,	num1
	mov	dl,	246d				;ascii code for division symbol
	call	printRes
	lea	edx,	remText
	call	WriteString				;additional code for print statement
	mov	eax,	rem
	call	WriteDec
	call	Crlf
;repeat
	call	Crlf
	lea	edx,	repPrompt
	call	WriteString
	call	ReadInt					;had to use ReadInt for conditional, ReadChar hangs, likely because I'm on Linux using wine and there's some irregularity with how wine handles the input buffer.
	cmp	eax,	1
	je		L1							;L1 beginning of program after initial intro message
	lea	edx,	ciao
	call	WriteString

	exit
main ENDP

printRes PROC						;procedure to print result format num1=eax, operand=dl, num2=ebx, result=ecx
	call	WriteDec
	mov	al,	' '
	call	WriteChar
	mov	al,	dl
	call	WriteChar
	mov	al,	' '
	call	WriteChar
	mov	eax,	ebx
	call	WriteDec
	mov	al,	' '
	call	WriteChar
	mov	al,	'='
	call	WriteChar
	mov	al,	' '
	call	WriteChar
	mov	eax,	ecx
	call	WriteDec

	ret
printRes	ENDP

END main
