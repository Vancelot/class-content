TITLE Project03     (Project03.asm)

; Author: Kerry Vance
; CS271 / Project 03                 Date: 18 Jan 19
; Description: Negative Number Accumulator

INCLUDE Irvine32.inc

LOWERLIM	=	-100
UPPERLIM	=	-1

.data
intro		BYTE	0Ah,0Ah,"Welcome to the Integer Accumulator",0Ah,"By: Kerry Vance",0Ah,0Ah,"What is your name? ",0
myName	BYTE	20 DUP(?)
hello		BYTE	0Ah,"Hello, ",0
dir0		BYTE	0Ah,"Enter numbers in the range [-100,-1]",0Ah,"Enter a non negative number to see results.",0Ah,0Ah,0
dir1		BYTE	0Ah,"Enter a number: ",0
sum		SDWORD	0
badNum	BYTE	0Ah,"Out of Range; Number must be within [-100,-1]",0
numON		BYTE	0Ah,"Numbers entered: ",0
sumPr		BYTE	0Ah,"Sum: ",0
avgPr		BYTE	0Ah,"Average: ",0
noNums	BYTE	0Ah,"No negative numbers entered. How you gonna divide by zero?",0Ah,0
fwell		BYTE	0Ah,"Goodbye",0Ah,0

.code
main PROC
;intro
	lea	edx,	intro
	call	WriteString
	mov	ecx,	SIZEOF myName
	lea	edx,	myName
	call	ReadString
	lea	edx,	dir0
	call	WriteString
	mov	ebx,	0
	mov	eax,	0
	jmp	L3
;loop
L1:										;Out of range (< -100)
	lea	edx,	badNum
	call	WriteString
	jmp	L3
L2:										;Good num, increment counter and accumulate sum
	inc	ebx
	add	sum,	eax
L3:										;Get numbers loop
	lea	edx,	dir1
	call	WriteString
	call	ReadInt
	cmp	eax,	LOWERLIM				;Compare with lower limit
	jl		L1
	cmp	eax,	UPPERLIM				;Reset sign flag
	js		L2
;results
	lea	edx,	numON
	call	WriteString
	mov	eax,	ebx					;ebx = count of numbers entered
	call	WriteDec
	cmp	ebx,	0
	jz		L4								;Avoid div by zero error
	lea	edx,	sumPr
	call	WriteString
	mov	eax,	sum
	call	WriteInt
	cdq									;Extend sum
	idiv	ebx
	lea	edx,	avgPr
	call	WriteString
	call	WriteInt						;avg in eax after idiv
	jmp	L5
;zero nums
L4:
	lea	edx,	noNums
	call	WriteString
;exit
L5:
	lea	edx,	fwell
	call	WriteString

	exit
main ENDP


END main
