TITLE Project06B     (Project06B.asm)

; Author: Kerry Vance
; CS271 / Project 06B                 Date: 17 Mar 19
; Description: Combinations Calculator

INCLUDE Irvine32.inc
INCLUDE Macros.inc

XPARAM	EQU [ebp+8]	
YPARAM	EQU [ebp+12]
ZPARAM	EQU [ebp+16]
WPARAM	EQU [ebp+20]
MAX	= 20

myWriteStr	MACRO dispStr	;uses string variable as arg
	push	edx
	lea	edx,	dispStr
	call	WriteString
	pop	edx
ENDM

.data
intro	BYTE	0Ah,"Welcome to the Amazing Combination Calculator",0Ah,09h,"-Kerry Vance",0Ah,0Ah,"I'll give you a combinatorics problem, you try your best, and I'll use my superior computer brain to see if you're right.",0Ah,0Ah,0
prompt0	BYTE	" choose ",0
prompt1	BYTE	"Combinations: ",0
prompt2	BYTE	" = ",0
prompt3	BYTE	0Ah,"You answered: ",0
prompt4	BYTE	"Congrats! You answered correct",0Ah,0
prompt5	BYTE	0Ah,"Mmmmm, so wrong. Try being smarter.",0Ah,0
prompt6	BYTE	0Ah,"Would you like to try again? (Yes/No): ",0
prompt7	BYTE	"Oh come on, no one likes a smart ass.",0Ah,0
ciao	BYTE	"Bye now! Come again",0Ah,0
errPrmt	BYTE	0Ah,"That wasn't a number genius, try again: ",0
ansPrmt	BYTE	0Ah,"How many ways to choose?: ",0
LO	DWORD	3
HI	DWORD	10
n	DWORD	?
r	DWORD	?
ansStr	BYTE	MAX+1 DUP (0)
usrAns	DWORD	0
result	DWORD	0
goAgain	BYTE	MAX+1 DUP (0)

.code
main PROC
	call	Randomize				;initialize psr
	call	introProc

L1:
	push	OFFSET n				;pass address of n on stack
	push	OFFSET r				;pass address of r on stack
	call	showProb

	push	OFFSET usrAns			;pass address of usrAns DWORD
	call	getDat

	push	OFFSET result			;pass address of result (correct answer)	
	push	OFFSET n				;pass address of n
	push	OFFSET r				;pass address of r
	call	combi

	push	result				;pass value of result
	push	usrAns				;pass value of usrAns
	push	n				;pass value of n
	push	r				;pass value of r
	call	showRes
	jmp	L3

L2:
	myWriteStr	prompt7
L3:
	myWriteStr	prompt6
	mov	ecx,	MAX
	lea	edx,	goAgain
	call	ReadString
	call	Crlf
	mov	al,	goAgain[0]
	cmp	al,	89
	je	L1
	cmp	al,	121
	je	L1
	cmp	al,	78
	je	L4
	cmp	al,	110
	je	L4
	jmp	L2
L4:
	myWriteStr	ciao

	exit
main ENDP

introProc PROC
	myWriteStr	intro
	ret
introProc ENDP

showProb PROC	;r in XPARAM, n in YPARAM
	push	ebp
	mov	ebp,	esp
	
	mov	ebx,	XPARAM			;address of r in ebx
	mov	ecx,	YPARAM			;address of n in ecx

	push	LO				;pass LO to getRand
	push	HI				;pass HI to getRand
	call	getRand				;return value between LO and HI for n
	mov	[ecx],	eax			;move to n

	push	1				;lower limit of r
	mov	eax,	[ecx]			;value of n, upper limit for r
	push	eax
	call	getRand
	mov	[ebx],	eax

	mov	eax,	[ecx]
	call	WriteDec
	myWriteStr	prompt0
	mov	eax,	[ebx]
	call	WriteDec
	call	Crlf

	pop	ebp
	ret	8
showProb ENDP

getDat PROC	;address of return variable in XPARAM
	push	ebp
	mov	ebp,	esp
	mov	eax,	0			;make sure upper half of al is empty

	mov	esi,	XPARAM			;address of usrAns in esi
	mov	[esi],	eax			;initialize to zero
	myWriteStr	prompt1
L1:
	mov	ecx,	MAX
	lea	edx,	ansStr
	call	ReadString
	mov	ebx,	0			;ebx counts number of chars in input
	jmp	L3
L2:
	inc	ebx
L3:
	mov	al,	ansStr[ebx]		;load char at index in ebx to al
	cmp	al,	0			;if null, end of string
	jz	L5
	sub	al,	48			;subtract 49 to get to actual numerical value
	cmp	al,	9			;if greater than 9
	jg	L4
	cmp	al,	0			;or less than zero
	jl	L4				;not a number
	jmp	L2				;else check next char
L4:
	myWriteStr	errPrmt
	jmp	L1
L5:
	cmp	ebx,	0			;if string is empty, also error
	jz	L4
	mov	ecx,	ebx			;number of chars in string is loop counter
	mov	ebx,	1			;10^0 position of first number
L6:
	mov	al,	ansStr[ecx-1]		;walk back down string
	sub	al,	48			;sub 48 to get numerical value
	mul	ebx				;multiply by appropriate power of ten in ebx
	add	[esi],	eax			;accumulate in return
	mov	eax,	10
	mul	ebx				;multiply ebx to next power of 10
	mov	ebx,	eax
	mov	eax,	0
	loop	L6				;loop over each char in string

	pop	ebp
	ret	4
getDat ENDP

combi PROC	;n in YPARAM, r in XPARAM, result in ZPARAM
	push	ebp
	mov	ebp,	esp

	mov	esi,	XPARAM
	mov	edi,	YPARAM

	mov	eax,	[edi]
	sub	eax,	[esi]
	call	fact				;(n - r)!
	push	eax				;store (n - r)!
	mov	eax,	[esi]
	call	fact				;r!
	pop	ebx				;(n - r)! in ebx
	mul	ebx				;r!(n - r)! in eax
	push	eax				;store
	mov	eax,	[edi]
	call	fact				;n! in eax
	mov	edx,	0			;clear edx
	pop	ebx				;pop r!(n - r)! from stack into denominator
	div	ebx				;divide n! in eax, by r!(n - r)! in ebx

	mov	esi,	ZPARAM
	mov	[esi],	eax			;put correct answer into result

	pop	ebp
	ret	12
combi ENDP

fact PROC		;returns factorial of eax in eax
	cmp	eax,	0
	jz	L1				;0! = 1
	push	eax				;store eax
	dec	eax
	call	fact				;call factorial with eax - 1
	pop	ebx				;multiplies by previous value on way up stack frames
	mul	ebx
	jmp	L2
L1:
	mov	eax,	1
L2:

	ret
fact ENDP

getRand PROC ;returns random number between XPARAM and YPARAM in eax
	push	ebp
	mov	ebp,	esp
	push	ebx

	mov	eax,	XPARAM
	mov	ebx,	YPARAM
	call	RandomRange
	add	eax,	ebx

	pop	ebx
	pop	ebp
	ret	8
getRand ENDP

showRes PROC	;r in XPARAM, n in YPARAM, usrAns in ZPARAM, result in WPARAM
	push	ebp
	mov	ebp,	esp

	mov	eax,	YPARAM
	call	WriteDec
	myWriteStr	prompt0
	mov	eax,	XPARAM
	call	WriteDec
	myWriteStr	prompt2
	mov	eax,	WPARAM
	call	WriteDec
	call	Crlf
	myWriteStr	prompt3
	mov	ebx,	eax
	mov	eax,	ZPARAM
	call	WriteDec
	call	Crlf

	cmp	eax,	ebx
	jne	L1
	myWriteStr	prompt4
	jmp	L2
L1:
	myWriteStr	prompt5
L2:

	pop	ebp
	ret	16
showRes ENDP

END main
