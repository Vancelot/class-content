TITLE Project02     (Project02.asm)

; Author: Kerry Vance
; CS271 / Project 02                 Date: 18 Jan 19
; Description: Fibonacci Numbers

INCLUDE Irvine32.inc

MAX		=		47

.data
header	BYTE	"Fibonacci Numbers",0Ah,"Kerry Vance",0Ah,0
ec			BYTE	"**EC: Alligned columns",0Ah,0Ah,0
intro		BYTE	"Enter your name: ",0
hello		BYTE	"Hello, ",0
myName	BYTE	20 DUP(?)
dir1		BYTE	0Ah,"Enter the number of Fibonacci numbers to be displayed",0AH,"Give an integer in the range [0,46]: ",0
fifthNum	DWORD	4
outRan	BYTE	0Ah,"Out of range, Enter a number between 0 and 46: ",0
bye		BYTE	0Ah,0Ah,"If droids could think, there'd be none of us here would there?",0Ah,"Goodbye, ",0

.code
main PROC
;intro
	lea	edx,	header
	call	WriteString
	lea	edx,	ec
	call	WriteString
	lea	edx,	intro
	call	WriteString
	mov	ecx,	SIZEOF myName
	lea	edx,	myName
	call	ReadString
	lea	edx,	hello
	call	WriteString
	lea	edx,	myName
	call	WriteString
	lea	edx,	dir1
	call	WriteString
	jmp	L2
;instructions
L1:							;Label for out of range condition
	lea	edx,	outRan
	call	WriteString
;get user data
L2:							;Label for initial number reading
	call	ReadInt
	cmp	eax,	MAX		;Input >= 47  out of range
	jge	L1
	cmp	eax,	0			;Input <= 0  out of range
	jle	L1
;display fibonacci sequence
	call	crlf
	mov	ecx,	eax		;Store nums in sequence to counter
	mov	eax,	0			;0 must be written manually
	call	WriteDec
	call	allign
	mov	ebx,	1			;1 initial second number in sequence
	dec	ecx				;Dec counter after writing first num
	cmp	ecx,	0			;If user entered 1, go to end
	jle	L6
L3:
	mov	edx,	eax		;Save num1 val to edx before addition
	add	eax,	ebx		;Add num2 to num1 and store in eax
	call	WriteDec
	mov	ebx,	edx		;num1 before addition becomes next loops num2
	dec	fifthNum
	cmp	fifthNum,	0
	jnz	L4					;Alligns unless every 5th num
	call	crlf
	mov	fifthNum,	5	;Reinitialize number counter to 5
	jmp	L5					;Skip allign on new line
L4:
	call	allign			;Allign columns based on # of digits
L5:
	loop	L3					;If more nums in seq requested, loop
;farewell
L6:
	lea	edx,	bye
	call	WriteString
	lea	edx,	myName
	call	WriteString
	call	crlf
	call	crlf

	exit
main ENDP

allign PROC					;Procedure for calculating digits in result and subtracting from spaces to print
	push	eax
	push	ebx
	push	ecx
	push	edx				;Save register state

	mov	ecx,	15			;mov max # of spaces into counter
AL1:
	mov	edx,	0			;Clear upper dividend
	mov	ebx,	10			;Initialize divisor as 10
	div	ebx				
	dec	ecx				;Decrement # of spaces based on digits
	cmp	eax,	0
	je		AL2				;If quotient is zero loop has counted all digits
	jmp	AL1
AL2:
	mov	al,	32			;mov space into al
AL3:
	call	WriteChar
	loop	AL3				;Write spaces. 1 less digit = 1 less space
	
	pop	edx
	pop	ecx
	pop	ebx
	pop	eax
	ret						;Restore register states and return
allign ENDP

END main
