TITLE Project04     (Project04.asm)

; Author: Kerry Vance
; CS271 / Project 04                 Date: 17 Feb 19
; Description: Composite Number Calculator

INCLUDE Irvine32.inc

MAX = 400

.data
intro		BYTE	0Ah,0Ah,"Hello and welcome to the Composite Number Calculator",0Ah,09h,"By: Kerry Vance",0Ah,"**EC: Alignment",0Ah,0Ah,0
prompt0	BYTE	"Enter the number of composites you'd like to see (0-400): ",0Ah,0
prompt1	BYTE	"Sorry please enter a number between 1 and 400: ",0
bye		BYTE	0Ah,"Definitely correct numbers by yours truly,",0Ah,09h,"-Kerry Vance",0Ah,0
primes		DWORD	10 DUP(0)

.code
main PROC
	mov	[primes + 4],	2
	mov	[primes + 8],	3
	mov	[primes],	2
	call	introProc
	call	getDat
	call	showComp
	call	farewell

	exit
main ENDP

introProc PROC USES edx
	lea	edx,	intro			;load intro address to edx
	call	WriteString			;write

	ret
introProc ENDP

getDat PROC
	lea	edx,	prompt0		;load instructions
	call	WriteString
L1:
	call	ReadDec			;Take input
	call	validate			;pass to validate PROC
	cmp	eax,	0			;checks validate returned error eax is 0
	jz	L1				;try again if bad input
	call	Crlf				;else move on

	ret
getDat ENDP

;Checks eax for valid input, returns 0 in eax if invalid, initializes counter ecx with valid input, and leaves eax unmodified
validate PROC					
	cmp	eax,	0
	jbe	L1				;jump to error label if zero or below
	cmp	eax,	MAX			;compare eax to MAX allowable value
	ja	L1				;jmp to error label if too large.
	jmp	L2				;else jump to end
L1:
	lea	edx,	prompt1		;print error message
	call	WriteString
	mov	eax,	0			;return error of 0 in eax
	jmp	L3				;jmp to end to avoid initializing counter register
L2:
	mov	ecx,	eax			;good input, intialize counter
L3:
	ret
validate ENDP

showComp PROC
	mov	eax,	3			;Begin at 3 because 1,2,3 are not composite
	mov	ebx,	0			;init ebx to 0, ebx counts numbers per line
L1:
	inc	eax				;eax is number to check
	push	eax				;store eax before check
	call	isComp
	cmp	eax,	0			;compare returned value
	pop	eax				;restore original eax
	jl	L1				;if less than 0 don't print
	call	WriteDec			;else write eax
	cmp	ebx,	9			;cmp ebx, 9
	jl	L2				;if less, jump to allign label
	call	Crlf				;else add newline
	mov	ebx,	0			;initialize ebx back to zero
	jmp	L3
L2:
	call	allign
	inc	ebx
L3:
	loop	L1				;After printing finding composite decrement counter and loop

	call	Crlf
	ret
showComp ENDP

;Checks if number in eax is composite, returns -1 if prime
isComp PROC USES ecx ebx
	push	eax				;store eax
	mov	ebx,	2			;divisor of 2
	mov	edx,	0			;initialize edx to 0
	div	ebx				;divide by 2
	mov	ecx,	eax			;quotient is now counter to check max possible divisor
	sub	ecx,	ebx			;find difference between max possible and current divisor
	pop	eax				;restore argument value
	cmp	edx,	0			;if remainder 0, eax is composite, jump to end tag
	jz	L3
	inc	ebx				;else inc ebx to 3
L1:
	push	eax				;store argument value
	mov	edx,	0			;init edx to 0
	div	ebx				;div by 3,5,7,9 etc.
	pop	eax				;restore argument
	add	ebx,	2			;add 2 to ebx because if not divisible by two, argument won't be divisible by any even number
	cmp	edx,	0			;if rem == 0, number is composite jump to end
	jz	L3
	loop	L1				;else continue to loop
L2:
	mov	eax,	-1			;if prime return -1 in eax
L3:
	ret
isComp ENDP

allign PROC	USES eax ebx ecx edx		;Procedure for calculating digits in result and subtracting from spaces to print

	mov	ecx,	6			;mov max # of spaces into counter
L1:
	mov	edx,	0			;Clear upper dividend
	mov	ebx,	10			;Initialize divisor as 10
	div	ebx				
	dec	ecx				;Decrement # of spaces based on digits
	cmp	eax,	0
	je	L2				;If quotient is zero loop has counted all digits
	jmp	L1
L2:
	mov	al,	32			;mov space into al
L3:
	call	WriteChar
	loop	L3				;Write spaces. 1 less digit = 1 less space
	
	ret					;Restore register states and return
allign ENDP

farewell PROC
	lea	edx,	bye
	call	WriteString

	ret
farewell ENDP

END main
