pval <- function(z, t = "2") {
   rv <- 0
   if (t == "l") {
      rv <- pnorm(z, lower.tail = TRUE)
   }
   else if (t == "r") {
      rv <- pnorm(z, lower.tail = FALSE)
   }
   else {
      rv <- 2 * pnorm(z, lower.tail = FALSE)
   }
   return(rv)
}

peval <- function(p, a) {
  cat("p - a\t\t= ",
        round(p - a, 3),
      "\na\t\t= ",
        a,
      "\np - a <= a\t= ",
        p - a <= a,
      "\n"
  )
  if (p - a <= a) {
    print("Evidence is strong to reject null")
  }
  else {
    print("Do not reject null")
  }
}

ci <- function(xbar=0, sig=0, n=0, ci=90, obj=c()) {
  ci <- switch(paste(ci), "99" = 2.576, "95" = 1.96, "90" = 1.645)
  if (length(obj) != 0) {
    xbar <- mean(obj)
    sig <- sd(obj)
    n <- length(obj)
  }
  low <- xbar - ((sig / sqrt(n)) * ci)
  high <- xbar + ((sig / sqrt(n)) * ci)
  return(c(low, high))
}

two.ci <- function(n1, n2, xbar1, xbar2, s1, s2, ci) {
  l <- xbar1 - xbar2
  r <- qt(
          1 - ((1 - ci) / 2),
          min(n1 - 1, n2 - 1)
         ) * (sqrt((s1^2 / n1) + (s2^2 / n2)))
  return(c(l - r, l + r))
}

tstat <- function(xbar, mu, s, n) {
  return((xbar - mu) / (s / sqrt(n)))
}

two.tstat <- function(n1, n2, xbar1, xbar2, s1, s2, dif=0) {
  return((xbar1 - xbar2 - dif) / sqrt((s1^2 / n1) + (s2^2 / n2)))
}
