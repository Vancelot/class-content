library(hash)

printhw <- function(h) {
  cat("\n\n\n", h[["TITLE"]], "\n\nGiven:\n")
  cat(h[["GIVEN"]])
  cat("\n\n\n")

  for (i in sort(keys(h))) {
    if (i != "TITLE" && i != "GIVEN") {
      cat(i, ": ", h[[i]], "\n\n\n")
    }
  }
}
