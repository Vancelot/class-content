source("../lib/hwutil.r")
source("../lib/confidence.r")

# Question 3
I <- 3
J <- 10
SSTr <- 593.8
SSE <- 4772.8

MSTr <- SSTr / (I - 1)
MSE <- SSE / (I * (J - 1))

F <- MSTr / MSE
F
1 - pf(F, I - 1, I * (J - 1))

# Question 4
I <- 4
J <- 3
MSTr <- 2627.3
MSE <- 1116.2

qf(0.95, I - 1, I * (J - 1))

F <- MSTr / MSE
F

# Question 5
Wheat <- c(5.3, 4.4, 6.1, 6.2, 6.8, 5.8)
Barley <- c(6.6, 8.0, 6.1, 7.5, 6.0, 5.7)
Maize <- c(5.7, 4.7, 6.4, 5.0, 6.1, 5.2)
Oats <- c(8.4, 6.2, 7.8, 7.1, 5.5, 7.2)
I <- 4
J <- length(Wheat)

mean(Wheat)
mean(Barley)
mean(Maize)
mean(Oats)
mean(c(Wheat, Barley, Maize, Oats))

thiamin <- c(Wheat, Barley, Maize, Oats)
grain <- c(rep("Wheat", 6), rep("Barley", 6), rep("Maize", 6), rep("Oats", 6))
mod <- aov(thiamin~grain)
summary(mod)
var(Wheat)
var(Barley)
var(Maize)
var(Oats)

sum(c(var(Wheat), var(Barley), var(Maize), var(Oats))) / I
