library(hash)

printProb <- function(n, P) {
   cat("Question", n, "\n")
   for(i in keys(P)){
      cat("P(", i, ")", "\t=", P[[i]], "\n")
   }
}

problem4 <- function()
{
   P <- hash()
   P[["R"]] <- 0.75
   p[["r'"]] <- 1 - P[["R"]]

   P[["A|R"]] <- 0.54
   P[["A'|R"]] <- 1 - P[["A|R"]]

   P[["A|R'"]] <- 0.1
   P[["A'|R'"]] <- 1 - P[["A|R'"]]

   P[["AnR"]] <- P[["A|R"]] * P[["R"]]
   P[["AnR'"]] <- P[["A|R'"]] * P[["R'"]]

   P[["A"]] <- P[["AnR"]] + P[["AnR'"]]

   P[["R|A"]] <- P[["AnR"]] / P[["A"]]

   printProb(4, P)
}


problem5 <- function() {
   P <- hash()
   p <- c(0.06, 0.29, 0.37, 0.16, 0.09, 0.03)

   P[["x>3"]] <- 0
   for (i in 4:5) {
      P[["x>3"]] <- P[["x>3"]]  + p[i+1]
   }

   P[["E(X)"]] = 0
   for(i in 1:length(p)) {
      P[["E(X)"]] = P[["E(X)"]] + ((i-1) * p[i])
   }

   P[["E(X^2)"]] = 0
   for(i in 1:length(p)) {
      P[["E(X^2)"]] = P[["E(X^2)"]] + (((i-1)^2) * p[i])
   }

   P[["V(X)"]] <- P[["E(X^2)"]] - (P[["E(X)"]]^2)

   P[["Y(X)"]] <- 0
   for(i in 1:length(p)) {
      P[["Y(X)"]] = P[["Y(X)"]] + (8*(i-1) * p[i])
   }

   P[["Y(X^2)"]] <- 0
   for(i in 1:length(p)) {
      P[["Y(X^2)"]] = P[["Y(X^2)"]] + ((8*(i-1))^2 * p[i])
   }

   P[["V(Y)"]] <- P[["Y(X^2)"]] - (P[["Y(X)"]]^2)

   printProb(5, P)
}

problem6 <- function()
{
   P <- hash()

   F <- function(x) {
      cdf <- c(0.08, 0.22, 0.33, 0.63, 0.80, 0.95, 1)
      return(cdf[x+1])
   }

   pmf <- c(F(0))
   for(i in 2:7) {
      pmf <- c(pmf, F(i-1))
      pmf[i] <- pmf[i] - F(i-2)
   }
   p <- function(x) {
      pmf[x + 1]
   }

   P[["X<=2"]] = F(2)
   P[["x>3"]] = 0

   for(i in 4:6){
      P[["x>3"]] = P[["x>3"]] + F(i) - F(i - 1)
   }

   P[["2<=x<=5"]] = 0
   for(i in 2:5) {
      P[["2<=x<=5"]] = P[["2<=x<=5"]] + p(i)
   }

   P[["2<x<5"]] = 0
   for(i in 3:4) {
      P[["2<x<5"]] = P[["2<x<5"]] + p(i)
   }

   P[["p(x)"]] = pmf

   P[["E(X)"]] = 0
   for(i in 0:6) {
      P[["E(X)"]] = P[["E(X)"]] + (i * p(i))
   }

   P[["E(X^2)"]] = 0
   for(i in 0:6) {
      P[["E(X^2)"]] = P[["E(X^2)"]] + (i^2 * p(i))
   }

   P[["V(X)"]] <- P[["E(X^2)"]] - (P[["E(X)"]]^2)

   P[["SD(X)"]] <- sqrt(P[["V(X)"]])

   printProb(6, P)
}

problem7 <- function()
{
   P <- hash()

   P[["p"]] <- 0.78
   P[["p'"]] <- 1 - P[["p"]]
   P[["n"]]  <- 5

   P[["P(X=4)"]] <- dbinom(4, P[["n"]], P[["p"]])
   P[["P(X<=4)"]] <- pbinom(4, P[["n"]], P[["p"]])
   P[["P(X>=4)"]] <- dbinom(4, P[["n"]], P[["p"]]) + dbinom(5,P[["n"]], P[["p"]])
   P[["E(X)"]] <- P[["n"]] * P[["p"]]
   P[["V(X)"]] <- sqrt(P[["n"]] * P[["p"]] * (1 - P[["p"]]))

   printProb(7, P)
}

problem8 <- function()
{
   P  <- hash()

   p <- 0.27
   n <- 4


   pmf <- c(dbinom(0,n,p))
   for(i in 1:4) {
      pmf <- c(pmf, dbinom(i, n, p))
   }

   P[["p(x)"]] <- pmf
   P[["E(X)"]] <- n * p
   P[["X<=1"]] <- pbinom(1, n, p)
   P[["X>=2"]] <- 0
   for(i in 2:4) {
      P[["X>=2"]] = P[["X>=2"]] + dbinom(i,n,p)
   }
   P[["E(X)"]] <- n * p
   P[["SD(X)"]] <- sqrt(n * p * (1 - p))

   printProb(8, P)
}

problem9 <- function()
{
   P  <- hash()

   l = 4

   P[["X=4"]] <- dpois(4, l)
   P[["X<=4"]] <- ppois(4,l)
   P[["X<4"]] <- ppois(3,l)
   P[["X>4"]] <- ppois(4,l, lower.tail = FALSE)
   P[["2<=x<=6"]] <- 0
   for(i in 2:6) {
      P[["2<=x<=6"]] = P[["2<=x<=6"]] + dpois(i, l)
   }

   printProb(9, P)
}

problem10 <- function()
{

   P  <- hash()

   l = 8

   P[["X=7"]] <- dpois(7,l)
   P[["X>=7"]] <- ppois(6,l,lower.tail=FALSE)
   P[["/15"]] <- 8/4

   printProb(10, P)
}

# problem4()
# problem5()
# problem6()
# problem7()
# problem8()
# problem9()
# problem10()
