source("stats.r")
library(hash)

printhw <- function(h) {
  cat("\n\n\n", h[["TITLE"]], "\n\nGiven:\n")
  cat(h[["GIVEN"]])
  cat("\n\n\n")

  for (i in sort(keys(h))) {
    if (i != "TITLE" && i != "GIVEN") {
      cat(i, ": ", h[[i]], "\n\n\n")
    }
  }
}

problem4 <- function() {
  qs <- hash()

  qs[["TITLE"]] <- "Problem 4 Solutions"

  qs[["GIVEN"]] <- " Average\t=\t58\n SD\t\t=\t7\n n\t\t=\t38"

  xavg <- 58
  xsd <- 7
  n <- 38
  Xsd <- xsd / sqrt(n)

  qs[["a) Distribution of mean times for n = 38"]] <- paste(
    "\n Average\t= ", xavg, "\n SD\t\t= ", Xsd
  )

  qs[["b) P(57 <= X <= 59)\t"]] <- paste(
    1 - (pnorm(57, xavg, Xsd) + (1 - pnorm(59, xavg, Xsd)))
  )

  qs[["c) P( X>= 59 )\t\t"]] <- 1 - pnorm(59, xavg, Xsd)
  printhw(qs)
}

cat(paste("Problem 5:", "\n----------"))
0.05 / sqrt(16)
0.05 / sqrt(64)

cat(paste("Problem 7:", "\n----------"))

mean(c((119.4 - ((119.4 - 118.6) / 2)), (119.6 - ((119.6 - 118.4) / 2))))

119.4 - 118.6
119.6 - 118.4


cat(paste("Problem 8:", "\n----------\n"))

avg <- 56.65
xsd <- 3.30

avg - ((xsd / sqrt(25)) * 1.96)
avg + ((xsd / sqrt(25)) * 1.96)

avg - ((xsd / sqrt(100)) * 1.96)
avg + ((xsd / sqrt(100)) * 1.96)

avg - ((xsd / sqrt(100)) * 2.576)
avg + ((xsd / sqrt(100)) * 2.576)

(xsd / sqrt(290)) * 2.576


cat(paste("Problem 10:", "\n-----------"))

1 - pnorm(49, 45, 8 / sqrt(25))


cat(paste("Problem 11:", "\n-----------"))

# z = 3.2, a = 0.5
z <- 3.3
a <- 0.05
p <- 1 - pnorm(z)
round(p, 3)
peval(p, a)

# z = 1.6, a = 0.01
z <- 1.8
a <- 0.01
p <- 1 - pnorm(z)
round(p, 3)
peval(p, a)

# z = -0.5, a = 0.1
z <- -0.5
a <- 0.1
p <- 1 - pnorm(z)
round(p, 3)
peval(p, a)


cat(paste("Problem 12:", "\n-----------"))

rs <- c(2044, 2088, 1994, 1852, 2025)
claim <- 2000
s <- 88.8

a <- 0.05

rss <- s / sqrt(length(rs))
xbar <- mean(rs)
z <- (xbar - claim) / rss
p <- 2 * pnorm(round(z, 2), lower.tail = FALSE)
p <- pval(z)

p
pval(-0.027)
peval(pval(-0.027), 0.1)
2 * pnorm(-0.027, lower.tail = FALSE)
peval(pnorm(-0.027), 0.1)

# Mean x
mean(rs)

# Standard Error
round(88.8 / sqrt(length(rs)), 3)

# z
round(z, 2)

# p
round(p, 4)

peval(p, a)
