source("stats.r")

printHW <- function(h) {
	cat("\n\n\n", h[["TITLE"]], "\n\nGiven:\n", h[["GIVEN"]], "\n\n\n")
	for(i in sort(keys(h))) {
		if(i != "TITLE" && i != "GIVEN") {
			cat(i, ": ", h[[i]], '\n\n\n')
		}
	}
}

problem2 <- function()
{
	s <- statsLib()
	qs <- hash()
	qs[["TITLE"]] <- paste("Problem 2 Solutions")


	s$Range$lower <- 0
	s$Range$upper <- 1

	s$pdf = function(x) {
		return(42 * (x^5) * (1-x))
	}

	qs[["GIVEN"]] <- paste("f(x)\n 42 * (x^5) * (1 - x)\t0 < x < 1\n 0\t\t\totherwise")

	r <- seq(0,1,0.001)
	plot(r, s$f(r), type='l', xlab= "", main= "Continuous VAR", lwd=2)

	plot(r, s$F(r), type='l', xlab= "", main= "CDF", lwd=2)

	qs[["b) F(x)"]] <- paste(cat("0\tx < 0\n42*((x^6 / 6) - (x^7 / 7))\t0 <= x <= 1\n1\tx > 1"))

	qs[["d) P(X <= 0.55) F(0.55)"]] <- s$F(0.55)
	qs[["e) P(0.3 <= X <= 0.55)"]] <- s$F(0.55) - s$F(0.3)
	qs[["f) P(0.3 <= X <= 0.75)"]] <- s$F(0.75) - s$F(0.3)

	l <- 0
	u <- 1
	result <- 0

	while(abs(result - 0.75) > 0.0001) {
		result <- s$F(l + (u - l) / 2)
		if(result < 0.75) {
			l = l + 0.0001
		}
		if(result > 0.75) {
			u = u - 0.0001
		}
	}
	qs[["g) 75th percentile"]] <- paste(l+ (u - l) / 2)

	#print(l+ (u - l) / 2)

	qs[["h) E(X)"]] <- s$E()
	qs[["h) SD(X)"]] <- s$SD()
	qs[["i) P(E(X) - SD(X) <= X <= E(X) + SD(X))"]] <- s$F(s$E() + s$V()) - s$F(s$E() - s$V())


	printHW(qs)
}

problem3 <- function()
{
	s <- statsLib()
	qs <- hash()
	qs[["TITLE"]] <- paste("Problem 3 Solutions")

	s$Range$lower <- 0
	s$Range$upper <- 4
	s$pdf = function(x) {
		return(x / 8)
	}
	qs[["GIVEN"]] <- paste(" F(x)\n\t0\t\tx < 0\n\tx^2 / 16\t0 <= x <= 4\n\t1\t\t4 <= x")

	qs[["a) P(X <= 3)"]] <- s$F(3)
	qs[["b) P(2.5 <= X <= 3)"]] <- s$F(3) - s$F(2.5)
	qs[["c) P(X > 3.5)"]] <- s$F(Inf) - s$F(3.5)

	r <- seq(0,4,0.01)
	plot(r, s$F(r), type='l', xlab= "", main= "Continuous VAR", lwd=2)

	qs[["d) Median checkout duration"]] <- 4 * ecdf(s$F(seq(0,4,0.01)))(.5)

	qs[["e) f(x) = F'(x) ="]] <- "x/8"
	qs[["f) E(X)"]] <- s$E()
	qs[["g) V(X)"]] <- s$V()
	qs[["g) SD(X)"]] <- s$SD()

	printHW(qs)
}

problem4 <- function()
{
	u <- UDist(a = -6, b = 6)
	qs <- hash()

	qs[["TITLE"]] <- "Problem 4 Solutions"
	qs[["GIVEN"]] <- "Uniform Distribution\n\tA = -6\n\tB = 6"

	qs[["a) P(X < 0)"]] <- u$F(0)
	qs[["b) P(-3 < X < 3)"]] <- u$F(5) - u$F(-3)
	qs[["c) P(-3 < X < 5)"]] <- u$F(5) - u$F(-3)


	k <- -5
	while(-6 < k && k + 4 < 6) {
		qs[["d) for -6 < k < k + 4 < 6, P(k < X < k + 4)"]] = u$F(k + 4) - u$F(k)
		k = k + 1
	}
	printHW(qs)
}

problem5 <- function() {
	e <- EDist()
	qs <- hash()

	qs[["TITLE"]] <- "Problem 5 Solutions"
	qs[["GIVEN"]] <- "lambda = 1"

	e$b = 1

	qs[["a) E(X)"]] <- e$E()
	qs[["b) SD(X)"]] <- e$SD()
	qs[["c) P(X <=2)"]] <- e$F(2)
	qs[["d) P(1 <= X <= 3)"]] <- e$F(3) - e$F(1)
	qs[["e) P(X >= 2)"]] <- 1 - e$F(2)

	printHW(qs)
}

problem6 <- function() {
	e <- EDist()
	qs <- hash()

	qs[["TITLE"]] <- "Problem 6 Solutions"
	qs[["GIVEN"]] <- "lambda = 0.01352"

	e$b <- 0.01352

	qs[["a) P(100)"]] <- e$F(100)
	qs[["a) P(200)"]] <- e$F(200)
	qs[["a) P(100 <= X <= 200)"]] <- e$F(200) - e$F(100)
	qs[["b) P(E(X) - 2SD(X) <= X <= E(X) + 2SD(X))"]] <- 1 - e$F(e$E() + (2 * e$SD())) + e$F(e$E() - (2 * e$SD()))

	qs[["c) P(X <= a) = 0.50"]] <- e$p(0.5)
	qs[["d) P(X<= a) = 0.95"]] <- e$p(0.95)

	printHW(qs)
}


problem8 <- function() {
	qs <- hash()

	qs[["TITLE"]] <- "Problem 7 Solutions"

	qs[["1a) P(X <= 0.23)"]] <- pnorm(0.23)
	qs[["1b) P(X >= 0.23)"]] <- 1 - pnorm(0.23)
	qs[["1c) P(X <= -4.48)"]] <- pnorm(-4.48)

	qs[["2a) 91st"]] <- qnorm(0.91)
	qs[["2a) 9th"]] <- qnorm(0.09)
	qs[["2a) 75th"]] <- qnorm(0.75)

	printHW(qs)
}

problem10 <- function() {
	qs <- hash()

	qs[["TITLE"]] <- "Problem 8 Solutions"
	qs[["GIVEN"]] <- "\tMean = 1050 um\n\tSD(X) = 150 um\n"

	qs[["a1) P(X <= 1350)"]] <- pnorm(1350, 1050, 150)
	qs[["a2) P(X > 950)"]] <- 1 - pnorm(950, 1050, 150)

	qs[["b) P(950 < X < 1350)"]] <- pnorm(1350, 1050, 150) - pnorm(950, 1050, 150)

	qs[["c) 2nd"]] <- qnorm(0.02, 1050, 150)

	qs[["d) 5 drops P(X > 1350)"]] <- 1 - pbinom(0, 5, (1 - pnorm(1350, 1050, 150)))

	printHW(qs)
}
