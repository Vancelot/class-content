library(hash)

setRefClass("Dist", fields = list(a = "numeric", b = "numeric"))
udist <- setRefClass("UDist", contains = "Dist",
   methods = list(
      f = function(x) {
         result <- double(length = length(x))
         for (i in seq_len(length(x))) {
            if (.self$a <= x[i] && x <= .self$b) {
               result[i] <- 1 / (.self$b - .self$a)
            }
            else {
               result[i] <- 0
            }
            return(result)
         }
      },
      E = function(x = "X") {
         return((.self$a + .self$b) / 2)
      },
      V = function() {
         return(((.self$b - .self$a)^2) / 12)
      },
      SD = function() {
         return(sqrt(.self$V()))
      },
      F = function(x) {
         return(punif(x, .self$a, .self$b))
      },
      p = function(x) {
         return(qunif(x, .self$a, .self$b))
      }
   )
)

gdist <- setRefClass("GDist", contains = "Dist",
   methods = list(
      f = function(x) {
         result <- double(length = length(x))
         for (i in seq_len(length(x))) {
            if (x[i] >= 0) {
               result[i] <- (
                  1 / (
                       .self$b^.self$a * gamma(.self$a))
                  ) * (x ^ (.self$a - 1) * exp(1) ^ ((-x / .self$b))
               )
            }
            else {
               result[i] <- 0
            }
         }
         return(result)
      },
      E = function(x = "X") {
         return(.self$a * .self$b)
      },
      V = function() {
         return(.self$a * .self$b^2)
      },
      SD = function() {
         return(sqrt(.self$V()))
      },
      F = function(x) {
         return(pgama(x, .self$a, rate = 1 / .self$b))
      },
      p = function(x) {
         qgamma(x, .self$a, rate = 1 / .self$b)
      }
   )
)

edist <- setRefClass("EDist", contains = "GDist",
   methods = list(
      initialize = function() {
         .self$a <- 1
      },
      f = function(x) {
         result <- double(length = length(x))
         for (i in seq_len(length(x))) {
            if (x >= 0) {
               result[i] <- .self$b * exp(1) ^ (-.self$b * x)
            }
            else {
               result[i] <- 0
            }
         }
         return(result)
      },
      E = function(x) {
         return(1 / .self$b)
      },
      V = function(x) {
         return(1 / .self$b^2)
      },
      F = function(x) {
         return(pexp(x, .self$b))
      },
      p = function(x) {
         return(qexp(x, .self$b))
      }
   )
)

ndist <- setRefClass("NDist", contains = "Dist",
   methods = list(
      f = function(x) {
         result <- double(length = length(x))
         for (i in seq_len(length(x))) {
            result[i] <- (
               1 / (
                  sqrt(2 * pi * .self$V()))
               ) * exp(1) ^ (((x - .self$E())^2) / 2 * .self$V()
            )
         }
      },
      E = function() {
         return(.self$a)
      },
      V = function() {
         return(.self$b)
      }
   )
)

sndist <- setRefClass("SNDist", contains = "NDist",
   methods = list(
      f = function(x) {
         result <- double(length = length(x))
         for (i in seq_len(length(x))) {
            result[i] <- callNextMethod((x - .self$E()) / .self$V())
         }
      }
   )
)

statsLib <- setRefClass("statsLib",
   fields = list(
      probs = "hash",
      pmf = "hash",
      pdf = "function",
      Range = "list"
   ),
   methods = list(
      initialize = function() {
         .self$probs <- hash()
         .self$range <- list()
      },
      f = function(x) {
         result <- double(length = length(x))
         for (i in seq_len(length(x))) {
            if (x[i] > .self$Range$lower && x[i] <= .self$Range$upper) {
               result[i] <- .self$pdf(x[i])
            }
            else {
               result[i] <- 0
            }
         }
         return(result)
      },
      F = function(x) {
         result <- double(length = length(x))
         for (i in seq_len(length(x))) {
            result[i] <- integrate(.self$f, .self$Range$lower, x[i])$value
         }
         return(result)
      },
      E = function(x = "X") {
         expVal <- function(x) {
            return(x * .self$f(x))
         }
         expVal2 <- function(x) {
            return(x^2 * .self$f(x))
         }
         if (x == "X") {
            return(integrate(expVal, -Inf, Inf)$value)
         }
         else if (x == "X^2") {
            return(integrate(expVal2, -Inf, Inf)$value)
         }
      },
      V = function() {
         return(.self$E("X^2") - .self$E()^2)
      },
      SD = function() {
         return(sqrt(.self$V()))
      },
      p = function(i) {
         return(pmf[[toString(i)]])
      },
      P = function(x) {
         if (length(x) == 1) {
            return(.self$probs[[x]])
         }
         else if (length(x) == 3) {
            a <- x[1]
            b <- x[3]
            op <- x[2]

            if (op == "n" || op == "U") {
               if (!is.null(.self$probs[[paste(x, collapse = "")]])) {
                  result <- .self$probs[[paste(x, collapse = "")]]
               }
               else {
                  result <- .self$P(c(b, op, a))
               }
            }
            else {
               result <- .self$probs[[paste(x, collapse = "")]]
            }
            return(result)
         }
      },
      findProbs = function(x) {
         if (length(x) == 1) {
            if (!has.key(paste(x, "'", sep = ""), .self$probs)) {
               .self$probs[[paste(x, "'", sep = "")]] <- 1 - .self$P(x)
            }
         }
         else {
            if (length(x) == 2) {
               a <- x[1]
               b <- x[2]
            }
            else if (length(x) == 3) {
               a <- x[1]
               b <- x[3]
            }

            if (grepl("\'", a, fixed = TRUE)) {
               notb <- unlist(strsplit(a, split = "'", fixed = TRUE))
            }
            else {
               nota <- paste(a, "'", sep = "")
            }

            if (grepl("\'", b, fixed = TRUE)) {
               notb <- unlist(strsplit(b, split = "'", fixed = TRUE))
            }
            else {
               notb <- paste(b, "'", sep = "")
            }

            found <- 1
            while (found > 0) {
               found <- 0

               # P(a) = P(Anb) + P(AnB')
               if (is.null(.self$P(a))
                  && !is.null(.self$P(c(a, "n", b)))
                  && !is.null(.self$P(c(a, "n", notb)))
               ) {
                  .self$probs[[a]] <- .self$P(
                      c(a, "n", b)) + .self$P(c(a, "n", notb)
                  )
                  .self$probs[[nota]] <- 1 - .self$P(a)
                  found <- found + 1
               }

               # P(a'|b) = 1 - P(a|b)
               if (is.null(.self$P(c(nota, "|", b)))
                  && !is.null(.self$P(c(a, "|", b)))
               ) {
                  .self$probs[[paste(
                     nota, "|", b, sep = ""
                  )]] <- 1 - .self$P(c(a, "|", b))
                  found <- found + 1
               }

               # P(AnB) = P(a) x P(b|a) = P(b) x P(a|b)
               if (is.null(.self$P(c(a, "n", b)))
                  && !is.null(.self$P(a))
                  && is.null(.self$P(c(b, "|", a)))
               ) {
                  .self$probs[[paste(
                     a, "n", b, sep = ""
                  )]] <- .self$P(a) * .self$P(c(b, "|", a))
                  found <- found + 1
               }

               # P(a'nB') = 1 - P(AnB)
               if (is.null(.self$P(c(nota, "n", notb)))
               && !is.null(.self$P(c(a, "n", b)))
               ) {
                  .self$probs[[paste(
                     nota, "n", notb, sep = ""
                   )]] <- 1 - .self$P(c(a, "n", b))
                  found <- found + 1
               }

               # P(AnB') = P(a) - P(AnB)
               if (is.null(.self$P(c(a, "n", notb)))
                  && !is.null(.self$P(a))
                  && !is.null(.self$P(c(a, "n", b)))) {
                  .self$probs[[paste(
                     a, "n", notb, sep = ""
                  )]] <- .self$P(a) - .self$P(c(a, "n", b))
                  found <- found + 1
               }

               # P(a'nB) = P(b) - P(AnB)
               if (is.null(.self$P(c(nota, "n", b)))
                  && !is.null(.self$P(a))
                  && !is.null(.self$P(c(a, "n", b)))) {
                  .self$probs[[paste(
                     nota, "n", b, sep = ""
                  )]] <- .self$P(b) - .self$P(c(a, "n", b))
                  found <- found + 1
               }

               # P(AUB) = P(a) + P(b) - P(AnB)
               if (is.null(.self$P(c(a, "U", b)))
                  && !is.null(.self$P(a))
                  && !is.null(.self$P(b))
                  && !is.null(.self$P(c(a, "n", b)))) {
                  .self$probs[[paste(
                     a, "U", b, sep = ""
                  )]] <- .self$P(a) + .self$P(b) - .self$P(c(a, "n", b))
                  found <- found + 1
               }

               # P(AUB') = P(a) + P(b') - P(AnB')
               if (is.null(.self$P(c(a, "U", notb)))
                  && !is.null(.self$P(a))
                  && !is.null(.self$P(notb))
                  && !is.null(.self$P(c(a, "n", notb)))) {
                  .self$probs[[paste(
                      a, "U", notb, sep = ""
                   )]] <- .self$P(a) + .self$P(notb) - .self$P(c(a, "n", notb))
                  found <- found + 1
               }

               # P(a'UB') = P(a') + P(b') - P(a'nB')
               if (is.null(.self$P(c(nota, "U", notb)))
                  && !is.null(.self$P(nota))
                  && !is.null(.self$P(notb))
                  && !is.null(.self$P(c(nota, "n", notb)))) {
                  .self$probs[[paste(
                     nota, "U", notb, sep = ""
                  )]] <- .self$P(nota) +
                         .self$P(notb) -
                         .self$P(c(nota, "n", notb))
                  found <- found + 1
               }

               # P(b|a) = P(anb) / P(a)
               if (is.null(.self$P(c(b, "|", a)))
                   && !is.null(.self$P(a, "n", b))
                   && !is.null(.self$P(a))
               ) {
                  .self$probs[[paste(
                     b, "|", a, sep = ""
                  )]] <- .self$P(c(a, "n", b) / .self$P(a))
                  found <- found + 1
               }

               # P(b|a) = (P(b) * P(a|b))
               #                /
               # ((P(b) * P(a|b)) + (P(b') * P(a|b')))
               if (is.null(.self$P(c(b, "|", a)))
                  && !is.null(.self$P(b))
                  && !is.null(.self$P(c(a, "|", b)))
                  && !is.null(.self$P(c(a, "|", notb)))
               ) {
                  .self$probs[[paste(
                     b, "|", a, sep = ""
                  )]] <- (.self$P(b) * .self$P(c(a, "|", b))) / ((.self$P(b) * P(c(a, "|", b))) + (.self$P(notb) * .self$P(c(a, "|", notb))))
                  found <- found + 1
               }
            }
         }
      },
      setProb = function(x, y) {
         stopifnot(y >= 0 && y <= 1)
         if (length(x) == 1) {
            .self$probs[[x]] <- y
         }
         else if (length(x) == 3) {
            .self$probs[[paste(x, collapse = "")]] <- y
            .self$findProbs(x)
            .self$findProbs(x[3], x[1])
         }
      }
   )
)

pval <- function(z, t = "2") {
   rv <- 0
   if (t == "l") {
      rv <- pnorm(z, lower.tail = TRUE)
   }
   else if (t == "r") {
      rv <- pnorm(z, lower.tail = FALSE)
   }
   else {
      rv <- 2 * pnorm(z, lower.tail = FALSE)
   }
   return(rv)
}

peval <- function(p, a) {
  cat("p - a\t\t= ",
        round(p - a, 3),
      "\na\t\t= ",
        a,
      "\np - a <= a\t= ",
        p - a <= a,
      "\n"
  )
  if (p - a <= a) {
    print("Evidence is strong to reject null")
  }
  else {
    print("Do not reject null")
  }
}
