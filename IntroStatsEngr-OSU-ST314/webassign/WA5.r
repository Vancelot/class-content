source("../lib/hwutil.r")
source("../lib/confidence.r")

data <- c(
  417, 420, 421, 423, 426, 429, 430, 435, 437, 439, 445, 447, 450, 451,
  459, 461, 465
)

boxplot(data, horizontal = TRUE)
mean(data)
sd(data)

t.test(data, mu = 452, alternative = "two.sided", conf.level = 0.95)

data <- c(2789, 2907, 2994, 2859, 2874)

t.test(data, mu = 3000, alternative = "two.sided", conf.level = 0.95)
sd(data)

p <- c(
      5.5,  3.9,  3.5,  2.0,  5.9,  2.4,  1.8,  2.9,  3.4,  0.9,  3.8,
      4.8, 3.4,   2.8,  4.4,  4.1,  4.0,  0.0,  3.6,  4.1,  6.8,  8.3,
      4.0,  8.9, 6.8,   5.0,  3.9,  2.4
)
hist(p)
mean(p)
sd(p)
t.test(p, mu = 5.0, conf.level = 0.90)

n1 <- 9
xbar1 <- 113.7
s1 <- 5.09

n2 <- 9
xbar2 <- 129.1
s2 <- 5.37

two.ci(n1, n2, xbar1, xbar2, s1, s2, 0.95)

n1 <- 9
xbar1 <- 23.78
s1 <- 0.53

n2 <- 4
xbar2 <- 21.85
s2 <- 0.53


degf <- min(n1 - 1, n2 - 1)


degf
tstatistic <- two.tstat(n1, n2, xbar1, xbar2, s1, s2)
tstatistic
round(1 - pt(tstatistic, degf), 4)
xbar1 - xbar2
(xbar1 - xbar2) - two.ci(n1, n2, xbar1, xbar2, s1, s2, 0.95)[1]

peval(round(1 - pt(two.tstat(n1, n2, xbar1, xbar2, s1, s2), degf), 4), 0.05)



yf <- c(
  28,   36,   32,   27,   28,   32,   31,   34,   32,   28,   28,   39,
  29, 34,   33,   27,   28,   32,   31,   34
)
of <- c(
  25,   20,   21,   23,   22,   17,   14,   23,   19,   15,   21,   17
)
boxplot(yf, of)
t.test(yf, of, alternative = "greater")
t.test(yf, of)$conf.in

u <- c(36.3, 55.0, 51.1, 38.6, 43.2, 48.8, 25.6, 49.7)
a <- c(28.5, 20.0, 46.0, 34.5, 36.0, 52.5, 26.5, 46.5)

t.test(u, a, paired = TRUE, conf.level = 0.99)
t.test(u, a, paired = TRUE)$p.value / 2

ua <- c()
for (i in seq_len(length(u))) {
  ua <- c(ua, u[[i]] - a[[i]])
}
sd(ua)
