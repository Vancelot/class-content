source("stats.r")

printHW <- function(h) {
	cat("\n\n\n", h[["TITLE"]], "\n\nGiven:\n")
	print(h[["GIVEN"]])
	cat("\n\n\n")

	for(i in sort(keys(h))) {
		if(i != "TITLE" && i != "GIVEN") {
			cat(i, ": ", h[[i]], '\n\n\n')
		}
	}
}

problem6 <- function()
{
	qs <- hash()

	qs[["TITLE"]] <- "Problem 6 Solutions"

	particles <- c(1, 3, 3, 12, 11, 15, 18, 10, 11, 4, 5, 3, 1, 2, 1)
	print(paste0(particles))
	qs[["GIVEN"]] <- paste("\nN particles\n")
	for(i in 0:14) {
		qs[["GIVEN"]] <- paste(qs[["GIVEN"]], i, ' ')
	}

	qs[["GIVEN"]] <- paste(qs[["GIVEN"]], "\nFrequency\n")
	for(i in particles) {
		qs[["GIVEN"]] <- paste(qs[["GIVEN"]], i, ' ')
	}


	barplot(particles, names=0:(length(particles) - 1),  space=0, ylab = "Percent", xlab = "Number", axes=TRUE, col="burlywood1")

	printHW(qs)
}

problem7 <- function()
{
	qs <- hash()

	qs[["TITLE"]] <- "Problem 7 Solutions"

	######################################################################
	# Use the R code to calculate the sample summary statistics, histogram and boxplot.
	# Can copy and paste directly to R script.
	IQ.data <- c(78, 	91, 	99, 	102, 	103, 	103, 	106, 	107, 	107, 	107, 	108, 	108,
	109, 	109, 	110, 	110, 	112, 	112, 	112, 	113, 	114, 	114, 	118, 	118,
	125, 	125, 	126, 	127, 	127, 	132, 	136, 	140, 	140, 	151)

	summary(IQ.data)
	sd(IQ.data)
	hist(IQ.data)
	boxplot(IQ.data, horizontal = TRUE)
	######################################################################

	qs[["GIVEN"]] <- summary(IQ.data)

	qs[["a) standard deviation"]] <- round(sd(IQ.data), digits = 1)

	printHW(qs)
}

problem8() <- function()
{
	qs <- hash()

	qs[["TITLE"]] <- "Problem 8 solutions"

	##########################################################################################
	# Use R to create a stemplot
	# Copy the data and code below
	# Run the code in R
	time=c( 	381, 	351, 	354, 	360, 	379, 	425, 	321, 	396, 	404, 	373, 	376, 	372, 	362,
		366, 	366, 	325, 	339, 	394, 	391, 	369, 	376, 	356, 	352, 	408, 	330, 	398)
	stem(time,2)
	#Note: If your stems from R are not the same as below. Change 1 in the stem function to 2.
	##########################################################################################

	qs[["GIVEN"]] <- time

	qs[["b) Mean and median"]] <- paste("\nMean: ", round(mean(time), digits = 2), "\tMedian: ", median(time))

	qs[["c) Increased without changing median"]] <- "Infinity"
	qs[["c) Decreased without changing median"]] <- 53

	for(i in 1:length(time)) {
		time[i] = time[i] / 60
	}

	qs[["d) Expressed in minutes"]] <- paste("\nMean: ", round(mean(time), digits = 2), "\tMedian: ", round(median(time), digits = 2))

	printHW(qs)
}

problem10 <- function()
{
	qs <- hash()
	qs[["TITLE"]] <- "Problem 10 solutions"

	ym = c(116.5, 115.7, 114.9, 115.2, 115.8)

	qs[["GIVEN"]] <- ym

	qs[["a1) mean"]] <- mean(ym)

	qs[["a2) deviations from mean"]] <- "\nx\t"
	for(i in 1:length(ym)) {
		qs[["a2) deviations from mean"]] = paste(qs[["a2) deviations from mean"]], ym[i], '\t')
	}
	qs[["a2) deviations from mean"]] = paste(qs[["a2) deviations from mean"]], "\n\t")
	for(i in 1:length(ym)) {
		qs[["a2) deviations from mean"]] = paste(qs[["a2) deviations from mean"]], round(ym[i] - mean(ym), digits = 2), '\t')
	}

	qs[["b) Sample variance and standard deviation"]] <- paste("\n\t s\t\t s^2\n\t", round(sd(ym), digits = 3), '\t', round(var(ym), digits = 3), '\n')

	for(i in 1:length(ym)) {
		ym[i] = ym[i] - 50
	}

	qs[["c) sample mean and variance of transformed values"]] <- paste("\n\t s\t\t s^2\n\t", round(sd(ym), digits = 3), "\t\t", round(var(ym), digits = 3), '\n')

	printHW(qs)
}

