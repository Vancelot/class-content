library(hash)

statsLib <- setRefClass ("statsLib",
   fields = list(probs = "hash",
                 pmf = "hash",
                 pdf = "function",
                 Range = "list"
   ),
   methods = list(
      initialize = function() {
         .self$probs <- hash()
         .self$Range <- list()
      },
      f = function(x) {
         result <- double(length = length(x))
         for (i in seq_len(length(x))) {
            if (x[i] > .self$Range$lower && x[i] <= .self$Range$upper) {
               result[i] <- .self$pdf(x[i])
            }
            else {
               result[i] <- 0
            }
         }
         return(result)
      },
      E = function(X = "X") {
         expVal <- function(x) {
            return(x * .self$f(x))
         }
         expVal2 <- function(x) {
            return(x^2 * .self$f(x))
         }
         if (X == "X") {
            return(integrate(expVal, -Inf, Inf)$value)
         }
         else if (X == "X^2") {
            return(integrate(expVal2, -Inf, Inf)$value)
         }
      },
      V = function() {
         return(.self$E("X^2") - .self$E()^2)
      },
      SD = function() {
         return(sqrt(.self$V()))
      },
      p = function(i) {
         return(pmf[[toString(i)]])
      },
      P = function(x) {
         if (length(x) == 1) {
            return(.self$probs[[x]])
         }
         else if (length(x) == 3) {
            A <- x[1]
            op <- x[2]
            B <- x[3]

            if (op == "n" || op == "U") {
               if (!is.null(.self$probs[[paste(x, collapse = "")]])) {
                  result <- .self$probs[[paste(x, collapse = "")]]
               }
               else {
                  result <- .self$probs[[paste(B, op, A, sep = "")]]
               }
            }
            else {
               result <- .self$probs[[paste(x, collapse = "")]]
            }
            return(result)
         }
      },
      findProbs = function(x) {
         if (length(x) == 1) {
            if (!has.key(paste(x, "'", sep = ""), .self$probs)) {
               .self$probs[[paste(x, "'", sep = "")]] <- 1 - .self$P(x)
            }
         }
         else {
            if (length(x) == 2) {
               A <- x[1]
               B <- x[2]
            }
            else if (length(x) == 3) {
               A <- x[1]
               B <- x[3]
            }

            if (grepl("\'", A, fixed = TRUE)) {
               notA <- unlist(strsplit(A, split = "'", fixed = TRUE))
            } else {
               notA <- paste(A, "'", sep = "")
            }

            if(grepl("\'", B, fixed = TRUE)) {
               notB <- unlist(strsplit(B, split = "'", fixed = TRUE))
            } else {
               notB <- paste(B, "'", sep = "")
            }

            found <- 1
            while (found > 0) {
               found <- 0

               if (is.null(.self$P(notA))
                  && !is.null(.self$P(A))
               ) {
                  .self$probs[[notA]] <- 1 - .self$P(A)
                  found <- found + 1
               }

               if (is.null(.self$P(notB))
                  && !is.null(.self$P(B))
               ) {
                  .self$probs[[notB]] <- 1 - .self$P(B)
                  found <- found + 1
               }

               # P(A) = P(Anb) + P(AnB')
               if (is.null(.self$P(A))
                  && !is.null(.self$P(c(A, "n", B)))
                  && !is.null(.self$P(c(A, "n", notB)))) {
                  .self$probs[[A]] <- .self$P(
                     c(A, "n", B)
                  ) + .self$P(c(A, "n", notB))
                  .self$probs[[notA]] <- 1 - .self$P(A)
                  found <- found + 1
               }

               # P(B) = P(BnA) + P(BnA')
               if (is.null(.self$P(B))
                  && !is.null(.self$P(c(B, "n", A)))
                  && !is.null(.self$P(c(B, "n", notA)))) {
                  .self$probs[[B]] <- .self$P(
                     c(B, "n", A)
                  ) + .self$P(c(B, "n", notA))
                  found <- found + 1
               }

               # P(AnB) = P(A) x P(B|A)
               if (is.null(.self$P(c(A, "n", B)))
                  && !is.null(.self$P(A))
                  && !is.null(.self$P(c(B, "|", A)))
               ) {
                  .self$probs[[paste(
                     A, "n", B, sep = ""
                  )]] <- .self$P(A) * .self$P(c(B, "|", A))
                  found <- found + 1
               }

               # P(AnB) = P(B) x P(A|B)
               if (is.null(.self$P(c(A, "n", B)))
                  && !is.null(.self$P(B))
                  && !is.null(.self$P(c(A, "|", B)))
               ) {
                  .self$probs[[paste(
                     A, "n", B, sep = ""
                  )]] <- .self$P(B) * .self$P(c(A, "|", B))
                  found <- found + 1
               }

               # P(A'nB') = 1 - P(AnB)
               if (is.null(.self$P(c(notA, "n", notB)))
                  && !is.null(.self$P(c(A, "n", B)))
               ) {
                  .self$probs[[paste(
                     notA, "n", notB, sep = ""
                  )]] <- 1 - .self$P(c(A, "n", B))
                  found <- found + 1
               }

               # P(AnB') = P(A) - P(AnB)
               if (is.null(.self$P(c(A, "n", notB)))
                  && !is.null(.self$P(A))
                  && !is.null(.self$P(c(A, "n", B)))
               ) {
                  .self$probs[[paste(
                     A, "n", notB, sep = ""
                  )]] <- .self$P(A) - .self$P(c(A, "n", B))
                  found <- found + 1
               }

               # P(A'nB) = P(B) - P(AnB)
               if (is.null(.self$P(c(notA, "n", B)))
                  && !is.null(.self$P(B))
                  && !is.null(.self$P(c(A, "n", B)))
               ) {
                  .self$probs[[paste(
                     notA, "n", B, sep = ""
                  )]] <- .self$P(B) - .self$P(c(A, "n", B))
                  found <- found + 1
               }

               # P(AUB) = P(A) + P(B) - P(AnB)
               if (is.null(.self$P(c(A, "U", B)))
                  && !is.null(.self$P(A))
                  && !is.null(.self$P(B))
                  && !is.null(.self$P(c(A, "n", B)))
               ) {
                  .self$probs[[paste(
                     A, "U", B, sep = ""
                  )]] <- .self$P(A) + .self$P(B) - .self$P(c(A, "n", B))
                  found <- found + 1
               }

               # P(AUB') = P(A) + P(B') - P(AnB')
               if (is.null(.self$P(c(A, "U", notB)))
                  && !is.null(.self$P(A))
                  && !is.null(.self$P(notB))
                  && !is.null(.self$P(c(A, "n", notB)))
               ) {
                  .self$probs[[paste(
                     A, "U", notB, sep = ""
                  )]] <- .self$P(A) + .self$P(notB) - .self$P(c(A, "n", notB))
                  found <- found + 1
               }

               # P(A'UB") = P(A') + P(B') - P(A'nB')
               if (is.null(.self$P(c(notA, "U", notB)))
                  && !is.null(.self$P(notA))
                  && !is.null(.self$P(notB))
                  && !is.null(.self$P(c(notA, "n", notB)))
               ) {
                  .self$probs[[paste(
                     notA, "U", notB, sep = ""
                  )]] <- .self$P(notA) +
                         .self$P(notB) -
                         .self$P(c(notA, "n", notB))
                  found <- found + 1
               }

#                # P(a|b') = 1 - P(a|b)
#                if (is.null(.self$P(c(A, "|", notB)))
#                   && !is.null(.self$P(c(A, "|", B)))
#                ) {
#                   .self$probs[[paste(
#                      A, "|", notB, sep = ""
#                   )]] <- 1 - .self$P(c(A, "|", B))
#                   found <- found + 1
#                }

               # P(b|a) = P(anb) / P(a)
               if (is.null(.self$P(c(B, "|", A)))
                   && !is.null(.self$P(c(A, "n", B)))
                   && !is.null(.self$P(A))
               ) {
                  .self$probs[[paste(
                     B, "|", A, sep = ""
                  )]] <- .self$P(c(A, "n", B)) / .self$P(A)
                  found <- found + 1
               }

               # P(b|a) = P(anb) / p(a)
               if (is.null(.self$P(c(B, "|", A)))
                   && !is.null(.self$P(c(A, "n", B)))
                   && !is.null(.self$P(A))
               ) {
                  .self$probs[[paste(
                     B, "|", A, sep = ""
                  )]] <- .self$P(c(A, "n", B)) / .self$P(A)
                  found <- found + 1
               }

               # P(b|a) = P(b) * P(A|B) / P(B) * P(A|B) + P(B') * P(A|B')
               if (is.null(.self$P(c(B, "|", A)))
                   && !is.null(.self$P(B))
                   && !is.null(.self$P(c(A, "|", B)))
                   && !is.null(.self$P(c(A, "|", notB)))
               ) {
                  .self$probs[[paste(
                     B, "|", A, sep = ""
                  )]] <- (
                     (.self$P(B) * .self$P(c(A, "|", B))) / (
                        (.self$P(B) * .self$P(c(A, "|", B))) +
                        (.self$P(notB) * .self$P(c(A, "|", notB)))
                     )
                  )
                  found <- found + 1
               }

               # P(a'|b) = 1 - P(a|b)
               if (is.null(.self$P(c(notA, "|", B)))
                   && !is.null(.self$P(c(A, "|", B)))
               ) {
                 .self$probs[[paste(
                   notA, "|", B, sep = ""
                 )]] <- 1 - .self$P(c(A, "|", B))
                 found <- found + 1
               }
            }
         }
      },
      setProb = function(x, y) {
         stopifnot(y >= 0 && y <= 1)
         if (length(x) == 1) {
            .self$probs[[x]] <- y
         }
         else if (length(x) == 3) {
            .self$probs[[paste(x, collapse = "")]] <- y
            .self$findProbs(x)
            .self$findProbs(c(x[3], x[1]))
            .self$findProbs(x)
         }
         .self$findProbs(x)
      }
   )
)

pval <- function(z, t = "2") {
   rv <- 0
   if (t == "l") {
      rv <- pnorm(z, lower.tail = TRUE)
   }
   else if (t == "r") {
      rv <- pnorm(z, lower.tail = FALSE)
   }
   else {
      rv <- 2 * pnorm(z, lower.tail = FALSE)
   }
   return(rv)
}

peval <- function(p, a) {
  cat("p - a\t\t= ",
        round(p - a, 3),
      "\na\t\t= ",
        a,
      "\np - a <= a\t= ",
        p - a <= a,
      "\n"
  )
  if (p - a <= a) {
    print("Evidence is strong to reject null")
  }
  else {
    print("Do not reject null")
  }
}
