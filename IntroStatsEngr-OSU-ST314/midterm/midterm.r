source("stats.r")

sl <- statsLib()

sl$setProb("f", 1 / 1000)
sl$setProb(c("t", "|", "f"), 0.98)
sl$setProb(c("t'", "|", "f'"), 0.98)

sl

p <- c(0.08, 0.24, 0.32, 0.19, 0.1, 0.07)

three_or_more <- 0
for (i in 4:6) {
   three_or_more <- three_or_more + p[i]
}
three_or_more

e <- 0
for (i in seq_len(length(p))) {
   e <- e + ((i - 1) * p[i])
}
e
sd(p)

round(1 - pexp(40, 1 / 18), digits = 4)
round(qexp(.5, 1 / 18), digits = 4)

1 / (1 / 18)

round(1 - pnorm(7.2, 7.5, 1.25), digits = 4)

mu <- 5
xsd <- 1.2
n <- 139
avg <- 5.2

ci <- c(avg - ((xsd / sqrt(n) * 1.96)), avg + ((xsd / sqrt(n) * 1.96)))
ci[2] - ci[1]

claim <- 8
s <- 0.8
xbar <- 8.26
n <- 27
a <- 0.05

rss <- s / sqrt(n)
z <- (xbar - claim) / rss
p <- pval(z)
p
peval(p, a)

(xbar - 8) / (0.8 / sqrt(n))

# con <- 1.645
con <- 1.96

rss
ci <- c(xbar - (rss * con), xbar + (rss * con))
round(ci, 4)
ci[2] - ci[1]
