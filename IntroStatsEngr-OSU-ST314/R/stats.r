library(hash)

setRefClass("Dist", fields = list(a = "numeric", b = "numeric"))
UDist <- setRefClass("UDist", contains = "Dist",
	methods = list(
		f = function(x) {
			result <- double(length = length(x))
			for(i in 1:length(x)) {
				if(.self$a <= x[i] && x <= .self$b) {
					result[i] = 1 / (.self$b - .self$a)
				}
				else {
					result[i] = 0
				}
				return(result)
			}
		},
		E = function(x = 'X') {
			return((.self$a + .self$b) / 2)
		},
		V = function() {
			return(((.self$b - .self$a)^2) / 12)
		},
		SD = function() {
			return(sqrt(.self$V()))
		},
		F = function(x) {
			return(punif(x, .self$a, .self$b))
		},
		p = function(x) {
			return(qunif(x, .self$a, .self$b))
		}
	)
)

GDist <- setRefClass("GDist", contains = "Dist",
	methods = list(
		f = function(x) {
			result <- double(length = length(x))
			for(i in 1:length(x)) {
				if(x[i] >= 0) {
					result[i] = (1 / (.self$b^.self$a * gamma(.self$a))) * (x^(.self$a - 1) * exp(1)^((-x / .self$b)))
				}
				else {
					result[i] = 0
				}
			}
			return(result)
		},
		E = function(x = 'X') {
			return(.self$a * .self$b)
		},
		V = function() {
			return(.self$a * .self$b^2)
		},
		SD = function() {
			return(sqrt(.self$V()))
		},
		F = function(x) {
			return(pgama(x, .self$a, rate = 1 / .self$b))
		},
		p = function(x) {
			qgamma(x, .self$a, rate = 1 / .self$b)
		}
	)
)

EDist <- setRefClass("EDist", contains = "GDist",
	methods = list(
		initialize = function() {
			.self$a = 1
		},
		f = function(x) {
			result <- double(length = length(x))
			for(i in 1:length(x)) {
				if(x >= 0) {
					result[i] = .self$b * exp(1)^(-.self$b * x)
				}
				else {
					result[i] = 0
				}
			}
			return(result)
		},
		E = function(x) {
			return(1 / .self$b)
		},
		V = function(x) {
			return(1 / .self$b^2)
		},
		F = function(x) {
			return(pexp(x, .self$b))
		},
		p = function(x) {
			return(qexp(x, .self$b))
		}
	)
)

NDist <- setRefClass("NDist", contains = "Dist",
	methods = list(
		f = function(x) {
			result <- double(length = length(x))
			for(i in 1:length(x)) {
				result[i] = (1 / (sqrt(2 * pi * .self$V()))) * exp(1)^(((x-.self$E())^2) / 2 * .self$V())
			}
		},
		E = function() {
			return(.self$a)
		},
		V = function() {
			return(.self$b)
		}
	)
)

SNDist <- setRefClass("SNDist", contains = "NDist",
	methods = list(
		f = function(x) {
			result <- double(length = length(x))
			for(i in 1:length(x)) {
				result[i] = callNextMethod((x - .self$E()) / .self$V())
			}
		}
	)
)

statsLib <- setRefClass("statsLib",
	fields = list(probs = "hash", pmf = "hash", pdf = "function", Range = "list"),
	methods = list(
		initialize = function() {
			.self$probs <- hash()
			.self$Range <- list()
		},
		f = function(x) {
			result <- double(length = length(x))
			for(i in 1:length(x)) {
				if(x[i] > .self$Range$lower && x[i] <= .self$Range$upper) {
					result[i] = .self$pdf(x[i])
				}
				else {
					result[i] = 0
				}
			}
			return(result)
		},
		F = function(x) {
			result <- double(length = length(x))
			for(i in 1:length(x)) {
				result[i] = integrate(.self$f, .self$Range$lower, x[i])$value
			}
			return(result)
		},
		E = function(X = 'X') {
			expVal <- function(x) {
				return(x * .self$f(x))
			}
			expVal2 <- function(x) {
				return(x^2 * .self$f(x))
			}
			if(X == 'X') {
				return(integrate(expVal, -Inf, Inf)$value)
			}
			else if(X == 'X^2') {
				return(integrate(expVal2, -Inf, Inf)$value)
			}
		},
		V = function() {
			return(.self$E('X^2') - .self$E()^2)
		},
		SD = function() {
			return(sqrt(.self$V()))
		},
		p = function(i) {
			return(pmf[[toString(i)]])
		},
		P = function(x) {
			if(length(x) == 1) {
				return(.self$probs[[x]])
			}
			else if(length(x) == 3)
			{
				A <- x[1]
				B <- x[3]
				op <- x[2]

				if(op == 'n' || op == 'U') {
					if(!is.null(.self$probs[[paste(x, collapse='')]])) {
						result <- .self$probs[[paste(x, collapse='')]]
					}
					else {
						result <- .self$probs[[paste(B, op, A, sep='')]]
					}
				}
				else {
					result <- .self$probs[[paste(x, collapse='')]]
				}
				return(result)
			}
		},
		findProbs = function(x)
		{
			if(length(x) == 1) {
				if(!has.key(paste(x, "'", sep=''), .self$probs)) {
					.self$probs[[paste(x, "'", sep="")]] <- 1 - .self$P(x)
				}
			}
			else
			{
				if(length(x) == 2) {
					A <- x[1]
					B <- x[2]
				}
				else if(length(x) == 3) {
					A <- x[1]
					B <- x[3]
				}
				if(grepl("\'", A, fixed=TRUE)) {
					notB <- unlist(strsplit(A, split="'", fixed=TRUE))
				}
				else {
					notA <- paste(A, "'", sep='')
				}

				if(grepl("\'", B, fixed=TRUE)) {
					notB <- unlist(strsplit(B, split="'", fixed=TRUE))
				}
				else {
					notB <- paste(B, "'", sep='')
				}
				found <- 1
				while(found > 0)
				{
					found <- 0

					# P(A) = P(Anb) + P(AnB')
					if(is.null(.self$P(A))
						&& !is.null(.self$P(c(A, 'n', B)))
						&& !is.null(.self$P(c(A, 'n', notB)))) {
						.self$probs[[A]] <- .self$P(c(A, 'n', B)) + .self$P(c(A, 'n', notB))
						.self$probs[[notA]] <- 1 - .self$P(A)
						found = found + 1
					}

					# P(B) = P(BnA) + P(BnA')
					if(is.null(.self$P(B))
						&& !is.null(.self$P(c(B, 'n', A)))
						&& !is.null(.self$P(c(B, 'n', notA)))) {
						.self$probs[[B]] <- .self$P(c(B, 'n', A)) + .self$P(c(B, 'n', notA))
						.self$probs[[notA]] <- 1 - .self$P(B)
						found = found + 1
					}

					# P(A'|B) = 1 - P(A|B)
					if(is.null(.self$P(c(notA, '|', B)))
						&& !is.null(.self$P(c(A, '|', B)))) {
							.self$probs[[paste(notA, '|', B, sep='')]] <- 1 - .self$P(c(A, '|', B))
							found = found + 1
					}

					# P(AnB) = P(A) x P(B|A)
					if(is.null(.self$P(c(A, 'n', B)))
						&& !(is.null(.self$P(A)) && is.null(.self$P(c(B, '|', A))))) {
						.self$probs[[paste(A, 'n', B, sep="")]] <- .self$P(A) * .self$P(c(B, '|', A))
						found = found + 1
					}

					# P(AnB) = P(B) x P(A|B)
					if(is.null(.self$P(c(A, 'n', B)))
						&& !is.null(.self$P(B))
						&& !is.null(.self$P(c(A, '|', B)))) {
						.self$probs[[paste(A, 'n', B, sep="")]] <- .self$P(B) * .self$P(c(A, '|', B))
						found = found + 1
					}

					# P(A'nB') = 1 - P(AnB)
					if(is.null(.self$P(c(notA, 'n', notB)))
						&& !is.null(.self$P(c(A, 'n', B)))) {
						.self$probs[[paste(notA, 'n', notB, sep='')]] <- 1 - .self$P(c(A, 'n', B))
						found = found + 1
					}

					# P(AnB') = P(A) - P(AnB)
					if(is.null(.self$P(c(A, 'n', notB)))
						&& !is.null(.self$P(A))
						&& !is.null(.self$P(c(A, 'n', B)))) {
						.self$probs[[paste(A, 'n', notB, sep='')]] <- .self$P(A) - .self$P(c(A, 'n', B))
						found = found + 1
					}

					# P(A'nB) = P(B) - P(AnB)
					if(is.null(.self$P(c(notA, 'n', B)))
						&& !is.null(.self$P(A))
						&& !is.null(.self$P(c(A, 'n', B)))) {
						.self$probs[[paste(notA, 'n', B, sep='')]] <- .self$P(B) - .self$P(c(A, 'n', B))
						found = found + 1
					}

					# P(AUB) = P(A) + P(B) - P(AnB)
					if(is.null(.self$P(c(A, 'U', B)))
						&& !is.null(.self$P(A))
						&& !is.null(.self$P(B))
						&& !is.null(.self$P(c(A, 'n', B)))) {
						.self$probs[[paste(A, 'U', B, sep='')]] <- .self$P(A) + .self$P(B) - .self$P(c(A, 'n', B))
						found = found + 1
					}

					# P(AUB') = P(A) + P(B') - P(AnB')
					if(is.null(.self$P(c(A, 'U', notB)))
						&& !is.null(.self$P(A))
						&& !is.null(.self$P(notB))
						&& !is.null(.self$P(c(A, 'n', notB)))) {
						.self$probs[[paste(A, 'U', notB, sep='')]] <- .self$P(A) + .self$P(notB) - .self$P(c(A, 'n', notB))
						found = found + 1
					}

					# P(A'UB') = P(A') + P(B') - P(A'nB')
					if(is.null(.self$P(c(notA, 'U', notB)))
						&& !is.null(.self$P(notA))
						&& !is.null(.self$P(notB))
						&& !is.null(.self$P(c(notA, 'n', notB)))) {
						.self$probs[[paste(notA, 'U', notB, sep='')]] <- .self$P(notA) + .self$P(notB) - .self$P(c(notA, 'n', notB))
						found = found + 1
					}

					# # A|B
					# .self$probs[[paste(A, '|', B, sep='')]] <- .self$P(c(A, 'n', B)) / .self$P(B)
					# # B|A
					# .self$probs[[paste(B, '|', A, sep='')]] <- .self$P(c(A, 'n', B)) / .self$P(A)
					# # A|B'
					# .self$probs[[paste(A, '|', notB, sep='')]] <- .self$P(c(A, 'n', notB)) / .self$P(notB)
					# # B'|A
					# .self$probs[[paste(notB, '|', A, sep='')]] <- .self$P(c(notB, 'n', A)) / .self$P(A)
					# # A'|B
					# .self$probs[[paste(notA, '|', B, sep='')]] <- .self$P(c(notA, 'n', B)) / .self$P(B)
					# # B|A'
					# .self$probs[[paste(B, '|', notA, sep='')]] <- .self$P(x) / .self$P(notA)
					# found = found + 1
				}
			}
		},
		setProb = function(x, y) {
			stopifnot(y >= 0 && y <= 1)
			if(length(x) == 1) {
				.self$probs[[x]] <- y
			}
			else if(length(x) == 3) {
				.self$probs[[paste(x, collapse ='')]] <- y
			}
			.self$findProbs(x)
		}
	)
)
