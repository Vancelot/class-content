// Implementation file for lab 4.
#include "house.hpp"
using namespace std;

void HouseType::print() const {
    cout << "Address: " << address << ", Price: " << price << ", Bedrooms" << bedrooms << endl;
}

// Place your member function definitions here:
// 1: The constructor.
HouseType::HouseType(const char addressArg[], double priceArg, int bedroomsArg)
{
	strcpy(address, addressArg);
	price = priceArg;
	bedrooms = bedroomsArg;
}
// 2: Function definition for isCheaper.
bool HouseType::isCheaper(const HouseType & otherHouse) const
{
	if (price < otherHouse.price)
		return true;
	else
		return false;
}
// 3: Function definition for hasMoreBedrooms.
bool HouseType::hasMoreBedrooms(const HouseType & otherHouse) const
{
	if(bedrooms > otherHouse.bedrooms)
		return true;
	else
		return false;
}


