// Header file for lab 4.
#ifndef HOUSE_HPP
#define HOUSE_HPP
#include <iostream>
#include <iomanip>
#include <cstring>

const int STR_SIZE = 100;

class HouseType {
    char address[STR_SIZE];
    double price;
    int bedrooms;
    public:
    void print() const; // Already implemented in house.cpp.
    // Implement the following functions in house.cpp
    HouseType(const char addressArg[], double priceArg, int bedroomsArg);
    bool isCheaper(const HouseType & otherHouse) const;
    bool hasMoreBedrooms(const HouseType & otherHouse) const;
};
#endif
