// Lab 4 app.cpp, Spring '18
// This is the driver for the member functions in the HouseType class.
#include "house.hpp"
using namespace std;

int main() {
	HouseType house1("123 Elm St.", 255000, 3); // Testing your constructor with arguments.
	HouseType house2("456 Maple St.", 320000, 4);

	house1.print();
	house2.print();

	if(house1.isCheaper(house2)) {  // Testing your isWinner code.
		cout << "Great job on is Cheaper code!" << endl;
	} else {
		cout << "Hmm, isCheaper code is not quite right." << endl;
	}

	if(house2.hasMoreBedrooms(house1)) {  // Testing your higherStanding code.
		cout << "Great job on hasMoreBedrooms code!" << endl;
	} else {
		cout << "Hmm, your hasMoreBedrooms code is not quite right." << endl;
	}

	return 0;
}
