#include<iostream>

using namespace std;

struct node
{
	int num;
	node *link;
};

void printList(node*);
void addNode(node*&, int);
void deleteList(node*&);

int main()
{
	int num;
	node *head = nullptr;

//	cout << "What number? ";
//	cin >> num;
	addNode(head, 3);
	addNode(head, 5);
	printList(head);
	deleteList(head);
	return 0;
}

void printList(node *h)
{
	node *temp = h;
	while(temp != nullptr) // key to traversal
	{
		cout << temp->num << endl;
		temp = temp->link;
	}
}

void addNode(node *&h, int num)
{
	node *temp = new node;
	temp->num = num;
	temp->link = nullptr;
	if(h == nullptr)
		h = temp;
	else
	{
		temp->link = h; //temp points to first node
		h = temp;
	}
}

void deleteList(node *&h)
{
	node *temp = h;
	while(h != nullptr)
	{
		h = temp->link;
		delete temp;
		temp = h;
	}
}
