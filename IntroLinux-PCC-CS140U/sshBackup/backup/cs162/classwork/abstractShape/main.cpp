//  main.cpp
//  Abstract Class, Shapes
//
//  Created by Robert Martin
//  CS162 PCC

#include "Shapes.hpp"

#include <iostream>

int main(int argc, const char * argv[]) {
    char choice = ' ';
    double length = 0.0, width = 0.0, radius = 0.0;
    double base = 0.0, height = 0.0;
    bool goodShape = true;
    //Shape s1; // Can't be done, because Shape is a pure virtual class.
    Shape * myShape;
    cout << "What shape would you like to create? (r/t/c) ";
    cin >> choice;
    if (choice == 'r') {
        cout << "What is the length and width of the rectangle? ";
        cin >> length >> width;
        myShape = new Rectangle(length, width);
    }
    else if(choice == 't') {
        cout << "What is the base and height of the triangle? ";
        cin >> base >> height;
        myShape = new Triangle(base, height);
    }
    else if(choice == 'c') {
        cout << "What is the radius of the circle? ";
        cin >> radius;
        myShape = new Circle(radius);
    }
    else {
        cout << "Unrecognized shape." << endl;
        goodShape = false;
    }
    if(goodShape) {
        cout << "The area of the " << myShape->getName() << " is ";
        cout << myShape->area() << endl;
    }
    delete myShape;
    return 0;
}
