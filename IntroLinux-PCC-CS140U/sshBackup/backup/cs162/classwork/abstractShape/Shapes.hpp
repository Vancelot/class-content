//  Abstract Class, Shapes
//  Shapes.hpp
//  Created by Robert Martin
//  CS162, PCC

#ifndef __Shapes__
#define __Shapes__

#include <iostream>
#include <cstring>
using namespace std;
const int strSize = 50;

class Shape 
{ // Pure Virtual Class
    char myName[strSize];
	 int * p;
public:
    virtual double area() = 0; // This makes Shape an abstract class.
    void setName(const char name[]);
    const char * getName();
    Shape(); // default constructor.
    Shape(const char name[]);
    virtual ~Shape();  // base class destructor.
};

class Triangle : public Shape 
{
public:
    double area();
    Triangle();
    Triangle(double b, double h);
    ~Triangle();
    
private:
	int * q;
    double height, base;
};

class Rectangle : public Shape 
{
	double length, width;
	int * q;
public:
    double area();
    Rectangle();
    Rectangle(double l, double w);
    ~Rectangle();
};

class Circle : public Shape 
{
	double radius;
	int * q;
    public:
        double area();
        Circle();
        Circle(double r);
        ~Circle();
        static const double PI;
};

#endif /* defined(__Shapes__) */
