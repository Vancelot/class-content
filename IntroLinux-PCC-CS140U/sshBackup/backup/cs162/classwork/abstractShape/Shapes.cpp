//  Abstract Class, Shapes
//  Shapes.cpp
//  Created by Robert Martin.
//  CS162 PCC

#include "Shapes.hpp"
// Initialize the static member of circle.
const double Circle::PI = 3.14159;

Shape::Shape() 
{
	p = new int;
	*p = 0;
    cout << "Base class constructor." << endl;
}

Shape::~Shape()
{
    cout << "Base class destructor." << endl;
	 if(p != nullptr)
	 	delete p;
}

Shape::Shape(const char name[]) 
{
	p = new int;
	*p = 0;
    strncpy(myName, name, strSize);
}

void Shape::setName(const char name[]) 
{
    strncpy(myName, name, strSize);
	 p = new int;
	 *p = 0;
}

const char * Shape::getName() 
{
    return myName;
}

Triangle::Triangle() : Shape("Triangle") 
{
    base = 0.0;
    height = 0.0;
	 q = new int;
	 *q = 11;
}

Triangle::Triangle(double b, double h) : Shape("Triangle") 
{
    base = b;
    height = h;
	 q = new int;
	 *q = 11;
}

double Triangle::area() 
{
    return base * 0.5 * height;
}

Triangle::~Triangle() 
{
    cout << "Triangle destructor." << endl;
	 if(q != nullptr)
	 	delete q;
}

Rectangle::Rectangle() : Shape("Rectangle") 
{
    length = 0.0;
    width = 0.0;
}

Rectangle::Rectangle(double l, double w) : Shape("Rectangle") 
{
    length = l;
    width = w;
}

double Rectangle::area() 
{
    return length * width;
}

Rectangle::~Rectangle() 
{
    cout << "Rectangle destructor." << endl;
}

Circle::Circle() : Shape("Circle") 
{
    radius = 0.0;
}

Circle::Circle(double r) : Shape("Circle") 
{
    radius = r;
}

double Circle::area() 
{
    return 2.0 * radius * PI;
}

Circle::~Circle() 
{
    cout << "Destructor for circle." << endl;
}
