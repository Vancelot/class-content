// Inheritance and Virtual Binding Example.
// Box implementation file.
// CS162 PCC

#include "box.hpp"
#include <iostream>

Box::Box() {  // Automatically invokes the base class constructor.
    height = 1.0;
}

Box::Box(double l, double w, double h) : Rect(l, w) {
    height = h;
}

double Box::getVolume() {
    return getLength() * getWidth() * height;
    // To have direct access to length and width, change length and
    //   width to protected. In the base class defintion
    //   (in the rectHeader.hpp file).
    
    // return length * width * height;
}

double Box::getHeight() {
    return height;
}

void Box::setHeight(double h) {
    height = h;
}

void Box::whatIsMyName() {
    std::cout << "My name is box." << std::endl;
}
