// Inheritance and Late Binding Example
// Rectangle Header file.
// CS162 PCC

#ifndef __HEADER__
#define __HEADER__
// libraries, prototypes, constants, etc.
class Rect {
    double length;
    double width;
    public:
    void someFunction();
    // "getters" (accessors)
    double getLength(); // member functioe prototypes.
    double getWidth();
    // "setters" (mutators)
    void setLength(double l);
    void setWidth(double w);
    Rect(); // default constructor - no args
    Rect(double, double);
    virtual void whatIsMyName();
    // virtual, late binding. runtime.
};

#endif
