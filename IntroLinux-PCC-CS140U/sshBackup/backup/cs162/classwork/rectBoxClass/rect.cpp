// Inheritance and Virtual Binding Example.
// Rectangle Implementation File.
// CS162 PCC

#include "rect.hpp"
#include <iostream>

// Default constructor.
Rect::Rect() {
    length = 1.0;
    width = 1.0;
}

// Constructor with arguments.
Rect::Rect(double l, double w) {
    length = l;
    width = w;
}

double Rect::getLength() {
    return length;
}

double Rect::getWidth() {
    return width;
}

void Rect::setLength(double l) {
    length = l;
}

void Rect::setWidth(double w) {
    width = w;
}

void Rect::whatIsMyName() {
    std::cout << "My name is rectangle." << std::endl;
}
