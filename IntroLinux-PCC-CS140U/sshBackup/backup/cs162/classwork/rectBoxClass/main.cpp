// Inheritance, Polymorphism,Virtual (Late) Binding Example.
// Main function file.
// CS162 PCC

#include <iostream>
#include "rect.hpp"
#include "box.hpp"
using namespace std;

int main() {
    char choice = ' ';
    double width = 0.0, length = 0.0, height = 0.0;
    Rect *p1;
    cout << "What shape do you want? (r/b) ";
    cin >> choice;
    if(choice == 'r') {
        cout << "Length and width: ";
        cin >> length >> width;
        p1 = new Rect(length, width);
    } else {
        cout << "Length, width, height: ";
        cin >> length >> width >> height;
        p1 = new Box(length, width, height);
    }
    p1->whatIsMyName(); // virtual, late binding, so box says box.
    // The correct function isn't selected until run time, after the program starts up.
    // What happens if you delete the key word virtual in front of the base class prototype?
    // OK, I'll tell you. Early (compile time) binding. The flow of control to the function
    // is set at compile time.
    cout << endl;
    delete p1;
    return 0;
}
