// Inheritance and Virtual Binding Example.
// Box Header file.
// CS162 PCC

#include "rect.hpp"

class Box : public Rect {
    double height;
    public:
    void someFunction(); // overrides someFunction in Rect.
    Box();
    Box(double l, double w, double h);
    double getHeight();
    void setHeight(double);
    double getVolume();
    void whatIsMyName(); // overrides the base class version.
    // Overriden functions have the same signature as the base class version.
};



