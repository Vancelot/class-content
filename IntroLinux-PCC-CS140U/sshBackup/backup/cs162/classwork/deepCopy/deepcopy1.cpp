// Robert Martin, CS162, PCC
//  Deep copy example code.
//  Example of overloaded copy constructor and assignment operator.

#include <iostream>
#include <cstring>

using namespace std;
const int SIZE = 100;

class Thing {
    int number;
    char * name;
    public:
    void setName(const char str[]);
    const char * getName();
	Thing & operator++();
	Thing operator++(int);
    Thing(); // default constructor.
    Thing(const char n[], int num);
    Thing(const Thing &otherThing); // copy constructor.
    const Thing & operator=(const Thing &otherThing);
	 bool operator==(const Thing &otherThing);
	 friend ostream & operator<<(ostream &, const Thing &);
	// friend Thing operator+(Thing &, Thing &);
	 Thing operator+(const Thing &);
    ~Thing(); // destructor prototype.
};

Thing Thing::operator++(int unused)
{
	Thing temp = *this;
	number++;
	return temp;
}

Thing & Thing::operator++()
{
	number++;
	return *this;
}

Thing Thing::operator+(const Thing &t)
{
	char * tempStr = new char[strlen(name) + strlen(t.name) + 1];
	int tempNum;
	strcpy(tempStr, name);
	strcat(tempStr, t.name);
	tempNum = number;
	tempNum += t.number;
	Thing temp(tempStr, tempNum);
	delete[] tempStr;
	return temp;
}

ostream & operator<<(ostream &out, const Thing &t)
{
	out << t.name << endl << t.number;
	return out;
}

bool Thing::operator==(const Thing &otherThing)
{
	if(strcmp(name, otherThing.name) == 0)
		return true;
	else
		return false;
}

// Overloaded assignment operator.
const Thing & Thing::operator=(const Thing &otherThing) {
    name = new char[strlen(otherThing.name) + 1];
    strcpy(name, otherThing.name);
	 number = otherThing.number;
    return *this; // return the current object.
                  //  Necessary for the next operation, if there is one,
                  //  that requires the object as an argument.
}

// Copy constructor: Thing t2(t1); name on line 30 belongs to t2
//   and t1 is referenced by otherThing.
Thing::Thing(const Thing &otherThing) {
    int len = strlen(otherThing.name);
    name = new char[len + 1]; // for null terminator, + 1.
    strcpy(name, otherThing.name);
    number = otherThing.number;
}

// Destructor definition. Will cause a crash with shallow copy.
Thing::~Thing() {
    if(name != nullptr) {
        delete[] name;
        name = nullptr;
    }
}

void Thing::setName(const char str[]) {
    strcpy(name, str);
}

const char * Thing::getName() {
    return name;
}

// Default Constructor definition.
Thing::Thing() {
    number = 0;
    name = nullptr;
}

// Constructor with arguments.
Thing::Thing(const char n[], int num) {
    name = new char[strlen(n) + 1];
    strcpy(name, n); 
    number = num;
}

int main() {
    Thing t1("Thing 1", 7); // pointer to a Thing struct.
    Thing t2(t1); // overloaded copy constructor, deep.
    Thing t3, t4;

	cout << "T1: \n" << t1 << endl << "T2: \n" << t2 << "\nT3: \n" << t3 << endl;
	
	t3 = t1 + t2;

	cout << "T1: \n" << t1 << endl << "T2: \n" << t2 << "\nT3: \n" << t3 << endl;
    // With deep copy, all the name pointers have their own memory.
} // automatic variables go out of scope, destructor for all automatic vars.

