//
//  Name.cpp
//  Composition
//
//  Created by Robert Martin on 7/11/16.
//  Copyright © 2016 MITI. All rights reserved.
//

#include "Name.hpp"

void Name::setFirstName(const char fName[]) {
    std::strncpy(firstName, fName, SIZE);
    firstName[SIZE - 1] = '\0';  // Make sure the string is null terminated.
}

void Name::setMiddleName(const char mName[]) {
    std::strncpy(middleName, mName, SIZE);
    middleName[SIZE - 1] = '\0';
}

void Name::setLastName(const char lName[]) {
    std::strncpy(lastName, lName, SIZE);
    lastName[SIZE - 1] = '\0';
}

void Name::sayName() const {
    std::cout << "My name is: " << firstName << " " << middleName << " " << lastName << std::endl;
}

Name::Name() { // Default Constructor.
    firstName[0] = '\0';
    middleName[0] = '\0';
    lastName[0] = '\0';
}

// Constructor with Arguments
Name::Name(const char fName[], const char mName[], const char lName[]) {
    std::strncpy(firstName, fName, SIZE);
    firstName[SIZE - 1] = '\0';  // Make sure the strings are null terminated.
    std::strncpy(middleName, mName, SIZE);
    firstName[SIZE - 1] = '\0';
    std::strncpy(lastName, lName, SIZE);
    lastName[SIZE - 1] = '\0';
}
