//
//  main.cpp
//  Composition and Static Member Example
//
//

#include <iostream>
#include "Employee.hpp"

int main(int argc, const char * argv[]) {
    std::cout << "There are " << Employee::numberOfEmployees << " employees." << std::endl;
    Employee emp1;
    Employee emp2("Jane", "Anne", "Smith", 7); // started in July, month 7
    emp2.sayName();
    std::cout << "There are " << Employee::numberOfEmployees << " employees." << std::endl;

    return 0;
}
