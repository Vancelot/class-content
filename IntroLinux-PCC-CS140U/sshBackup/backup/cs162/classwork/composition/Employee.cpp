//
//  Employee.cpp
//  Composition Example
//
//

#include "Employee.hpp"

int Employee::numberOfEmployees = 0;

Employee::Employee() {
    numberOfEmployees++;
}

Employee::Employee(const char fName[], const char mName[], const char lName[], int month) : empName(fName, mName, lName) { // Initialization List
	startMonth = month;
	numberOfEmployees++;
}

void Employee::sayName() const {
    empName.sayName();
}
