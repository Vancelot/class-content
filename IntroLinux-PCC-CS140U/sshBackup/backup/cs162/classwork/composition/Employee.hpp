//
//  Employee.hpp
//  Composition
//
//

#ifndef Employee_hpp
#define Employee_hpp

#include "Name.hpp"

class Employee {
    int startMonth;
    Name empName; // member object, constructed first
public:
    static int numberOfEmployees;
    Employee(); // invoked after member objects are created.
    Employee(const char [], const char [], const char [], int);
    void sayName() const;
};


#endif /* Employee_hpp */
