//
//  Name.hpp
//  Composition
//
//  Created by Robert Martin on 7/11/16.
//  Copyright © 2016 MITI. All rights reserved.
//

#ifndef Name_hpp
#define Name_hpp

#include <cstring>
#include <iostream>
const int SIZE = 256;

class Name {
    char firstName[SIZE];
    char middleName[SIZE];
    char lastName[SIZE];
    
public:
    void setFirstName(const char []);
    void setMiddleName(const char []);
    void setLastName(const char []);
    void sayName() const;
    Name();  // default constructor
    Name(const char first[], const char middle[], const char last[]);
};

#endif /* Name_hpp */
