#include<iostream>
#include<vector>
#include<iomanip>

using namespace std;

template<class Type>
class Arithmetic
{
	public:
	Type multiply(Type x, Type y);
	Type add(Type x, Type y);
};

template<class T>
bool equal(T item0, T item1)
{
	if (item0 == item1)
		return true;
	else
		return false;
}

int main()
{
	double x = 2.0, y = 3.0;
	vector<double> myDoubles;
	Arithmetic<double> dNum;
	Arithmetic<int> iNum;

	myDoubles.push_back(2.5);
	cout << showpoint << setprecision(2) << myDoubles[0] << endl;
	cout << iNum.add(3, 4) << endl;
	cout << dNum.multiply(6.5, 2.0) << endl;
	
	return 0;
}

template<class Type>
Type Arithmetic<Type>::multiply(Type x, Type y)
{
	return x * y;
}

template<class Type>
Type Arithmetic<Type>::add(Type x, Type y)
{
	return x + y;
}
