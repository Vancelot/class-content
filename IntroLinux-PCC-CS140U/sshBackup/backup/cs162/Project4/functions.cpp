// Kerry Vance
// CS 162
// Sources: 

#include"header.hpp"

cardType::cardType(char newName[], double newPrice, char newSet[], char newType[], int newCount) // constructor w/args
{
	name = new char[strlen(newName) + 1];
	set = new char[strlen(newSet) + 1];
	type = new char[strlen(newType) + 1];

	strcpy(name, newName);
	price = newPrice;
	strcpy(set, newSet);
	strcpy(type, newType);
	cardCount = newCount;
}

const char *cardType::getName()
{
	return name;
}
	
cardColl::cardColl()
{
	count = 0;
}

const double cardType::getPrice()
{
	return price;
}

const char *cardType::getSet()
{
	return set;
}

const char *cardType::getType()
{
	return type;
}

const int cardType::getCount()
{
	return cardCount;
}

cardType::~cardType()
{
	if(name != nullptr)
	{
		delete[] name;
		name = nullptr;
	}
	if(set != nullptr)
	{
		delete[] set;
		set = nullptr;
	}
	if(type != nullptr)
	{
		delete[] type;
		type = nullptr;
	}
}


int cardColl::loadCards()
{
	ifstream inFile;
	char inFileName[SIZE];// = "cardCollection.txt"; //Comment out inFileName initialization
	char tempName[SIZE], tempSet[SIZE], tempType[SIZE];
	int tempCount = 0;
	double tempPrice = 0;

	while(!inFile.is_open())
	{
		cout << "Enter name of collection  to open: "; //Uncomment these two lines to choose file
		cin.getline(inFileName, SIZE);
		strcpy(fileName, inFileName);
		inFile.open(inFileName);
		if(strcmp(inFileName, "quit") == 0)
			exit(0);
		if(!inFile.is_open())
		{
			cout << "File not found. Enter one of the following file names or enter \"quit\" to exit\n";
			system("ls");
		}
	}

	while(!inFile.eof())
	{
		inFile.getline(tempName, SIZE, ',');
		inFile >> tempPrice;
		inFile.ignore();
		inFile.getline(tempSet, SIZE, ',');
		inFile.getline(tempType, SIZE, ',');
		inFile >> tempCount;
		inFile.ignore();
		cards[count] = new cardType(tempName, tempPrice, tempSet, tempType, tempCount);
		count++;
		inFile.peek();
	}
	inFile.close();
	return count;
}

void cardColl::searchCard() const
{
	char searchCard[SIZE], compareCard[SIZE];
	int check, index = 0;
	bool findCheck = false;

	cout << "Enter a card to search: ";
	cin.getline(searchCard, SIZE);

	for (int i = 0; i < SIZE - 1; i++)
		searchCard[i] = toupper(searchCard[i]);
	for (int i = 0; i < count; i++)
	{
		for(int j = 0; j < SIZE - 1; j++)
		{
			compareCard[j] = toupper(cards[i]->getName()[j]);
		}
		check = strcmp(searchCard, compareCard);
		if (check == 0)
		{
			index = i;
			findCheck = true;
			break;
		}
		else
		findCheck = false;			
	}
	if (findCheck == true)
	{
		cout << "\nFound card: " << cards[index]->getName() << endl << endl;
		printOneCard(index);
	}
	else
	{
		cout << "\nCard not found\n\n";
	}
}

void cardColl::addCard()
{
	char cardName[SIZE], cardSet[SIZE], cardType[SIZE];
	double cardPrice;
	int cardCount;
	if(count > SIZE - 1)
		cout << "The database is full\n\n";
	else
	{
		cout << "Enter card name: ";
		cin.getline(cardName, SIZE);
		cout << "Enter card set: ";
		cin.getline(cardSet, SIZE);
		cout << "Enter card type: ";
		cin.getline(cardType, SIZE);
		cout << "Enter card price: ";
		cin >> cardPrice;
		while(cin.fail() || cardPrice < 0)
		{	
			cin.clear();
			cin.ignore(SIZE, '\n');
			cout << "INVALID ENTRY\nEnter card price: ";
			cin >> cardPrice;
		}
		cout << "How many of this card do you have? ";
		cin >> cardCount;
		while(cin.fail() || cardCount < 0)
		{	
			cin.clear();
			cin.ignore(SIZE, '\n');
			cout << "INVALID ENTRY\nEnter card count: ";
			cin >> cardCount;
		}
		cin.ignore();

		cards[count] = new ::cardType(cardName, cardPrice, cardSet, cardType, cardCount);
		cout << "\nCard added successfully.\n\n";
		printOneCard(count);
		count++;
	}
}

void cardColl::printCardType() const
{
	int width = 20;

	cout << left << setw(30) << "Name" << setw(10) << "Price" << setw(30) << "Set" << setw(width) << "Type" << setw(width) << "Count" << endl << setfill('-') << setw(100) << '-' << endl << setfill(' ');

	for(int i = 0; i < count; i++)
	{
	cout << setprecision(2) << showpoint << fixed << left << setw(30) << cards[i]->getName() << '$' << setw(10) << cards[i]->getPrice() << setw(30) << cards[i]->getSet() << setw(width) << cards[i]->getType() << setw(width) << cards[i]->getCount() << endl;
	}
	cout << endl;
}

void cardColl::printOneCard(int index) const
{
	cout << "Card #" << index + 1 << " in collection." << endl << "Price: $" << setprecision(2) << showpoint << fixed << cards[index]->getPrice() << endl << "Set: " << cards[index]->getSet() << endl << "Type: " << cards[index]->getType() << endl << "Card Count: " << cards[index]->getCount() << endl << endl;
}

void cardColl::writeCards()
{
	ofstream outFile;

	outFile.open(fileName);

	for (int i = 0; i < count; i++)
	{
		outFile << cards[i]->getName() << ',' << cards[i]->getPrice() << ',' << cards[i]->getSet() << ',' << cards[i]->getType() << ',' << cards[i]->getCount() << '\n';
	}
}

cardColl::~cardColl()
{
	for (int i = 0; i < count; i++)
	{
		if (cards[i] != nullptr)
			delete cards[i];
	}
}

