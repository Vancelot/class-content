// Kerry Vance
// CS 162
// Sources: 

#include"header.hpp"

int main()
{
	cardColl myCollection;
	int choice = 0;

	cout << "Welcome to the Magic card collection program.\n";
	cout << "Successfully loaded " << myCollection.loadCards() << " cards.\n";

	while (choice != 4)
	{
		cout << left <<  "What would you like to do?\n" << setw(25) << "1) Search card" << setw(25) << "2) List all cards" << endl  << setw(25) << "3) Add new card" << setw(25) << "4) Quit" << endl;
		cin >> choice;
		cin.ignore();
		while (cin.fail())
		{
			cin.clear();
			cin.ignore(SIZE, '\n');
			cout << "Please enter a number 1-4: ";
			cin >> choice;
			cin.ignore();
		}

		switch(choice)
		{
			case 1:
				myCollection.searchCard();
				break;
			case 2:
				myCollection.printCardType();
				break;
			case 3:
				myCollection.addCard();
				break;
		}
	}
	myCollection.writeCards();
	return 0;
}
