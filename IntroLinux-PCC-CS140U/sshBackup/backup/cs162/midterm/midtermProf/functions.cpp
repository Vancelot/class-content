// CS162 Midterm Proficiency
// array.cpp, function definition / implementation file. 

#include "array.hpp"

//put the assigned function definitions here
int findMinEven(int list[], int size)
{
	int check, min = 0;
	for (int i = 0; i < size; i++)
	{
		if(list[i] % 2 == 0)
		{
			check = list[i];
			if(check < min || min == 0)
				min = check;
		}
	}
	return min;
}

bool deleteIndex(int list[], int index, int &size)
{
	bool check = false;
	if(index < 0 || index > size)
		return check;
	for(int i = index; i < size; i++)
		list[i] = list[i + 1];
	size--;
	check = true;
	return check;
}

// *** End of array.cpp ***
