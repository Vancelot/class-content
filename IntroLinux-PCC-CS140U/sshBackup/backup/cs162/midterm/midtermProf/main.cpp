// Main function for CS162 midterm proficiency.
#include "array.hpp"

using namespace std;

int main()
{
    int list[CAP], index;
    int size = 15;
    build(list, size); // build() places random numbers in list.

    display(list, size); // display() will send the entire array to the screen.

    //PLEASE PUT YOUR CODE HERE to call the functions assigned

	cout << "The minimum even number in the list is: " << findMinEven(list, size) << endl;
	cout << "Which index would you like to remove? (Pick a number between 0 and " << size - 1 << ')';
	cin >> index;
	while(cin.fail())
	{
		cin.clear();
		cin.ignore(100, '\n');
		cout << "Please enter an integer value for the index you'd like to remove:\n";
		cin >> index;
	}
	cout << endl;

	if(deleteIndex(list, index, size))
	{
		cout << "That index has been deleted from the list\nList after deletion:\n";
		display(list, size); // This second call to display() will verify your code.
		cout << "The new minimum even number in the list is: " << findMinEven(list, size) << endl;
   }
	else
		cout << "Invalid index, list not changed.\n";
    return 0;
}
// *** End of main.cpp ***
