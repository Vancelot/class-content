// CS162 Spring '18, main function for Lab 3
// This is a simple program to keep track of movies.
#include "header.hpp"
#include <iostream>
#include <iomanip>

int main() {
    greatMovies myFavorites;
    myFavorites.totalMovies = 0;

    addAMovie(myFavorites, "Aliens", 1986, .98);
    addAMovie(myFavorites, "Black Panther", 2018, .97);
    addAMovie(myFavorites, "The Fifth Element", 1997, .72);
    addAMovie(myFavorites, "The Adventures of Buckaroo Banzai", 1984, .71);
    addAMovie(myFavorites, "The World According to Garp", 1982,.79);
    addAMovie(myFavorites, "Hidden Figures", 2017,.93);

    std::cout << "The movies in the database are:\n";
    if(myFavorites.totalMovies > 0)
        listMovies(myFavorites);
    else
        std::cout << "There are no movies in the database." << std::endl;
    return 0;
}
