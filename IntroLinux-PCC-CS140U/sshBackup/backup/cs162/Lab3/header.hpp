// CS162, Spring '18
// Lab 3 Header file.

#ifndef __INVENTORY__
#define __INVENTORY__

const int STR_SIZE = 128;
const int LIST_SIZE = 100;
const bool SUCCESS = true;

// This struct can contain the information about one movie.
struct movieType {
    char movieName[STR_SIZE];
    int releaseDate;
    double tomatoScore;
};

// This struct contains an array of movieType structs.
// It also contains the total movies contained in the array.
// When the program starts, totalMovies will be set to zero.
// Use totalMovies to keep track of the current index.
struct greatMovies {
    movieType movieList[LIST_SIZE];
    int totalMovies;
};

// Prototypes
// TO-DO: Implement these two functions in the functions.cpp file.
void addAMovie(greatMovies &, const char name[], int date, double tomato);
void listMovies(const greatMovies &);

#endif

