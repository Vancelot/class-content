// Kerry Vance
// This is the functions file for lab3.
// Implement the functions listed.
#include "header.hpp"
#include<cstring>
#include<iostream>

using namespace std;
// addAMovie().
// This function will add a new movie to movies.
// The movie has a name, release date, and rotten tomatoe score, in that order, passed in.
// Use the greatMovies member called totalMovies to place the new movie in the array.
// NOTE: Because you don't know the size of name, use strncpy() to copy name to movieName.
// Remember to explicitly null terminate the movieName c-string.
void addAMovie(greatMovies & movies, const char name[], int releaseDate, double tomato) 
{
	strncpy(movies.movieList[movies.totalMovies].movieName, name, STR_SIZE);
	movies.movieList[movies.totalMovies].movieName[STR_SIZE] = '\n';
	movies.movieList[movies.totalMovies].releaseDate = releaseDate;
	movies.movieList[movies.totalMovies].tomatoScore = tomato;
	movies.totalMovies++;	
}


// listMovies().
// Place the code here to print the movies list in movies.
void listMovies(const greatMovies & movies)
{
	for (int i = 0; i < movies.totalMovies; i++)
	{
		cout << "Movie number:  " << i + 1 << endl << "Name: " << movies.movieList[i].movieName << endl << "Release Date: " << movies.movieList[i].releaseDate << endl << "Tomato Score: " << movies.movieList[i].tomatoScore * 100 << '%' << endl << endl;
	}
}
