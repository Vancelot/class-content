// lab 2 main function.

#include "header.hpp"

int main()
{
    double score;
    char grade;

    score = getAvg();
    grade = determineGrade(score);
    printMsg(grade);

    return 0;
}
