#include "header.hpp"

double getAvg()
{
	double finalAvg;

	cout << "Please enter your final average: ";
	cin >> finalAvg;
	while(!cin || finalAvg < 0)
	{
		cin.clear();
		cin.ignore(MAX_CHAR, '\n');
		cout << "input has to be numerical and positive!" << endl;
		cin >> finalAvg;
	}
	cin.ignore(MAX_CHAR, '\n');
	return finalAvg;
}

char determineGrade(double finalAvg)
{
	char grade = 'F';	
	if(finalAvg >= MIN_A)
	{
		grade = 'A';	
	}
	else if(finalAvg >= MIN_B)
	{
		grade = 'B';
	}
	else if(finalAvg >= MIN_C)
	{
		grade = 'C';
	}
	return grade;
}

void printMsg(char grade)
{
	char msg[MAX_CHAR];
	switch(grade)
	{
	case 'A':
		strcpy(msg, "A, Excellent!");
		break;
	case 'B':
		strcpy(msg, "B, Good job!");
		break;
	case 'C':
		strcpy(msg, "C, You've passed!");
		break;
	default:
		strcpy(msg, "No pass, time to study!");
		break;
	}
	cout << msg << endl;
}
