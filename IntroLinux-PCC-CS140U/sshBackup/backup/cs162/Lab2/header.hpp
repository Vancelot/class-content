// Lab 2 Header file.
#ifndef __HEADER__
#define __HEADER__

#include<iostream>
#include<cstring>

using namespace std;

const int MAX_CHAR = 100;
const int MIN_A = 90;
const int MIN_B = 80;
const int MIN_C = 70;

double getAvg();
char determineGrade(double);
void printMsg(char grade);

#endif
