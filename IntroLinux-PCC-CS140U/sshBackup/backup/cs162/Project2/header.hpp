#ifndef __HEADER__
#define __HEADER__

#include<iostream>
#include<fstream>
#include<iomanip>
#include<cstring>

using namespace std;

const int SIZE = 100;

const int ERROR_INT = -999;
const char ERROR_CSTRING[SIZE] = "ERRORERROR";
const double ERROR_DOUBLE = -999.99;

struct cardType
{
	char name[SIZE];
	double price;
	char set[SIZE];
	char type[SIZE];
	int cardCount;
};

class cardColl
{
	cardType cards[SIZE];
	int count = 0;

	public:
	void loadCards();
	void searchCard() const;
	void printCardType() const;
	void addCard();
	void printOneCard(int) const;
};

#endif
