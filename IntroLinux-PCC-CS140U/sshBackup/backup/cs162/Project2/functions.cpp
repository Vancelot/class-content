#include"header.hpp"

void cardColl::loadCards()
{
	ifstream inFile;
	char inFileName[SIZE];// = "cardCollection.txt"; //Comment out inFileName initialization
	int index = 0;

	while(!inFile.is_open())
	{
		cout << "Enter name of collection to open: "; //Uncomment these two lines to choose file
		cin.getline(inFileName, SIZE);
		inFile.open(inFileName);
		if(strcmp(inFileName, "quit") == 0)
			exit(0);
		if(!inFile.is_open())
		{
			cout << "File not found. Enter one of the following file names or enter \"quit\" to exit\n";
			system("ls");
		}
	}

	while(!inFile.eof())
	{
		inFile.getline(cards[index].name, SIZE, ',');
		inFile >> cards[index].price;
		inFile.ignore();
		inFile.getline(cards[index].set, SIZE, ',');
		inFile.getline(cards[index].type, SIZE, ',');
		inFile >> cards[index].cardCount;
		inFile.ignore();
		index++;
		count++;
		inFile.peek();
	}
	inFile.close();
	cout << "Successfully loaded " << count - 1 << " cards.\n";
}

void cardColl::searchCard() const
{
	char searchCard[SIZE], compareCard[SIZE];
	int check, index = 0;
	bool findCheck = false;

	cout << "Enter a card to search: ";
	cin.getline(searchCard, SIZE);

	for (int i = 0; i < SIZE - 1; i++)
		searchCard[i] = toupper(searchCard[i]);
	for (int i = 0; i < count; i++)
	{
		for(int j = 0; j < SIZE - 1; j++)
		{
			compareCard[j] = toupper(cards[i].name[j]);
		}
		check = strcmp(searchCard, compareCard);
		if (check == 0)
		{
			index = i;
			findCheck = true;
			break;
		}
		else
		findCheck = false;			
	}
	if (findCheck == true)
	{
		cout << "\nFound card: " << cards[index].name << endl << endl;
		printOneCard(index);
	}
	else
	{
		cout << "\nCard not found\n\n";
	}
}

void cardColl::addCard()
{
	char cardName[SIZE], cardSet[SIZE], cardType[SIZE];
	double cardPrice;
	int cardCount;
	if(count > SIZE - 1)
		cout << "The database is full\n\n";
	else
	{
		cout << "Enter card name: ";
		cin.getline(cardName, SIZE);
		cout << "Enter card set: ";
		cin.getline(cardSet, SIZE);
		cout << "Enter card type: ";
		cin.getline(cardType, SIZE);
		cout << "Enter card price: ";
		cin >> cardPrice;
		while(cin.fail() || cardPrice < 0)
		{	
			cin.clear();
			cin.ignore(SIZE, '\n');
			cout << "INVALID ENTRY\nEnter card price: ";
			cin >> cardPrice;
		}
		cout << "How many of this card do you have? ";
		cin >> cardCount;
		while(cin.fail() || cardCount < 0)
		{	
			cin.clear();
			cin.ignore(SIZE, '\n');
			cout << "INVALID ENTRY\nEnter card count: ";
			cin >> cardCount;
		}
		cin.ignore();
	
		strcpy(cards[count].name, cardName);
		strcpy(cards[count].set, cardSet);
		strcpy(cards[count].type, cardType);
		cards[count].price = cardPrice;
		cards[count].cardCount = cardCount;
	
		cout << "\nCard added successfully.\n\n";
		printOneCard(count);
		count++;
	}
}

void cardColl::printCardType() const
{
	int width = 20;

	cout << left << setw(30) << "Name" << setw(10) << "Price" << setw(30) << "Set" << setw(width) << "Type" << setw(width) << "Count" << endl << setfill('-') << setw(100) << '-' << endl << setfill(' ');

	for(int i = 0; i < count; i++)
	{
	cout << setprecision(2) << showpoint << fixed << left << setw(30) << cards[i].name << '$' << setw(10) << cards[i].price << setw(30) << cards[i].set << setw(width) << cards[i].type << setw(width) << cards[i].cardCount << endl;
	}
	cout << endl;
}

void cardColl::printOneCard(int index) const
{
	cout << "Card #" << index + 1 << " in collection." << endl << "Price: $" << setprecision(2) << showpoint << fixed << cards[index].price << endl << "Set: " << cards[index].set << endl << "Type: " << cards[index].type << endl << "Card Count: " << cards[index].cardCount << endl << endl;
}

