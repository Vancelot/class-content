//array.cpp 

#include "array.h"

//put the implementations of your assigned functions here
int numOfEven(int list[], int size) 
{
	int count = 0;
	for (int i = 0; i < size; i++)
	{
		if (list[i] % 2 == 0)
			count++;
	}
	return count;
}

bool insert(int list[], int & size, int newInt, int pos)
{
	size++;
	if (pos <= 0 || pos > size)
	{
		cout << "Invalid position" << endl;
		return false;
	}
	else if(pos == size)
	{
		list[pos] = newInt;
		size++;
		return true;
	}
	else
	{
		for (int i = size; i >= pos; i--)
		{
			list[i] = list[i - 1];
		}
		list[pos] = newInt;
		return true;
	}
}

// *** End of array.cpp ***
