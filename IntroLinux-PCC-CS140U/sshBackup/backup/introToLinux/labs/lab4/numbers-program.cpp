//There were 26 occurences of number.
//a dummy program to do some simple calculations.

#include <iostream>
using namespace std;

int sum(int value1, int value2);
int product(int value1, int value2);

int main()
{
	int value1;
	int value2;

	cout << "Please enter an integer: ";
	cin >> value1;

	cout << "You've entered: " << value1 << endl;
	
	cout << "Please enter another integer: ";
	cin >> value2;

	cout << "You've entered: " << value2 << endl;

	cout << "value1 + value2 = " << sum(value1, value2) << endl;
	cout << "value1 * value2 = " << product(value1, value2) << endl;

	return 0;
}

int sum(int value1, int value2)
{
	return value1 + value2;
}

int product(int value1, int value2)
{
	return value1 * value2;
}
