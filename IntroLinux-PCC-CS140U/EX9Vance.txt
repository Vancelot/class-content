Chapter 17 Exercises:

1. What is the difference between the scp and sftp utilities?
sftp uses an ftp like protocol over ssh, it allows for file exploring, putting files from the local machine, and getting files from the remote machine. scp is secure copy protocol it cannot list directories on the remote machine.

2. How can you use ssh to find out who is logged in on a remote system?
type w on the machine you're logged into via ssh.

6. When you try to connect to a remote system using an OpenSSH client and you see a message warning you that the remote host identification has changed, what has happened? What should you do?
If you recently generated a new ssh key for that machine you should delete the old one. If you have not you should generate a new one and remove all other keys from the known_hosts file
