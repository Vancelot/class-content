#include<iostream>
#include<time.h>

using namespace std;

int main()
{
	int winner, choice, removed, newChoice;
	char change;
	
	srand(time(NULL));
	
	newChoice = 0;
	winner = rand()%3 + 1;

	do
	{
	cout << "Please pick a number between one and three" << endl;
	cin >> choice;
	if (choice > 3 || choice < 1)
		{
			cout << "I said BETWEEN one and three... try again: ";
			cin >> choice;
		}
	} while (choice > 3 || choice < 1);

	do
	{
		removed = rand()%3 + 1;
	} while(removed == winner || removed == choice);

	cout << "Door " << removed << " has been removed. Would you like to change doors or keep your choice? (y or n)" << endl;
	
	cin >> change;
	
	if (change == 'y')
		while (newChoice == 0 || newChoice == choice || newChoice == removed)
		newChoice++;
	else if (change == 'n')
		newChoice = choice;
	
	if (newChoice == winner)
		cout << "Congrats you won the game!\n";
	else if(newChoice != winner)
		cout << "Better luck next time sucka!\n";

	cout << "winner: " << winner << endl;
	cout << "choice: " << choice << endl;
	cout << "newChoice: " << newChoice << endl;
	cout << "removed: " << removed << endl;
	
	return 0;
}
