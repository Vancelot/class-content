#! /bin/bash

count=0
filename=""

if [[ -d $1 ]]
	then
	for i in $( ls $1 )	
		do
		size=${#i}
		if [ "$size" -gt "$count" ]
			then
			count=$size
			filename=$i
		fi
		done
	echo The longest filename in $1 is $filename with $count characters
	else
	echo $1 is not a directory, please try again.
fi

